// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_Remittance21TxnEventMapper extends EventMapper<FT_Remittance21TxnEvent> {

public FT_Remittance21TxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_Remittance21TxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_Remittance21TxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_Remittance21TxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_Remittance21TxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_Remittance21TxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_Remittance21TxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getRemAdd1());
            preparedStatement.setString(i++, obj.getRemType());
            preparedStatement.setString(i++, obj.getRemAdd3());
            preparedStatement.setDouble(i++, obj.getInrAmount());
            preparedStatement.setString(i++, obj.getRemAdd2());
            preparedStatement.setTimestamp(i++, obj.getTrnDate());
            preparedStatement.setString(i++, obj.getPurposeCode());
            preparedStatement.setString(i++, obj.getBenCity());
            preparedStatement.setString(i++, obj.getType());
            preparedStatement.setString(i++, obj.getBranch());
            preparedStatement.setString(i++, obj.getBenCntryCode());
            preparedStatement.setString(i++, obj.getBenAdd3());
            preparedStatement.setString(i++, obj.getPurposeDesc());
            preparedStatement.setString(i++, obj.getBenAdd2());
            preparedStatement.setString(i++, obj.getRemAcctNo());
            preparedStatement.setString(i++, obj.getRemName());
            preparedStatement.setString(i++, obj.getBenAdd1());
            preparedStatement.setString(i++, obj.getBenBic());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getMsgtype());
            preparedStatement.setString(i++, obj.getTranRefNo());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getRemCustId());
            preparedStatement.setString(i++, obj.getAccountCategory());
            preparedStatement.setString(i++, obj.getBenName());
            preparedStatement.setString(i++, obj.getBenAcctNo());
            preparedStatement.setDouble(i++, obj.getUsdEqvAmt());
            preparedStatement.setString(i++, obj.getBenCustId());
            preparedStatement.setString(i++, obj.getRemCntryCode());
            preparedStatement.setString(i++, obj.getCptyAcNo());
            preparedStatement.setString(i++, obj.getSystem());
            preparedStatement.setString(i++, obj.getSno());
            preparedStatement.setString(i++, obj.getTranCurr());
            preparedStatement.setString(i++, obj.getRemCity());
            preparedStatement.setString(i++, obj.getClientAccNo());
            preparedStatement.setString(i++, obj.getRemBic());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_REMITTANCE_21_txn]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_Remittance21TxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_Remittance21TxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_REMITTANCE_21_txn"));
        putList = new ArrayList<>();

        for (FT_Remittance21TxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "REM_ADD1",  obj.getRemAdd1());
            p = this.insert(p, "REM_TYPE",  obj.getRemType());
            p = this.insert(p, "REM_ADD3",  obj.getRemAdd3());
            p = this.insert(p, "INR_AMOUNT", String.valueOf(obj.getInrAmount()));
            p = this.insert(p, "REM_ADD2",  obj.getRemAdd2());
            p = this.insert(p, "TRN_DATE", String.valueOf(obj.getTrnDate()));
            p = this.insert(p, "PURPOSE_CODE",  obj.getPurposeCode());
            p = this.insert(p, "BEN_CITY",  obj.getBenCity());
            p = this.insert(p, "TYPE",  obj.getType());
            p = this.insert(p, "BRANCH",  obj.getBranch());
            p = this.insert(p, "BEN_CNTRY_CODE",  obj.getBenCntryCode());
            p = this.insert(p, "BEN_ADD3",  obj.getBenAdd3());
            p = this.insert(p, "PURPOSE_DESC",  obj.getPurposeDesc());
            p = this.insert(p, "BEN_ADD2",  obj.getBenAdd2());
            p = this.insert(p, "REM_ACCT_NO",  obj.getRemAcctNo());
            p = this.insert(p, "REM_NAME",  obj.getRemName());
            p = this.insert(p, "BEN_ADD1",  obj.getBenAdd1());
            p = this.insert(p, "BEN_BIC",  obj.getBenBic());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "MSGTYPE",  obj.getMsgtype());
            p = this.insert(p, "TRAN_REF_NO",  obj.getTranRefNo());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "REM_CUST_ID",  obj.getRemCustId());
            p = this.insert(p, "ACCOUNT_CATEGORY",  obj.getAccountCategory());
            p = this.insert(p, "BEN_NAME",  obj.getBenName());
            p = this.insert(p, "BEN_ACCT_NO",  obj.getBenAcctNo());
            p = this.insert(p, "USD_EQV_AMT", String.valueOf(obj.getUsdEqvAmt()));
            p = this.insert(p, "BEN_CUST_ID",  obj.getBenCustId());
            p = this.insert(p, "REM_CNTRY_CODE",  obj.getRemCntryCode());
            p = this.insert(p, "CPTY_AC_NO",  obj.getCptyAcNo());
            p = this.insert(p, "SYSTEM",  obj.getSystem());
            p = this.insert(p, "SNO",  obj.getSno());
            p = this.insert(p, "TRAN_CURR",  obj.getTranCurr());
            p = this.insert(p, "REM_CITY",  obj.getRemCity());
            p = this.insert(p, "CLIENT_ACC_NO",  obj.getClientAccNo());
            p = this.insert(p, "REM_BIC",  obj.getRemBic());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_REMITTANCE_21_txn"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_REMITTANCE_21_txn]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_REMITTANCE_21_txn]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_Remittance21TxnEvent obj = new FT_Remittance21TxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setRemAdd1(rs.getString("REM_ADD1"));
    obj.setRemType(rs.getString("REM_TYPE"));
    obj.setRemAdd3(rs.getString("REM_ADD3"));
    obj.setInrAmount(rs.getDouble("INR_AMOUNT"));
    obj.setRemAdd2(rs.getString("REM_ADD2"));
    obj.setTrnDate(rs.getTimestamp("TRN_DATE"));
    obj.setPurposeCode(rs.getString("PURPOSE_CODE"));
    obj.setBenCity(rs.getString("BEN_CITY"));
    obj.setType(rs.getString("TYPE"));
    obj.setBranch(rs.getString("BRANCH"));
    obj.setBenCntryCode(rs.getString("BEN_CNTRY_CODE"));
    obj.setBenAdd3(rs.getString("BEN_ADD3"));
    obj.setPurposeDesc(rs.getString("PURPOSE_DESC"));
    obj.setBenAdd2(rs.getString("BEN_ADD2"));
    obj.setRemAcctNo(rs.getString("REM_ACCT_NO"));
    obj.setRemName(rs.getString("REM_NAME"));
    obj.setBenAdd1(rs.getString("BEN_ADD1"));
    obj.setBenBic(rs.getString("BEN_BIC"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setMsgtype(rs.getString("MSGTYPE"));
    obj.setTranRefNo(rs.getString("TRAN_REF_NO"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setRemCustId(rs.getString("REM_CUST_ID"));
    obj.setAccountCategory(rs.getString("ACCOUNT_CATEGORY"));
    obj.setBenName(rs.getString("BEN_NAME"));
    obj.setBenAcctNo(rs.getString("BEN_ACCT_NO"));
    obj.setUsdEqvAmt(rs.getDouble("USD_EQV_AMT"));
    obj.setBenCustId(rs.getString("BEN_CUST_ID"));
    obj.setRemCntryCode(rs.getString("REM_CNTRY_CODE"));
    obj.setCptyAcNo(rs.getString("CPTY_AC_NO"));
    obj.setSystem(rs.getString("SYSTEM"));
    obj.setSno(rs.getString("SNO"));
    obj.setTranCurr(rs.getString("TRAN_CURR"));
    obj.setRemCity(rs.getString("REM_CITY"));
    obj.setClientAccNo(rs.getString("CLIENT_ACC_NO"));
    obj.setRemBic(rs.getString("REM_BIC"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_REMITTANCE_21_txn]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_Remittance21TxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_Remittance21TxnEvent> events;
 FT_Remittance21TxnEvent obj = new FT_Remittance21TxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_REMITTANCE_21_txn"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_Remittance21TxnEvent obj = new FT_Remittance21TxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setRemAdd1(getColumnValue(rs, "REM_ADD1"));
            obj.setRemType(getColumnValue(rs, "REM_TYPE"));
            obj.setRemAdd3(getColumnValue(rs, "REM_ADD3"));
            obj.setInrAmount(EventHelper.toDouble(getColumnValue(rs, "INR_AMOUNT")));
            obj.setRemAdd2(getColumnValue(rs, "REM_ADD2"));
            obj.setTrnDate(EventHelper.toTimestamp(getColumnValue(rs, "TRN_DATE")));
            obj.setPurposeCode(getColumnValue(rs, "PURPOSE_CODE"));
            obj.setBenCity(getColumnValue(rs, "BEN_CITY"));
            obj.setType(getColumnValue(rs, "TYPE"));
            obj.setBranch(getColumnValue(rs, "BRANCH"));
            obj.setBenCntryCode(getColumnValue(rs, "BEN_CNTRY_CODE"));
            obj.setBenAdd3(getColumnValue(rs, "BEN_ADD3"));
            obj.setPurposeDesc(getColumnValue(rs, "PURPOSE_DESC"));
            obj.setBenAdd2(getColumnValue(rs, "BEN_ADD2"));
            obj.setRemAcctNo(getColumnValue(rs, "REM_ACCT_NO"));
            obj.setRemName(getColumnValue(rs, "REM_NAME"));
            obj.setBenAdd1(getColumnValue(rs, "BEN_ADD1"));
            obj.setBenBic(getColumnValue(rs, "BEN_BIC"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setMsgtype(getColumnValue(rs, "MSGTYPE"));
            obj.setTranRefNo(getColumnValue(rs, "TRAN_REF_NO"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setRemCustId(getColumnValue(rs, "REM_CUST_ID"));
            obj.setAccountCategory(getColumnValue(rs, "ACCOUNT_CATEGORY"));
            obj.setBenName(getColumnValue(rs, "BEN_NAME"));
            obj.setBenAcctNo(getColumnValue(rs, "BEN_ACCT_NO"));
            obj.setUsdEqvAmt(EventHelper.toDouble(getColumnValue(rs, "USD_EQV_AMT")));
            obj.setBenCustId(getColumnValue(rs, "BEN_CUST_ID"));
            obj.setRemCntryCode(getColumnValue(rs, "REM_CNTRY_CODE"));
            obj.setCptyAcNo(getColumnValue(rs, "CPTY_AC_NO"));
            obj.setSystem(getColumnValue(rs, "SYSTEM"));
            obj.setSno(getColumnValue(rs, "SNO"));
            obj.setTranCurr(getColumnValue(rs, "TRAN_CURR"));
            obj.setRemCity(getColumnValue(rs, "REM_CITY"));
            obj.setClientAccNo(getColumnValue(rs, "CLIENT_ACC_NO"));
            obj.setRemBic(getColumnValue(rs, "REM_BIC"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_REMITTANCE_21_txn]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_REMITTANCE_21_txn]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"REM_ADD1\",\"REM_TYPE\",\"REM_ADD3\",\"INR_AMOUNT\",\"REM_ADD2\",\"TRN_DATE\",\"PURPOSE_CODE\",\"BEN_CITY\",\"TYPE\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"BEN_ADD3\",\"PURPOSE_DESC\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"REM_NAME\",\"BEN_ADD1\",\"BEN_BIC\",\"HOST_ID\",\"MSGTYPE\",\"TRAN_REF_NO\",\"TRAN_AMT\",\"REM_CUST_ID\",\"ACCOUNT_CATEGORY\",\"BEN_NAME\",\"BEN_ACCT_NO\",\"USD_EQV_AMT\",\"BEN_CUST_ID\",\"REM_CNTRY_CODE\",\"CPTY_AC_NO\",\"SYSTEM\",\"SNO\",\"TRAN_CURR\",\"REM_CITY\",\"CLIENT_ACC_NO\",\"REM_BIC\"" +
              " FROM EVENT_FT_REMITTANCE_21_txn";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [REM_ADD1],[REM_TYPE],[REM_ADD3],[INR_AMOUNT],[REM_ADD2],[TRN_DATE],[PURPOSE_CODE],[BEN_CITY],[TYPE],[BRANCH],[BEN_CNTRY_CODE],[BEN_ADD3],[PURPOSE_DESC],[BEN_ADD2],[REM_ACCT_NO],[REM_NAME],[BEN_ADD1],[BEN_BIC],[HOST_ID],[MSGTYPE],[TRAN_REF_NO],[TRAN_AMT],[REM_CUST_ID],[ACCOUNT_CATEGORY],[BEN_NAME],[BEN_ACCT_NO],[USD_EQV_AMT],[BEN_CUST_ID],[REM_CNTRY_CODE],[CPTY_AC_NO],[SYSTEM],[SNO],[TRAN_CURR],[REM_CITY],[CLIENT_ACC_NO],[REM_BIC]" +
              " FROM EVENT_FT_REMITTANCE_21_txn";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`REM_ADD1`,`REM_TYPE`,`REM_ADD3`,`INR_AMOUNT`,`REM_ADD2`,`TRN_DATE`,`PURPOSE_CODE`,`BEN_CITY`,`TYPE`,`BRANCH`,`BEN_CNTRY_CODE`,`BEN_ADD3`,`PURPOSE_DESC`,`BEN_ADD2`,`REM_ACCT_NO`,`REM_NAME`,`BEN_ADD1`,`BEN_BIC`,`HOST_ID`,`MSGTYPE`,`TRAN_REF_NO`,`TRAN_AMT`,`REM_CUST_ID`,`ACCOUNT_CATEGORY`,`BEN_NAME`,`BEN_ACCT_NO`,`USD_EQV_AMT`,`BEN_CUST_ID`,`REM_CNTRY_CODE`,`CPTY_AC_NO`,`SYSTEM`,`SNO`,`TRAN_CURR`,`REM_CITY`,`CLIENT_ACC_NO`,`REM_BIC`" +
              " FROM EVENT_FT_REMITTANCE_21_txn";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_REMITTANCE_21_txn (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"REM_ADD1\",\"REM_TYPE\",\"REM_ADD3\",\"INR_AMOUNT\",\"REM_ADD2\",\"TRN_DATE\",\"PURPOSE_CODE\",\"BEN_CITY\",\"TYPE\",\"BRANCH\",\"BEN_CNTRY_CODE\",\"BEN_ADD3\",\"PURPOSE_DESC\",\"BEN_ADD2\",\"REM_ACCT_NO\",\"REM_NAME\",\"BEN_ADD1\",\"BEN_BIC\",\"HOST_ID\",\"MSGTYPE\",\"TRAN_REF_NO\",\"TRAN_AMT\",\"REM_CUST_ID\",\"ACCOUNT_CATEGORY\",\"BEN_NAME\",\"BEN_ACCT_NO\",\"USD_EQV_AMT\",\"BEN_CUST_ID\",\"REM_CNTRY_CODE\",\"CPTY_AC_NO\",\"SYSTEM\",\"SNO\",\"TRAN_CURR\",\"REM_CITY\",\"CLIENT_ACC_NO\",\"REM_BIC\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[REM_ADD1],[REM_TYPE],[REM_ADD3],[INR_AMOUNT],[REM_ADD2],[TRN_DATE],[PURPOSE_CODE],[BEN_CITY],[TYPE],[BRANCH],[BEN_CNTRY_CODE],[BEN_ADD3],[PURPOSE_DESC],[BEN_ADD2],[REM_ACCT_NO],[REM_NAME],[BEN_ADD1],[BEN_BIC],[HOST_ID],[MSGTYPE],[TRAN_REF_NO],[TRAN_AMT],[REM_CUST_ID],[ACCOUNT_CATEGORY],[BEN_NAME],[BEN_ACCT_NO],[USD_EQV_AMT],[BEN_CUST_ID],[REM_CNTRY_CODE],[CPTY_AC_NO],[SYSTEM],[SNO],[TRAN_CURR],[REM_CITY],[CLIENT_ACC_NO],[REM_BIC]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`REM_ADD1`,`REM_TYPE`,`REM_ADD3`,`INR_AMOUNT`,`REM_ADD2`,`TRN_DATE`,`PURPOSE_CODE`,`BEN_CITY`,`TYPE`,`BRANCH`,`BEN_CNTRY_CODE`,`BEN_ADD3`,`PURPOSE_DESC`,`BEN_ADD2`,`REM_ACCT_NO`,`REM_NAME`,`BEN_ADD1`,`BEN_BIC`,`HOST_ID`,`MSGTYPE`,`TRAN_REF_NO`,`TRAN_AMT`,`REM_CUST_ID`,`ACCOUNT_CATEGORY`,`BEN_NAME`,`BEN_ACCT_NO`,`USD_EQV_AMT`,`BEN_CUST_ID`,`REM_CNTRY_CODE`,`CPTY_AC_NO`,`SYSTEM`,`SNO`,`TRAN_CURR`,`REM_CITY`,`CLIENT_ACC_NO`,`REM_BIC`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

