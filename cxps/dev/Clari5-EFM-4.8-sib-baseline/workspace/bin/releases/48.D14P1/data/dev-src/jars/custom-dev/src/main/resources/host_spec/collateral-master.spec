cxps.noesis.glossary.entity.collateral-master{
       db-name = COLLATERAL_MASTER
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = LOANACCOUNT, column = LOANACCOUNT, type = "string:50",key=true}
               { name = COLLATERAL_ID, column = COLLATERAL_ID, type = "string:50",key=true }
               { name = COLLATERAL_VALUE, column = COLLATERAL_VALUE, type = "number:20,4"}
	        { name = COLLATERAL_STATUS, column = COLLATERAL_STATUS, type = "string:50" }
               { name = COLLATERAL_ACCT_NUM, column = COLLATERAL_ACCT_NUM, type = "string:50" }
               { name = COLLATERAL_ACCT_NAME, column = COLLATERAL_ACCT_NAME, type = "string:100"}
		{ name = ENTRY_USERID, column = ENTRY_USERID, type = "string:50" }
               { name = VERIFICATION_USERID, column = VERIFICATION_USERID, type = "string:20" }
               { name = COLLATERAL_ACCT_INT_RATE, column = COLLATERAL_ACCT_INT_RATE, type = "number:20,4"}	
		{ name = COLLATERAL_ADDED_DATE, column = COLLATERAL_ADDED_DATE, type = timestamp }
               { name = COLLATERAL_DELETED_DATE, column = COLLATERAL_DELETED_DATE, type = timestamp }
               { name = CL5_FLG, column = CL5_FLG, type = "string:20"}
		{ name = VERSION_NO, column = VERSION_NO, type = "number:20,0" }
               { name = LAST_MODIFIED_TIME, column = LAST_MODIFIED_TIME, type =timestamp }
               { name = ID, column = ID, type = "string:200"}
		{ name = ACCT_STATUS, column = ACCT_STATUS, type = "string:100" }
               ]
		indexes {
               collateral-master-idx : [ LOANACCOUNT,COLLATERAL_ID ]
        }
       }

