package clari5.custom.dev;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class UserMap {

    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(UserMap.class);



    public static HashMap<String,String> getFactCode() {
        String FACT_CODE = null;
        String RULE = null;
        String sql = "select * from FACT_RULE ";
        HashMap<String,String> map = new HashMap<>();
        try( Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql);
             ResultSet  rs = ps.executeQuery()) {

            cxpsLogger.info("Fact RULE Map ----- Loading");
            while (rs.next()) {
                FACT_CODE = rs.getString("FACT_CODE");
                RULE = rs.getString("RULE");
                cxpsLogger.info("Adding [ " +FACT_CODE+ " ] RULE NUMBER [ " +RULE+ " ] ");
                map.put(FACT_CODE,RULE);
            }
            con.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception on FACT_RULE");
        }
        return map;
    }



    public static ConcurrentHashMap<String,String> getFctUserMap() {

        String USER_LIST = null;
        String FACT_NAME = null;
        String sql = "select * from FACT_USER ";
        ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();
        try(Connection con = Rdbms.getConnection();
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet  rs = ps.executeQuery()) {
            cxpsLogger.info("Fact User Map ----- Loading");
            while (rs.next()) {
                USER_LIST = rs.getString("USER_LIST");
                FACT_NAME = rs.getString("FACT_NAME");
                cxpsLogger.info("Adding [ " +FACT_NAME+ " ] UserList [ " +USER_LIST+ " ] ");
                map.put(FACT_NAME,USER_LIST);
            }
            con.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            cxpsLogger.info("Exception on FACT_USER");
        }

        return map;
    }

    public static HashMap<String,String> getSolRegionMap() {

        String SOL = null;
        String REGION_CODE = null;
        String sql = "select * from REGION_SOL ";
        HashMap<String,String> map = new HashMap<>();
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql);
             ResultSet  rs = ps.executeQuery()){
            cxpsLogger.info("SOLREGION  Map ----- Loading");
            while (rs.next()) {
                SOL = rs.getString("SOL");
                REGION_CODE = rs.getString("REGION_CODE");
                cxpsLogger.info("Adding SOL [ " +SOL+ " ] RegionCode [ " +REGION_CODE+ " ] ");
                map.put(SOL,REGION_CODE);
            }
            con.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
            cxpsLogger.info("Exception on REGION_SOL");
            e.printStackTrace();
        }
        return map;
    }

    public static ConcurrentHashMap<String,String> getRegionUser() {

        String USER_LIST = null;
        String REGION_CODE = null;
        String sql = "select * from REGION_USER ";
        ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();
        try(Connection con = Rdbms.getConnection();
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet  rs = ps.executeQuery()) {
            cxpsLogger.info("Region USER Map ----- Loading");
            while (rs.next()) {
                REGION_CODE = rs.getString("REGION_CODE");
                USER_LIST = rs.getString("USER_LIST");
                cxpsLogger.info("Adding RegionCode [ " +REGION_CODE+ " ] UserList [ " +USER_LIST+ " ] ");
                map.put(REGION_CODE,USER_LIST);
            }
            con.close();
            ps.close();
            rs.close();

        } catch (Exception e) {
            cxpsLogger.info("Exception on REGION_USER");
            e.printStackTrace();
        }
        return map;
    }



    public static ConcurrentHashMap<String,String> getHoRegionUser() {

        String USER_LIST = null;
        String REGION_CODE = null;
        String sql = "select * from HO_REGION_USER ";
        ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql);
             ResultSet  rs = ps.executeQuery()){
            cxpsLogger.info("HO REGION User Map ----- Loading");
            while (rs.next()) {
                REGION_CODE = rs.getString("REGION_CODE");
                USER_LIST = rs.getString("USER_LIST");
                cxpsLogger.info("Adding HO Region Code [ " +REGION_CODE+ " ] UserList [ " +USER_LIST+ " ] ");
                map.put(REGION_CODE,USER_LIST);
            }
            con.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
            cxpsLogger.info("Exception on HO_REGION_USER");
            e.printStackTrace();
        }
        return map;
    }


    public static  ConcurrentHashMap<String, Integer> getUser(HashMap<String, String> map, String factname, ConcurrentHashMap<String, Integer> chashMap) {
        for (String key : map.keySet()) {
            try {
                if (key.equalsIgnoreCase(factname)) {
                    if (map.get(key).contains(",")) {
                            String arr[] = map.get(key).split(",");
                            int length = arr.length - 1;
                            if (chashMap.size() == 0) {
                                chashMap = new ConcurrentHashMap<>();
                                chashMap.put(key, 0);
                                cxpsLogger.info("Intially Map null hence first user--- > [ " + arr[0] + " ] ");
                                return chashMap;
                            } else if (chashMap.size() > 0 && chashMap.get(key) < length) {
                                int val = chashMap.get(key);
                                cxpsLogger.info("Assiging User --- > [ " + arr[val + 1] + " ] ");
                                chashMap.replace(key, val + 1);
                                return chashMap;
                            } else {
                                if (chashMap.get(key) == length) {
                                    cxpsLogger.info("Both are same length " + arr[0]);
                                    chashMap.replace(key, 0);
                                    return chashMap;
                                }
                            }

                    } else {
                        cxpsLogger.info("return user since it dont have comma --->  [ " + map.get(key) + " ] ");
                        chashMap.put(key, 0);
                        return chashMap;
                    }
                }
            }
            catch (Exception e)
            {
             cxpsLogger.info("Exception While getting User");
             e.printStackTrace();
            }
        }
        return chashMap;
    }





    public static HashMap<String,String> getRegionName() {

        String REGION_CODE = null;
        String REGION_NAME  = null;
        String sql = "select * from REGION_USER ";
        HashMap<String,String> map = new HashMap<>();
        try (Connection con = Rdbms.getConnection();
             PreparedStatement ps = con.prepareStatement(sql);
             ResultSet  rs = ps.executeQuery()){
            cxpsLogger.info("REGION NAME Map ----- Loading");
            while (rs.next()) {
                REGION_CODE = rs.getString("REGION_CODE");
                REGION_NAME = rs.getString("REGION_NAME");
                cxpsLogger.info("Adding RegionCode [ " +REGION_CODE+ " ] RegionName [ " +REGION_NAME+ " ] ");
                map.put(REGION_CODE,REGION_NAME);
            }
            con.close();
            ps.close();
            rs.close();
        } catch (Exception e) {
            cxpsLogger.info("Exception on REGION_NAME");
            e.printStackTrace();
        }
        return map;
    }


    public static  ConcurrentHashMap<String, String> getMakerCheker(){

        String RO_MAKER = null;
        String RO_PPC = null;
        String REGION_NAME = null;
        String REGION_CODE = null ;
        String HO_CHECKER = null;
        String HO_PPC = null;

        String sql = "select * from MAKER_CHECKER ";
        try(Connection con = Rdbms.getConnection();
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery()) {
            cxpsLogger.info("MAKER-CHECKER Map ----- Loading");
            while (rs.next()) {
                RO_MAKER = rs.getString("RO_MAKER");
                // RO_PPC = rs.getString("RO_PPC");
                // REGION_NAME = rs.getString("REGION_NAME");
                // REGION_CODE = rs.getString("REGION_CODE");
                HO_CHECKER = rs.getString("HO_CHECKER");
                // HO_PPC = rs.getString("HO_PPC");
                cxpsLogger.info("Adding   RegionCode [ " +RO_MAKER+ " ]  HO_CHECKER [ " +HO_CHECKER+ " ]  ");
                CustomCmsFormatter.checkermap.put(RO_MAKER,HO_CHECKER);
            }}catch (Exception e){
            cxpsLogger.info("Exception on MAKER-CHECKER");
            e.printStackTrace();
        }


        return  CustomCmsFormatter.checkermap;
    }
}
