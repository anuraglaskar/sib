package clari5.custom.dev;

import clari5.custom.sib.SibBatchProcessor;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMS;
import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CmsUserScenariomap {
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection connection = null;

    public  static CxpsLogger cxpsLogger = CxpsLogger.getLogger(CmsUserScenariomap.class);


    public  String getscenarioMap(String factname) {
        String userlist = null;
        String assigne = null;
        String cur_user = null;
        String name = null;
        String sql = "select * from FACT_USER where FACT_NAME = '" + factname + "'";
        try {
            connection = Rdbms.getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                userlist = rs.getString("USER_LIST");
                cur_user = rs.getString("CUR_USER");
                name = rs.getString("FACT_NAME");
            }
            if (userlist != null) {
                String[] arr = userlist.split(",");

                if (name != null )
                {
                    if(userlist!= null && userlist.contains(",")) {
                        if (cur_user != null) {
                            int index_cur_user = Integer.parseInt(cur_user);
                            if (index_cur_user < arr.length - 1) {
                                int j = index_cur_user + 1;
                                assigne = arr[j];
                                System.out.println("insert current assignee index " + j);
                                System.out.println("inside assignee1" + assigne);
                                String query = "update FACT_USER  set CUR_USER = '" + j + "' where  FACT_NAME = '" + factname + "'";
                                ps = connection.prepareStatement(query);
                                ps.executeUpdate();
                                connection.commit();
                                connection.close();
                                return assigne;
                            } else {
                                assigne = arr[0];
                                System.out.println("inside assignee2" + assigne);
                                String query = "update FACT_USER  set CUR_USER = '" + 0 + "' where  FACT_NAME = '" + factname + "'";
                                ps = connection.prepareStatement(query);
                                ps.executeUpdate();
                                connection.commit();
                                connection.close();
                                return assigne;
                            }

                         } else {
                            assigne = arr[0];
                            System.out.println("inside assignee3" + assigne);
                            String query = "update FACT_USER  set CUR_USER = '" + 0 + "' where  FACT_NAME = '" + factname + "'";
                            ps = connection.prepareStatement(query);
                            ps.executeUpdate();
                            connection.commit();
                            connection.close();
                            return assigne;
                        }
                    }
                    else {
                        assigne = arr[0];
                        connection.close();
                        return assigne;
                    }
                } else {
                    connection.close();
                    return null;
                }
            } else {
                connection.close();
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public String getAcctId(String entityId,String eventId,String factname)
    {
        String cust_id = null;
        String sol_id = null;
        connection = Rdbms.getConnection();

        try {
            System.out.println("hostAcctId is " +entityId);
            String sql = "select \"acctCustID\" from dda where \"hostAcctId\" = '" + entityId + "'";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                cust_id = rs.getString("acctCustID");
            }
            System.out.println("acctCustID is "+cust_id);
            connection.close();
            if (cust_id != null) {
                String user = getBranchCustomer(cust_id,eventId,entityId,factname);
                System.out.println("assignee is "+user);
                return user;
            } else {

                String acctsol = getAcctsol(eventId,factname,entityId);
                if(acctsol != null) {
                    String user = getBranchuserAssignee(acctsol);
                    return user;
                }
                return null;

            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return null;

    }

    public String getBranchUser(String entityId,String eventId, String factname) {
        if (entityId.startsWith("C_F_")) {
            String branchuser = getBranchCustomer(entityId,eventId,entityId,factname);
            return branchuser;
        } else if (entityId.startsWith("R_F_")) {

            String region_sol = entityId.replace("R_F_", "");
            System.out.println("replaced branch R_F_ "+region_sol);
            String bruser = getBranchuserAssignee(region_sol);
            return bruser;

        } else {

            String acctId = entityId.replace("A_F_","");
            System.out.println("replaced A_F_ "+acctId);
            String acctuser = getAcctId(acctId,eventId,factname);
            if( acctuser == null)
            {
                String acctsol = getAcctsol(eventId,factname,acctId);
                if( acctsol != null) {
                    String bruser = getBranchuserAssignee(acctsol);
                    System.out.println("user is" + bruser);
                    return bruser;
                }
                else {
                    return null;
                }
            }
            System.out.println("user "+acctuser);
            return  acctuser;

        }
    }

    public String getBranchCustomer(String entityId,String eventId,String acctId,String factname) {
        String userlist = null;
        String assigne = null;
        String cur_user = null;
        String sol = null;
        String region_code1 = null;
        String region_code = null;
        System.out.println("cust id inside branchcustomer " +entityId );
        String sql = "select \"primarySOL\" from customer where \"primaryCxCifId\" = '" + entityId + "'";
        connection = Rdbms.getConnection();

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                sol = rs.getString("primarySOL");
            }
            System.out.println("sol is" +sol);
            if (sol != null) {
                String sql3 = "select REGION_CODE from REGION_SOL where SOL = '" + sol + "'";
                ps = connection.prepareStatement(sql3);
                rs = ps.executeQuery();
                while (rs.next()) {
                    region_code = rs.getString("REGION_CODE");
                }

                System.out.println("region code is" +region_code);

                if (region_code != null) {
                    String sql2 = "select * from REGION_USER where REGION_CODE = '" + region_code + "'";
                    ps = connection.prepareStatement(sql2);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        userlist = rs.getString("USER_LIST");
                        cur_user = rs.getString("CUR_USER");
                        region_code1 = rs.getString("REGION_CODE");

                    }
                    System.out.println("User is " +userlist);
                   if(userlist!= null && userlist.contains(",")) {
                       String[] arr = userlist.split(",");
                       if (region_code1 == null || userlist == null) {
                           return null;
                       }
                       if (cur_user != null) {
                           int index_cur_user = Integer.parseInt(cur_user);
                           if (index_cur_user < arr.length - 1) {
                               assigne = arr[index_cur_user + 1];
                               System.out.println("update current assignee index " + (index_cur_user + 1));
                               String query = "update REGION_USER  set CUR_USER = '" + (index_cur_user + 1) + "' where  REGION_CODE = '" + region_code + "'";
                               ps = connection.prepareStatement(query);
                               ps.executeUpdate();
                               connection.commit();
                               connection.close();
                               return assigne;
                           } else {
                               assigne = arr[0];
                               String query = "update REGION_USER  set CUR_USER = '" + 0 + "' where  REGION_CODE = '" + region_code + "'";
                               ps = connection.prepareStatement(query);
                               ps.executeUpdate();
                               connection.commit();
                               connection.close();
                               return assigne;
                           }
                       } else {

                           assigne = arr[0];
                           String query = "update REGION_USER  set CUR_USER = '" + 0 + "' where  REGION_CODE = '" + region_code + "'";
                           ps = connection.prepareStatement(query);
                           ps.executeUpdate();
                           connection.commit();
                           connection.close();
                           return assigne;
                       }
                   }
                   else {
                        System.out.println("returing user as " +userlist);
                        connection.close();
                        return userlist;
                   }

                } else {
                    connection.close();
                    return null;
                }
            } else {
                connection.close();
                String acctsol = getAcctsol(eventId,factname,acctId);
                if(acctsol != null) {
                    String user = getBranchuserAssignee(acctsol);
                    return user;
                }
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    public String getTranSOL(String eventId) {

        String sol = null;
        String sql = "select tran_br_id from event_ft_account_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                sol = rs.getString("tran_br_id");
            }
            connection.close();

        } catch (SQLException e) {
            cxpsLogger.info("Exception while getting record from DB " +e);
            cxpsLogger.info("Retrying DB");
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return sol;

    }
    public String getAcctSOL(String eventId) {

        String sol = null;
        String sql = "select acct_sol_id from event_ft_account_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                sol = rs.getString("acct_sol_id");
            }
            connection.close();

        } catch (SQLException e) {
            cxpsLogger.info("Exception while getting record from DB " +e);
            cxpsLogger.info("Retrying DB");
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return sol;

    }


    public String getBranchuserAssignee(String sol)
    {   String userlist = null;
        String assigne = null;
        String cur_user = null;
        String region_code1 = null;
        String region_code = null;
        connection = Rdbms.getConnection();
        try {
            if (sol != null) {
                String sql3 = "select REGION_CODE from REGION_SOL where SOL = '" + sol + "'";
                ps = connection.prepareStatement(sql3);
                rs = ps.executeQuery();
                while (rs.next()) {
                    region_code = rs.getString("REGION_CODE");
                }
                System.out.println("region code" +region_code);
                if (region_code != null) {
                    String sql2 = "select * from REGION_USER where REGION_CODE = '" + region_code + "'";
                    ps = connection.prepareStatement(sql2);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        userlist = rs.getString("USER_LIST");
                        cur_user = rs.getString("CUR_USER");
                        region_code1 = rs.getString("REGION_CODE");

                    }

                    System.out.println("User is " +userlist);
                    if(userlist!= null && userlist.contains(",")) {

                        String[] arr = userlist.split(",");
                        if (region_code1 == null || userlist == null) {
                        return null;
                        }
                        if (cur_user != null) {
                        int index_cur_user = Integer.parseInt(cur_user);
                        if (index_cur_user < arr.length - 1) {
                            assigne = arr[index_cur_user + 1];
                            System.out.println("update current assignee index " + (index_cur_user + 1));
                            String query = "update REGION_USER  set CUR_USER = '" + (index_cur_user + 1) + "' where  REGION_CODE = '" + region_code + "'";
                            ps = connection.prepareStatement(query);
                            ps.executeUpdate();
                            connection.commit();
                            connection.close();
                            return assigne;
                        } else {
                            assigne = arr[0];
                            String query = "update REGION_USER  set CUR_USER = '" + 0 + "' where  REGION_CODE = '" + region_code + "'";
                            ps = connection.prepareStatement(query);
                            ps.executeUpdate();
                            connection.commit();
                            connection.close();
                            return assigne;
                        }
                        } else {

                        assigne = arr[0];
                        String query = "update REGION_USER  set CUR_USER = '" + 0 + "' where  REGION_CODE = '" + region_code + "'";
                        ps = connection.prepareStatement(query);
                        ps.executeUpdate();
                        connection.commit();
                        connection.close();
                        return assigne;

                        }
                    }
                    else {
                        System.out.println("returing user as " +userlist);
                        connection.close();
                        return userlist;
                    }


                } else {
                    connection.close();
                    return null;
                }
            } else {
                connection.close();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    public String getcollateralUser(String eventId, String factname)
    {

        String acctnumber = null;
        String sql = "select loan_acct_no from event_nft_collateral_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                acctnumber = rs.getString("loan_acct_no");
            }
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if( acctnumber != null)
        {
            String nccoluser = getAcctId(acctnumber,eventId,factname);
            return nccoluser;
        }
        return null;
    }



    public String getDDUser(String eventId,String factname)
    {

        String acctnumber = null;
        String sql = "select dd_issue_acct from event_nft_dd_cancel_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                acctnumber = rs.getString("dd_issue_acct");
            }
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if( acctnumber != null)
        {
            String nccoluser = getAcctId(acctnumber,eventId,factname);
            return nccoluser;
        }
        return null;
    }

    public String getDDUserbranch(String eventId)
    {

        String acctnumber = null;
        String branch = null;
        cxpsLogger.info("Into DD USER BRANCH");
        String sql = "select dd_cancel_acct from event_nft_dd_cancel_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                acctnumber = rs.getString("dd_cancel_acct");
            }
            cxpsLogger.info("dd cancel acct number from DD cancel txn [ "+acctnumber+ " ] ");
            String sql2 = "select \"acctBrID\" from dda where \"hostAcctId\" = '" + acctnumber + "'";
            ps = connection.prepareStatement(sql2);
            rs = ps.executeQuery();
            while (rs.next()) {
                branch = rs.getString("acctBrID");
            }
            connection.close();
            cxpsLogger.info("dd cancel branch  from DDA is [ "+branch+ " ]");
            return branch;

        } catch (SQLException e) {
            cxpsLogger.info("Exception while getting record from DB " +e);
            cxpsLogger.info("Retrying DB");
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }




    public List<String> getRegion(String branch) {


        String region_code = null;
        String region_name = null;
        connection = Rdbms.getConnection();
        List<String> list = new ArrayList<String>();
        try {

            if( branch != null)
            {
                String sql3 = "select REGION_CODE from REGION_SOL where SOL = '" + branch + "'";
                ps = connection.prepareStatement(sql3);
                rs = ps.executeQuery();
                while (rs.next()) {
                    region_code = rs.getString("REGION_CODE");
                }
                list.add(region_code);

                if (region_code != null) {
                    String sql2 = "select REGION_NAME from REGION_USER where REGION_CODE = '" + region_code + "'";
                    ps = connection.prepareStatement(sql2);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        region_name = rs.getString("REGION_NAME");

                    }
                    list.add(region_name);
                }
                connection.close();

            }

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;

    }

    public List<String> getTrandetail(String eventId) {

        String branch = null;
        String region_code = null;
        String region_name = null;
        String sql = "select tran_br_id from event_ft_account_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        List<String> list = new ArrayList<String>();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                branch = rs.getString("tran_br_id");
            }

            if( branch != null)
            {
                list.add(branch);
                String sql3 = "select REGION_CODE from REGION_SOL where SOL = '" + branch + "'";
                ps = connection.prepareStatement(sql3);
                rs = ps.executeQuery();
                while (rs.next()) {
                    region_code = rs.getString("REGION_CODE");
                }


                if (region_code != null) {

                    list.add(region_code);
                    String sql2 = "select REGION_NAME from REGION_USER where REGION_CODE = '" + region_code + "'";
                    ps = connection.prepareStatement(sql2);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        region_name = rs.getString("REGION_NAME");

                    }
                    list.add(region_name);
                }
                connection.close();
                return list;

            }

            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;

    }



    public String getcollateralbranch(String eventId) {

        String acctnumber = null;
        String branch = null;
        cxpsLogger.info("getting collateral branch");
        String sql = "select loan_acct_no from event_nft_collateral_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                acctnumber = rs.getString("loan_acct_no");
            }
            cxpsLogger.info("loan acct number from collateral [ "+acctnumber+ " ] ");
            String sql2 = "select \"acctBrID\" from dda where \"hostAcctId\" = '" + acctnumber + "'";
            ps = connection.prepareStatement(sql2);
            rs = ps.executeQuery();
            while (rs.next()) {
                branch = rs.getString("acctBrID");
            }
            connection.close();
            cxpsLogger.info("Collateral branch  from DDA is [ "+branch+ " ]");
           return branch;

        } catch (SQLException e) {
            cxpsLogger.info("Exception while getting record from DB " +e);
            cxpsLogger.info("Retrying DB");
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return  null;

    }


    public String getUBPbranch(String eventId)
    {

        String branch = null;
        String acctnumber = null;
        cxpsLogger.info("Into UBP branch");
        String sql = "select acct_no from event_nft_ubp_acct_open_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                acctnumber = rs.getString("acct_no");
            }
            cxpsLogger.info("acct Number from UBP txn [ "+acctnumber+ " ] ");
            String sql2 = "select \"acctBrID\" from dda where \"hostAcctId\" = '" + acctnumber + "'";
            ps = connection.prepareStatement(sql2);
            rs = ps.executeQuery();
            while (rs.next()) {
                branch = rs.getString("acctBrID");
            }
            connection.close();
            cxpsLogger.info("UBP branch from DDA [ "+branch+ " ] ");
            return branch;
        } catch (SQLException e) {
            cxpsLogger.info("Exception while getting record from DB " +e);
            cxpsLogger.info("Retrying DB");
            e.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public String getLoanbranch(String eventId)
    {

        String acctnumber = null;
        String branch = null;
        cxpsLogger.info("Into LOAN INT RATE branch");
        String sql = "select loan_acct_no from event_nft_loan_int_rate_txn where event_id = '" + eventId + "'";
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                acctnumber = rs.getString("loan_acct_no");
            }
            cxpsLogger.info("acct Number from LOAN INT RATE txn [ "+acctnumber+ " ] ");
            String sql2 = "select \"acctBrID\" from dda where \"hostAcctId\" = '" + acctnumber + "'";
            ps = connection.prepareStatement(sql2);
            rs = ps.executeQuery();
            while (rs.next()) {
                branch = rs.getString("acctBrID");
            }
            connection.close();
            cxpsLogger.info("LOAN INT RATE branch from DDA [ "+branch+ " ] ");
            return branch;

        } catch (SQLException e) {
            cxpsLogger.info("Exception while getting record from DB " +e);
            cxpsLogger.info("Retrying DB");
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return  null;
    }

    public String getAcctsol(String eventId, String factname ,String entityId)
    {
        String sol_id = null;
        if( factname.equalsIgnoreCase("S16") || factname.equalsIgnoreCase("S17"))
        {
            sol_id = entityId.substring(0,4);
            return sol_id;
        }
        else {
            connection = Rdbms.getConnection();
            String sql2 = "select acct_sol_id from event_ft_account_txn where event_id = '" +eventId+ "'";
            try {
                ps = connection.prepareStatement(sql2);
                rs = ps.executeQuery();
                while (rs.next()) {
                    sol_id = rs.getString("acct_sol_id");
                 }
                connection.close();
                return sol_id;
            } catch (SQLException e) {
            e.printStackTrace();
            }
        }

        return null;
    }



}