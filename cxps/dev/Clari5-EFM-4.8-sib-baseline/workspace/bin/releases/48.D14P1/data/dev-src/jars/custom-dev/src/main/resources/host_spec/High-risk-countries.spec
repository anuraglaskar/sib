cxps.noesis.glossary.entity.High-risk-countries { 
        db-name = HIGH_RISK_COUNTRIES
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = country-code, column = COUNTRY_CODE, type = "string:50", key=true }
                { name = country-name, column = COUNTRY_NAME, type = "string:50", key=false }

                ]
        }
          
