# Generated Code
cxps.noesis.glossary.entity.DDA {
	db-name = DDA
	generate = false
	db_column_quoted = true
	extends: cxps.noesis.glossary.entity.Account
	tablespace = CXPS_USERS
	attributes = [
		{ name = last-modified-time, column = LAST_MODIFIED_TIME, type = "timestamp" }
		{ name = version-no, column = version_no, type = "number:15" }
		{ name = delFlag, column = delFlag, type = "char" }
		{ name = mis-code, column = misCode, type = "string:20" }
		{ name = acct-br-i-d, column = acctBrID, type = "string:10" }
		{ name = mailing-addr, column = mailingAddr, type = "string:10" }
		{ name = acct-stmt-print-count175, column = acctStmtPrintCount175, type = "number:10" }
		{ name = acct-ledger-bal-reptd, column = acctLedgerBalReptd, type = "decimal:23,3" }
		{ name = od-grant-dt, column = odGrantDt, type = "date" }
		{ name = od-sanction-amt, column = odSanctionAmt, type = "decimal:23,3" }
		{ name = acct-stmt-next-print-dt, column = acctStmtNextPrintDt, type = "date" }
		{ name = acct-avbl-bal-reptd, column = acctAvblBalReptd, type = "decimal:23,3" }
		{ name = acct-freeze-count, column = acctFreezeCount, type = "number:15" }
		{ name = last-stmt-to-date, column = lastStmtToDate, type = "date" }
		{ name = dda-int-rate, column = ddaIntRate, type = "decimal:8,2" }
		{ name = acct-foat-bal-estd, column = acctFoatBalEstd, type = "decimal:23,3" }
		{ name = nom-addr-city, column = nomAddrCity, type = "string:250" }
		{ name = acct-scheme-code, column = acctSchemeCode, type = "string:20" }
		{ name = od-protect-flag, column = odProtectFlag, type = "string:1" }
		{ name = no-check-retn-out-till-dt, column = noCheckRetnOutTillDt, type = "number:15" }
		{ name = acct-m-o-p, column = acctMOP, type = "string:20" }
		{ name = acctId, column = acctID, type = "string:50", key=true}
		{ name = cumulative-cr, column = cumulativeCr, type = "decimal:23,3" }
		{ name = nom-addr-cntry, column = nomAddrCntry, type = "string:50" }
		{ name = acct-clear-bal-estd, column = acctClearBalEstd, type = "decimal:23,3" }
		{ name = acct-segment, column = acctSegment, type = "string:20" }
		{ name = acct-stmt-mode, column = acctStmtMode, type = "string:20" }
		{ name = no-checks-un-used, column = noChecksUnUsed, type = "number:15" }
		{ name = no-checks-issd, column = noChecksIssd, type = "number:10" }
		{ name = acct-freeze-code, column = acctFreezeCode, type = "char" }
		{ name = corp-id, column = corpId, type = "string:20" }
		{ name = cumulative-tran, column = cumulativeTran, type = "decimal:23,3" }
		{ name = GLSUBHEADCODE, column = GLSUBHEADCODE, type = "string:200" }
		{ name = ACCTAMB, column = ACCTAMB, type = "number:23,3" }
		{ name = acct-closing-bal-lowest6m, column = acctClosingBalLowest6m, type = "decimal:23,3" }
		{ name = acct-name, column = acctName, type = "string:80" }
		{ name = acct-lien-amt, column = acctLienAmt, type = "decimal:23,3" }
		{ name = acct-scheme-type, column = acctSchemeType, type = "string:40" }		
		{ name = net-bk-i-d, column = netBkID, type = "string:40" }		
		{ name = last-bank-induced-tran-dt, column = lastBankInducedTranDt, type = "date" }
		{ name = mailing-addr-state, column = mailingAddrState, type = "string:50" }
		{ name = no-check-tran, column = noCheckTran, type = "number:15" }
		{ name = no-checks-paid, column = noChecksPaid, type = "number:15" }		
		{ name = no-checks-stoppd, column = noChecksStoppd, type = "number:15" }		
		{ name = no-atm-tran, column = noATMTran, type = "number:15" }		
		{ name = dda-acct-stat, column = ddaAcctStat, type = "string:20" }
		{ name = acct-cust-id, column = acctCustID, type = "string:50" }
		{ name = acct-curr-type, column = acctCurrType, type = "string:10" }
		{ name = last-stmt-from-dt, column = lastStmtFromDt, type = "date" }
		{ name = no-check-retn-till-dt, column = noCheckRetnTillDt, type = "number:15" }
		{ name = acct-clear-bal-reptd, column = acctClearBalReptd, type = "decimal:23,3" }
		{ name = last-cust-induced-tran-dt, column = lastCustInducedTranDt, type = "date" }
		{ name = addon-dr-card-issue-status, column = addonDrCardIssueStatus, type = "char" }
		{ name = acct-open-dt, column = acctOpenDt, type = "date" }
		{ name = acct-close-dt, column = acctCloseDt, type = "date" }
		{ name = last-chlvs-iss-qty, column = lastChlvsIssQty, type = "number:15" }
		{ name = last-chlvs-re-ord-qty, column = lastChlvsReOrdQty, type = "number:15" }
		{ name = last-chbk-re-ord-dt, column = lastChbkReOrdDt, type = "date" }
		{ name = acct-foat-bal-reptd, column = acctFoatBalReptd, type = "decimal:23,3" }
		{ name = int-dr-acct-id, column = intDrAcctID, type = "string:50" }
		{ name = acct-stmt-print-count, column = acctStmtPrintCount, type = "decimal:8,2" }
		{ name = mailing-addr-country, column = mailingAddrCountry, type = "string:50" }
		{ name = mailing-addr-country2, column = mailingAddrCountry2, type = "string:50" }
		{ name = mailing-addr-country3, column = mailingAddrCountry3, type = "string:50" }
		{ name = mailing-addr-country4, column = mailingAddrCountry4, type = "string:50" }
		{ name = mailing-addr-country5, column = mailingAddrCountry5, type = "string:50" }
		{ name = chqbk-allowed-flag, column = chqbkAllowedFlag, type = "char" }
		{ name = od-unutilized-amt, column = odUnutilizedAmt, type = "decimal:13,3" }
		{ name = source-of-funds, column = sourceOfFunds, type = "string:10" }
		{ name = last-stmt-print-dt, column = lastStmtPrintDt, type = "date" }
		{ name = nominee-relation-type, column = nomineeRelationType, type = "string:20" }		
		{ name = acct-ledger-bal-estd, column = acctLedgerBalEstd, type = "decimal:23,3" }
		{ name = acct-dr-card-issue-status, column = acctDrCardIssueStatus, type = "char" }
		{ name = acct-stmt-freq, column = acctStmtFreq, type = "string:20" }
		{ name = nom-addr-state, column = nomAddrState, type = "string:50" }
		{ name = last-chbk-iss-dt, column = lastChbkIssDt, type = "date" }
		{ name = no-br-visit, column = noBrVisit, type = "number" }
		{ name = acct-bal-lowest6m, column = acctBalLowest6m, type = "decimal:23,3" }
		{ name = acct-bal-highest6m, column = acctBalHighest6m, type = "decimal:23,3" }
		{ name = acct-monthly-turnover, column = acctMonthlyTurnover, type = "decimal:23,3" }
		{ name = no-check-retn-in-last6m, column = noCheckRetnInLast6m, type = "number:15" }
		{ name = acct-nominee-flag, column = acctNomineeFlag, type = "char" }
		{ name = last-freeze-dt, column = lastFreezeDt, type = "date" }
		{ name = nom-addr-zip, column = nomAddrZip, type = "string:10" }
		{ name = last-unfreeze-dt, column = lastUnfreezeDt, type = "date" }
		{ name = od-expiry-dt, column = odExpiryDt, type = "date" }
		{ name = last-stmt-print-channel, column = lastStmtPrintChannel, type = "string:20" }
		{ name = acct-frez-rsn-code, column = acctFrezRsnCode, type = "string:100" }
		{ name = acct-closing-bal-highest6m, column = acctClosingBalHighest6m, type = "decimal:23,3" }
		{ name = nominee-add-dt, column = nomineeAddDt, type = "date" }
		{ name = nominee-d-o-b, column = nomineeDOB, type = "date" }		
		{ name = no-call-centre-intern, column = noCallCentreIntern, type = "number:15" }		
		{ name = cumulative-dr, column = cumulativeDr, type = "decimal:23,3" }
		{ name = sms-alert-regn-flag, column = smsAlertRegnFlag, type = "char" }
		{ name = acct-status, column = acctStatus, type = "string:20" }
		{ name = no-check-retn-out-last6m, column = noCheckRetnOutLast6m, type = "number:15" }
		{ name = acct-gl-type, column = acctGLType, type = "string:20" }
		{ name = cumulative-cash-dr, column = cumulativeCashDr, type = "decimal:23,3" }
		{ name = cumulative-cash-cr, column = cumulativeCashCr, type = "decimal:23,3" }
		{ name = acct-category, column = acctCategory, type = "string:20" }
		{ name = acct-avg-monthly-invard-remit, column = acctAvgMonthlyInwardRemit, type = "decimal:23,3" }
		{ name = no-checks-destryd, column = noChecksDestryd, type = "number:15" }
		{ name = nominee-addr, column = nomineeAddr, type = "string:250" }
		{ name = mailing-addr-zip, column = mailingAddrZip, type = "string:10" }
		{ name = bp-osg-count, column = bpOsgCount, type = "number:15" }
		{ name = host-id, column = hostId, type = "char" }
		{ name = host-acct-id, column = hostAcctId, type = "string:50" }
		{ name = last-bal-update-dt, column = lastBalUpdateDt, type = "date" }		
		{ name = acct-avbl-bal-estd, column = acctAvblBalEstd, type = "decimal:23,3" }
		{ name = nxt-prob-chbk-iss-dt, column = nxtProbChbkIssDt, type = "date" }
		{ name = int-cr-acct-id, column = intCrAcctID, type = "string:50" }		
		{ name = acct-m-a-b, column = acctMAB, type = "string:20" }
		{ name = mailing-addr-city, column = mailingAddrCity, type = "string:100" }
		{ name = si-setup-flag, column = siSetupFlag, type = "char" }		
		{ name = nominee-name, column = nomineeName, type = "string:40" }
		{ name = acct-a-q-b, column = acctAQB, type = "decimal:23,3" }
		{ name = cumulative-cash-cr-monthly,     column = cumulative_cash_cr_monthly,     type = "decimal:23,3" }
		{ name = cumulative-cash-dr-monthly,     column = cumulative_cash_dr_monthly,     type = "decimal:23,3" }
		{ name = cumulative-cr-turnover-monthly, column = cumulative_cr_turnover_monthly, type = "decimal:23,3" }
		{ name = cumulative-dr-turnover-monthly, column = cumulative_dr_turnover_monthly, type = "decimal:23,3" }
	    { name = purpose-code, column = purposeCode, type = "string:20" }
		{ name = acct-avbl-bal-estd-LCY, column = acctAvblBalEstdLCY, type = "decimal:38,2" }
		{ name = last-maintenance-date, column = lastMaintenanceDate, type = "date" }
	    { name = pur-acct-open-code, column = purAcctOpenCode, type = "string:20" }
	    { name = pur-acct-open-code-desc, column = purAcctOpenCodeDesc, type = "string:20" }
	    { name = anticipated-vol, column = anticipatedVol, type = "decimal:10,2" }
	    { name = exp-mode-of-tran, column = expModeOfTran, type = "string:20" }
	    { name = director-memo, column = directorMemo, type = "string:20" }
	    { name = acct-ownership-code, column = acctOwnershipCode, type = "string:10" }
	    { name = nature-of-business, column = natureOfBusiness, type = "string:10" }
		{ name = last-txn-amount, column = lastTxnAmount, type = "decimal:23,3" }
		{ name = interest-rate, column = interestRate, type = "decimal:5,2" }
		{ name = maturity-date, column = maturityDate, type = "date" }
		{ name = avg-monthly-balance, column = avgMonthlyBalance, type = "decimal:10,2" }
	    { name = employee-flag, column = EmployeeFlag, type = "string:5" }
		{ name =ACCT_OCCP_CODE, column = ACCT_OCCP_CODE, type = "string:200" }
		{ name =SANCTIONLIMIT, column = SANCTIONLIMIT, type = "number:20,4" }
		{ name =DRAWINGPOWER, column = DRAWINGPOWER, type = "string:200" }
		{ name =COLLATERALVALUE, column = COLLATERALVALUE, type = "string:200" }
		{ name =COLLATERALID, column = COLLATERALID, type = "string:200" }
		{ name =EFFECTIVE_INT_RATE, column = EFFECTIVE_INT_RATE, type = "number:20,4" }
		{ name =ACCOUNT_ENTITY, column = ACCOUNT_ENTITY, type = "string:200" }
		{ name = ACCT_RENEWAL_DATE, column = ACCT_RENEWAL_DATE, type = "date" }
		{ name =LOAN_CLOSURE_AMT, column = LOAN_CLOSURE_AMT, type = "number:20,4" }
		{ name =ACCT_TYPE, column = ACCT_TYPE, type = "string:200" }
		{ name =LOAN_AMT, column = LOAN_AMT, type = "number:20,4" }
		{ name = TOD_GRANT_DATE, column = TOD_GRANT_DATE, type = "date" }
		{ name =TOD_AMT, column = TOD_AMT, type = "number:20,4" }	
		{ name = TOD_EXPIRY_DATE, column = TOD_EXPIRY_DATE, type = "date" }
		{ name = TOD_CLOSED_DATE, column = TOD_CLOSED_DATE, type = "date" }
		{ name =TOD_MANUAL, column = TOD_MANUAL, type = "string:10" }
		{ name =JOINT_CUSTIDS, column = JOINT_CUSTIDS, type = "string:200" } 
		{ name =sanctionamt, column = sanctionamt, type = "number:20,4" }
		{ name =JOIN_CUSTID, column = JOIN_CUSTID, type = "string:200" } 
		
			
	]
	indexes {
                dda-custid-idx : [ acctCustID ]
		NCK_DDA_pe = [ hostAcctId ]
	}
}
