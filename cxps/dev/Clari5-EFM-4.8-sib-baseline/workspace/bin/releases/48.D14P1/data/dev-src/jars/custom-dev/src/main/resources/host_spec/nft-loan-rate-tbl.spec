cxps.noesis.glossary.entity.nft-loan-rate-tbl{
       db-name = NFT_LOAN_INT_RATE_TBL
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = LOANACCTNO, column = LOANACCTNO, type = "string:100"}
               { name = LOANACCT_INT_RATE, column = LOANACCT_INT_RATE, type = "number:20,0"}
               { name = DEPOSITACCTNO, column = DEPOSITACCTNO, type = "string:100"}
		{ name = DEPOSITACCT_INT_RATE, column = DEPOSITACCT_INT_RATE, type = "number:20,4"}
	        { name = CUST_ID, column = CUST_ID, type = "string:50" }
               { name = CL5_FLG, column = CL5_FLG, type = "string:50" }
               { name = VERSION_NO, column = VERSION_NO, type = "number:20,0"}
               { name = DD_CANCEL_CUSTID, column = DD_CANCEL_CUSTID, type = "string:200" }
               { name = CL5_FLG, column = CL5_FLG, type = "string:10"}	
		{ name = LAST_MODIFIED_TIME, column = LAST_MODIFIED_TIME, type = timestamp}
               { name = ID, column = ID, type = "string:200",key =true}	
               ]
       }

