package clari5.custom.sib.audit;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.PriorityQueue;

import clari5.custom.sib.exceptions.ConfigurationException;

public class FileAudit implements IAuditClient, Runnable {
	public static boolean continueLogging = true;
	private static FileAudit logger;
	private final String logPath;
	private final LogLevel logLevel;
	private PriorityQueue<String> queue;
	PrintWriter out;

	private FileAudit(LogLevel logLevel, String logPath) throws ConfigurationException {
		this.logLevel = logLevel;
		this.logPath = logPath;
		queue = new PriorityQueue<String>();
		Thread dequeueTask = new Thread(this, "Dequeue Task");
		dequeueTask.start();
	}
	public void run() {
		try {
			if(out == null) {
				out = new PrintWriter(new BufferedWriter(new FileWriter(logPath, true)));
			}
			
			while(continueLogging) {
				if(getCount() == 0) {
					// As of now keeping a sleep, will later be modified with a thread sync.
					Thread.sleep(1000);
				}
				else {
					out.println(dequeue());
					out.flush();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static FileAudit getLogger(LogLevel logLevel, String logPath) throws ConfigurationException {
		if(logger == null) {
			logger = new FileAudit(logLevel, logPath);
		}

		return logger;
	}
	@Override
	public void log(String message) {
		log(LogLevel.ALL, message);
	}
	@Override
	public void log(LogLevel level, String message) {
		if (PermitLog(level)) {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime());
			String formattedMessage = "ThreadId - ["+Thread.currentThread().getId()+"] - "+timeStamp+" --> "+message;
			this.enqueue(formattedMessage);
		}
	}
	private boolean PermitLog(LogLevel messLevel) {
		if (logLevel == LogLevel.ALL) {
			return true;
		}
		else if (logLevel == LogLevel.OFF){
			return false;
		}
		else if (messLevel.compareTo(logLevel) < 0) {
			return false;
		}

		return true;
	}
	private void enqueue(String message) {
		synchronized(this) {
			queue.add(message);
		}
	}
	private String dequeue() {
		String message = "";
		synchronized(this) {
			message = queue.poll();
			System.out.println(message);
		}
		
		return message;
	}
	private int getCount() {
		int qSize = 0;
		synchronized(this) {
			qSize = queue.size();
		}
		return qSize;
	}
}