package clari5.custom.sib.audit;

import java.util.PriorityQueue;

import clari5.custom.sib.db.DBTask;

public class DatabaseAudit implements IAuditClient, Runnable {
	public static boolean continueLogging = true;
	private static DatabaseAudit dbLogger;
	private PriorityQueue<String> queue;

	private DatabaseAudit() {
		queue = new PriorityQueue<String>();
		Thread dequeueTask = new Thread(this, "DB_Audit_Task");
		dequeueTask.start();
	}
	public static DatabaseAudit getLogger() {
		if(dbLogger == null) {
			dbLogger = new DatabaseAudit();
		}

		return dbLogger;
	}

	public void run() {

		/*try {
			while(continueLogging) {
			if(getCount() == 0) {
				// As of now keeping a sleep, will later be modified with a thread sync.
				Thread.sleep(10000);
			}
			else {
				DBTask.updateAudit(dequeue().split("\\<\\$\\@\\#\\*\\%\\@\\>"));
			}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}*/
	}
	@Override
	public void log(String message) {
		log(LogLevel.ALL, message);
	}
	@Override
	public void log(LogLevel level, String message) {
		synchronized(this) {
			this.enqueue(message);
		}
	}
	private void enqueue(String message) {
		synchronized(this) {
			queue.add(message);
		}
	}
	private String dequeue() {
		String message = "";
		synchronized(this) {
			message = queue.poll();
		}

		return message;
	}
	private int getCount() {
		int qSize = 0;
		synchronized(this) {
			qSize = queue.size();
		}
		return qSize;
	}
}
