package clari5.custom.sib.builder;

public class Calculate {

    public static  String getFLAG(String dpintrate, String lnintrate) {
        String FLAG= null;
        if( dpintrate != null && lnintrate != null) {
            try {
                double s = Double.parseDouble(dpintrate) - Double.parseDouble(lnintrate);
                System.out.println("double value " + s);
                if (s < 1) {
                    FLAG = "s1";
                    return FLAG;
                } else if (s < 2 ) {
                    FLAG = "s2";
                    return FLAG;
                } else {
                    FLAG = "n";
                    return FLAG;
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else {
            return null;
        }
    }

    /*public static void main(String[] args) {
        String flg = Calculate.getFLAG("1","0.5");
        System.out.println(flg);
    }*/
}
