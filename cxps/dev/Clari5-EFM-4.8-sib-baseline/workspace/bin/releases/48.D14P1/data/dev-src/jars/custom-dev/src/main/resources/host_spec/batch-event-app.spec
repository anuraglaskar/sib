cxps.noesis.glossary.entity.batch-event-app {
        db-name = BATCH_EVENT_APP
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = TABLE_NAME, column = TABLE_NAME, type = "string:200" }
                { name = CREATED_ON, column = CREATED_ON, type = timestamp }
                ]
        }

