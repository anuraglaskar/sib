package clari5.custom.sib.data;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class NFT_LoanIntrateTxn extends ITableData {
	private String tableName = "NFT_LOAN_INT_RATE_TBL";
	private String event_type = "nft_loanintratetxn";


	private  String LOANACCTNO;
    private  String	LOANACCT_INT_RATE;
	private  String DEPOSITACCTNO;
    private  String	DEPOSITACCT_INT_RATE;
	private  String CUST_ID;
	private  String ID;

	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getLOANACCTNO() {
		return LOANACCTNO;
	}

	public void setLOANACCTNO(String LOANACCTNO) {
		this.LOANACCTNO = LOANACCTNO;
	}

	public String getLOANACCT_INT_RATE() {
		return LOANACCT_INT_RATE;
	}

	public void setLOANACCT_INT_RATE(String LOANACCT_INT_RATE) {
		this.LOANACCT_INT_RATE = LOANACCT_INT_RATE;
	}

	public String getDEPOSITACCTNO() {
		return DEPOSITACCTNO;
	}

	public void setDEPOSITACCTNO(String DEPOSITACCTNO) {
		this.DEPOSITACCTNO = DEPOSITACCTNO;
	}

	public String getDEPOSITACCT_INT_RATE() {
		return DEPOSITACCT_INT_RATE;
	}

	public void setDEPOSITACCT_INT_RATE(String DEPOSITACCT_INT_RATE) {
		this.DEPOSITACCT_INT_RATE = DEPOSITACCT_INT_RATE;
	}

	public String getCUST_ID() {
		return CUST_ID;
	}

	public void setCUST_ID(String CUST_ID) {
		this.CUST_ID = CUST_ID;
	}

	@Override
	public String getTableName() {
		return tableName;
	}

}
