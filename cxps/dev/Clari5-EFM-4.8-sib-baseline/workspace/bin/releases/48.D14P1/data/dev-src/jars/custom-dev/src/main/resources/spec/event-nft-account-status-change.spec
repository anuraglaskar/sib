cxps.events.event.nft-account-status-change{
  table-name : EVENT_NFT_ACCOUNTSTATUSCHANG
  event-mnemonic: NAS
  workspaces : {
    ACCOUNT : account_id,
    CUSTOMER: cust_id
  }
  event-attributes : {
	account-id: {db : true ,raw_name : account_id ,type : "string:200", custom-getter:Account_id}
	cust-id: {db : true ,raw_name : cust_id ,type : "string:200", custom-getter:Cust_id}
	init-acct-status: {db : true ,raw_name : init_acct_status ,type : "string:200", custom-getter:Init_acct_status}
	final-acct-status: {db : true ,raw_name : final_acct_status ,type : "string:200", custom-getter:Final_acct_status}
	avl-bal: {db : true ,raw_name : avl_bal ,type : "number:11,2", custom-getter:Avl_bal}
	user-id: {db : true ,raw_name : user_id ,type : "string:200", custom-getter:User_id}
	branch-id: {db : true ,raw_name : branch_id ,type : "string:200", custom-getter:Branch_id}
	branch-id-desc: {db : true ,raw_name : branch_id_desc ,type : "string:200", custom-getter:Branch_id_desc}
	acct-open-dt: {db : true ,raw_name : acct_open_dt ,type : timestamp, custom-getter:Acct_open_dt}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
}
}
