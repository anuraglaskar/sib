cxps.noesis.glossary.entity.user-designation {
       db-name = USER_DESIGNATION
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = userid, column = USERID, type = "string:10", key=true }
               { name = empid, column = EMPID, type = "string:20", key=false }
               { name = designation, column = DESIGNATION, type = "string:20", key=false }
               ]
       }

