cxps.noesis.glossary.entity.maker-checker {
        db-name = MAKER_CHECKER
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = RO_MAKER, column = RO_MAKER, type = "string:30"}
                { name = RO_PPC, column = RO_PPC, type = "string:20" }
		        { name = REGION_NAME, column = REGION_NAME, type = "string:20", key=true }
		        { name = REGION_CODE, column = REGION_CODE, type = "string:30" }
		        { name = HO_CHECKER, column = HO_CHECKER, type = "string:20"}
		        { name = HO_PPC, column = HO_PPC, type = "string:30" }

                ]
        }

