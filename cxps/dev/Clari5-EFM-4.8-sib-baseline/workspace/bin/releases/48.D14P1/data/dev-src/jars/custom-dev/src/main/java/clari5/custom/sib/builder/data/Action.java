package clari5.custom.sib.builder.data;

import java.util.List;

public class Action {
	// Change it to appropriate name if an action other than in db is encountered.
	private String destinationTableName;
	private String action;
	private List<String> sourceKeys;
	private List<String> where;
	private String target;
	private String value;
	public String getDestinationTableName() {
		return destinationTableName;
	}
	public void setDestinationTableName(String destinationTableName) {
		this.destinationTableName = destinationTableName;
	}
	public String getAction() {
		return this.action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public List<String> getSourceKeys() {
		return this.sourceKeys;
	}
	public void setSourceKeys(List<String> sourceKeys) {
		this.sourceKeys = sourceKeys;
	}
	public List<String> getWhere() {
		return this.where;
	}
	public void setWhere(List<String> where) {
		this.where = where;
	}
	public String getTarget() {
		return this.target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}