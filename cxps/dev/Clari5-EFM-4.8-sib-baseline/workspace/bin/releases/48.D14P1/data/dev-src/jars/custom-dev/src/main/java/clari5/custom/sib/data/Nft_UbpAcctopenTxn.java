package clari5.custom.sib.data;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Nft_UbpAcctopenTxn extends ITableData {
	private String tableName = "NFT_UBP_ACCT_OPEN_TBL";
	private String event_type = "nft_ubpacctopentxn";
    private String ID;
    private String ACCTNO;
    private String ACCT_BRANCH_ID;
    private String CUST_ID;
	private String CUST_SOL_ID;
	private String SYS_TIME;


	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getACCTNO() {
		return ACCTNO;
	}

	public void setACCTNO(String ACCTNO) {
		this.ACCTNO = ACCTNO;
	}

	public String getACCT_BRANCH_ID() {
		return ACCT_BRANCH_ID;
	}

	public void setACCT_BRANCH_ID(String ACCT_BRANCH_ID) {
		this.ACCT_BRANCH_ID = ACCT_BRANCH_ID;
	}

	public String getCUST_ID() {
		return CUST_ID;
	}

	public void setCUST_ID(String CUST_ID) {
		this.CUST_ID = CUST_ID;
	}

	public String getCUST_SOL_ID() {
		return CUST_SOL_ID;
	}

	public void setCUST_SOL_ID(String CUST_SOL_ID) {
		this.CUST_SOL_ID = CUST_SOL_ID;
	}

	public String getSYS_TIME() {
		return SYS_TIME;
	}

	public void setSYS_TIME(String SYS_TIME) {
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis()));
		this.SYS_TIME = timeStamp;
	}

	@Override
	public String getTableName() {
		return tableName;
	}

}
