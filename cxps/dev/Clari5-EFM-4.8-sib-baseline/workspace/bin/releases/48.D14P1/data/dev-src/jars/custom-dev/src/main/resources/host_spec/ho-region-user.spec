cxps.noesis.glossary.entity.ho-region-user {
        db-name = HO_REGION_USER
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = REGION_CODE, column = REGION_CODE, type = "string:50", key=true }
                { name = USER_LIST, column = USER_LIST, type = "string:30"}
		        { name = FLAG, column = FLAG, type = "string:40" }
                     ]
        }
