// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
//import clari5.platform.logger.CXLog;
//import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_COLLATERAL_TXN", Schema="rice")
public class NFT_CollateralTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String collateralStatus;
       @Field public java.sql.Timestamp collateralOpenDate;
       @Field(size=50) public String eventName;
       @Field(size=50) public String collateralId;
       @Field(size=50) public String eventsubtype;
       @Field(size=50) public String eventtype;
       @Field public java.sql.Timestamp collateralModDate;
       @Field(size=50) public String hostId;
       @Field public java.sql.Timestamp sysTime;
       @Field public java.sql.Timestamp eventts;
       @Field(size=50) public String loanAcctNo;
       @Field(size=50) public String acctStatus;


    @JsonIgnore
    public ITable<NFT_CollateralTxnEvent> t = AEF.getITable(this);

	public NFT_CollateralTxnEvent(){}

    public NFT_CollateralTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("CollateralTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setCollateralStatus(json.getString("Collateral_Status"));
            setCollateralOpenDate(EventHelper.toTimestamp(json.getString("Collateral_Open_date")));
            setEventName(json.getString("event-name"));
            setCollateralId(json.getString("Collateral_Id"));
            setEventsubtype(json.getString("eventsubtype"));
            setEventtype(json.getString("eventtype"));
            setCollateralModDate(EventHelper.toTimestamp(json.getString("Collateral_Mod_date")));
            setHostId(json.getString("host-id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setLoanAcctNo(json.getString("LoanAcctNo"));
            setAcctStatus(json.getString("Acct_status"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "CT"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getCollateralStatus(){ return collateralStatus; }

    public java.sql.Timestamp getCollateralOpenDate(){ return collateralOpenDate; }

    public String getEventName(){ return eventName; }

    public String getCollateralId(){ return collateralId; }

    public String getEventsubtype(){ return eventsubtype; }

    public String getEventtype(){ return eventtype; }

    public java.sql.Timestamp getCollateralModDate(){ return collateralModDate; }

    public String getHostId(){ return hostId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getLoanAcctNo(){ return loanAcctNo; }

    public String getAcctStatus(){ return acctStatus; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setCollateralStatus(String val){ this.collateralStatus = val; }
    public void setCollateralOpenDate(java.sql.Timestamp val){ this.collateralOpenDate = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setCollateralId(String val){ this.collateralId = val; }
    public void setEventsubtype(String val){ this.eventsubtype = val; }
    public void setEventtype(String val){ this.eventtype = val; }
    public void setCollateralModDate(java.sql.Timestamp val){ this.collateralModDate = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setLoanAcctNo(String val){ this.loanAcctNo = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
       // ICXLog cxLog = CXLog.fenter("derive.NFT_CollateralTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.loanAcctNo);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.collateralId);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

       // cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_CollateralTxn");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "CollateralTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}