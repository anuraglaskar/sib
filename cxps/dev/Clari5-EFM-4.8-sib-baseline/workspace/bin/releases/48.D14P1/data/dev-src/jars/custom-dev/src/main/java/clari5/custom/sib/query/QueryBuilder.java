package clari5.custom.sib.query;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import clari5.custom.sib.config.BepCon;
import clari5.platform.logger.CxpsLogger;
import clari5.rdbms.*;


public class QueryBuilder {

    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    ResultSet rs1 = null;
    String eventname = null;
    java.sql.Timestamp eventts = null;
    String tableName1 = null;
    public  static CxpsLogger cxpsLogger = CxpsLogger.getLogger(QueryBuilder.class);


    public boolean select(String tableName, String columnName) {

        int wait = 0;
        try {
            wait = BepCon.getConfig().getWaittime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String query = "select * from BATCH_EVENT_APP where TABLE_NAME = '" + columnName + "'" ;
                //+ "and CREATED_ON < SYSTIMESTAMP - INTERVAL '" + wait + "' MINUTE";
        con = Rdbms.getAppConnection();
        cxpsLogger.info("Connection OPENED");
        try {
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                eventname = rs.getString("table_name");
                eventts = rs.getTimestamp("created_on");
            }
           cxpsLogger.info("event ts : [ "+eventts+"]");
            if (eventname != null && eventts != null) {

                cxpsLogger.info("Table name [ " +columnName+ " ] : Already exists and  hence lock can't be acquired wait till it releases");
                System.out.println("Table name [ " +columnName+ " ] : Already exists and  hence lock can't be acquired wait till it releases");

                boolean rmlock = checkTableLock(eventts);
               cxpsLogger.info(rmlock);
                if(rmlock){
                    delete(eventname);
                    System.out.println("Time exceeds than limit hence table lock removed");
                }
                return false;
               /* cxpsLogger.info("inside creating new entry since time is leas");
                String delete = "delete from " + tableName + " where table_name = '" + eventname + "'";
                ps = con.prepareStatement(delete);
                int i = ps.executeUpdate();
                if (i > 0) {
                    con.commit();
                }

                String insert = "Insert into " + tableName + "(TABLE_NAME,CREATED_ON) values ('" + eventname + "',CURRENT_TIMESTAMP)";
                ps = con.prepareStatement(insert);
                int j = ps.executeUpdate();
                if (j > 0) {
                    con.commit();
                    return true;
                } */

            } else {

               /* String query1 = "select * from BATCH_EVENT_APP where TABLE_NAME = '" + columnName + "'";
                ps = con.prepareStatement(query1);
                rs1 = ps.executeQuery();
                while (rs1.next()) {
                    tableName1 = rs1.getString("table_name");
                }
                if (tableName1 != null) {
                    return false;
                } else { */
                    String insert = "Insert into " + tableName + "(TABLE_NAME,CREATED_ON) values ('" + columnName + "',CURRENT_TIMESTAMP)";
                    ps = con.prepareStatement(insert);
                    int j = ps.executeUpdate();
                    if (j > 0) {
                        con.commit();
                        cxpsLogger.info("Table name [ " +columnName+ " ]  : Inserted Successfully and acquired lock");
                        return true;
                    }
            }

        } catch (SQLException e) {
          cxpsLogger.info("unique constraint");
          return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (con != null) {
                    cxpsLogger.info("Connection CLOSED");
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;

    }

    public boolean delete(String tablentry) {
        cxpsLogger.info("Removing  lock for table [ " +tablentry+ " ] ");
        con = Rdbms.getAppConnection();
        cxpsLogger.info("Connection OPENED");
        String delete = "delete from BATCH_EVENT_APP where table_name = '" + tablentry + "'";
        try {
            ps = con.prepareStatement(delete);
            int i = 0;
            i = ps.executeUpdate();
            if (i > 0) {
                con.commit();
                cxpsLogger.info("Sucessfully removed lock  for [ " +tablentry+ " ] " );
                return true;
            }
            else {
                cxpsLogger.info("UNABLE TO REMOVE LOCK since delete is failing  for [ " +tablentry+ " ] ");
                return false;
            }

        }
        catch (SQLException e) {
            cxpsLogger.info("UNABLE TO REMOVE LOCK for [ " +tablentry+ " ] . Check Exception ");
            e.printStackTrace();
            return  false;
        }
        finally {
            try {
                if (con != null) {
                    con.close();
                    cxpsLogger.info("Connection CLOSED");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }



    private boolean checkTableLock(Timestamp ts) {
        boolean status = false;
        if(ts != null) {
            try {
                cxpsLogger.info("into remove lock since time is high : "+ts.getTime() );
                int seconds = (int) (new Date().getTime() - ts.getTime()) / 1000;
                cxpsLogger.info("Seconds : ["+seconds+"]");
                int minutes = (seconds % 3600) / 60;
                System.out.println("minutes-> " + minutes);
                int lckmin =  BepCon.getConfig().getWaittime();
                if ( minutes >=  lckmin && minutes > 0) {
                    status = true;
                }
                else if(((new Date().getTime() - ts.getTime()) / 3600000) >= 1){
                    status = true;
                }
                else
                {
                    return status;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return status;
    }
}
