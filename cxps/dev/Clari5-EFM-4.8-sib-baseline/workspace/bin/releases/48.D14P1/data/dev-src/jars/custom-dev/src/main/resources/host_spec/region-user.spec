cxps.noesis.glossary.entity.region-user {
        db-name = REGION_USER
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = REGION_CODE, column = REGION_CODE, type = "string:30", key=true }
                { name = USER_LIST, column = USER_LIST, type = "string:20" }
		        { name = REGION_NAME, column = REGION_NAME, type = "string:20"}
		        { name = USER_LIST, column = USER_LIST, type = "string:20"}
		        { name = FLAG, column = FLAG, type = "string:20"}

                ]
        }

