package clari5.custom.dev;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.CxpsLogger;
import cxps.apex.utils.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class RegionLoader extends Thread {

    private static CxpsLogger cxpsLogger = CxpsLogger.getLogger(RegionLoader.class);

    public void run() {
        while(true){
            try
            {
                // Displaying the thread that is running
                System.out.println ("Thread " + Thread.currentThread().getId() + " is running");
                updateRegUsr();
                cxpsLogger.info("Map & REG USR updated Sucessfully");

                updateHoRegUsr();
                cxpsLogger.info("Map & HO REG USR updated Sucessfully");

                updateFctUsr();
                cxpsLogger.info("Map & FACT USR updated Sucessfully");
                Thread.sleep(30000);
                
            }
            catch (Exception e)
            {
                // Throwing an exception
                System.out.println ("Exception is caught");
                e.printStackTrace();
            }
        }
    }

    public void updateRegUsr()
    {
        String USER_LIST = null;
        String REGION_CODE = null;
        String sql = "select * from REGION_USER  Where FLAG = 'Y'";
        List<String> list = new ArrayList<>();
        try (Connection con = Rdbms.getAppConnection();
             PreparedStatement ps = con.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()){
            while (rs.next()) {
                cxpsLogger.info("Region USER Map ----- Getting Updated to assign");
                REGION_CODE = rs.getString("REGION_CODE");
                USER_LIST = rs.getString("USER_LIST");
                list.add(REGION_CODE);
                if(CustomCmsFormatter.regUsrmap.contains(REGION_CODE)) {
                    cxpsLogger.info("Replacing On REG USR MAP RegionCode [ " +REGION_CODE+ " ] UserList [ " +USER_LIST+ " ] ");
                    CustomCmsFormatter.regUsrmap.replace(REGION_CODE,USER_LIST);
                    CustomCmsFormatter.conCurrRegUsrMap.replace(REGION_CODE, 0);

                }
                else {
                    cxpsLogger.info("Adding On REG USR MAP RegionCode [ " +REGION_CODE+ " ] UserList [ " +USER_LIST+ " ] ");
                    CustomCmsFormatter.regUsrmap.put(REGION_CODE,USER_LIST);
                }
            }
            if(list != null && list.size() > 0) {
                String updateQuery = "UPDATE REGION_USER SET FLAG = 'N' where FLAG = 'Y'";
                 //and REGION_CODE in ('" + StringUtils.join(list, "','") + "')" + "";
                try(PreparedStatement ups = con.prepareStatement(updateQuery)){
                    int i = ups.executeUpdate();
                    con.commit();
                    if(i > 0){
                        cxpsLogger.info("Updated Sucesss for Region [ " +list.toString() + "] ");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            cxpsLogger.info("Exception on REGION_USER");
            e.printStackTrace();
        }
    }

    public void updateHoRegUsr()
    {
        String USER_LIST = null;
        String REGION_CODE = null;
        String sql = "select * from HO_REGION_USER  Where FLAG = 'Y'";
        List<String> list = new ArrayList<>();
        try (Connection con = Rdbms.getAppConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rs = ps.executeQuery()){
            while (rs.next()) {
                cxpsLogger.info("HO Region USER Map ----- Getting Updated to assign");
                REGION_CODE = rs.getString("REGION_CODE");
                USER_LIST = rs.getString("USER_LIST");
                list.add(REGION_CODE);
                if(CustomCmsFormatter.hoUsrmap.contains(REGION_CODE)) {
                    cxpsLogger.info("Replacing On  HO REG USR MAP RegionCode [ " +REGION_CODE+ " ] UserList [ " +USER_LIST+ " ] ");
                    CustomCmsFormatter.hoUsrmap.replace(REGION_CODE,USER_LIST);
                    CustomCmsFormatter.conCurrHoUsrMap.replace(REGION_CODE, 0);
                }
                else {
                    cxpsLogger.info("Adding On  HO REG USR MAP RegionCode [ " +REGION_CODE+ " ] UserList [ " +USER_LIST+ " ] ");
                    CustomCmsFormatter.hoUsrmap.put(REGION_CODE,USER_LIST);
                }
            }
            if(list != null && list.size() > 0) {
                String updateQuery = "UPDATE HO_REGION_USER SET FLAG = 'N' where FLAG = 'Y'";
            //and REGION_CODE in ('" + StringUtils.join(list, "','") + "')" + "";
                try(PreparedStatement ups = con.prepareStatement(updateQuery)){
                    int i = ups.executeUpdate();
		    con.commit();
                    if(i > 0){
                        cxpsLogger.info("Updated Sucesss for  Ho Region [ " +list.toString() + "] ");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            cxpsLogger.info("Exception on  Ho REGION_USER");
            e.printStackTrace();
        }
    }


    public void updateFctUsr()
    {

        String USER_LIST = null;
        String FACT_NAME = null;
        String sql = "select * from FACT_USER  Where FLAG = 'Y'";
        List<String> list = new ArrayList<>();
        try (Connection con = Rdbms.getAppConnection();
             PreparedStatement ps = con.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()){
            while (rs.next()) {
                cxpsLogger.info(" FACT_USER USER Map ----- Getting Updated to assign");
                FACT_NAME = rs.getString("FACT_NAME");
                USER_LIST = rs.getString("USER_LIST");
                list.add(FACT_NAME);
                if(CustomCmsFormatter.fctUsrmap.contains(FACT_NAME)) {
                    cxpsLogger.info("Replacing On  FACT USR MAP RegionCode [ " +FACT_NAME+ " ] UserList [ " +USER_LIST+ " ] ");
                    CustomCmsFormatter.fctUsrmap.replace(FACT_NAME,USER_LIST);
                    CustomCmsFormatter.conCurrFactUsrMap.replace(FACT_NAME, 0);
                }
                else {
                    cxpsLogger.info("Adding On  FACT USR MAP RegionCode [ " +FACT_NAME+ " ] UserList [ " +USER_LIST+ " ] ");
                    CustomCmsFormatter.fctUsrmap.put(FACT_NAME,USER_LIST);
                }
            }
            if(list != null && list.size() > 0 ) {
                String updateQuery = "UPDATE FACT_USER SET FLAG = 'N' where FLAG = 'Y'";
		//and FACT_NAME in ('" + StringUtils.join(list, "','") + "')" + "";
                try(PreparedStatement ups = con.prepareStatement(updateQuery)){
                    int i = ups.executeUpdate();
		    con.commit();
                    if(i > 0){
                        CustomCmsFormatter.fctUsrmap.clear();
                        cxpsLogger.info("Updated Sucesss for fact [ " +list.toString() + "] ");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            cxpsLogger.info("Exception on FACT_USER");
            e.printStackTrace();
        }
    }
}
