cxps.events.event.nft-dd-cancel-txn{
table-name : EVENT_NFT_DD_CANCEL_TXN
event-mnemonic : DC
workspaces : {
  NONCUSTOMER : "dd-num"
}

event-attributes : {

dd-num : { db : true, raw_name : DD_num, type : "string:50" }
dd-issue-date : { db : true, raw_name : DD_issue_date, type : "timestamp" }
dd-cancel-date : { db : true, raw_name : DD_cancel_date, type : "timestamp" }
dd-issue-amt : { db : true, raw_name : DD_issue_amt, type : "decimal:32,2" }
dd-cancel-amt : { db : true, raw_name : DD_cancel_amt, type : "decimal:32,2" }
dd-issue-acct : { db : true, raw_name : DD_Issue_acct, type : "string:50" }
dd-cancel-acct : { db : true, raw_name : DD_cancel_acct, type : "string:50" }
dd-issue-custid : { db : true, raw_name : DD_issue_custid, type : "string:50" }
dd-cancel-custid : { db : true, raw_name : Dd_cancel_custid, type : "string:50" }
eventtype : { db : true, raw_name : eventtype, type : "string:50" }
eventsubtype : { db : true, raw_name : eventsubtype, type : "string:50" }
host-id  : { db : true, raw_name : host-id, type : "string:50" }
event-name : { db : true, raw_name : event-name, type : "string:50" }
eventts : { db : true, raw_name : eventts, type : "timestamp" }
sys-time : { db : true, raw_name : sys-time, type : "timestamp" }


}
}
