cxps.noesis.glossary.entity.region-sol {
        db-name = REGION_SOL
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = SOL, column = SOL, type = "string:20", key=true }
                { name = REGION_CODE, column = REGION_CODE, type = "string:30" }

                     ]
        }


