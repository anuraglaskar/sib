cxps.events.event.ft-remittance-txn{
table-name : EVENT_FT_REMITTANCE_TXN
event-mnemonic : RM
workspaces : {
  ACCOUNT : "ben-acct-no,rem-acct-no"
  CUSTOMER : "rem-cust-id,ben-cust-id"
  NONCUSTOMER : "tran-ref-no,amt-rem-ben-acct,app-ben-name,ben-name-country"
}

event-attributes : {

ben-name-country : { db : true, raw_name : SYSTEM, type : "string:50",derivation:"""CustomDerivation.getBenNameCountry(this)""" }
app-ben-name : { db : true, raw_name : SYSTEM, type : "string:50",derivation:"""CustomDerivation.getAppBenName(this)""" }
amt-rem-ben-acct : { db : true, raw_name : SYSTEM, type : "string:50",derivation:"""CustomDerivation.getAmtRemBenAcct(this)""" }
system : { db : true, raw_name : SYSTEM, type : "string:50" }
rem-type : { db : true, raw_name : REM_TYPE, type : "string:50" }
branch : { db : true, raw_name : BRANCH, type : "string:50" }
account-category : { db : true, raw_name : ACCOUNT_CATEGORY, type : "string:50" }
type : { db : true, raw_name : TYPE          , type : "string:50" }
msgtype : { db : true, raw_name : MSGTYPE, type : "string:50" }
sno : { db : true, raw_name : SNO           , type : "string:50" }
tran-ref-no : { db : true, raw_name : TRAN_REF_NO   , type : "string:50" }
tran-curr : { db : true, raw_name : TRAN_CURR     , type : "string:50" }
tran-amt : { db : true, raw_name : TRAN_AMT      , type : "decimal:32,2" }
usd-eqv-amt : { db : true, raw_name : USD_EQV_AMT   , type : "decimal:32,2" }
inr-amount : { db : true, raw_name : INR_AMOUNT    , type : "decimal:32,2" }
purpose-code : { db : true, raw_name : PURPOSE_CODE  , type : "string:50" }
purpose-desc : { db : true, raw_name : PURPOSE_DESC  , type : "string:50" }
rem-cust-id : { db : true, raw_name : REM_CUST_ID, type : "string:50" }
rem-name : { db : true, raw_name : REM_NAME      , type : "string:50" }
rem-add1 : { db : true, raw_name : REM_ADD1      , type : "string:50" }
rem-add2 : { db : true, raw_name : REM_ADD2      , type : "string:50" }
rem-add3 : { db : true, raw_name : REM_ADD3      , type : "string:50" }
rem-city : { db : true, raw_name : REM_CITY, type : "string:50" }
rem-cntry-code : { db : true, raw_name : REM_CNTRY_CODE, type : "string:50" }
ben-cust-id : { db : true, raw_name : BEN_CUST_ID, type : "string:50" }
ben-name : { db : true, raw_name : BEN_NAME      , type : "string:50" }
ben-add1 : { db : true, raw_name : BEN_ADD1      , type : "string:50" }
ben-add2 : { db : true, raw_name : BEN_ADD2      , type : "string:50" }
ben-add3 : { db : true, raw_name : BEN_ADD3      , type : "string:50" }
ben-city : { db : true, raw_name : BEN_CITY, type : "string:50" }
ben-cntry-code : { db : true, raw_name : BEN_CNTRY_CODE, type : "string:50" }
client-acc-no : { db : true, raw_name : CLIENT_ACC_NO , type : "string:50" }
cpty-ac-no : { db : true, raw_name : CPTY_AC_NO    , type : "string:50" }
ben-acct-no : { db : true, raw_name : BEN_ACCT_NO   , type : "string:50" }
ben-bic : { db : true, raw_name : BEN_BIC       , type : "string:50" }
rem-acct-no : { db : true, raw_name : REM_ACCT_NO   , type : "string:50" }
rem-bic : { db : true, raw_name : REM_BIC       , type : "string:50" }
trn-date : { db : true, raw_name : TRN_DATE, type : "timestamp" }
host-id  : { db : true, raw_name : host-id, type : "string:50" }


}
}
