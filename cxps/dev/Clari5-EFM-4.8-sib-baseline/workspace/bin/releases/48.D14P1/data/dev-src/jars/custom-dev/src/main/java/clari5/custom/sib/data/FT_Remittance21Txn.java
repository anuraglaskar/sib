package clari5.custom.sib.data;

public class FT_Remittance21Txn extends ITableData {
	private String tableName = "FT_REMITTANCE_21";
	private String event_type = "ft_remittance21txn";


	private  String SYSTEM;
	private  String REM_TYPE;
	private  String BRANCH;
	private  String ACCOUNT_CATEGORY;
	private  String TYPE;
	private  String MSGTYPE;
	private  String SNO;
	private  String TRAN_REF_NO;
	private  String TRAN_CURR;
	private  String TRAN_AMT;
	private  String USD_EQV_AMT;
	private  String INR_AMOUNT;
	private  String PURPOSE_CODE;
	private  String PURPOSE_DESC;
	private  String REM_CUST_ID;
	private  String REM_NAME;
	private  String REM_ADD1;
	private  String REM_ADD2;
	private  String REM_ADD3;
	private  String REM_CITY;
	private  String REM_CNTRY_CODE;
	private  String BEN_CUST_ID;
	private  String BEN_NAME;
	private  String BEN_ADD1;
	private  String BEN_ADD2;
	private  String BEN_ADD3;
	private  String BEN_CITY;
    private  String	BEN_CNTRY_CODE;
	private  String CLIENT_ACC_NO;
    private  String	CPTY_AC_NO;
	private  String BEN_ACCT_NO;
	private  String	BEN_BIC;
	private  String REM_ACCT_NO;
    private  String	REM_BIC;
	private  String TRN_DATE;
	private  String ID;

	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getSYSTEM() {
		return SYSTEM;
	}

	public void setSYSTEM(String SYSTEM) {
		this.SYSTEM = SYSTEM;
	}

	public String getREM_TYPE() {
		return REM_TYPE;
	}

	public void setREM_TYPE(String REM_TYPE) {
		this.REM_TYPE = REM_TYPE;
	}

	public String getBRANCH() {
		return BRANCH;
	}

	public void setBRANCH(String BRANCH) {
		this.BRANCH = BRANCH;
	}

	public String getACCOUNT_CATEGORY() {
		return ACCOUNT_CATEGORY;
	}

	public void setACCOUNT_CATEGORY(String ACCOUNT_CATEGORY) {
		this.ACCOUNT_CATEGORY = ACCOUNT_CATEGORY;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String TYPE) {
		this.TYPE = TYPE;
	}

	public String getMSGTYPE() {
		return MSGTYPE;
	}

	public void setMSGTYPE(String MSGTYPE) {
		this.MSGTYPE = MSGTYPE;
	}

	public String getSNO() {
		return SNO;
	}

	public void setSNO(String SNO) {
		this.SNO = SNO;
	}

	public String getTRAN_REF_NO() {
		return TRAN_REF_NO;
	}

	public void setTRAN_REF_NO(String TRAN_REF_NO) {
		this.TRAN_REF_NO = TRAN_REF_NO;
	}

	public String getTRAN_CURR() {
		return TRAN_CURR;
	}

	public void setTRAN_CURR(String TRAN_CURR) {
		this.TRAN_CURR = TRAN_CURR;
	}

	public String getTRAN_AMT() {
		return TRAN_AMT;
	}

	public void setTRAN_AMT(String TRAN_AMT) {
		this.TRAN_AMT = TRAN_AMT;
	}

	public String getUSD_EQV_AMT() {
		return USD_EQV_AMT;
	}

	public void setUSD_EQV_AMT(String USD_EQV_AMT) {
		this.USD_EQV_AMT = USD_EQV_AMT;
	}

	public String getINR_AMOUNT() {
		return INR_AMOUNT;
	}

	public void setINR_AMOUNT(String INR_AMOUNT) {
		this.INR_AMOUNT = INR_AMOUNT;
	}

	public String getPURPOSE_CODE() {
		return PURPOSE_CODE;
	}

	public void setPURPOSE_CODE(String PURPOSE_CODE) {
		this.PURPOSE_CODE = PURPOSE_CODE;
	}

	public String getPURPOSE_DESC() {
		return PURPOSE_DESC;
	}

	public void setPURPOSE_DESC(String PURPOSE_DESC) {
		this.PURPOSE_DESC = PURPOSE_DESC;
	}

	public String getREM_CUST_ID() {
		return REM_CUST_ID;
	}

	public void setREM_CUST_ID(String REM_CUST_ID) {
		this.REM_CUST_ID = REM_CUST_ID;
	}

	public String getREM_NAME() {
		return REM_NAME;
	}

	public void setREM_NAME(String REM_NAME) {
		this.REM_NAME = REM_NAME;
	}

	public String getREM_ADD1() {
		return REM_ADD1;
	}

	public void setREM_ADD1(String REM_ADD1) {
		this.REM_ADD1 = REM_ADD1;
	}

	public String getREM_ADD2() {
		return REM_ADD2;
	}

	public void setREM_ADD2(String REM_ADD2) {
		this.REM_ADD2 = REM_ADD2;
	}

	public String getREM_ADD3() {
		return REM_ADD3;
	}

	public void setREM_ADD3(String REM_ADD3) {
		this.REM_ADD3 = REM_ADD3;
	}

	public String getREM_CITY() {
		return REM_CITY;
	}

	public void setREM_CITY(String REM_CITY) {
		this.REM_CITY = REM_CITY;
	}

	public String getREM_CNTRY_CODE() {
		return REM_CNTRY_CODE;
	}

	public void setREM_CNTRY_CODE(String REM_CNTRY_CODE) {
		this.REM_CNTRY_CODE = REM_CNTRY_CODE;
	}

	public String getBEN_CUST_ID() {
		return BEN_CUST_ID;
	}

	public void setBEN_CUST_ID(String BEN_CUST_ID) {
		this.BEN_CUST_ID = BEN_CUST_ID;
	}

	public String getBEN_NAME() {
		return BEN_NAME;
	}

	public void setBEN_NAME(String BEN_NAME) {
		this.BEN_NAME = BEN_NAME;
	}

	public String getBEN_ADD1() {
		return BEN_ADD1;
	}

	public void setBEN_ADD1(String BEN_ADD1) {
		this.BEN_ADD1 = BEN_ADD1;
	}

	public String getBEN_ADD2() {
		return BEN_ADD2;
	}

	public void setBEN_ADD2(String BEN_ADD2) {
		this.BEN_ADD2 = BEN_ADD2;
	}

	public String getBEN_ADD3() {
		return BEN_ADD3;
	}

	public void setBEN_ADD3(String BEN_ADD3) {
		this.BEN_ADD3 = BEN_ADD3;
	}

	public String getBEN_CITY() {
		return BEN_CITY;
	}

	public void setBEN_CITY(String BEN_CITY) {
		this.BEN_CITY = BEN_CITY;
	}

	public String getBEN_CNTRY_CODE() {
		return BEN_CNTRY_CODE;
	}

	public void setBEN_CNTRY_CODE(String BEN_CNTRY_CODE) {
		this.BEN_CNTRY_CODE = BEN_CNTRY_CODE;
	}

	public String getCLIENT_ACC_NO() {
		return CLIENT_ACC_NO;
	}

	public void setCLIENT_ACC_NO(String CLIENT_ACC_NO) {
		this.CLIENT_ACC_NO = CLIENT_ACC_NO;
	}

	public String getCPTY_AC_NO() {
		return CPTY_AC_NO;
	}

	public void setCPTY_AC_NO(String CPTY_AC_NO) {
		this.CPTY_AC_NO = CPTY_AC_NO;
	}

	public String getBEN_ACCT_NO() {
		return BEN_ACCT_NO;
	}

	public void setBEN_ACCT_NO(String BEN_ACCT_NO) {
		this.BEN_ACCT_NO = BEN_ACCT_NO;
	}

	public String getBEN_BIC() {
		return BEN_BIC;
	}

	public void setBEN_BIC(String BEN_BIC) {
		this.BEN_BIC = BEN_BIC;
	}

	public String getREM_ACCT_NO() {
		return REM_ACCT_NO;
	}

	public void setREM_ACCT_NO(String REM_ACCT_NO) {
		this.REM_ACCT_NO = REM_ACCT_NO;
	}

	public String getREM_BIC() {
		return REM_BIC;
	}

	public void setREM_BIC(String REM_BIC) {
		this.REM_BIC = REM_BIC;
	}

	public String getTRN_DATE() {
		return TRN_DATE;
	}

	public void setTRN_DATE(String TRN_DATE) {
		this.TRN_DATE = TRN_DATE;
	}

	@Override
	public String getTableName() {
		return tableName;
	}

}
