package clari5.custom.sib.builder;

import clari5.custom.sib.audit.AuditManager;
import clari5.custom.sib.config.BepCon;
import clari5.platform.util.CxRest;
import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;
import clari5.platform.util.ECClient;

public class Clari5GatewayManager {
    public static CxpsLogger logger = CxpsLogger.getLogger(Clari5GatewayManager.class);
    static {
        ECClient.configure(null);
    }

    public static boolean send(String event_id,JSONObject json, long event_ts) throws Exception {
        String entity_id = "sibbankEntity";
        boolean status= ECClient.enqueue("HOST",entity_id,event_id,json.toString());
        System.out.println("The event with event_id, " + event_id + " was sent to clari5 and clari5 status is, " + status);
        return status;
    }
}
