package clari5.custom.sib.builder.data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import clari5.custom.sib.db.DBTask;

public class Trigger {
	private String trigger;
	private String when;
	private String column;
	private List<String> keys;
	private List<String> where;
	private String value;

	public String getTrigger() {
		return this.trigger;
	}
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}
	public String getWhen() {
		return this.when;
	}
	public void setWhen(String when) {
		this.when = when;
	}
	public String getColumn() {
		return this.column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public List<String> getKeys() {
		return this.keys;
	}
	public void setKeys(List<String> keys) {
		this.keys = keys;
	}
	public List<String> getWhere() {
		return this.where;
	}
	public void setWhere(List<String> where) {
		this.where = where;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean process(JSONObject row, String eventName) throws SQLException {
		boolean shouldProceed = true;
		switch(this.getTrigger().split("\\.")[0]) {
		case "db": {
			shouldProceed = this.processFromDB(row, eventName);
			break;
		}
		case "Row": {
			shouldProceed = this.processFromRow(row, eventName);
			break;
		}
		}
		return shouldProceed;
	}
	private boolean processFromDB(JSONObject row, String eventName) throws SQLException {
		String destinationTable = this.getTrigger().split("\\.")[1];
		List<String> valuesOfKeys = new ArrayList<String>();
		boolean shouldProceed = true;
		for(String valueOfKey : this.getWhere()) {
			if(valueOfKey.split("\\.")[0].equals("Row")) {
				valuesOfKeys.add(row.getString(valueOfKey.split("\\.")[1]));
			}
		}
		Map<String, String> result = DBTask.selectAllColumns(destinationTable, this.getKeys().toArray(new String[0]), valuesOfKeys.toArray(new String[0]));
		switch(this.getWhen()) {
		case "exists": {
			if(!result.get(this.getColumn()).toLowerCase().equals(this.getValue().toLowerCase())) {
				shouldProceed = false;
			}
			break;
		}
		case "startswith": {
			if(!result.get(this.getColumn()).toLowerCase().startsWith(this.getValue().toLowerCase())) {
				shouldProceed = false;
			}
			break;
		}
		case "contains": {
			if(!result.get(this.getColumn()).toLowerCase().contains(this.getValue().toLowerCase())) {
				shouldProceed = false;
			}
			break;
		}
		}
		return shouldProceed;
	}
	private boolean processFromRow(JSONObject row, String eventName) throws SQLException {
		boolean shouldProceed = true;
		switch(this.getWhen()) {
		case "exists": {
			if(!row.getString(this.getColumn()).toLowerCase().equals(this.getValue().toLowerCase())) {
				shouldProceed = false;
			}
			break;
		}
		case "startswith": {
			if(!row.getString(this.getColumn()).toLowerCase().startsWith(this.getValue().toLowerCase())) {
				shouldProceed = false;
			}
			break;
		}
		case "contains": {
			if(!row.getString(this.getColumn()).toLowerCase().contains(this.getValue().toLowerCase())) {
				shouldProceed = false;
			}
			break;
		}
		}
		return shouldProceed;
	}
}