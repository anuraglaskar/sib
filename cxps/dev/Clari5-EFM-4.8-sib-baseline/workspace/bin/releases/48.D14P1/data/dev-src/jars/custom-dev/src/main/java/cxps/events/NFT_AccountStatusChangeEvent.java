// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
//import clari5.platform.logger.CXLog;
//import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_ACCOUNTSTATUSCHANG", Schema="rice")
public class NFT_AccountStatusChangeEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=200) public String branchIdDesc;
       @Field(size=200) public String accountId;
       @Field(size=11) public Double avlBal;
       @Field(size=200) public String userId;
       @Field public java.sql.Timestamp acctOpenDt;
       @Field(size=200) public String branchId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=200) public String custId;
       @Field(size=200) public String finalAcctStatus;
       @Field(size=200) public String initAcctStatus;


    @JsonIgnore
    public ITable<NFT_AccountStatusChangeEvent> t = AEF.getITable(this);

	public NFT_AccountStatusChangeEvent(){}

    public NFT_AccountStatusChangeEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("AccountStatusChange");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setBranchIdDesc(json.getString("branch_id_desc"));
            setAccountId(json.getString("account_id"));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setUserId(json.getString("user_id"));
            setAcctOpenDt(EventHelper.toTimestamp(json.getString("acct_open_dt")));
            setBranchId(json.getString("branch_id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setCustId(json.getString("cust_id"));
            setFinalAcctStatus(json.getString("final_acct_status"));
            setInitAcctStatus(json.getString("init_acct_status"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "NAS"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getAccountId(){ return accountId; }

    public Double getAvlBal(){ return avlBal; }

    public String getUserId(){ return userId; }

    public java.sql.Timestamp getAcctOpenDt(){ return acctOpenDt; }

    public String getBranchId(){ return branchId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public String getFinalAcctStatus(){ return finalAcctStatus; }

    public String getInitAcctStatus(){ return initAcctStatus; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setAcctOpenDt(java.sql.Timestamp val){ this.acctOpenDt = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setFinalAcctStatus(String val){ this.finalAcctStatus = val; }
    public void setInitAcctStatus(String val){ this.initAcctStatus = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getBranch_id_desc(){ return branchIdDesc; }
    @JsonIgnore
    public String getAccount_id(){ return accountId; }
    @JsonIgnore
    public Double getAvl_bal(){ return avlBal; }
    @JsonIgnore
    public String getUser_id(){ return userId; }
    @JsonIgnore
    public java.sql.Timestamp getAcct_open_dt(){ return acctOpenDt; }
    @JsonIgnore
    public String getBranch_id(){ return branchId; }
    @JsonIgnore
    public java.sql.Timestamp getSys_time(){ return sysTime; }
    @JsonIgnore
    public String getCust_id(){ return custId; }
    @JsonIgnore
    public String getFinal_acct_status(){ return finalAcctStatus; }
    @JsonIgnore
    public String getInit_acct_status(){ return initAcctStatus; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
       // ICXLog cxLog = CXLog.fenter("derive.NFT_AccountStatusChangeEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
        String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
        wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));

       // cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_AccountStatusChange");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "AccountStatusChange");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
