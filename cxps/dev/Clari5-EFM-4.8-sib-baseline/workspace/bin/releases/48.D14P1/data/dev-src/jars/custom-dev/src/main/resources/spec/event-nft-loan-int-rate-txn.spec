cxps.events.event.nft-loan-int-rate-txn{
table-name : EVENT_NFT_LOAN_INT_RATE_TXN
event-mnemonic : LI
workspaces : {
  ACCOUNT : "loan-acct-no"
}

event-attributes : {

loan-acct-no : { db : true, raw_name : LoanAcctNo, type : "string:200" }
loan-acct-int-rate : { db : true, raw_name : LoanAcct_Int_Rate, type : "number:20,4" }
deposit-acct-no : { db : true, raw_name : DepositAcctNo, type : "string:200" }
deposit-acct-int-rate : { db : true, raw_name : DepositAcct_Int_Rate, type : "number:20,4" }
cust-i-d : { db : true, raw_name : Cust_ID, type : "string:200" }
flag : { db : true, raw_name : Flag, type : "string:50" }
eventtype : { db : true, raw_name : eventtype, type : "string:50" }
eventsubtype : { db : true, raw_name : eventsubtype, type : "string:50" }
host-id  : { db : true, raw_name : host-id, type : "string:50" }
event-name : { db : true, raw_name : event-name, type : "string:50" }
eventts : { db : true, raw_name : eventts, type : "timestamp" }
sys-time : { db : true, raw_name : sys-time, type : "timestamp" }

}
}


