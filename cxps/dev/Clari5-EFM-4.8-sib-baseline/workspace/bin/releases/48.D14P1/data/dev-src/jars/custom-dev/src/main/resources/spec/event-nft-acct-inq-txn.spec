cxps.events.event.nft-acct-inq{
table-name : EVENT_NFT_ACCOUNTINQUIRY
  event-mnemonic: NA
  workspaces : {
    ACCOUNT : acct-id,
   # CUSTOMER :	cust-id,
    USER : user-id,
   # NONCUSTOMER: noncust-key
    }
  event-attributes : {
	menu-id: {db : true ,raw_name : menu_id ,type : "string:200", custom-getter:Menu_id}
	acct-id: {db : true ,raw_name : acct_id ,type : "string:200", custom-getter:Acct_id}
	user-id: {db : true ,raw_name : user_id ,type : "string:200", custom-getter:User_id}
	rm-code-flag: {db : true ,raw_name : RM_Code_flag ,type : "string:200", custom-getter:RM_Code_flag }
	avl-bal: {db : true ,raw_name : avl_bal ,type : "number:11,2", custom-getter:Avl_bal}
	cust-id: {db : true ,raw_name : cust_id ,type : "string:200", custom-getter:Cust_id }
	branch-id: {db : true ,raw_name : branchid ,type : "string:200"}
	branch-id-desc: {db : true ,raw_name : branchiddesc ,type : "string:200"}
	acct-status: {db : true ,raw_name : acct_status ,type : "string:200", custom-getter:Acct_status}
	sys-time: {db : true ,raw_name : sys_time ,type : timestamp, custom-getter:Sys_time}
	host-id: {db : true ,raw_name : host_id ,type : "string:200", custom-getter:Host_id}
	eventts: {db : true ,raw_name : eventts ,type : timestamp}
	cust-type: {db : true ,raw_name : cust_type ,type : "string:200", custom-getter:Cust_type}
	noncust-key: {db : true ,type : "string:200", derivation: """cxps.noesis.core.EventHelper.concat(user-id, acct-id)"""}
	}
}


