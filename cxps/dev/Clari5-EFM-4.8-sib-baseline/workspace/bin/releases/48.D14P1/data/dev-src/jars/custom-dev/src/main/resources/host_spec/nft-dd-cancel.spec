cxps.noesis.glossary.entity.nft-dd-cancel{
       db-name = NFT_DD_CANCEL
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = DD_NUM, column = DD_NUM, type =timestamp}
               { name = DD_ISSUE_DATE, column = DD_ISSUE_DATE, type = timestamp}
               { name = DD_ISSUE_AMT, column = BEN_ADD1, type = "number:20,4"}
		{ name = DD_CANCEL_AMT, column = DD_CANCEL_AMT, type = "number:20,4"}
	        { name = DD_ISSUE_ACCT, column = DD_ISSUE_ACCT, type = "string:50" }
               { name = DD_CANCEL_ACCT, column = DD_CANCEL_ACCT, type = "string:50" }
               { name = DD_ISSUE_CUSTID, column = DD_ISSUE_CUSTID, type = "string:200"}
               { name = DD_CANCEL_CUSTID, column = DD_CANCEL_CUSTID, type = "string:200" }
               { name = CL5_FLG, column = CL5_FLG, type = "string:10"}	
		{ name = ID, column = ID, type = "string:200",key=true}
               { name = LAST_MODIFIED_TIME, column = LAST_MODIFIED_TIME, type =timestamp }	
               ]
       }

