cxps.noesis.glossary.entity.nft-upb-acct-open-tbl{
       db-name = NFT_UBP_ACCT_OPEN_TBL
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
               { name = ACCTNO, column = ACCTNO, type = "string:100"}
               { name = ACCT_BRANCH_ID, column = ACCT_BRANCH_ID, type = "string:100"}
		{ name = CUST_ID, column = CUST_ID, type = "string:50" }
		{ name = CUST_SOL_ID, column = CUST_SOL_ID, type = "string:50" }
		{ name = CL5_FLG, column = CL5_FLG, type = "string:10"}
		 { name = VERSION_NO, column = VERSION_NO, type = "number:20,0"}
		{ name = LAST_MODIFIED_TIME, column = LAST_MODIFIED_TIME, type = timestamp}
		 { name = ID, column = ID, type = "string:200",key =true}	
             
               ]
       }

