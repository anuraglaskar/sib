package clari5.custom.dev.service.struct;

import clari5.platform.fileq.Clari5Payload;
import clari5.platform.util.CxJson;
import com.fasterxml.jackson.annotation.JsonIgnore;
import cxps.apex.shared.IWSEvent;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Shashank Devisetty
 * Organization: CustomerXPs
 * Created On: 10/04/2019
 * Reviewed By:
 */

public class DerivatorResult {
    private List<DerivatorObj> derivatorObjList;
    private List<Clari5Payload> clari5Payloads;
    private List<IWSEvent> eventList;

    public static class DerivatorObj {
        private String wsKey;
        private String eventId;
        private String eventName;

        public String getWsKey() {
            return wsKey;
        }

        public void setWsKey(String wsKey) {
            this.wsKey = wsKey;
        }

        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }
    }

    public List<IWSEvent> getEventList() {
        return eventList;
    }

    public void setEventList(List<IWSEvent> eventList) {
        this.eventList = eventList;
    }

    public List<DerivatorObj> getDerivatorObjList() {
        return derivatorObjList;
    }

    public void setDerivatorObjList(List<DerivatorObj> derivatorObjList) {
        this.derivatorObjList = derivatorObjList;
    }

    public List<Clari5Payload> getClari5Payloads() {
        return clari5Payloads;
    }

    public void setClari5Payloads(List<Clari5Payload> clari5Payloads) {
        this.clari5Payloads = clari5Payloads;
    }
}
