// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_DdCancelTxnEventMapper extends EventMapper<NFT_DdCancelTxnEvent> {

public NFT_DdCancelTxnEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_DdCancelTxnEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_DdCancelTxnEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_DdCancelTxnEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_DdCancelTxnEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_DdCancelTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_DdCancelTxnEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getDdNum());
            preparedStatement.setTimestamp(i++, obj.getDdCancelDate());
            preparedStatement.setDouble(i++, obj.getDdCancelAmt());
            preparedStatement.setTimestamp(i++, obj.getDdIssueDate());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getDdIssueCustid());
            preparedStatement.setString(i++, obj.getDdCancelAcct());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setString(i++, obj.getDdIssueAcct());
            preparedStatement.setDouble(i++, obj.getDdIssueAmt());
            preparedStatement.setString(i++, obj.getDdCancelCustid());
            preparedStatement.setString(i++, obj.getEventName());
            preparedStatement.setString(i++, obj.getEventsubtype());
            preparedStatement.setString(i++, obj.getEventtype());
            preparedStatement.setString(i++, obj.getHostId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_DD_CANCEL_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_DdCancelTxnEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_DdCancelTxnEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DD_CANCEL_TXN"));
        putList = new ArrayList<>();

        for (NFT_DdCancelTxnEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "DD_NUM",  obj.getDdNum());
            p = this.insert(p, "DD_CANCEL_DATE", String.valueOf(obj.getDdCancelDate()));
            p = this.insert(p, "DD_CANCEL_AMT", String.valueOf(obj.getDdCancelAmt()));
            p = this.insert(p, "DD_ISSUE_DATE", String.valueOf(obj.getDdIssueDate()));
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "DD_ISSUE_CUSTID",  obj.getDdIssueCustid());
            p = this.insert(p, "DD_CANCEL_ACCT",  obj.getDdCancelAcct());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "DD_ISSUE_ACCT",  obj.getDdIssueAcct());
            p = this.insert(p, "DD_ISSUE_AMT", String.valueOf(obj.getDdIssueAmt()));
            p = this.insert(p, "DD_CANCEL_CUSTID",  obj.getDdCancelCustid());
            p = this.insert(p, "EVENT_NAME",  obj.getEventName());
            p = this.insert(p, "EVENTSUBTYPE",  obj.getEventsubtype());
            p = this.insert(p, "EVENTTYPE",  obj.getEventtype());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_DD_CANCEL_TXN"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_DD_CANCEL_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_DD_CANCEL_TXN]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_DdCancelTxnEvent obj = new NFT_DdCancelTxnEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setDdNum(rs.getString("DD_NUM"));
    obj.setDdCancelDate(rs.getTimestamp("DD_CANCEL_DATE"));
    obj.setDdCancelAmt(rs.getDouble("DD_CANCEL_AMT"));
    obj.setDdIssueDate(rs.getTimestamp("DD_ISSUE_DATE"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setDdIssueCustid(rs.getString("DD_ISSUE_CUSTID"));
    obj.setDdCancelAcct(rs.getString("DD_CANCEL_ACCT"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setDdIssueAcct(rs.getString("DD_ISSUE_ACCT"));
    obj.setDdIssueAmt(rs.getDouble("DD_ISSUE_AMT"));
    obj.setDdCancelCustid(rs.getString("DD_CANCEL_CUSTID"));
    obj.setEventName(rs.getString("EVENT_NAME"));
    obj.setEventsubtype(rs.getString("EVENTSUBTYPE"));
    obj.setEventtype(rs.getString("EVENTTYPE"));
    obj.setHostId(rs.getString("HOST_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_DD_CANCEL_TXN]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_DdCancelTxnEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_DdCancelTxnEvent> events;
 NFT_DdCancelTxnEvent obj = new NFT_DdCancelTxnEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_DD_CANCEL_TXN"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_DdCancelTxnEvent obj = new NFT_DdCancelTxnEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setDdNum(getColumnValue(rs, "DD_NUM"));
            obj.setDdCancelDate(EventHelper.toTimestamp(getColumnValue(rs, "DD_CANCEL_DATE")));
            obj.setDdCancelAmt(EventHelper.toDouble(getColumnValue(rs, "DD_CANCEL_AMT")));
            obj.setDdIssueDate(EventHelper.toTimestamp(getColumnValue(rs, "DD_ISSUE_DATE")));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setDdIssueCustid(getColumnValue(rs, "DD_ISSUE_CUSTID"));
            obj.setDdCancelAcct(getColumnValue(rs, "DD_CANCEL_ACCT"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setDdIssueAcct(getColumnValue(rs, "DD_ISSUE_ACCT"));
            obj.setDdIssueAmt(EventHelper.toDouble(getColumnValue(rs, "DD_ISSUE_AMT")));
            obj.setDdCancelCustid(getColumnValue(rs, "DD_CANCEL_CUSTID"));
            obj.setEventName(getColumnValue(rs, "EVENT_NAME"));
            obj.setEventsubtype(getColumnValue(rs, "EVENTSUBTYPE"));
            obj.setEventtype(getColumnValue(rs, "EVENTTYPE"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DD_CANCEL_TXN]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_DD_CANCEL_TXN]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"DD_NUM\",\"DD_CANCEL_DATE\",\"DD_CANCEL_AMT\",\"DD_ISSUE_DATE\",\"SYS_TIME\",\"DD_ISSUE_CUSTID\",\"DD_CANCEL_ACCT\",\"EVENTTS\",\"DD_ISSUE_ACCT\",\"DD_ISSUE_AMT\",\"DD_CANCEL_CUSTID\",\"EVENT_NAME\",\"EVENTSUBTYPE\",\"EVENTTYPE\",\"HOST_ID\"" +
              " FROM EVENT_NFT_DD_CANCEL_TXN";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [DD_NUM],[DD_CANCEL_DATE],[DD_CANCEL_AMT],[DD_ISSUE_DATE],[SYS_TIME],[DD_ISSUE_CUSTID],[DD_CANCEL_ACCT],[EVENTTS],[DD_ISSUE_ACCT],[DD_ISSUE_AMT],[DD_CANCEL_CUSTID],[EVENT_NAME],[EVENTSUBTYPE],[EVENTTYPE],[HOST_ID]" +
              " FROM EVENT_NFT_DD_CANCEL_TXN";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`DD_NUM`,`DD_CANCEL_DATE`,`DD_CANCEL_AMT`,`DD_ISSUE_DATE`,`SYS_TIME`,`DD_ISSUE_CUSTID`,`DD_CANCEL_ACCT`,`EVENTTS`,`DD_ISSUE_ACCT`,`DD_ISSUE_AMT`,`DD_CANCEL_CUSTID`,`EVENT_NAME`,`EVENTSUBTYPE`,`EVENTTYPE`,`HOST_ID`" +
              " FROM EVENT_NFT_DD_CANCEL_TXN";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_DD_CANCEL_TXN (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"DD_NUM\",\"DD_CANCEL_DATE\",\"DD_CANCEL_AMT\",\"DD_ISSUE_DATE\",\"SYS_TIME\",\"DD_ISSUE_CUSTID\",\"DD_CANCEL_ACCT\",\"EVENTTS\",\"DD_ISSUE_ACCT\",\"DD_ISSUE_AMT\",\"DD_CANCEL_CUSTID\",\"EVENT_NAME\",\"EVENTSUBTYPE\",\"EVENTTYPE\",\"HOST_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[DD_NUM],[DD_CANCEL_DATE],[DD_CANCEL_AMT],[DD_ISSUE_DATE],[SYS_TIME],[DD_ISSUE_CUSTID],[DD_CANCEL_ACCT],[EVENTTS],[DD_ISSUE_ACCT],[DD_ISSUE_AMT],[DD_CANCEL_CUSTID],[EVENT_NAME],[EVENTSUBTYPE],[EVENTTYPE],[HOST_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`DD_NUM`,`DD_CANCEL_DATE`,`DD_CANCEL_AMT`,`DD_ISSUE_DATE`,`SYS_TIME`,`DD_ISSUE_CUSTID`,`DD_CANCEL_ACCT`,`EVENTTS`,`DD_ISSUE_ACCT`,`DD_ISSUE_AMT`,`DD_CANCEL_CUSTID`,`EVENT_NAME`,`EVENTSUBTYPE`,`EVENTTYPE`,`HOST_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

