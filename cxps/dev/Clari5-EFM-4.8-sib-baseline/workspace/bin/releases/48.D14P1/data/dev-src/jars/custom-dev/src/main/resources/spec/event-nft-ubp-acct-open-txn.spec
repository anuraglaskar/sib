cxps.events.event.nft-ubp-acct-open-txn{
table-name : EVENT_NFT_UBP_ACCT_OPEN_TXN
event-mnemonic : UB
workspaces : {
  ACCOUNT : "acct-no"
}

event-attributes : {

acct-no : { db : true, raw_name : AcctNo, type : "string:50" }
acct-branch-id : { db : true, raw_name : Acct_Branch_id, type : "string:50" }
cust-id : { db : true, raw_name : Cust_Id, type : "string:50" }
cust-sol-id : { db : true, raw_name : Cust_Sol_Id, type : "string:50" }
eventtype : { db : true, raw_name : eventtype, type : "string:50" }
eventsubtype : { db : true, raw_name : eventsubtype, type : "string:50" }
host-id  : { db : true, raw_name : host-id, type : "string:50" }
event-name : { db : true, raw_name : event-name, type : "string:50" }
eventts : { db : true, raw_name : eventts, type : "timestamp" }
sys-time : { db : true, raw_name : sys-time, type : "timestamp" }

}
}
