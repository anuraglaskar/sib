package clari5.custom.sib.audit;

public interface IAuditClient {
	void log(String message);
	void log(LogLevel level, String message);
}
