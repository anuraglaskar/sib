package clari5.custom.dev;

import clari5.aml.cms.CmsException;
import clari5.aml.cms.newjira.*;
import clari5.platform.applayer.Clari5;
import clari5.platform.util.CxJson;
import clari5.platform.util.Hocon;
import cxps.apex.utils.CxpsLogger;
import cxps.eoi.Incident;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jabert on 21/11/18.
 */
public class CustomCmsFormatter extends DefaultCmsFormatter {

    protected static Map<String, Map<String, Integer>> groupUserCasesMap;
    protected static ConcurrentHashMap<String, Integer> conCurrFactUsrMap = new ConcurrentHashMap<>();
    protected static HashMap<String,String> regSolmap = new HashMap<>();
    protected static HashMap<String,String> regNamemap = new HashMap<>();
    protected static ConcurrentHashMap<String, String> fctUsrmap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String,String> regUsrmap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String,String>  hoUsrmap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrRegUsrMap = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Integer> conCurrHoUsrMap = new ConcurrentHashMap<>();
    public static  HashMap<String,String>  factCodemap = new HashMap<>();
    public static  ConcurrentHashMap<String,String> checkermap = new ConcurrentHashMap<>();
    public  static  HashMap<String,String> loadmap = new HashMap<>();
    public static CxpsLogger cxpsLogger = CxpsLogger.getLogger(CustomCmsFormatter.class);



    public CustomCmsFormatter() throws CmsException {
        cmsJira = (CmsJira) Clari5.getResource("newcmsjira");
    }

    public void processUserMap() {
        if (fctUsrmap.size() == 0) {
            fctUsrmap = UserMap.getFctUserMap();
            cxpsLogger.info("Usermap Loaded");
        }
    }
    public  void  processRegionSolMap(){
         if(regSolmap.size() == 0){
              regSolmap = UserMap.getSolRegionMap();
              cxpsLogger.info("SOL Region Map Loaded");
             }
    }
    public  void processRegionUserMap(){
        if(regUsrmap.size() == 0){
            regUsrmap = UserMap.getRegionUser();
            cxpsLogger.info("Region User Map loaded");
        }
    }
    public  void processHoUserMap(){
        if(hoUsrmap.size() == 0){
            hoUsrmap = UserMap.getHoRegionUser();
            cxpsLogger.info("Ho Region Map Loaded");
        }
    }
    public  void processFactCode(){
        if( factCodemap.size() == 0){
            factCodemap = UserMap.getFactCode();
            cxpsLogger.info("Fact Rule is Loaded");
        }
    }
    public  void processRegionName(){
        if(regNamemap.size() == 0){
            regNamemap = UserMap.getRegionName();
            cxpsLogger.info("Region Code - Region Name Map Loaded");
            try {
               Thread thread = new Thread(new RegionLoader());
               thread.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    public void processCheckerMap(){

        if(checkermap.size() == 0){
            checkermap= UserMap.getMakerCheker();
            cxpsLogger.info("Region Code - HO_CHECKER Map Loaded");
        }
    }

    @Override
    public String getCaseAssignee(Incident caseEvent) throws CmsException {
        cxpsLogger.info("Into CUSTOM - Case Assignee");
        if (cmsJira == null) {
            logger.fatal("Unable to get resource cmsjira");
            throw new CmsException("Unable to get resource cmsjira");
        }
        return "cxpsaml";
    }


    @Override
    public String getIncidentAssignee(Incident caseEvent) throws CmsException {

        try {
            cxpsLogger.info("Into CUSTOM - Incident Assignee");
            if (cmsJira == null) {
                logger.fatal("Unable to get resource cmsjira");
                throw new CmsException("Unable to get resource cmsjira");
            }

            String user = null;
            String arr[] = null;
            String factkey = null;
            String rule = null;
            String branch = null;
            String regionCode = null;
            String inciUser = null;
            String entityId = caseEvent.entityId;
            String eventId = caseEvent.eventId;
            cxpsLogger.info("Trigger ID is [ " + caseEvent.triggerId + " ] ");
            cxpsLogger.info("Entity id inside getIncidentAssignee is: [ " + entityId + " ] ");
            cxpsLogger.info("Event Id : [ " + caseEvent.eventId + " ] ");
            CmsUserScenariomap userScenariomap = new CmsUserScenariomap();
            String factName = caseEvent.factName;
            processUserMap();
            processRegionSolMap();
            processRegionUserMap();
            processHoUserMap();
            processFactCode();
            processRegionName();
            processCheckerMap();


            rule = factCodemap.get(factName);
            if (rule != null) {
                switch (rule) {

                    case "RULE1":
                        //if( factName.equalsIgnoreCase("S_301")||factName.equalsIgnoreCase("S02")||factName.equalsIgnoreCase("S04")||factName.equalsIgnoreCase("S07")||factName.equalsIgnoreCase("S300")){
                        cxpsLogger.info("Into HO user assigning based on Tran SOL");
                        branch = userScenariomap.getTranSOL(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE hence assigning default CXPS");
                            return "cxpsaml";
                        }
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("REGION CODE : [ "+regionCode+ " ] ");
                        inciUser = getUserSyncHo(regionCode);
                        return inciUser;
                        /*conCurrHoUsrMap = UserMap.getUser(hoUsrmap, regionCode, conCurrHoUsrMap);
                            if (conCurrHoUsrMap.size() > 0 && hoUsrmap.size() > 0) {
                                if (hoUsrmap.get(regionCode).contains(",")) {
                                    arr = hoUsrmap.get(regionCode).split(",");
                                    user = arr[conCurrHoUsrMap.get(regionCode)];
                                    cxpsLogger.info("User  on list for HO user mapping is" + user);
                                    return user;
                                } else {
                                    cxpsLogger.info("Single user  [ " + regUsrmap.get(regionCode) + " ] ");
                                    return regUsrmap.get(regionCode);
                                }
                            } else {
                                cxpsLogger.info("Can't able to get User hence assigning default CXPS");
                                return "cxpsaml";
                            }*/



                    case "RULE2":
                        //if(factName.equalsIgnoreCase("S01")||factName.equalsIgnoreCase("S08")||factName.equalsIgnoreCase("S09")||factName.equalsIgnoreCase("S11")||factName.equalsIgnoreCase("S12")||factName.equalsIgnoreCase("S13")||factName.equalsIgnoreCase("S14")||factName.equalsIgnoreCase("S15")||factName.equalsIgnoreCase("S05")){
                        cxpsLogger.info("Into RO user assiging based on Transaction SOL");
                        branch = userScenariomap.getTranSOL(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE hence assigning default CXPS");
                            return "cxpsaml";
                        }
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("REGION CODE : [ "+regionCode+ " ] ");
                        inciUser = getUserSync(regionCode);
                        return inciUser;
                       /* conCurrRegUsrMap = UserMap.getUser(regUsrmap, regionCode, conCurrRegUsrMap);
                        if (conCurrRegUsrMap.size() > 0 && regUsrmap.size() > 0) {
                            if (regUsrmap.get(regionCode).contains(",")) {
                                arr = regUsrmap.get(regionCode).split(",");
                                user = arr[conCurrRegUsrMap.get(regionCode)];
                                cxpsLogger.info("User  on list for RO user mapping is [ " + user + " ] ");
                                return user;
                            } else {
                                return regUsrmap.get(regionCode);
                            }
                        } else {
                            cxpsLogger.info("Can't able to get User hence assigning default CXPS");
                            return "cxpsaml";
                        }*/


                    case "RULE3":
                        //if(factName.equalsIgnoreCase("S03")||factName.equalsIgnoreCase("S06")||factName.equalsIgnoreCase("S10")||factName.equalsIgnoreCase("S301")) {
                        cxpsLogger.info("Into RO user assiging based on Account SOL");
                        branch = userScenariomap.getAcctSOL(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getAcctSOL(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getAcctSOL(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE hence assigning default CXPS");
                            return "cxpsaml";
                        }
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("REGION CODE : [ "+regionCode+ " ] ");
                        inciUser = getUserSync(regionCode);
                        return inciUser;
                       /* conCurrRegUsrMap = UserMap.getUser(regUsrmap, regionCode, conCurrRegUsrMap);
                        if (conCurrRegUsrMap.size() > 0 && regUsrmap.size() > 0) {
                            if (regUsrmap.get(regionCode).contains(",")) {
                                arr = regUsrmap.get(regionCode).split(",");
                                user = arr[conCurrRegUsrMap.get(regionCode)];
                                cxpsLogger.info("User  on list for RO user mapping is [ " + user + " ] ");
                                return user;
                            } else {
                                return regUsrmap.get(regionCode);
                            }
                        } else {
                            cxpsLogger.info("Can't able to get User hence assigning default CXPS");
                            return "cxpsaml";
                        }*/

                    case "RULE4":
                        //if(factName.equalsIgnoreCase("S20")){
                        cxpsLogger.info("Into RO user assiging based on Account SOL for SCN 20");
                        branch = userScenariomap.getDDUserbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getDDUserbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getDDUserbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE / DDA hence assigning default CXPS");
                            return "cxpsaml";
                        }
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("REGION CODE : [ "+regionCode+ " ] ");
                        inciUser = getUserSync(regionCode);
                        return inciUser;
                        /*conCurrRegUsrMap = UserMap.getUser(regUsrmap, regionCode, conCurrRegUsrMap);
                        if (conCurrRegUsrMap.size() > 0 && regUsrmap.size() > 0) {
                            if (regUsrmap.get(regionCode).contains(",")) {
                                arr = regUsrmap.get(regionCode).split(",");
                                user = arr[conCurrRegUsrMap.get(regionCode)];
                                cxpsLogger.info("User  on list for RO user mapping is [ " + user + " ] ");
                                return user;
                            } else {
                                return regUsrmap.get(regionCode);
                            }
                        } else {
                            cxpsLogger.info("Can't able to get User hence assigning default CXPS");
                            return "cxpsaml";
                        }*/


                    case "RULE5":
                        //if(factName.equalsIgnoreCase("S16")||factName.equalsIgnoreCase("S17")){
                        cxpsLogger.info("Into RO user assiging based on Account SOL for SCN 16 & 17");
                        branch = userScenariomap.getcollateralbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getcollateralbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getcollateralbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE / DDA hence assigning default CXPS");
                            return "cxpsaml";
                        }
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("REGION CODE : [ "+regionCode+ " ] ");
                        inciUser = getUserSync(regionCode);
                        return inciUser;
                        /*conCurrRegUsrMap = UserMap.getUser(regUsrmap, regionCode, conCurrRegUsrMap);
                        if (conCurrRegUsrMap.size() > 0 && regUsrmap.size() > 0) {
                            if (regUsrmap.get(regionCode).contains(",")) {
                                arr = regUsrmap.get(regionCode).split(",");
                                user = arr[conCurrRegUsrMap.get(regionCode)];
                                cxpsLogger.info("User  on list for RO user mapping is [ " + user + " ] ");
                                return user;
                            } else {
                                return regUsrmap.get(regionCode);
                            }
                        } else {
                            cxpsLogger.info("Can't able to get User hence assigning default CXPS");
                            return "cxpsaml";
                        }*/

                    case "RULE6":
                        //if(factName.equalsIgnoreCase("S18")){
                        cxpsLogger.info("Into RO user assiging based on Account SOL for SCN 18");
                        branch = userScenariomap.getLoanbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getLoanbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getLoanbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE / DDA hence assigning default CXPS");
                            return "cxpsaml";
                        }
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("REGION CODE : [ "+regionCode+ " ] ");
                        inciUser = getUserSync(regionCode);
                        return inciUser;
                        /*conCurrRegUsrMap = UserMap.getUser(regUsrmap, regionCode, conCurrRegUsrMap);
                        if (conCurrRegUsrMap.size() > 0 && regUsrmap.size() > 0) {
                            if (regUsrmap.get(regionCode).contains(",")) {
                                arr = regUsrmap.get(regionCode).split(",");
                                user = arr[conCurrRegUsrMap.get(regionCode)];
                                cxpsLogger.info("User  on list for RO user mapping is [ " + user + " ] ");
                                return user;
                            } else {
                                return regUsrmap.get(regionCode);
                            }
                        } else {
                            cxpsLogger.info("Can't able to get User hence assigning default CXPS");
                            return "cxpsaml";
                        }*/


                    case "RULE7":
                        //if(factName.equalsIgnoreCase("S19")) {
                        cxpsLogger.info("Into RO user assiging based on Account SOL for SCN 19");
                        branch = userScenariomap.getUBPbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getUBPbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getUBPbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE / DDA hence assigning default CXPS");
                            return "cxpsaml";
                        }
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("REGION CODE : [ "+regionCode+ " ] ");
                        inciUser = getUserSync(regionCode);
                        return inciUser;
                       /* conCurrRegUsrMap = UserMap.getUser(regUsrmap, regionCode, conCurrRegUsrMap);
                        if (conCurrRegUsrMap.size() > 0 && regUsrmap.size() > 0) {
                            if (regUsrmap.get(regionCode).contains(",")) {
                                arr = regUsrmap.get(regionCode).split(",");
                                user = arr[conCurrRegUsrMap.get(regionCode)];
                                cxpsLogger.info("User  on list for RO user mapping is [ " + user + " ] ");
                                return user;
                            } else {
                                return regUsrmap.get(regionCode);
                            }
                        } else {
                            cxpsLogger.info("Can't able to get User hence assigning default CXPS");
                            return "cxpsaml";
                        }*/


                    case "RULE8":
                        if (fctUsrmap != null) {
                            for (String a : fctUsrmap.keySet()) {
                                if (a == factName) ;
                                {
                                    factkey = a;
                                    cxpsLogger.info("Fact User Mapping present");
                                }
                            }
                        }
                        cxpsLogger.info("FACT KEY : [ "+factkey+ " ] ");
                        inciUser = getFactUserSync(factkey);
                        return inciUser;

                      /*conCurrFactUsrMap = UserMap.getUser(fctUsrmap, factName, conCurrFactUsrMap);
                       if (conCurrFactUsrMap.size() != 0 && fctUsrmap != null && fctUsrmap.size() > 0 && factkey != null) {
                            if (fctUsrmap.get(factName).contains(",")) {
                                arr = fctUsrmap.get(factName).split(",");
                                user = arr[conCurrFactUsrMap.get(factName)];
                                cxpsLogger.info("User  on list for fact scenario mapping is" + user);
                                return user;
                            } else {
                                user = fctUsrmap.get(factName);
                                cxpsLogger.info("single user scenario mapping is" + user);
                                return user;
                            }
                        }*/


                    default:
                        System.out.println("UNABLE TO GET RULE [ " + rule + " ] for factname  [ " + factName + " ] ");
                        break;

             /* OLD CMS LOGIC SIB
             if (entityId.startsWith("A_F_") || entityId.startsWith("C_F_") || entityId.startsWith("R_F_")) {

                   cxpsLogger.info("Entity ID" + entityId);
                   String branchuser = userScenariomap.getBranchUser(entityId, caseEvent.getEventId(),caseEvent.getFactname());
                   if (branchuser == null) {
                       return "cxpsaml";
                   }
                   return branchuser;

               } else if (entityId.startsWith("T_F_") || entityId.startsWith("U_F_")) {
                   String tranuser = userScenariomap.getTranuser(caseEvent.getEventId());
                   if (tranuser == null) {
                       return "cxpsaml";
                   }
                   return tranuser;
               } else if (entityId.startsWith("N_F_") && caseEvent.getFactname().equalsIgnoreCase("S08")) {
                   String ncuser = userScenariomap.getTranuser(caseEvent.getEventId());
                   if (ncuser == null) {
                       return "cxpsaml";
                   }
                   return ncuser;
               } else if (entityId.startsWith("N_F_") && caseEvent.getFactname().equalsIgnoreCase("S17")) {
                   String ncusercol = userScenariomap.getcollateralUser(caseEvent.getEventId(),caseEvent.getFactname());
                   if (ncusercol == null) {
                       return "cxpsaml";
                   }
                   return ncusercol;
               } else if (entityId.startsWith("N_F_") && caseEvent.getFactname().equalsIgnoreCase("S20")) {
                   String ncusercol = userScenariomap.getDDUser(caseEvent.getEventId(),caseEvent.getFactname());
                   if (ncusercol == null) {
                       return "cxpsaml";
                   }
                   return ncusercol;
               } else {

                       return "cxpsaml";
               }*/
                }
            }
            cxpsLogger.info("UNABLE TO GET USER HENCE ASSIGNING AS DEFAULT CXPS");
            return "cxpsaml";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            cxpsLogger.info("Exception while Creating Incident "+e);
        }
        return null;

    }


    public CxJson populateIncidentJson(Incident event, String jiraParentId, boolean isupdate) throws CmsException, IOException {
        CustomCmsModule cmsMod = CustomCmsModule.getInstance(event.moduleId);
        CxJson json = CMSUtility.getInstance(event.moduleId).populateIncidentJson(event, jiraParentId, isupdate);
        
        int score = Integer.parseInt(event.incidentScore);
        cxpsLogger.info("Score as Integer [ " +score+ " ] ");
        CxJson wrapper = json.get("fields");

       cxpsLogger.info("Json : "+wrapper);


        if(isupdate){
            cxpsLogger.info("replacing score during update");
            json.remove(loadmap.get("score"));
            json.put(loadmap.get("score"),score);
        }
        else {
            json.get("fields").remove(loadmap.get("score"));
            json.get("fields").put(loadmap.get("score"), score);
        }
         cxpsLogger.info("Score as Integer on  JSON [ "+json+ " ] ");


        if(!isupdate) {
            cxpsLogger.info("Into custom Populate incident JSON  during creation of Alert");
            // String summaryId = cmsMod.getCustomFields().get("CmsIncident-message").getJiraId();

            String factname = event.factName;
            System.out.println("factname : [ "+factname+ " ]");
            String eventId = event.eventId;
            String rule = null;
            String branch = null;
            String regionCode = null;
            String regionName = null;



            String assigneefrmevent = event.assignee;
            if (assigneefrmevent != null) {
                JSONObject assignejson = new JSONObject();
                assignejson.put("name", assigneefrmevent);
                json.get("fields").remove("assignee");
                json.get("fields").put("assignee", assignejson);
                System.out.println("Changed Assigne since assignee is wrong due to wrong triggered event ID  :[ " + json + " ]");
            }



            cxpsLogger.info("factname  : [ " + factname + " ]");
            CxJson dropdownFieldJson = new CxJson();

            
              if (loadmap.size()==0){
                  loadconf();
                  cxpsLogger.info("Loading custom fields form Conf ");
              }


              String maker  = wrapper.get("assignee").get("name").toString();
              String checker = checkermap.get(maker);

            dropdownFieldJson.put("value", factname);
            wrapper.put(loadmap.get("factname"), dropdownFieldJson);
            wrapper.put(loadmap.get("maker"), maker);
            wrapper.put(loadmap.get("checker") ,checker);
            json.put("fields",wrapper);
            cxpsLogger.info("Maker-Checker JSON===============>"+json);



            CmsUserScenariomap userScenariomap = new CmsUserScenariomap();
            CxJson branchValue = new CxJson();
            CxJson regionCodeValue = new CxJson();
            CxJson regionNameValue = new CxJson();

            rule = factCodemap.get(factname);
            cxpsLogger.info("RULE : [ " +rule+ " ]");
            if (rule != null ) {
                switch (rule) {

                    case "RULE1":
                        cxpsLogger.info("Getting branch & Region code based on Tran SOL");
                        branch = userScenariomap.getTranSOL(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE ");
                            branchValue.put("value", "Unidentified");
                            regionCodeValue.put("value", "Unidentified");
                            regionNameValue.put("value", "Unidentified");
                            wrapper.put(loadmap.get("branchValue"), branchValue);
                            wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                            wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                            return json.put("fields", wrapper);

                        }
                        cxpsLogger.info("Branch RULE1: [ " + branch + " ] ");
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("Region Code : [ " + regionCode + " ] ");
                        regionName = regNamemap.get(regionCode);
                        cxpsLogger.info("Region Name : [ " + regionName + " ] ");
                        branchValue.put("value", branch);
                        regionCodeValue.put("value", regionCode);
                        regionNameValue.put("value", regionName);
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);


                    case "RULE2":
                        cxpsLogger.info("Getting branch & Region code based on Tran SOL");
                        branch = userScenariomap.getTranSOL(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getTranSOL(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE ");
                            branchValue.put("value", "Unidentified");
                            regionCodeValue.put("value", "Unidentified");
                            regionNameValue.put("value", "Unidentified");
                            wrapper.put(loadmap.get("branchValue"), branchValue);
                            wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                            wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                            return json.put("fields", wrapper);

                        }
                        cxpsLogger.info("Branch RULE2: [ " + branch + " ] ");
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("Region Code : [ " + regionCode + " ] ");
                        regionName = regNamemap.get(regionCode);
                        cxpsLogger.info("Region Name : [ " + regionName + " ] ");
                        branchValue.put("value", branch);
                        regionCodeValue.put("value", regionCode);
                        regionNameValue.put("value", regionName);
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);


                    case "RULE3":
                        cxpsLogger.info("Getting branch & Region code based on Account SOL");
                        branch = userScenariomap.getAcctSOL(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getAcctSOL(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getAcctSOL(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE ");
                            branchValue.put("value", "Unidentified");
                            regionCodeValue.put("value", "Unidentified");
                            regionNameValue.put("value", "Unidentified");
                            wrapper.put(loadmap.get("branchValue"), branchValue);
                            wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                            wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                            return json.put("fields", wrapper);
                        }
                        cxpsLogger.info("Branch RULE3: [ " + branch + " ] ");
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("Region Code : [ " + regionCode + " ] ");
                        regionName = regNamemap.get(regionCode);
                        cxpsLogger.info("Region Name : [ " + regionName + " ] ");
                        branchValue.put("value", branch);
                        regionCodeValue.put("value", regionCode);
                        regionNameValue.put("value", regionName);
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);


                    case "RULE4":
                        cxpsLogger.info("Getting branch & Region code on Account SOL for SCN 20");
                        branch = userScenariomap.getDDUserbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getDDUserbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getDDUserbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE ");
                            branchValue.put("value", "Unidentified");
                            regionCodeValue.put("value", "Unidentified");
                            regionNameValue.put("value", "Unidentified");
                            wrapper.put(loadmap.get("branchValue"), branchValue);
                            wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                            wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                            return json.put("fields", wrapper);

                        }
                        cxpsLogger.info("Branch RULE4 : [ " + branch + " ] ");
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("Region Code : [ " + regionCode + " ] ");
                        regionName = regNamemap.get(regionCode);
                        cxpsLogger.info("Region Name : [ " + regionName + " ] ");
                        branchValue.put("value", branch);
                        regionCodeValue.put("value", regionCode);
                        regionNameValue.put("value", regionName);
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);

                    case "RULE5":
                        cxpsLogger.info("Getting branch & Region based on Account SOL for SCN 16 or 17");
                        branch = userScenariomap.getcollateralbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getcollateralbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getcollateralbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE ");
                            branchValue.put("value", "Unidentified");
                            regionCodeValue.put("value", "Unidentified");
                            regionNameValue.put("value", "Unidentified");
                            wrapper.put(loadmap.get("branchValue"), branchValue);
                            wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                            wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                            return json.put("fields", wrapper);

                        }
                        cxpsLogger.info("Branch RULE5: [ " + branch + " ] ");
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("Region Code : [ " + regionCode + " ] ");
                        regionName = regNamemap.get(regionCode);
                        cxpsLogger.info("Region Name : [ " + regionName + " ] ");
                        branchValue.put("value", branch);
                        regionCodeValue.put("value", regionCode);
                        regionNameValue.put("value", regionName);
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);


                    case "RULE6":
                        cxpsLogger.info("Getting branch & Region based on Account SOL for SCN 18");
                        branch = userScenariomap.getLoanbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getLoanbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getLoanbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE ");
                            branchValue.put("value", "Unidentified");
                            regionCodeValue.put("value", "Unidentified");
                            regionNameValue.put("value", "Unidentified");
                            wrapper.put(loadmap.get("branchValue"), branchValue);
                            wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                            wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                            return json.put("fields", wrapper);

                        }
                        cxpsLogger.info("Branch RULE6 : [ " + branch + " ] ");
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("Region Code : [ " + regionCode + " ] ");
                        regionName = regNamemap.get(regionCode);
                        cxpsLogger.info("Region Name : [ " + regionName + " ] ");
                        branchValue.put("value", branch);
                        regionCodeValue.put("value", regionCode);
                        regionNameValue.put("value", regionName);
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);


                    case "RULE7":
                        cxpsLogger.info("Getting branch & Region based on Account SOL for SCN 19");
                        branch = userScenariomap.getUBPbranch(eventId);
                        if (branch == null) {
                            branch = userScenariomap.getUBPbranch(eventId);
                        }
                        if (branch == null) {
                            branch = userScenariomap.getUBPbranch(eventId);
                        }
                        if (branch == null) {
                            cxpsLogger.info("UNABLE to get BRANCH from EVENT TABLE ");
                            branchValue.put("value", "Unidentified");
                            regionCodeValue.put("value", "Unidentified");
                            regionNameValue.put("value", "Unidentified");
                            wrapper.put(loadmap.get("branchValue"), branchValue);
                            wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                            wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                            return json.put("fields", wrapper);

                        }
                        cxpsLogger.info("Branch RULE7 : [ " + branch + " ] ");
                        regionCode = regSolmap.get(branch);
                        cxpsLogger.info("Region Code : [ " + regionCode + " ] ");
                        regionName = regNamemap.get(regionCode);
                        cxpsLogger.info("Region Name : [ " + regionName + " ] ");
                        branchValue.put("value", branch);
                        regionCodeValue.put("value", regionCode);
                        regionNameValue.put("value", regionName);
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);


                    case "RULE8":

                        cxpsLogger.info("RULE8 hence No Branch");
                        branchValue.put("value", "Unidentified");
                        regionCodeValue.put("value", "Unidentified");
                        regionNameValue.put("value", "Unidentified");
                        wrapper.put(loadmap.get("branchValue"), branchValue);
                        wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                        wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                        return json.put("fields", wrapper);


                    default:
                        System.out.println("UNABLE TO GET RULE [ " + rule + " ] for factname  [ " + factname + " ] ");
                        break;

                }
            }
            else {
                cxpsLogger.info("RULE NULL  hence No Branch ");
                branchValue.put("value", "Unidentified");
                regionCodeValue.put("value", "Unidentified");
                regionNameValue.put("value", "Unidentified");
                wrapper.put(loadmap.get("branchValue"), branchValue);
                wrapper.put(loadmap.get("regionCodeValue"), regionCodeValue);
                wrapper.put(loadmap.get("regionNameValue"), regionNameValue);
                return json.put("fields", wrapper);
            }



                //old populate inicident JSON

           /* if (entityId.startsWith("A_F_") || entityId.startsWith("C_F_")) {

                if (event.getFactname().equalsIgnoreCase("S19")) {

                    String branch = cmsUserScenariomap.getUBPbranch(event.getEventId());


                    if( branch != null && !branch.equalsIgnoreCase("Not Available")) {
                        CxJson branchjson = new CxJson();
                        branchjson.put("value", branch);
                        wrapper.put("customfield_11903", branchjson);
                    }
                    else {
                        CxJson branchjson = new CxJson();
                        branchjson.put("value", "Select a Value");
                        wrapper.put("customfield_11903", branchjson);
                    }

                    List<String> list1 = cmsUserScenariomap.getRegion(branch);

                    if (list1 != null && list1.size() == 2) {
                        CxJson cxJson = new CxJson();
                        cxJson.put("value", list1.get(1).toString());

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", list1.get(0).toString());

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }
                    else {
                        CxJson cxJson = new CxJson();
                        cxJson.put("value", "Select a Value");

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", "Select a Value");

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);

                    }
                } else if(event.getFactname().equalsIgnoreCase("S18")) {

                    String branch = cmsUserScenariomap.getLoanbranch(event.getEventId());

                    if( branch != null && !branch.equalsIgnoreCase("Not Available")) {
                        CxJson branchjson = new CxJson();
                        branchjson.put("value", branch);

                        wrapper.put("customfield_11903", branchjson);
                    }
                    else {
                        CxJson branchjson = new CxJson();
                        branchjson.put("value", "Select a Value");

                        wrapper.put("customfield_11903", branchjson);
                    }
                    List<String> list1 = cmsUserScenariomap.getRegion(branch);

                    if (list1 != null && list1.size() == 2) {
                        CxJson cxJson = new CxJson();
                        cxJson.put("value", list1.get(1).toString());

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", list1.get(0).toString());

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }
                    else {
                        CxJson cxJson = new CxJson();v
                        cxJson.put("value", "Select a Value");

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", "Select a Value");

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }

                }
                else if(event.getFactname().equalsIgnoreCase("S16"))
                {
                    String branch = wrapper.getString("customfield_10215");

                    if( branch != null && !branch.equalsIgnoreCase("Not Available"))
                    {
                        CxJson branchjson = new CxJson();
                        branchjson.put("value", branch);
                        wrapper.put("customfield_11903", branchjson);

                        List<String> list1 = cmsUserScenariomap.getRegion(branch);

                        if (list1 != null && list1.size() == 2) {
                            CxJson cxJson = new CxJson();
                            cxJson.put("value", list1.get(1).toString());

                            CxJson cxJson1 = new CxJson();
                            cxJson1.put("value", list1.get(0).toString());

                            wrapper.put("customfield_11901", cxJson);
                            wrapper.put("customfield_11902", cxJson1);
                        }
                        else {
                            CxJson cxJson = new CxJson();
                            cxJson.put("value", "Select a Value");

                            CxJson cxJson1 = new CxJson();
                            cxJson1.put("value", "Select a Value");

                            wrapper.put("customfield_11901", cxJson);
                            wrapper.put("customfield_11902", cxJson1);
                        }
                    }

                    else {

                        branch = cmsUserScenariomap.getcollateralbranch(event.getEventId());
                        if (branch != null && !branch.equalsIgnoreCase("Not Available")) {

                            CxJson branchjson = new CxJson();
                            branchjson.put("value", branch);
                            wrapper.put("customfield_11903", branchjson);
                        } else {
                            CxJson branchjson = new CxJson();
                            branchjson.put("value", "Select a Value");
                            wrapper.put("customfield_11903", branchjson);

                        }
                        List<String> list1 = cmsUserScenariomap.getRegion(branch);

                        if (list1 != null && list1.size() == 2) {
                            CxJson cxJson = new CxJson();
                            cxJson.put("value", list1.get(1).toString());

                            CxJson cxJson1 = new CxJson();
                            cxJson1.put("value", list1.get(0).toString());

                            wrapper.put("customfield_11901", cxJson);
                            wrapper.put("customfield_11902", cxJson1);
                        } else {
                            CxJson cxJson = new CxJson();
                            cxJson.put("value", "Select a Value");

                            CxJson cxJson1 = new CxJson();
                            cxJson1.put("value", "Select a Value");

                            wrapper.put("customfield_11901", cxJson);
                            wrapper.put("customfield_11902", cxJson1);
                        }
                    }

                }
                else
                {
                    String branch = wrapper.getString("customfield_10215");

                    if( branch != null && !branch.equalsIgnoreCase("Not Available")) {
                        CxJson branchjson = new CxJson();
                        branchjson.put("value", branch);

                        wrapper.put("customfield_11903", branchjson);
                    }
                    else {

                        CxJson branchjson = new CxJson();
                         branch = cmsUserScenariomap.getAcctsol(eventId,event.getFactname(),event.getEntityId().substring(4));
                        if( branch != null) {

                            branchjson.put("value", branch);
                        }
                        else {
                            branchjson.put("value", "Select a Value");
                        }

                        wrapper.put("customfield_11903", branchjson);
                    }
                    List<String> list1 = cmsUserScenariomap.getRegion(branch);


                    if (list1 != null && list1.size() == 2) {
                        CxJson cxJson = new CxJson();
                        cxJson.put("value", list1.get(1).toString());

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", list1.get(0).toString());

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }
                    else {
                        CxJson cxJson = new CxJson();
                        cxJson.put("value", "Select a Value");

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", "Select a Value");

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }
                }
            }
            if (entityId.startsWith("R_F_")) {

                String branch = entityId.replace("R_F_", "");

                if( branch != null) {
                    CxJson branchjson = new CxJson();
                    branchjson.put("value", branch);

                    wrapper.put("customfield_11903", branchjson);
                }
                else {
                    CxJson branchjson = new CxJson();
                    branchjson.put("value", "Select a Value");

                    wrapper.put("customfield_11903", branchjson);
                }
                List<String> list1 = cmsUserScenariomap.getRegion(branch);
                if (list1 != null && branch != null && list1.size() == 2) {
                    wrapper.put("customfield_10215", branch);

                    CxJson cxJson = new CxJson();
                    cxJson.put("value", list1.get(1).toString());

                    CxJson cxJson1 = new CxJson();
                    cxJson1.put("value", list1.get(0).toString());

                    wrapper.put("customfield_11901", cxJson);
                    wrapper.put("customfield_11902", cxJson1);
                }
                else {
                    CxJson cxJson = new CxJson();
                    cxJson.put("value", "Select a Value");

                    CxJson cxJson1 = new CxJson();
                    cxJson1.put("value", "Select a Value");

                    wrapper.put("customfield_11901", cxJson);
                    wrapper.put("customfield_11902", cxJson1);

                }
            }
            if (entityId.startsWith("T_F_") || entityId.startsWith("U_F_")) {

                List<String> detailList = cmsUserScenariomap.getTrandetail(event.getEventId());

                if (detailList != null && detailList.size() == 3) {
                    wrapper.put("customfield_10215", detailList.get(0).toString());

                    CxJson branchjson = new CxJson();
                    branchjson.put("value", detailList.get(0).toString());

                    wrapper.put("customfield_11903", branchjson);

                    CxJson cxJson = new CxJson();
                    cxJson.put("value", detailList.get(2).toString());

                    CxJson cxJson1 = new CxJson();
                    cxJson1.put("value", detailList.get(1).toString());

                    wrapper.put("customfield_11901", cxJson);
                    wrapper.put("customfield_11902", cxJson1);

                }
                else {
                    CxJson branchjson = new CxJson();
                    branchjson.put("value", "Select a Value");

                    wrapper.put("customfield_11903", branchjson);

                    CxJson cxJson = new CxJson();
                    cxJson.put("value", "Select a Value");

                    CxJson cxJson1 = new CxJson();
                    cxJson1.put("value", "Select a Value");

                    wrapper.put("customfield_11901", cxJson);
                    wrapper.put("customfield_11902", cxJson1);

                }
            }
            if (entityId.startsWith("N_F_") && event.getFactname().equalsIgnoreCase("S08")) {
                List<String> detailList = cmsUserScenariomap.getTrandetail(event.getEventId());

                if (detailList != null && detailList.size() == 3) {
                    wrapper.put("customfield_10215", detailList.get(0).toString());

                    CxJson branchjson = new CxJson();
                    branchjson.put("value", detailList.get(0).toString());

                    wrapper.put("customfield_11903", branchjson);

                    CxJson cxJson = new CxJson();
                    cxJson.put("value", detailList.get(2).toString());

                    CxJson cxJson1 = new CxJson();
                    cxJson1.put("value", detailList.get(1).toString());

                    wrapper.put("customfield_11901", cxJson);
                    wrapper.put("customfield_11902", cxJson1);
                }
                else {

                    CxJson branchjson = new CxJson();
                    branchjson.put("value", "Select a Value");

                    wrapper.put("customfield_11903", branchjson);

                    CxJson cxJson = new CxJson();
                    cxJson.put("value", "Select a Value");

                    CxJson cxJson1 = new CxJson();
                    cxJson1.put("value", "Select a Value");

                    wrapper.put("customfield_11901", cxJson);
                    wrapper.put("customfield_11902", cxJson1);

                }
            }
            if (entityId.startsWith("N_F_") && event.getFactname().equalsIgnoreCase("S17")) {


                String branch = wrapper.getString("customfield_10215");

                if( branch != null && !branch.equalsIgnoreCase("Not Available"))
                {
                    CxJson branchjson = new CxJson();
                    branchjson.put("value", branch);
                    wrapper.put("customfield_11903", branchjson);

                    List<String> list1 = cmsUserScenariomap.getRegion(branch);

                    if (list1 != null && list1.size() == 2) {
                        CxJson cxJson = new CxJson();
                        cxJson.put("value", list1.get(1).toString());

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", list1.get(0).toString());

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }
                    else {
                        CxJson cxJson = new CxJson();
                        cxJson.put("value", "Select a Value");

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", "Select a Value");

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }
                }
                else {
                    branch = cmsUserScenariomap.getcollateralbranch(event.getEventId());
                    List<String> list1 = cmsUserScenariomap.getRegion(branch);

                    if (list1 != null && branch != null && list1.size() == 2) {
                        wrapper.put("customfield_10215", branch);

                        CxJson branchjson = new CxJson();
                        branchjson.put("value", branch);

                        wrapper.put("customfield_11903", branchjson);

                        CxJson cxJson = new CxJson();
                        cxJson.put("value", list1.get(1).toString());

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", list1.get(0).toString());

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);

                    } else {

                        CxJson branchjson = new CxJson();
                        branchjson.put("value", "Select a Value");

                        wrapper.put("customfield_11903", branchjson);

                        CxJson cxJson = new CxJson();
                        cxJson.put("value", "Select a Value");

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", "Select a Value");

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);

                    }
                }
            }
            if (entityId.startsWith("N_F_") && event.getFactname().equalsIgnoreCase("S20")) {
                String branch = cmsUserScenariomap.getDDUserbranch(event.getEventId());
                List<String> list1 = null;
                if( branch != null) {

                    list1 = cmsUserScenariomap.getRegion(branch);

                    if (list1 != null && list1.size() == 2) {
                        wrapper.put("customfield_10215", branch);

                        CxJson branchjson = new CxJson();
                        branchjson.put("value", branch);

                        wrapper.put("customfield_11903", branchjson);

                        CxJson cxJson = new CxJson();
                        cxJson.put("value", list1.get(1).toString());

                        CxJson cxJson1 = new CxJson();
                        cxJson1.put("value", list1.get(0).toString());

                        wrapper.put("customfield_11901", cxJson);
                        wrapper.put("customfield_11902", cxJson1);
                    }
                }
                else
                {
                    CxJson branchjson = new CxJson();
                    branchjson.put("value", "Select a Value");

                    wrapper.put("customfield_11903", branchjson);

                    CxJson cxJson = new CxJson();
                    cxJson.put("value", "Select a Value");

                    CxJson cxJson1 = new CxJson();
                    cxJson1.put("value", "Select a Value");

                    wrapper.put("customfield_11901", cxJson);
                    wrapper.put("customfield_11902", cxJson1);
                }
            }



            return json.put("fields", wrapper);
           }
             else {
               return json;
           } */



        }

        return  json;
    }




    public static synchronized String getUserSync(String regncode) {
        String assignUser = null;
        for (String key : regUsrmap.keySet()) {
            try {
                if (key.equalsIgnoreCase(regncode)) {
                    cxpsLogger.info("REGION CODE PRESENT IN REGION USER MAP");
                    if (regUsrmap.get(key).contains(",")) {
                        String arr[] = regUsrmap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrRegUsrMap.get(key) == null) {
                            conCurrRegUsrMap.put(key, 0);
                            cxpsLogger.info("Intially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrRegUsrMap.size() > 0 && conCurrRegUsrMap.get(key) < length) {
                            int val = conCurrRegUsrMap.get(key);
                            cxpsLogger.info("Assiging User --- > [ " + arr[val + 1] + " ] ");
                            conCurrRegUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrRegUsrMap.get(key) >= length) {
                                cxpsLogger.info("Both are same length " + arr[0]);
                                conCurrRegUsrMap.replace(key, 0);
                            }
                        }
                            assignUser = arr[conCurrRegUsrMap.get(regncode)];
                            return assignUser;


                    } else {
                        cxpsLogger.info("return user since it dont have comma --->  [ " + regUsrmap.get(key) + " ] ");
                        assignUser =  regUsrmap.get(key);
                        return assignUser;
                    }


                }


            }
            catch (Exception e)
            {
                cxpsLogger.info("Exception While getting  RO User");
                e.printStackTrace();
                if(key.equalsIgnoreCase(regncode));
                {
                    return regUsrmap.get(key).contains(",") ? regUsrmap.get(key).split(",")[0] : regUsrmap.get(key);

                }


            }
        }
        return assignUser;
    }


    public static synchronized String getUserSyncHo(String regncode) {
        String assignUser = null;
        for (String key : hoUsrmap.keySet()) {
            try {
                if (key.equalsIgnoreCase(regncode)) {
                    cxpsLogger.info("REGION CODE PRESENT IN HO REGION USER MAP");
                    if (hoUsrmap.get(key).contains(",")) {
                        String arr[] = hoUsrmap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrHoUsrMap.get(key) == null) {
                            conCurrHoUsrMap.put(key, 0);
                            cxpsLogger.info("Intially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrHoUsrMap.size() > 0 && conCurrHoUsrMap.get(key) < length) {
                            int val = conCurrHoUsrMap.get(key);
                            cxpsLogger.info("Assiging User --- > [ " + arr[val + 1] + " ] ");
                            conCurrHoUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrHoUsrMap.get(key) >= length) {
                                cxpsLogger.info("Both are same length " + arr[0]);
                                conCurrHoUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrHoUsrMap.get(regncode)];
                        return assignUser;

                    } else {
                        cxpsLogger.info("return user since it dont have comma --->  [ " + hoUsrmap.get(key) + " ] ");
                        assignUser =  hoUsrmap.get(key);
                        return assignUser;
                    }


                }


            }
            catch (Exception e)
            {
                cxpsLogger.info("Exception While getting HO User");
                e.printStackTrace();
                if(key.equalsIgnoreCase(regncode));
                {
                    return hoUsrmap.get(key).contains(",") ? hoUsrmap.get(key).split(",")[0] : hoUsrmap.get(key);

                }

            }
        }
        return assignUser;
    }


    public static synchronized String getFactUserSync(String factname) {
        String assignUser = null;
        for (String key : fctUsrmap.keySet()) {
            try {
                if (key.equalsIgnoreCase(factname)) {
                    cxpsLogger.info("FACTNAME PRESENT IN FACT USER MAP");
                    if (fctUsrmap.get(key).contains(",")) {
                        String arr[] = fctUsrmap.get(key).split(",");
                        int length = arr.length - 1;
                        if (conCurrFactUsrMap.get(key) == null) {
                            conCurrFactUsrMap.put(key, 0);
                            cxpsLogger.info("Intially Map null hence first user--- > [ " + arr[0] + " ] ");
                        } else if (conCurrFactUsrMap.size() > 0 && conCurrFactUsrMap.get(key) < length) {
                            int val = conCurrFactUsrMap.get(key);
                            cxpsLogger.info("Assiging User --- > [ " + arr[val + 1] + " ] ");
                            conCurrFactUsrMap.replace(key, val + 1);
                        } else {
                            if (conCurrFactUsrMap.get(key) >= length) {
                                cxpsLogger.info("Both are same length " + arr[0]);
                                conCurrFactUsrMap.replace(key, 0);
                            }
                        }
                        assignUser = arr[conCurrFactUsrMap.get(factname)];
                        return assignUser;

                    } else {
                        cxpsLogger.info("return user since it dont have comma --->  [ " + fctUsrmap.get(key) + " ] ");
                        assignUser =  fctUsrmap.get(key);
                        return assignUser;
                    }


                }


            }
            catch (Exception e)
            {
                cxpsLogger.info("Exception While getting  Fact User");
                e.printStackTrace();
                if(key.equalsIgnoreCase(factname));
                {
                    return fctUsrmap.get(key).contains(",") ? fctUsrmap.get(key).split(",")[0] : fctUsrmap.get(key);

                }
            }
        }
        return assignUser;
    }

    public void loadconf(){

        Hocon conf = new Hocon();
        conf.loadFromContext("custom-fields.conf");
        Hocon custfields = conf.get("custom-fields");
        if (custfields != null) {
            List<String> keys = custfields.getKeysAsList();
            for (String key : keys) {
                String val = custfields.getString(key);
                logger.info("the custom field for " + key + " : " + val);
                System.out.println("the custom field for " + key + " : " + val);
                loadmap.put(key, val);
            }
        }

    }


}
