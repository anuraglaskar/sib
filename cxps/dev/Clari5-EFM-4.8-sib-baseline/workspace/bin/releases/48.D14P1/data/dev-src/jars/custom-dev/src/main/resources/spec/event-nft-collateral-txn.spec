cxps.events.event.nft-collateral-txn{
table-name : EVENT_NFT_COLLATERAL_TXN
event-mnemonic : CT
workspaces : {
  ACCOUNT : "loan-acct-no"
  NONCUSTOMER : "collateral-id"
}

event-attributes : {

loan-acct-no : { db : true, raw_name : LoanAcctNo, type : "string:50" }
collateral-open-date : { db : true, raw_name : Collateral_Open_date, type : "timestamp" }
collateral-mod-date : { db : true, raw_name : Collateral_Mod_date, type : "timestamp" }
collateral-status : { db : true, raw_name : Collateral_Status, type : "string:50" }
collateral-id : { db : true, raw_name : Collateral_Id, type : "string:50" }
acct-status : { db : true, raw_name : Acct_status, type : "string:50" }
eventtype : { db : true, raw_name : eventtype, type : "string:50" }
eventsubtype : { db : true, raw_name : eventsubtype, type : "string:50" }
host-id  : { db : true, raw_name : host-id, type : "string:50" }
event-name : { db : true, raw_name : event-name, type : "string:50" }
eventts : { db : true, raw_name : eventts, type : "timestamp" }
sys-time : { db : true, raw_name : sys-time, type : "timestamp" }


}
}
