package clari5.custom.sib.data;

import clari5.custom.sib.builder.EventBuilder;
import clari5.custom.sib.builder.MsgMetaData;
import clari5.custom.sib.data.bootstrap.TableMap;

public abstract class ITableData {

	public boolean process() throws Exception {
		MsgMetaData m = TableMap.getTableMap().get(this.getTableName());
		EventBuilder eb = new EventBuilder();
		eb.process(this);
		return true;
	}
	public abstract String getTableName();
}
