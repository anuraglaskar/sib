package clari5.custom.sib.data;

import javax.print.DocFlavor;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class NFT_DdcancelTxn extends ITableData {
	private String tableName = "NFT_DD_CANCEL";
	private String event_type = "nft_ddcanceltxn";

	private  String DD_NUM;
        private  String	DD_ISSUE_DATE;
	private  String DD_CANCEL_DATE;
        private  String	DD_ISSUE_AMT;
	private  String DD_CANCEL_AMT;
        private  String	DD_ISSUE_ACCT;
        private  String  DD_CANCEL_ACCT;
	private  String	DD_ISSUE_CUSTID;
	private  String DD_CANCEL_CUSTID;
        private String ID;

	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getDD_NUM() {
		return DD_NUM;
	}

	public void setDD_NUM(String DD_NUM) {
		this.DD_NUM = DD_NUM;
	}

	public String getDD_ISSUE_DATE() {
		return DD_ISSUE_DATE;
	}

	public void setDD_ISSUE_DATE(String DD_ISSUE_DATE) {
		this.DD_ISSUE_DATE = DD_ISSUE_DATE;
	}

	public String getDD_CANCEL_DATE() {
		return DD_CANCEL_DATE;
	}

	public void setDD_CANCEL_DATE(String DD_CANCEL_DATE) {
		this.DD_CANCEL_DATE = DD_CANCEL_DATE;
	}

	public String getDD_ISSUE_AMT() {
		return DD_ISSUE_AMT;
	}

	public void setDD_ISSUE_AMT(String DD_ISSUE_AMT) {
		this.DD_ISSUE_AMT = DD_ISSUE_AMT;
	}

	public String getDD_CANCEL_AMT() {
		return DD_CANCEL_AMT;
	}

	public void setDD_CANCEL_AMT(String DD_CANCEL_AMT) {
		this.DD_CANCEL_AMT = DD_CANCEL_AMT;
	}

	public String getDD_ISSUE_ACCT() {
		return DD_ISSUE_ACCT;
	}

	public void setDD_ISSUE_ACCT(String DD_ISSUE_ACCT) {
		this.DD_ISSUE_ACCT = DD_ISSUE_ACCT;
	}

	public String getDD_CANCEL_ACCT() {
		return DD_CANCEL_ACCT;
	}

	public void setDD_CANCEL_ACCT(String DD_CANCEL_ACCT) {
		this.DD_CANCEL_ACCT = DD_CANCEL_ACCT;
	}

	public String getDD_ISSUE_CUSTID() {
		return DD_ISSUE_CUSTID;
	}

	public void setDD_ISSUE_CUSTID(String DD_ISSUE_CUSTID) {
		this.DD_ISSUE_CUSTID = DD_ISSUE_CUSTID;
	}

	public String getDD_CANCEL_CUSTID() {
		return DD_CANCEL_CUSTID;
	}

	public void setDD_CANCEL_CUSTID(String DD_CANCEL_CUSTID) {
		this.DD_CANCEL_CUSTID = DD_CANCEL_CUSTID;
	}

	@Override
	public String getTableName() {
		return tableName;
	}

}
