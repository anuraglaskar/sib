cxps.events.event.ft-account-txn {
table-name : EVENT_FT_ACCOUNT_TXN
  event-mnemonic: FA
  workspaces : {
    ACCOUNT : acct-id,
    USER : agent-id,
    CUSTOMER: cust-id,
    TRANSACTION: tran-id,
    BRANCH: tran-br-id
  }
   db_column_quoted = true

  event-attributes : {
    acct-id: {db : true ,raw_name : account_id ,type : "string:20000", custom-getter:Acct_id}
    cust-id: {db : true ,raw_name : cust_id ,type : "string:20000", custom-getter:Cust_id }
    user-id: {db : true ,raw_name : User_id ,type : "string:20000", custom-getter:User_id }
    branch-id: {db : true ,raw_name : Branch_Id ,type : "string:20000", custom-getter:Branch_Id}

    host-id : {db : true ,raw_name : HostId ,type : "string:2000"}
    channel-type : {db : true ,raw_name : channel_type ,type : "string:2000"}
    channel-id : {db : true ,raw_name : channel_id ,type : "string:2000"}
    agent-type : {db : true ,raw_name : agent_type ,type : "string:2000"}
    agent-id : {db : true ,raw_name : pstd_user_id ,type : "string:2000"}
    verify-user : {db : true ,raw_name : pstd_user_id ,type : "string:2000"}
    source-bank : {db : true ,raw_name : bank_code ,type : "string:2000"}
    dest-bank : {db : true ,raw_name : bank_code ,type : "string:2000"}
    cust-ref-type : {db : true ,raw_name : cust_ref_type ,type : "string:2000"}
    cust-ref-id : {db : true ,raw_name : acid ,type : "string:2000"}
    cx-cif-id : {db : true ,raw_name : cx_cif_i_d ,type : "string:2000"}
    txn-ref-type : {db : true ,raw_name : txn_ref_type ,type : "string:2000"}
    txn-ref-id : {db : true ,raw_name : tran_id ,type : "string:2000"}
    tran-id : {db : true ,raw_name : tran_id ,type : "string:2000"}
    txn-date : {db : true ,raw_name : sys_time ,type : timestamp}
    action-at-host : {db : true ,raw_name : module_id ,type : "string:2000"}
    txn-amt : {db : true ,raw_name : tran_amt ,type : "number:11,2", evidence : true }
    txn-amt-cur : {db : true ,raw_name : tran_crncy_code ,type : "string:2000"}
    txn-value-date : {db : true ,raw_name : value_date ,type : timestamp}
    exception : {db : true ,raw_name : exception ,type : "string:2000"}
    txn-status : {db : true ,raw_name : pstd_flg ,type : "string:2000"}
    remarks : {db : true ,raw_name : tran_rmks ,type : "string:2000"}
    source-or-dest-act-id : {db : true ,raw_name : accountId ,type : "string:2000",evidence : true  }
    account-id : {db : true ,raw_name : accountId ,type : "string:2000",evidence : true  }
    remitter-name : {db : true ,raw_name : remitter_name ,type : "string:2000"}
    beneficiary-name : {db : true ,raw_name : beneficiary_name ,type : "string:2000"}
    instrument-type : {db : true ,raw_name : instrmnt_type ,type : "string:2000"}
    instrument-id : {db : true ,raw_name : instrmnt_type ,type : "number:20"}
    ledger-bal : {db : true ,raw_name :clr_bal_amt ,type : "number:11,2"}
    ACCTAMB : {db : true ,raw_name :ACCTAMB ,type : "number:11,2"}
    balance-cur : {db : true ,raw_name : balance_cur ,type : "string:2000"}
    shadow-bal : {db : true ,raw_name : un_clr_bal_amt ,type : "number:11,2"}
    avl-bal : {db : true ,raw_name : avl_bal ,type : "number:11,2"}
    eff-avl-bal : {db : true ,raw_name : eff_avl_bal ,type : "number:11,2"}
    txn-dr-cr : {db : true ,raw_name : part_tran_type ,type : "string:2000"}
    evt-inducer : {db : true ,raw_name : evt_inducer ,type : "string:2000"}
    bill-id : {db : true ,raw_name : ref_num ,type : "string:2000"}
    zone-date : {db : true ,raw_name : zone_date ,type : timestamp}
    zone-code : {db : true ,raw_name : zone_code ,type : "string:2000"}
    payee-id : {db : true ,raw_name : payee_id ,type : "string:2000"}
    merchant-categ : {db : true ,raw_name : merchant_categ ,type : "string:2000"}
    tran-srl-no : {db : true ,raw_name : part_tran_srl_num ,type : "string:2000", evidence : true }
    part-tran-srl-num : {db : true ,raw_name : part_tran_srl_num ,type : "string:2000", evidence : true }
    tran-category : {db : true ,raw_name : tran_category ,type : "string:2000"}
    payer-id-temp : {db : true ,raw_name : acid ,type : "string:2000"}
    inst-alpha : {db : true ,raw_name : instrmnt_alpha ,type : "string:2000"}
    tran-type : {db : true ,raw_name : tran_type ,type : "string:2000", evidence : true }
    tran-sub-type : {db : true ,raw_name : tran_sub_type ,type : "string:2000",evidence : true }
    tran-entry-dt : {db : true ,raw_name : entry_date ,type : timestamp}
    tran-posted-dt : {db : true ,raw_name : pstd_date ,type : timestamp}
    tran-verified-dt : {db : true ,raw_name : vfd_date ,type : timestamp}
    tran-br-id : {db : true ,raw_name : br_code ,type : "string:2000"}
    rate : {db : true ,raw_name : rate ,type : "number:11,2"}
    ref-tran-amt : {db : true ,raw_name : ref_tran_amt ,type : "number:11,2"}
    ref-currency : {db : true ,raw_name : ref_tran_crncy ,type : "string:2000"}
    acct-sol-id : {db : true ,raw_name : acct_sol_id ,type : "string:2000"}
    bal-modi-time : {db : true ,raw_name : sys_time ,type : timestamp}
    scheme-type : {db : true ,raw_name : schm_type ,type : "string:2000"}
    scheme-code : {db : true ,raw_name : schm_code ,type : "string:2000"}
    place-holder : {db : true ,raw_name : place_holder ,type : "string:2000"}
    account-ownership : {db : true ,raw_name : acct_ownership ,type : "string:2000"}
    branch-id-desc : {db : true ,raw_name : branch_id_desc ,type : "string:2000"}
    employee-id : {db : true ,raw_name : emp_id ,type : "string:2000"}
    cre-dr-ptran : {db : true ,raw_name : cre_dr_ptran ,type : "string:2000"}
    pur-acct-num : {db : true ,raw_name : pur_acct_num ,type : "string:2000"}
    payee-name : {db : true ,raw_name : payeename ,type : "string:2000"}
    payee-city : {db : true ,raw_name : payeecity ,type : "string:2000"}
    online-batch : {db : true ,raw_name : onlinebatch ,type : "string:2000"}
    kyc-status : {db : true ,raw_name : KYC_Stat ,type : "string:2000"}
    acct-status : {db : true ,raw_name : Acct_Stat ,type : "string:2000"}
    sanction-amt : {db : true ,raw_name : sanction_amt ,type : "number:11,2"}
    fcnr-flag : {db : true ,raw_name : fcnr_flag ,type : "string:2000"}
    eod : {db : true ,raw_name : eod ,type : "number:20"}
    terminal-ip-addr : {db : true ,raw_name : ip_address ,type : "string:2000"}
    direct-channel-id : {db : true ,raw_name : dc_id ,type : "string:2000"}
    direct-channel-controller-id : {db : true ,raw_name : dcc_id ,type : "string:2000"}
    device-id : {db : true ,raw_name : device_id ,type : "string:2000"}
    acct-occp-code : {raw_name : acct_occp_code ,type : "string:2000"}
    add-entity-id1 : {raw_name : add_entity_id1 ,type : "string:2000"}
    add-entity-id2 : {raw_name : add_entity_id2 ,type : "string:2000"}
    add-entity-id3 : {raw_name : add_entity_id3 ,type : "string:2000"}
    add-entity-type1 : {raw_name : add_entity_type1 ,type : "string:2000"}
    add-entity-type2 : {raw_name : add_entity_type2 ,type : "string:2000"}
    add-entity-type3 : {raw_name : add_entity_type3 ,type : "string:2000"}
    channel-desc : {db : true ,raw_name : channel_desc ,type : "string:2000"}
    entity-id : {db : true ,raw_name : entity_id ,type : "string:2000"}
    entity-type : {db : true ,raw_name : entity_type ,type : "string:2000"}
    entry-user : {db : true ,raw_name : entry_user ,type : "string:2000"}
    event-sub-type : {db : true ,raw_name : event_sub_type ,type : "string:2000"}
    event-time : {db : true ,raw_name : event_time ,type : "string:2000"}
    event-type : {db : true ,raw_name : event_type ,type : "string:2000"}
    hdrmkrs : {db : true ,raw_name : hdrmkrs ,type : "string:2000"}
    host-user-id : {db : true ,raw_name : host_user_id ,type : "string:2000"}
    mode-oprn-code : {db : true ,raw_name : mode_oprn_code ,type : "string:2000"}
    msg-name : {db : true ,raw_name : msg_name ,type : "string:2000"}
    msg-source : {db : true ,raw_name : msg_source ,type : "string:2000"}
    tran-date : {db : true ,raw_name : tran_date ,type : timestamp, custom-getter: Tran_date}
    bin : {db : true ,raw_name : BIN ,type : "string:2000", custom-getter: BIN}
    country-code : {db : true ,raw_name : country_code ,fr: true, type : "string:2000", custom-getter:Country_code}
    dev-owner-id : {db : true ,raw_name : dev_owner_id ,fr: true, type : "string:2000", custom-getter: Dev_owner_id}
    cust-card-id : {db : true ,raw_name : cust_card_id ,fr: true, type : "string:2000", custom-getter:Cust_card_id}
    acct-open-date : {db : true ,raw_name : acctopendate ,fr: true, type : timestamp}
    sequence-number : {db : true ,raw_name : sequence_number ,type : "string:2000" }
    solamt : {db : true ,raw_name : solamt , type : "number:11,2" }
    acct-bal : {db : true ,raw_name : acct_bal ,type : "number:11,2" }
    mc-code : {db : true ,raw_name : mcCode ,type : "string:2000" }
    instrmnt-num : {db : true ,raw_name : instrmnt_num ,type : "string:2000" }
    first4chqno : {db : true ,raw_name : first4chqno ,type : "string:2000" }
    instrmnt_date : {db : true ,raw_name : instrmnt_date ,type : timestamp }
    status : {db : true ,raw_name : status ,type : "string:2000" }
    entry-user : {db : true ,raw_name : entryUser,type : "string:2000" }
    mode-oprn-code : {db : true ,raw_name : modeOprnCode ,type : "string:2000" }
    work-class : {db : true ,raw_name : work_class ,type : "string:2000" }
    requsted-amt : {db : true ,raw_name : requsted_amt ,type : "string:2000" }
    card-no : {db : true ,raw_name : card_no ,type : "string:2000" }
    gl-sub-head-code : {db : true,raw_name : gl_sub_head_code ,type : "string:50"}
    staffflag : {db : true ,raw_name : staffflag ,type : "string:2000" }
    sys-time : { db : true, raw_name : sys_time, type : "timestamp" }
    acc-name : {db : true ,raw_name : acct_name ,type : "string:2000"}
    acct-name : {db : true, type : "string:2000", derivation:"""cxps.events.CustomDerivation.acctName(accName)"""}
    tran-part : {db : true ,raw_name : tran_particular ,type : "string:2000"}
    ben-name : {db : true,raw_name : tran_particular, type : "string:2000", derivation:"""cxps.events.CustomDerivation.benName(benName)"""}
    description : {db : true ,raw_name :tran_particular, type : "string:2000"}
    is-staff : {db : true ,type : "string:2"}
    is-staff : {db : true ,type : "string:2"}
    cust-dob : {db : true ,raw_name : cust_dob ,type : timestamp}
    entryusr-designation :{db : true ,type : "string:100"}
    verifyusr-designation :{db : true ,type : "string:100"}
    entry-verif-flg : {db : true ,type : "string:2", derivation:"""cxps.events.CustomDerivation.getEntryVerifFlg(this)"""}
    joint-cust-id : {db : true, type : "string:50", derivation:"""cxps.events.CustomDerivation.getJointCustId(this)"""}
    agent-id : {db : true ,raw_name :AGENT_ID, type : "string:2000"}  
    direct-channel-controller-id : {db : true ,raw_name :DIRECT_CHANNEL_CONTROLLER_ID, type : "string:2000"} 
    source-bank : {db : true ,raw_name :SOURCE_BANK, type : "string:2000"} 
   }
}

