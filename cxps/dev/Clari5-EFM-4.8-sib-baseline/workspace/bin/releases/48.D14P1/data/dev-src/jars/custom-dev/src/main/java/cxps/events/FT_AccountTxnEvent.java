// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
//import clari5.platform.logger.CXLog;
//import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_ACCOUNT_TXN", Schema="rice")
public class FT_AccountTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=2000) public String exception;
       @Field(size=2000) public String beneficiaryName;
       @Field public java.sql.Timestamp zoneDate;
       @Field(size=2000) public String fcnrFlag;
       @Field(size=100) public String entryusrDesignation;
       @Field(size=2000) public String bin;
       @Field(size=2000) public String onlineBatch;
       @Field(size=2000) public String tranSubType;
       @Field(size=2000) public String txnRefType;
       @Field(size=2000) public String txnDrCr;
       @Field(size=2000) public String hostId;
       @Field(size=2000) public String hostUserId;
       @Field public java.sql.Timestamp balModiTime;
       @Field(size=2000) public String destBank;
       @Field(size=2000) public String directChannelId;
       @Field(size=2000) public String sequenceNumber;
       @Field(size=20000) public String branchId;
       @Field public java.sql.Timestamp sysTime;
       @Field public java.sql.Timestamp tranVerifiedDt;
       @Field(size=2000) public String purAcctNum;
       @Field(size=2000) public String payeeId;
       @Field(size=2000) public String acctOccpCode;
       @Field(size=2000) public String acctStatus;
       @Field(size=2000) public String status;
       @Field(size=2000) public String entityType;
       @Field(size=2000) public String zoneCode;
       @Field(size=2000) public String countryCode;
       @Field public java.sql.Timestamp acctOpenDate;
       @Field(size=2000) public String merchantCateg;
       @Field(size=2000) public String instAlpha;
       @Field(size=2000) public String msgSource;
       @Field(size=2000) public String payerIdTemp;
       @Field(size=2000) public String terminalIpAddr;
       @Field(size=2) public String entryVerifFlg;
       @Field(size=2000) public String mcCode;
       @Field(size=2000) public String entryUser;
       @Field(size=2000) public String addEntityId2;
       @Field(size=2000) public String addEntityId1;
       @Field(size=2) public String isStaff;
       @Field public java.sql.Timestamp txnDate;
       @Field(size=2000) public String addEntityId3;
       @Field(size=2000) public String eventType;
       @Field(size=20000) public String userId;
       @Field(size=50) public String glSubHeadCode;
       @Field(size=2000) public String partTranSrlNum;
       @Field(size=2000) public String accountOwnership;
       @Field(size=20) public Double sanctionAmt;
       @Field(size=2000) public String kycStatus;
       @Field(size=2000) public String agentId;
       @Field(size=20) public Double acctAMB;
       @Field(size=2000) public String tranType;
       @Field(size=2000) public String channelDesc;
       @Field(size=2000) public String workClass;
       @Field(size=2000) public String verifyUser;
       @Field(size=2000) public String branchIdDesc;
       @Field(size=2000) public String creDrPtran;
       @Field(size=2000) public String txnRefId;
       @Field(size=2000) public String sourceBank;
       @Field(size=2000) public String payeeCity;
       @Field(size=2000) public String devOwnerId;
       @Field(size=2000) public String deviceId;
       @Field(size=20) public Double acctBal;
       @Field(size=2000) public String staffflag;
       @Field(size=2000) public String tranId;
       @Field(size=2000) public String cardNo;
       @Field(size=2000) public String entityId;
       @Field(size=2000) public String requstedAmt;
       @Field(size=2000) public String eventSubType;
       @Field(size=2000) public String accountId;
       @Field(size=2000) public String placeHolder;
       @Field(size=2000) public String directChannelControllerId;
       @Field(size=2000) public String payeeName;
       @Field(size=2000) public String channelType;
       @Field(size=2000) public String acctName;
       @Field(size=2000) public String custRefType;
       @Field(size=2000) public String custCardId;
       @Field(size=2000) public String tranSrlNo;
       @Field(size=20) public Double solamt;
       @Field(size=20) public Double avlBal;
       @Field(size=2000) public String evtInducer;
       @Field public java.sql.Timestamp txnValueDate;
       @Field public java.sql.Timestamp tranPostedDt;
       @Field(size=2000) public String schemeType;
       @Field public java.sql.Timestamp custDob;
       @Field(size=20000) public String acctId;
       @Field(size=2000) public String agentType;
       @Field public java.sql.Timestamp tranEntryDt;
       @Field(size=2000) public String cxCifId;
       @Field(size=2000) public String actionAtHost;
       @Field(size=2000) public String schemeCode;
       @Field(size=2000) public String custRefId;
       @Field(size=2000) public String description;
       @Field(size=2000) public String sourceOrDestActId;
       @Field(size=2000) public String eventTime;
       @Field(size=2000) public String channelId;
       @Field(size=2000) public String billId;
       @Field(size=20) public Double rate;
       @Field(size=2000) public String txnStatus;
       @Field(size=20) public Double txnAmt;
       @Field(size=20) public Double effAvlBal;
       @Field(size=2000) public String tranCategory;
       @Field(size=20) public Long instrumentId;
       @Field(size=2000) public String balanceCur;
       @Field(size=2000) public String instrmntNum;
       @Field(size=100) public String verifyusrDesignation;
       @Field public java.sql.Timestamp instrmntDate;
       @Field(size=2000) public String addEntityType1;
       @Field(size=20) public Double ledgerBal;
       @Field(size=2000) public String employeeId;
       @Field(size=2000) public String addEntityType3;
       @Field(size=2000) public String acctSolId;
       @Field(size=2000) public String addEntityType2;
       @Field(size=2000) public String benName;
       @Field(size=50) public String jointCustId;
       @Field(size=2000) public String txnAmtCur;
       @Field(size=2000) public String refCurrency;
       @Field(size=2000) public String modeOprnCode;
       @Field(size=2000) public String msgName;
       @Field(size=20000) public String custId;
       @Field public java.sql.Timestamp tranDate;
       @Field(size=2000) public String hdrmkrs;
       @Field(size=2000) public String tranBrId;
       @Field(size=2000) public String remitterName;
       @Field(size=2000) public String instrumentType;
       @Field(size=20) public Double shadowBal;
       @Field(size=20) public Double refTranAmt;
       @Field(size=20) public Long eod;
       @Field(size=2000) public String first4chqno;
       @Field(size=2000) public String remarks;


    @JsonIgnore
    public ITable<FT_AccountTxnEvent> t = AEF.getITable(this);

	public FT_AccountTxnEvent(){}

    public FT_AccountTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("AccountTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setException(json.getString("exception"));
            setBeneficiaryName(json.getString("beneficiary_name"));
            setZoneDate(EventHelper.toTimestamp(json.getString("zone_date")));
            setFcnrFlag(json.getString("fcnr_flag"));
            setEntryusrDesignation(json.getString("entryusr_designation"));
            setBin(json.getString("BIN"));
            setOnlineBatch(json.getString("onlinebatch"));
            setTranSubType(json.getString("tran_sub_type"));
            setTxnRefType(json.getString("txn_ref_type"));
            setTxnDrCr(json.getString("part_tran_type"));
            setHostId(json.getString("HostId"));
            setHostUserId(json.getString("host_user_id"));
            setBalModiTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setDestBank(json.getString("bank_code"));
            setDirectChannelId(json.getString("dc_id"));
            setSequenceNumber(json.getString("sequence_number"));
            setBranchId(json.getString("Branch_Id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys_time")));
            setTranVerifiedDt(EventHelper.toTimestamp(json.getString("vfd_date")));
            setPurAcctNum(json.getString("pur_acct_num"));
            setPayeeId(json.getString("payee_id"));
            setAcctOccpCode(json.getString("acct_occp_code"));
            setAcctStatus(json.getString("Acct_Stat"));
            setStatus(json.getString("status"));
            setEntityType(json.getString("entity_type"));
            setZoneCode(json.getString("zone_code"));
            setCountryCode(json.getString("country_code"));
            setAcctOpenDate(EventHelper.toTimestamp(json.getString("acctopendate")));
            setMerchantCateg(json.getString("merchant_categ"));
            setInstAlpha(json.getString("instrmnt_alpha"));
            setMsgSource(json.getString("msg_source"));
            setPayerIdTemp(json.getString("acid"));
            setTerminalIpAddr(json.getString("ip_address"));
            setEntryVerifFlg(json.getString("entry_verif_flg"));
            setMcCode(json.getString("mcCode"));
            setEntryUser(json.getString("entryUser"));
            setAddEntityId2(json.getString("add_entity_id2"));
            setAddEntityId1(json.getString("add_entity_id1"));
            setTxnDate(EventHelper.toTimestamp(json.getString("sys_time")));
            setAddEntityId3(json.getString("add_entity_id3"));
            setEventType(json.getString("event_type"));
            setUserId(json.getString("User_id"));
            setGlSubHeadCode(json.getString("gl_sub_head_code"));
            setPartTranSrlNum(json.getString("part_tran_srl_num"));
            setAccountOwnership(json.getString("acct_ownership"));
            setSanctionAmt(EventHelper.toDouble(json.getString("sanction_amt")));
            setKycStatus(json.getString("KYC_Stat"));
            setAgentId(json.getString("pstd_user_id"));
            setAcctAMB(EventHelper.toDouble(json.getString("balance_cur")));
            setTranType(json.getString("tran_type"));
            setChannelDesc(json.getString("channel_desc"));
            setWorkClass(json.getString("work_class"));
            setVerifyUser(json.getString("pstd_user_id"));
            setBranchIdDesc(json.getString("branch_id_desc"));
            setCreDrPtran(json.getString("cre_dr_ptran"));
            setTxnRefId(json.getString("tran_id"));
            setSourceBank(json.getString("bank_code"));
            setPayeeCity(json.getString("payeecity"));
            setDevOwnerId(json.getString("dev_owner_id"));
            setDeviceId(json.getString("device_id"));
            setAcctBal(EventHelper.toDouble(json.getString("acct_bal")));
            setStaffflag(json.getString("staffflag"));
            setTranId(json.getString("tran_id"));
            setCardNo(json.getString("card_no"));
            setEntityId(json.getString("entity_id"));
            setRequstedAmt(json.getString("requsted_amt"));
            setEventSubType(json.getString("event_sub_type"));
            setAccountId(json.getString("accountId"));
            setPlaceHolder(json.getString("place_holder"));
            setDirectChannelControllerId(json.getString("dcc_id"));
            setPayeeName(json.getString("payeename"));
            setChannelType(json.getString("channel_type"));
            setAcctName(json.getString("acct_name"));
            setCustRefType(json.getString("cust_ref_type"));
            setCustCardId(json.getString("cust_card_id"));
            setTranSrlNo(json.getString("part_tran_srl_num"));
            setSolamt(EventHelper.toDouble(json.getString("solamt")));
            setAvlBal(EventHelper.toDouble(json.getString("avl_bal")));
            setEvtInducer(json.getString("evt_inducer"));
            setTxnValueDate(EventHelper.toTimestamp(json.getString("value_date")));
            setTranPostedDt(EventHelper.toTimestamp(json.getString("pstd_date")));
            setSchemeType(json.getString("schm_type"));
            setAcctId(json.getString("account_id"));
            setAgentType(json.getString("agent_type"));
            setTranEntryDt(EventHelper.toTimestamp(json.getString("entry_date")));
            setCxCifId(json.getString("cx_cif_i_d"));
            setActionAtHost(json.getString("module_id"));
            setSchemeCode(json.getString("schm_code"));
            setCustRefId(json.getString("acid"));
            setDescription(json.getString("tran_particular"));
            setSourceOrDestActId(json.getString("accountId"));
            setEventTime(json.getString("event_time"));
            setChannelId(json.getString("channel_id"));
            setBillId(json.getString("ref_num"));
            setRate(EventHelper.toDouble(json.getString("rate")));
            setTxnStatus(json.getString("pstd_flg"));
            setTxnAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setEffAvlBal(EventHelper.toDouble(json.getString("eff_avl_bal")));
            setTranCategory(json.getString("tran_category"));
            setInstrumentId(EventHelper.toLong(json.getString("instrmnt_type")));
            setBalanceCur(json.getString("balance_cur"));
            setInstrmntNum(json.getString("instrmnt_num"));
           // setVerifyusrDesignation(json.getString("verifyusr_designation"));
            setInstrmntDate(EventHelper.toTimestamp(json.getString("instrmnt_date")));
            setAddEntityType1(json.getString("add_entity_type1"));
            setLedgerBal(EventHelper.toDouble(json.getString("clr_bal_amt")));
            setEmployeeId(json.getString("emp_id"));
            setAddEntityType3(json.getString("add_entity_type3"));
            setAcctSolId(json.getString("acct_sol_id"));
            setAddEntityType2(json.getString("add_entity_type2"));
            setBenName(json.getString("tran_particular"));
            setTxnAmtCur(json.getString("tran_crncy_code"));
            setRefCurrency(json.getString("ref_tran_crncy"));
            setModeOprnCode(json.getString("modeOprnCode"));
            setMsgName(json.getString("msg_name"));
            setCustId(json.getString("cust_id"));
            setTranDate(EventHelper.toTimestamp(json.getString("tran_date")));
            setHdrmkrs(json.getString("hdrmkrs"));
            setTranBrId(json.getString("br_code"));
            setRemitterName(json.getString("remitter_name"));
            setInstrumentType(json.getString("instrmnt_type"));
            setShadowBal(EventHelper.toDouble(json.getString("un_clr_bal_amt")));
            setRefTranAmt(EventHelper.toDouble(json.getString("ref_tran_amt")));
            setEod(EventHelper.toLong(json.getString("eod")));
            setFirst4chqno(json.getString("first4chqno"));
            setRemarks(json.getString("tran_rmks"));

        setDerivedValues();

    }


    private void setDerivedValues() {

        SOLamt soLamt = new SOLamt();
        setSolamt(soLamt.getSOLamt(getTranBrId()));
        setEntryVerifFlg(cxps.events.CustomDerivation.getEntryVerifFlg(this));
        if(getBenName().startsWith("BEN NAME: ")) {
	        setBenName(cxps.events.CustomDerivation.benName(benName));
        }
	setJointCustId(cxps.events.CustomDerivation.getJointCustId(this));
	if(getTranBrId().equalsIgnoreCase("0000")){
		setTranBrId(getAcctSolId());
	}
    }

    /* Getters */
    @Override
    public String getMnemonic() { return "FA"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getException(){ return exception; }

    public String getBeneficiaryName(){ return beneficiaryName; }

    public java.sql.Timestamp getZoneDate(){ return zoneDate; }

    public String getFcnrFlag(){ return fcnrFlag; }

    public String getEntryusrDesignation(){ return entryusrDesignation; }

    public String getBin(){ return bin; }

    public String getOnlineBatch(){ return onlineBatch; }

    public String getTranSubType(){ return tranSubType; }

    public String getTxnRefType(){ return txnRefType; }

    public String getTxnDrCr(){ return txnDrCr; }

    public String getHostId(){ return hostId; }

    public String getHostUserId(){ return hostUserId; }

    public java.sql.Timestamp getBalModiTime(){ return balModiTime; }

    public String getDestBank(){ return destBank; }

    public String getDirectChannelId(){ return directChannelId; }

    public String getSequenceNumber(){ return sequenceNumber; }

    public String getBranchId(){ return branchId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public java.sql.Timestamp getTranVerifiedDt(){ return tranVerifiedDt; }

    public String getPurAcctNum(){ return purAcctNum; }

    public String getPayeeId(){ return payeeId; }

    public String getAcctOccpCode(){ return acctOccpCode; }

    public String getAcctStatus(){ return acctStatus; }

    public String getStatus(){ return status; }

    public String getEntityType(){ return entityType; }

    public String getZoneCode(){ return zoneCode; }

    public String getCountryCode(){ return countryCode; }

    public java.sql.Timestamp getAcctOpenDate(){ return acctOpenDate; }

    public String getMerchantCateg(){ return merchantCateg; }

    public String getInstAlpha(){ return instAlpha; }

    public String getMsgSource(){ return msgSource; }

    public String getPayerIdTemp(){ return payerIdTemp; }

    public String getTerminalIpAddr(){ return terminalIpAddr; }

    public String getEntryVerifFlg(){ return entryVerifFlg; }

    public String getMcCode(){ return mcCode; }

    public String getEntryUser(){ return entryUser; }

    public String getAddEntityId2(){ return addEntityId2; }

    public String getAddEntityId1(){ return addEntityId1; }

    public java.sql.Timestamp getTxnDate(){ return txnDate; }

    public String getAddEntityId3(){ return addEntityId3; }

    public String getEventType(){ return eventType; }

    public String getUserId(){ return userId; }

    public String getGlSubHeadCode(){ return glSubHeadCode; }

    public String getPartTranSrlNum(){ return partTranSrlNum; }

    public String getAccountOwnership(){ return accountOwnership; }

    public Double getSanctionAmt(){ return sanctionAmt; }

    public String getKycStatus(){ return kycStatus; }

    public String getAgentId(){ return agentId; }

    public Double getAcctAMB(){ return acctAMB; }

    public String getTranType(){ return tranType; }

    public String getChannelDesc(){ return channelDesc; }

    public String getWorkClass(){ return workClass; }

    public String getVerifyUser(){ return verifyUser; }

    public String getBranchIdDesc(){ return branchIdDesc; }

    public String getCreDrPtran(){ return creDrPtran; }

    public String getTxnRefId(){ return txnRefId; }

    public String getSourceBank(){ return sourceBank; }

    public String getPayeeCity(){ return payeeCity; }

    public String getDevOwnerId(){ return devOwnerId; }

    public String getDeviceId(){ return deviceId; }

    public Double getAcctBal(){ return acctBal; }

    public String getStaffflag(){ return staffflag; }

    public String getTranId(){ return tranId; }

    public String getCardNo(){ return cardNo; }

    public String getEntityId(){ return entityId; }

    public String getRequstedAmt(){ return requstedAmt; }

    public String getEventSubType(){ return eventSubType; }

    public String getAccountId(){ return accountId; }

    public String getPlaceHolder(){ return placeHolder; }

    public String getDirectChannelControllerId(){ return directChannelControllerId; }

    public String getPayeeName(){ return payeeName; }

    public String getChannelType(){ return channelType; }

    public String getAcctName(){ return acctName; }

    public String getCustRefType(){ return custRefType; }

    public String getCustCardId(){ return custCardId; }

    public String getTranSrlNo(){ return tranSrlNo; }

    public Double getSolamt(){ return solamt; }

    public Double getAvlBal(){ return avlBal; }

    public String getEvtInducer(){ return evtInducer; }

    public java.sql.Timestamp getTxnValueDate(){ return txnValueDate; }

    public java.sql.Timestamp getTranPostedDt(){ return tranPostedDt; }

    public String getSchemeType(){ return schemeType; }

    public java.sql.Timestamp getCustDob(){ return custDob; }

    public String getAcctId(){ return acctId; }

    public String getAgentType(){ return agentType; }

    public java.sql.Timestamp getTranEntryDt(){ return tranEntryDt; }

    public String getCxCifId(){ return cxCifId; }

    public String getActionAtHost(){ return actionAtHost; }

    public String getSchemeCode(){ return schemeCode; }

    public String getCustRefId(){ return custRefId; }

    public String getDescription(){ return description; }

    public String getSourceOrDestActId(){ return sourceOrDestActId; }

    public String getEventTime(){ return eventTime; }

    public String getChannelId(){ return channelId; }

    public String getBillId(){ return billId; }

    public Double getRate(){ return rate; }

    public String getTxnStatus(){ return txnStatus; }

    public Double getTxnAmt(){ return txnAmt; }

    public Double getEffAvlBal(){ return effAvlBal; }

    public String getTranCategory(){ return tranCategory; }

    public Long getInstrumentId(){ return instrumentId; }

    public String getBalanceCur(){ return balanceCur; }

    public String getInstrmntNum(){ return instrmntNum; }

    public String getVerifyusrDesignation(){ return verifyusrDesignation; }

    public java.sql.Timestamp getInstrmntDate(){ return instrmntDate; }

    public String getAddEntityType1(){ return addEntityType1; }

    public Double getLedgerBal(){ return ledgerBal; }

    public String getEmployeeId(){ return employeeId; }

    public String getAddEntityType3(){ return addEntityType3; }

    public String getAcctSolId(){ return acctSolId; }

    public String getAddEntityType2(){ return addEntityType2; }

    public String getBenName(){ return benName; }

    public String getTxnAmtCur(){ return txnAmtCur; }

    public String getRefCurrency(){ return refCurrency; }

    public String getModeOprnCode(){ return modeOprnCode; }

    public String getMsgName(){ return msgName; }

    public String getCustId(){ return custId; }

    public java.sql.Timestamp getTranDate(){ return tranDate; }

    public String getHdrmkrs(){ return hdrmkrs; }

    public String getTranBrId(){ return tranBrId; }

    public String getRemitterName(){ return remitterName; }

    public String getInstrumentType(){ return instrumentType; }

    public Double getShadowBal(){ return shadowBal; }

    public Double getRefTranAmt(){ return refTranAmt; }

    public Long getEod(){ return eod; }

    public String getFirst4chqno(){ return first4chqno; }

    public String getRemarks(){ return remarks; }

    public String getIsStaff(){ return isStaff; }

    public String getJointCustId(){ return jointCustId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setException(String val){ this.exception = val; }
    public void setBeneficiaryName(String val){ this.beneficiaryName = val; }
    public void setZoneDate(java.sql.Timestamp val){ this.zoneDate = val; }
    public void setFcnrFlag(String val){ this.fcnrFlag = val; }
    public void setEntryusrDesignation(String val){ this.entryusrDesignation = val; }
    public void setBin(String val){ this.bin = val; }
    public void setOnlineBatch(String val){ this.onlineBatch = val; }
    public void setTranSubType(String val){ this.tranSubType = val; }
    public void setTxnRefType(String val){ this.txnRefType = val; }
    public void setTxnDrCr(String val){ this.txnDrCr = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setHostUserId(String val){ this.hostUserId = val; }
    public void setBalModiTime(java.sql.Timestamp val){ this.balModiTime = val; }
    public void setDestBank(String val){ this.destBank = val; }
    public void setDirectChannelId(String val){ this.directChannelId = val; }
    public void setSequenceNumber(String val){ this.sequenceNumber = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setTranVerifiedDt(java.sql.Timestamp val){ this.tranVerifiedDt = val; }
    public void setPurAcctNum(String val){ this.purAcctNum = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setAcctOccpCode(String val){ this.acctOccpCode = val; }
    public void setAcctStatus(String val){ this.acctStatus = val; }
    public void setStatus(String val){ this.status = val; }
    public void setEntityType(String val){ this.entityType = val; }
    public void setZoneCode(String val){ this.zoneCode = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setAcctOpenDate(java.sql.Timestamp val){ this.acctOpenDate = val; }
    public void setMerchantCateg(String val){ this.merchantCateg = val; }
    public void setInstAlpha(String val){ this.instAlpha = val; }
    public void setMsgSource(String val){ this.msgSource = val; }
    public void setPayerIdTemp(String val){ this.payerIdTemp = val; }
    public void setTerminalIpAddr(String val){ this.terminalIpAddr = val; }
    public void setEntryVerifFlg(String val){ this.entryVerifFlg = val; }
    public void setMcCode(String val){ this.mcCode = val; }
    public void setEntryUser(String val){ this.entryUser = val; }
    public void setAddEntityId2(String val){ this.addEntityId2 = val; }
    public void setAddEntityId1(String val){ this.addEntityId1 = val; }
    public void setTxnDate(java.sql.Timestamp val){ this.txnDate = val; }
    public void setAddEntityId3(String val){ this.addEntityId3 = val; }
    public void setEventType(String val){ this.eventType = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setGlSubHeadCode(String val){ this.glSubHeadCode = val; }
    public void setPartTranSrlNum(String val){ this.partTranSrlNum = val; }
    public void setAccountOwnership(String val){ this.accountOwnership = val; }
    public void setSanctionAmt(Double val){ this.sanctionAmt = val; }
    public void setKycStatus(String val){ this.kycStatus = val; }
    public void setAgentId(String val){ this.agentId = val; }
    public void setAcctAMB(Double val){ this.acctAMB = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setChannelDesc(String val){ this.channelDesc = val; }
    public void setWorkClass(String val){ this.workClass = val; }
    public void setVerifyUser(String val){ this.verifyUser = val; }
    public void setBranchIdDesc(String val){ this.branchIdDesc = val; }
    public void setCreDrPtran(String val){ this.creDrPtran = val; }
    public void setTxnRefId(String val){ this.txnRefId = val; }
    public void setSourceBank(String val){ this.sourceBank = val; }
    public void setPayeeCity(String val){ this.payeeCity = val; }
    public void setDevOwnerId(String val){ this.devOwnerId = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setAcctBal(Double val){ this.acctBal = val; }
    public void setStaffflag(String val){ this.staffflag = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setCardNo(String val){ this.cardNo = val; }
    public void setEntityId(String val){ this.entityId = val; }
    public void setRequstedAmt(String val){ this.requstedAmt = val; }
    public void setEventSubType(String val){ this.eventSubType = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setPlaceHolder(String val){ this.placeHolder = val; }
    public void setDirectChannelControllerId(String val){ this.directChannelControllerId = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setChannelType(String val){ this.channelType = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustRefType(String val){ this.custRefType = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setTranSrlNo(String val){ this.tranSrlNo = val; }
    public void setSolamt(Double val){ this.solamt = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setEvtInducer(String val){ this.evtInducer = val; }
    public void setTxnValueDate(java.sql.Timestamp val){ this.txnValueDate = val; }
    public void setTranPostedDt(java.sql.Timestamp val){ this.tranPostedDt = val; }
    public void setSchemeType(String val){ this.schemeType = val; }
    public void setCustDob(java.sql.Timestamp val){ this.custDob = val; }
    public void setAcctId(String val){ this.acctId = val; }
    public void setAgentType(String val){ this.agentType = val; }
    public void setTranEntryDt(java.sql.Timestamp val){ this.tranEntryDt = val; }
    public void setCxCifId(String val){ this.cxCifId = val; }
    public void setActionAtHost(String val){ this.actionAtHost = val; }
    public void setSchemeCode(String val){ this.schemeCode = val; }
    public void setCustRefId(String val){ this.custRefId = val; }
    public void setDescription(String val){ this.description = val; }
    public void setSourceOrDestActId(String val){ this.sourceOrDestActId = val; }
    public void setEventTime(String val){ this.eventTime = val; }
    public void setChannelId(String val){ this.channelId = val; }
    public void setBillId(String val){ this.billId = val; }
    public void setRate(Double val){ this.rate = val; }
    public void setTxnStatus(String val){ this.txnStatus = val; }
    public void setTxnAmt(Double val){ this.txnAmt = val; }
    public void setEffAvlBal(Double val){ this.effAvlBal = val; }
    public void setTranCategory(String val){ this.tranCategory = val; }
    public void setInstrumentId(Long val){ this.instrumentId = val; }
    public void setBalanceCur(String val){ this.balanceCur = val; }
    public void setInstrmntNum(String val){ this.instrmntNum = val; }
    public void setVerifyusrDesignation(String val){ this.verifyusrDesignation = val; }
    public void setInstrmntDate(java.sql.Timestamp val){ this.instrmntDate = val; }
    public void setAddEntityType1(String val){ this.addEntityType1 = val; }
    public void setLedgerBal(Double val){ this.ledgerBal = val; }
    public void setEmployeeId(String val){ this.employeeId = val; }
    public void setAddEntityType3(String val){ this.addEntityType3 = val; }
    public void setAcctSolId(String val){ this.acctSolId = val; }
    public void setAddEntityType2(String val){ this.addEntityType2 = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setTxnAmtCur(String val){ this.txnAmtCur = val; }
    public void setRefCurrency(String val){ this.refCurrency = val; }
    public void setModeOprnCode(String val){ this.modeOprnCode = val; }
    public void setMsgName(String val){ this.msgName = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setTranDate(java.sql.Timestamp val){ this.tranDate = val; }
    public void setHdrmkrs(String val){ this.hdrmkrs = val; }
    public void setTranBrId(String val){ this.tranBrId = val; }
    public void setRemitterName(String val){ this.remitterName = val; }
    public void setInstrumentType(String val){ this.instrumentType = val; }
    public void setShadowBal(Double val){ this.shadowBal = val; }
    public void setRefTranAmt(Double val){ this.refTranAmt = val; }
    public void setEod(Long val){ this.eod = val; }
    public void setFirst4chqno(String val){ this.first4chqno = val; }
    public void setRemarks(String val){ this.remarks = val; }
    public void setIsStaff(String val){ this.isStaff = val; }
    public void setJointCustId(String val){ this.jointCustId = val; }

    /* Custom Getters*/
    @JsonIgnore
    public String getBIN(){ return bin; }
    @JsonIgnore
    public String getBranch_Id(){ return branchId; }
    @JsonIgnore
    public String getCountry_code(){ return countryCode; }
    @JsonIgnore
    public String getUser_id(){ return userId; }
    @JsonIgnore
    public String getDev_owner_id(){ return devOwnerId; }
    @JsonIgnore
    public String getCust_card_id(){ return custCardId; }
    @JsonIgnore
    public String getAcct_id(){ return acctId; }
    @JsonIgnore
    public String getCust_id(){ return custId; }
    @JsonIgnore
    public java.sql.Timestamp getTran_date(){ return tranDate; }


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
       // ICXLog cxLog = CXLog.fenter("derive.FT_AccountTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String branchKey = h.getCxKeyGivenHostKey(WorkspaceName.BRANCH, getHostId(), this.tranBrId);
        wsInfoSet.add(new WorkspaceInfo("Branch", branchKey));

        String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctId);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        if (custId != null && !custId.equalsIgnoreCase("")) {
            String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
            wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        }

        String tran_remarks = getRemarks().toLowerCase();

        // Added by bhagya for Scenario 37 on 26-02-2020
        // Strated from,
	    if (acctId.equals("8263256000000012") || acctId.equals("8263256000000010")) {
            if (tran_remarks.startsWith("ben a/c no.")) {
                String txn_rmks = tran_remarks.substring(11);
                String hostNoncustKey2 = getAcctId() + txn_rmks;
                String noncustomerKey2 = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), hostNoncustKey2);
                wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey2));
        }
    }
        //Ended here
        
        else if (tran_remarks.startsWith("ben a/c no")) {
                //System.out.println("TRAN REMARKS [ " + tran_remarks + " ] ");
                String noncustomerKey1 = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), tran_remarks);
                wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey1));
            }

        else {
            String hostNoncustKey =  getInstrmntNum()  + getAcctId() ;
            String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), hostNoncustKey);
            wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
        }
            String hostTranKey = getTranId() + getTranDate();
            String transactionKey = h.getCxKeyGivenHostKey(WorkspaceName.TRANSACTION, getHostId(), hostTranKey);
            wsInfoSet.add(new WorkspaceInfo("Transaction", transactionKey));

        String userKey= h.getCxKeyGivenHostKey(WorkspaceName.USER, getHostId(), this.agentId);
        wsInfoSet.add(new WorkspaceInfo("User", userKey));

       // cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        json.put("tran_sub_type",getTranSubType());
        json.put("part_tran_srl_num",getPartTranSrlNum());
        json.put("tran_type",getTranType());
        json.put("account_id",getAccountId());
        json.put("tran_srl_no",getTranSrlNo());
        json.put("solamt",getSolamt());
        json.put("source_or_dest_act_id",getSourceOrDestActId());
        json.put("txn_amt",getTxnAmt());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_AccountTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "AccountTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        json.put("country_code",getCountryCode());
        json.put("acct_open_date",getAcctOpenDate());
        json.put("dev_owner_id",getDevOwnerId());
        json.put("cust_card_id",getCustCardId());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
