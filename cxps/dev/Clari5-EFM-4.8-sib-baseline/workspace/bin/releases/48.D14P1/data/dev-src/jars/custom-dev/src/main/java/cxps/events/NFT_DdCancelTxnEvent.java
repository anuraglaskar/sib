// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
//import clari5.platform.logger.CXLog;
//import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_DD_CANCEL_TXN", Schema="rice")
public class NFT_DdCancelTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String ddNum;
       @Field public java.sql.Timestamp ddCancelDate;
       @Field(size=32) public Double ddCancelAmt;
       @Field public java.sql.Timestamp ddIssueDate;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=50) public String ddIssueCustid;
       @Field(size=50) public String ddCancelAcct;
       @Field public java.sql.Timestamp eventts;
       @Field(size=50) public String ddIssueAcct;
       @Field(size=32) public Double ddIssueAmt;
       @Field(size=50) public String ddCancelCustid;
       @Field(size=50) public String eventName;
       @Field(size=50) public String eventsubtype;
       @Field(size=50) public String eventtype;
       @Field(size=50) public String hostId;


    @JsonIgnore
    public ITable<NFT_DdCancelTxnEvent> t = AEF.getITable(this);

	public NFT_DdCancelTxnEvent(){}

    public NFT_DdCancelTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("DdCancelTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setDdNum(json.getString("DD_num"));
            setDdCancelDate(EventHelper.toTimestamp(json.getString("DD_cancel_date")));
            setDdCancelAmt(EventHelper.toDouble(json.getString("DD_cancel_amt")));
            setDdIssueDate(EventHelper.toTimestamp(json.getString("DD_issue_date")));
            setSysTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setDdIssueCustid(json.getString("DD_issue_custid"));
            setDdCancelAcct(json.getString("DD_cancel_acct"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setDdIssueAcct(json.getString("DD_Issue_acct"));
            setDdIssueAmt(EventHelper.toDouble(json.getString("DD_issue_amt")));
            setDdCancelCustid(json.getString("Dd_cancel_custid"));
            setEventName(json.getString("event-name"));
            setEventsubtype(json.getString("eventsubtype"));
            setEventtype(json.getString("eventtype"));
            setHostId(json.getString("host-id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "DC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getDdNum(){ return ddNum; }

    public java.sql.Timestamp getDdCancelDate(){ return ddCancelDate; }

    public Double getDdCancelAmt(){ return ddCancelAmt; }

    public java.sql.Timestamp getDdIssueDate(){ return ddIssueDate; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getDdIssueCustid(){ return ddIssueCustid; }

    public String getDdCancelAcct(){ return ddCancelAcct; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getDdIssueAcct(){ return ddIssueAcct; }

    public Double getDdIssueAmt(){ return ddIssueAmt; }

    public String getDdCancelCustid(){ return ddCancelCustid; }

    public String getEventName(){ return eventName; }

    public String getEventsubtype(){ return eventsubtype; }

    public String getEventtype(){ return eventtype; }

    public String getHostId(){ return hostId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setDdNum(String val){ this.ddNum = val; }
    public void setDdCancelDate(java.sql.Timestamp val){ this.ddCancelDate = val; }
    public void setDdCancelAmt(Double val){ this.ddCancelAmt = val; }
    public void setDdIssueDate(java.sql.Timestamp val){ this.ddIssueDate = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setDdIssueCustid(String val){ this.ddIssueCustid = val; }
    public void setDdCancelAcct(String val){ this.ddCancelAcct = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setDdIssueAcct(String val){ this.ddIssueAcct = val; }
    public void setDdIssueAmt(Double val){ this.ddIssueAmt = val; }
    public void setDdCancelCustid(String val){ this.ddCancelCustid = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setEventsubtype(String val){ this.eventsubtype = val; }
    public void setEventtype(String val){ this.eventtype = val; }
    public void setHostId(String val){ this.hostId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        //ICXLog cxLog = CXLog.fenter("derive.NFT_DdCancelTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.ddNum);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));

       // cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_DdCancelTxn");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "DdCancelTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}