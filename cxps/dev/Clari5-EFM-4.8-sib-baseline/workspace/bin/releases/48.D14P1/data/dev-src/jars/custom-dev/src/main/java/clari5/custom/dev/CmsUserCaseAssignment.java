package clari5.custom.dev;

import clari5.platform.applayer.Clari5;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.platform.rdbms.RDBMS;

import javax.print.DocFlavor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Yashaswi
 */
public class CmsUserCaseAssignment {

    public static Map<String, Map<String, Integer>> channelUserGroupMap;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public Map<String, Integer> process(String fact_name) throws Exception {
        CxConnection con = getConnection();
        Map<String, String> user_channel = getUserChannel(con);
        //System.out.println("user_channel map data: " + user_channel);
        Map<String, String> fact_channel = getFactChannel(con);
        //System.out.println("fact_channel map data: " + fact_channel);

       Map<String,Integer> fact_user = getFactuser(con,fact_name);

        Map<String, Integer> userGroupCount = getUserGroupMap(user_channel, fact_channel, fact_name);
        //System.out.println("userGroupCount map data: " + userGroupCount);
        return userGroupCount;


    }

    public Map<String,Integer> getFactuser(CxConnection connection,String factname)
    {
        return null;
    }

    public Map<String, String> getUserChannel(CxConnection connection) throws Exception {
        HashMap<String, String> chanel_user = new HashMap<>();
        connection.query(this.userChannelSelectQuery(connection.getDbType()), resultSet -> {
            while (resultSet.next()) {
                chanel_user.put(resultSet.getString("user_id"), resultSet.getString("channel_id"));
            }
        });
        return chanel_user;
    }



    public Map<String, String> getFactChannel(CxConnection connection) throws Exception {
        HashMap<String, String> fact_channel = new HashMap<>();
        connection.query(this.factChannelSelectQuery(connection.getDbType()), resultSet -> {
            while (resultSet.next()) {
                fact_channel.put(resultSet.getString("fact_name"), resultSet.getString(""));
            }
        });
        try {
            connection.close();
            System.out.println("data inserted closing connection...");
        } catch (Exception ex) {
        }
        return fact_channel;
    }

    public Map<String, Integer> getUserGroupMap(Map<String, String> user_channel, Map<String, String> fact_channel, String factName) {
        if (channelUserGroupMap == null) {
            channelUserGroupMap = new HashMap<>();
            Map<String, Integer> remUserCount = new HashMap<>();
            Map<String, Integer> noncustUserCount = new HashMap<>();
            Map<String, Integer> branchUserCount = new HashMap<>();
            Map<String, Integer> userUserCount = new HashMap<>();
            Map<String, Integer> defaultUserCount = new HashMap<>();


            for (Map.Entry<String, String> entry : user_channel.entrySet()) {
                String userKey = entry.getKey();
                String userValue = entry.getValue();
                String[] userValueSplit = userValue.split(",");
                if (userValueSplit.length>1) {
                    for (int i = 0; i < userValueSplit.length; i++) {
                        //System.out.println("data inside userValueSplit value: "+userValueSplit[i]);
                        //System.out.println("data inside userValueSplit key: "+userKey);

                        if (userValueSplit[i].equalsIgnoreCase("REM")) {
                            remUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("NC")) {
                            noncustUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("BR")) {
                            branchUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("US")) {
                            userUserCount.put(userKey, 0);
                        }
                        if (userValueSplit[i].equalsIgnoreCase("DF")) {
                            defaultUserCount.put(userKey, 0);
                        }

                    }
                } else {
                    //System.out.println("data inside userValue value: "+userValue);
                    //System.out.println("data inside userValue key: "+userKey);

                    if (userValue.equalsIgnoreCase("REM")) {
                        remUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("NC")) {
                        noncustUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("BR")) {
                        branchUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("US")) {
                        userUserCount.put(userKey, 0);
                    }
                    if (userValue.equalsIgnoreCase("DF")) {
                        defaultUserCount.put(userKey, 0);
                    }

                }
            }
            channelUserGroupMap.put("REM", remUserCount);
            channelUserGroupMap.put("NC", noncustUserCount);
            channelUserGroupMap.put("BR", branchUserCount);
            channelUserGroupMap.put("US", userUserCount);
            channelUserGroupMap.put("DF", defaultUserCount);
        }

        String channel = null;
        if (fact_channel.containsKey(factName)) {
            channel = fact_channel.get(factName);

            //System.out.println("channel from fact_name is: " + channel);
        }
        Map<String, Integer> finalCountMap = new HashMap<>();

        if (channelUserGroupMap.containsKey(channel)) {
            finalCountMap = channelUserGroupMap.get(channel);
        }
        //System.out.println("inside final count map: " + finalCountMap);
        return finalCountMap;
    }

    public static CxConnection getConnection() {
        //Clari5.batchBootstrap("test", "efm-clari5.conf");
        RDBMS rdbms = (RDBMS) Clari5.rdbms();
        if (rdbms == null) throw new RuntimeException("RDBMS as a resource is unavailable");
        return rdbms.getConnection();
        /*RDBMSSession session = null;
        session = Rdbms.getAppSession();
        System.out.println("session is: "+session);
        CxConnection con = session.getCxConnection();*/
        /*if (con == null) throw new RuntimeException("not able acquire connection from RDBMS");
        return con;*/
    }

    public String factChannelTableName() {
        return "fact_channel";
    }

    public String userChannelTableName() {
        return "user_channel";
    }
    public String branchChannelTableName() {
        return "branch_user";
    }


    public String userChannelSelectQuery(DbTypeEnum dbTypeEnum) {
        switch (dbTypeEnum) {
            case ORACLE:
                return "SELECT user_id, channel_id" +
                        " from " + this.userChannelTableName();
            case SQLSERVER:
                return "SELECT [user_id], [channel_id]" +
                        " from " + this.userChannelTableName();
        }
        return null;
    }

    public String branchChannelSelectQuery(DbTypeEnum dbTypeEnum) {
        switch (dbTypeEnum) {
            case ORACLE:
                return "SELECT br_code, user_id" +
                        " from " + this.branchChannelTableName();
            case SQLSERVER:
                return "SELECT [user_id], [br_code]" +
                        " from " + this.branchChannelTableName();
        }
        return null;
    }

    public String factChannelSelectQuery(DbTypeEnum dbTypeEnum) {
        switch (dbTypeEnum) {
            case ORACLE:
                return "SELECT fact_name, channel_mapping" +
                        " from " + this.factChannelTableName();
            case SQLSERVER:
                return "SELECT [fact_name], [channel_mapping]" +
                        " from " + this.factChannelTableName();
        }
        return null;
    }


    public List processEntity(String entityid) throws Exception {
        CxConnection con = getConnection();
        Map<String, String> map = getBranchUser(con);
        System.out.println("branch user map data: " + map);
        String brCode = getBrCodeFromDda(con, entityid);
        System.out.println("brcode is: " + brCode);
        List list = new ArrayList();
        if (brCode != null) {
            Map<String, Integer> userGroupCount = getUserGroupMap(map, brCode);
            System.out.println("userGroupcount map is: " + userGroupCount);
            list.add(userGroupCount);
            list.add(brCode);
        }
        System.out.println("List of br code and user group map: "+list);
        return list;
    }

    public Map<String, String> getBranchUser(CxConnection connection) throws Exception {
        HashMap<String, String> branch_user = new HashMap<>();
        connection.query(this.branchChannelSelectQuery(connection.getDbType()), resultSet -> {
            while (resultSet.next()) {
                branch_user.put(resultSet.getString("BR_CODE"), resultSet.getString("USER_ID"));
            }
        });
        return branch_user;
    }

    public String getBrCodeFromDda(CxConnection connection, String entityId) {
        String brCode = null;
        String sql = "select acctBrID from DDA where acctId = '" + entityId + "' OR acctCustID = '"+entityId+"'";
        try( PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                brCode = rs.getString("acctBrID");
            }
            return brCode;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, Integer> getUserGroupMap(Map<String, String> branch_user, String brCode) {

        for (Map.Entry<String, String> entry : branch_user.entrySet()) {
            String userId = entry.getValue();
            String userBrcode = entry.getKey();
            if (channelUserGroupMap == null) {
                channelUserGroupMap = new HashMap<>();
                String[] splitUserId = userId.split("-");
                if (splitUserId.length > 1) {
                    Map<String, Integer> userCount = new HashMap<>();
                    for (int i = 0; i < splitUserId.length; i++) {
                        userCount.put(splitUserId[i], 0);
                    }
                    channelUserGroupMap.put(userBrcode, userCount);
                } else {
                    Map<String, Integer> userCount = new HashMap<>();
                    userCount.put(userId, 0);
                    channelUserGroupMap.put(userBrcode, userCount);
                }
            } else {
                if (!channelUserGroupMap.containsKey(userBrcode)) {
                    String[] splitUserId = userId.split("-");
                    if (splitUserId.length > 1) {
                        Map<String, Integer> userCount = new HashMap<>();
                        for (int i = 0; i < splitUserId.length; i++) {
                            userCount.put(splitUserId[i], 0);
                        }
                        channelUserGroupMap.put(userBrcode, userCount);
                    } else {
                        Map<String, Integer> userCount = new HashMap<>();
                        userCount.put(userId, 0);
                        channelUserGroupMap.put(userBrcode, userCount);
                    }
                } else {
                    channelUserGroupMap.get(userBrcode);
                }
            }
        }
        Map<String, Integer> finalMap = new HashMap<>();
        if (channelUserGroupMap.containsKey(brCode)) {
            finalMap = channelUserGroupMap.get(brCode);
        }
        System.out.println("before returning final Map: "+finalMap);
        return finalMap;
    }
}
