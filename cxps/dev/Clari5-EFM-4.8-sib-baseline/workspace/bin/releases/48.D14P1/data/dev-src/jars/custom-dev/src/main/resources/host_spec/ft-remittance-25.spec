cxps.noesis.glossary.entity.ft-remittance-25{
       db-name = FT_REMITTANCE_25
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
       attributes = [
		{ name = CL5_FLG, column = CL5_FLG, type = "string:200" }
		{ name = SYSTEM, column = SYSTEM, type = "string:2000" }
		{ name = REM_TYPE, column = REM_TYPE, type = "string:2000" }
		{ name = BRANCH, column = BRANCH, type = "string:1000" }
		{ name = ACCOUNT_CATEGORY, column = ACCOUNT_CATEGORY, type = "string:2000" }
		 { name = TYPE, column = TYPE, type = "string:1000" }
		{ name = MSGTYPE, column = MSGTYPE, type = "string:1000" }
		{ name = SNO, column = SNO, type = "string:2000" }
		{ name = TRAN_REF_NO, column = TRAN_REF_NO, type = "string:2000" }
		{ name = TRAN_CURR, column = TRAN_CURR, type = "string:2000" }
		{ name = TRAN_AMT, column = TRAN_AMT, type = "number:32,2" }
		{ name = USD_EQV_AMT, column = USD_EQV_AMT, type = "number:32,2" }
		{ name = INR_AMOUNT, column = INR_AMOUNT, type = "number:32,2" }
		{ name = PURPOSE_CODE, column = PURPOSE_CODE, type = "string:2000" }
		{ name = PURPOSE_DESC, column = PURPOSE_DESC, type = "string:2000" }
		{ name = REM_CUST_ID, column = REM_CUST_ID, type = "string:2000" }
		 { name = REM_NAME, column = REM_NAME, type ="string:2000" }
		{ name = REM_ADD1, column = REM_ADD1, type = "string:2000" }	
		{ name = REM_ADD2, column = REM_ADD2, type = "string:2000" }
		{ name = REM_ADD3, column = REM_ADD3, type = "string:2000" }
		{ name = REM_CITY, column = REM_CITY, type = "string:2000" }
		 { name = REM_CNTRY_CODE, column = REM_CNTRY_CODE, type = "string:2000"}
		{ name = BEN_CUST_ID, column = BEN_CUST_ID, type = "string:2000" }
		{ name = BEN_NAME, column = BEN_NAME, type = "string:2000"}
		{ name = BEN_ADD1, column = BEN_ADD1, type = "string:2000"}
		{ name = BEN_ADD2, column = BEN_ADD2, type = "string:2000"}
		{ name = BEN_ADD3, column = BEN_ADD3, type = "string:2000"}
		{ name = BEN_CITY, column = BEN_CITY, type = "string:2000" }
		{ name = BEN_CNTRY_CODE, column = BEN_CNTRY_CODE, type = "string:2000" }
		{ name = CLIENT_ACC_NO, column = CLIENT_ACC_NO, type = "string:2000" }
		{ name = CPTY_AC_NO, column = CPTY_AC_NO, type = "string:2000" }
		{ name = BEN_ACCT_NO, column = BEN_ACCT_NO, type = "string:2000" }	
		{ name = BEN_BIC, column = BEN_BIC, type = "string:2000" }
		{ name = REM_ACCT_NO, column = REM_ACCT_NO, type = "string:2000"}
		{ name = REM_BIC, column = REM_BIC, type = "string:2000" }
		{ name = TRN_DATE, column = TRN_DATE, type = "string:2000" }
		{ name = EVENT_ID, column = EVENT_ID, type = "string:2000" }
		{ name = ID, column = ID, type = "string:2000" }
		{ name = HOST_ID, column = HOST_ID, type = "string:2000"}
               ]
		indexes {
               FT_REMITTANCE_25_IDX1 : [ CL5_FLG ],
		SFMS_BENEACCTNO_IDX :[BEN_ACCT_NO],
		SFMS_BENENAME_IDX : [BEN_NAME],
		SFMS_BENE_CUSTID_IDX : [BEN_CUST_ID],
		SFMS_REMACCTUNO_IDX : [REM_ACCT_NO],
		SFMS_REMENAME_IDX : [REM_NAME],
		SFMS_REME_CUSTID_IDX : [REM_CUST_ID]

        }
       }

