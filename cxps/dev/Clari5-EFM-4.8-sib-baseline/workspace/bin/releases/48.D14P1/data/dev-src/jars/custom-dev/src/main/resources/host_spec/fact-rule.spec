cxps.noesis.glossary.entity.fact-rule {
        db-name = FACT_RULE
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = FACT_CODE, column = FACT_CODE, type = "string:10", key=true }
                { name = RULE, column = RULE, type = "string:20" }
                     ]
        }



