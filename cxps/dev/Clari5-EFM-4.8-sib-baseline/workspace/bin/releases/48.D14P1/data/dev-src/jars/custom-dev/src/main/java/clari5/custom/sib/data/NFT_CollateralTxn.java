package clari5.custom.sib.data;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class NFT_CollateralTxn extends ITableData {
	private String tableName = "COLLATERAL_MASTER";
	private String event_type = "nft_collateraltxn";
    private String ID;
    private String LOANACCOUNT;
    private String COLLATERAL_ID;
    private String COLLATERAL_STATUS;
	private String COLLATERAL_ADDED_DATE;
	private String COLLATERAL_DELETED_DATE;
	private String ACCT_STATUS;
	private String SYS_TIME;


	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getLOANACCOUNT() {
		return LOANACCOUNT;
	}

	public void setLOANACCOUNT(String LOANACCOUNT) {
		this.LOANACCOUNT = LOANACCOUNT;
	}

	public String getCOLLATERAL_ID() {
		return COLLATERAL_ID;
	}

	public void setCOLLATERAL_ID(String COLLATERAL_ID) {
		this.COLLATERAL_ID = COLLATERAL_ID;
	}

	public String getCOLLATERAL_STATUS() {
		return COLLATERAL_STATUS;
	}

	public void setCOLLATERAL_STATUS(String COLLATERAL_STATUS) {
		this.COLLATERAL_STATUS = COLLATERAL_STATUS;
	}

	public String getCOLLATERAL_ADDED_DATE() {
		return COLLATERAL_ADDED_DATE;
	}

	public void setCOLLATERAL_ADDED_DATE(String COLLATERAL_ADDED_DATE) {
		this.COLLATERAL_ADDED_DATE = COLLATERAL_ADDED_DATE;
	}

	public String getCOLLATERAL_DELETED_DATE() {
		return COLLATERAL_DELETED_DATE;
	}

	public void setCOLLATERAL_DELETED_DATE(String COLLATERAL_DELETED_DATE) {
		this.COLLATERAL_DELETED_DATE = COLLATERAL_DELETED_DATE;
	}

	public String getACCT_STATUS() {
		return ACCT_STATUS;
	}

	public void setACCT_STATUS(String ACCT_STATUS) {
		this.ACCT_STATUS = ACCT_STATUS;
	}

	public String getSYS_TIME() {
		return SYS_TIME;
	}

	public void setSYS_TIME(String SYS_TIME) {
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis()));
		this.SYS_TIME = timeStamp;
	}

	@Override
	public String getTableName() {
		return tableName;
	}

}
