package clari5.custom.sib.builder;

import org.json.JSONObject;
import cxps.apex.utils.CxpsLogger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.Timestamp;
import java.util.HashMap;

import clari5.custom.sib.builder.data.Duplicates;
import clari5.custom.sib.builder.data.Message;
import clari5.custom.sib.config.BepCon;
import clari5.custom.sib.data.ITableData;
import clari5.custom.sib.data.bootstrap.TableMap;
import clari5.custom.sib.builder.data.Message;
import clari5.custom.sib.data.bootstrap.MessageMappingManager;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.io.BufferedWriter;

public class EventBuilder {
	public static CxpsLogger logger = CxpsLogger.getLogger(EventBuilder.class);
	private static int eventIdDuplicator = 0;
	protected static TableMap tblJsonMappingConfig;
	private static HashMap<String,String> tableToClassMap;


	static {
		try {
			System.out.println("messagemap initialized ");
			tblJsonMappingConfig = TableMap.getTableMap();
			if(tblJsonMappingConfig == null)
				System.out.println("ERROR : tblJsonMappingConfig is null.");
			tableToClassMap=BepCon.getConfig().getTableClassMap();
		}catch (Exception e){
			System.out.println("Error while loading tbl-json-mapping config file");
		}
	}


	public boolean process(ITableData row) throws Exception {
		JSONObject jrow = new JSONObject(row);
		logger.debug(" Json row generated is "+jrow.toString());
		String tableName = jrow.getString("tableName");
		System.out.println("table name is "+tableName);
		MsgMetaData msgmetas = tblJsonMappingConfig.get(tableName.toUpperCase());
		if(msgmetas==null){
			logger.error(" Could not load table to json mapping properties for "+tableName);
			throw new Exception("Could not load table to json mapping properties for "+tableName);
		}

		Map.Entry<String,Message> msgmeta=msgmetas.entrySet().iterator().next();

			String eventKey = msgmeta.getKey();
			logger.info("Event Key ["+eventKey+" ]");
			String eventName=tableToClassMap.get(eventKey);
			logger.info("Event Name is "+eventName);
			eventName=eventName.toLowerCase();
			logger.info("Table name ["+eventKey+" Event name ["+eventName+"]");
			//if (tm.shouldProceed(jrow, eventName)) {
				//Duplicates duplicates = new Duplicates();
				Map<String, String> msg = msgmeta.getValue().process(jrow);
				JSONObject original = new JSONObject(msg);
				// The duplicate rows requirement came later, hence could not design properly. Refactor this later.
				// Creating multiple lists since duplication occurs very rare.
				// Also addAll might be a heavy operation even if the size is 0.
					long event_ts = System.currentTimeMillis();
					String event_id= eventName+System.nanoTime();
					JSONObject finalEvent = processJson(original, eventName,event_id);
					// Remove it later.
					logger.info(" Final Event is generated is "+finalEvent);
					try {
						String fileName=BepCon.getConfig().getFiledebugoutputpath()+"/"+event_id+".msg";
						Files.write(Paths.get(fileName), finalEvent.toString().getBytes());
						//Files.write(Paths.get(BepCon.getConfig().getFiledebugoutputpath()), finalEvent.toString().getBytes());
						logger.info("Message for event id"+event_id+"is written successfully in path "+fileName);
					}catch (Exception e){
						System.out.println("Debug path for message is not configured please configure else event log file will not be written");
					}

					boolean status = Clari5GatewayManager.send(event_id,finalEvent, event_ts);
					return status;
			//	}



	}
	private JSONObject processJson(JSONObject msg, String eventName,String event_id) throws Exception {
		JSONObject j = new JSONObject();
		j.put("event-name", eventName);
		j.put("event-id", event_id);

		String[] eventdetails = eventName.split("\\_");
		j.put("eventtype", eventdetails[0]);
		j.put("eventsubtype", eventdetails[1]);
		if(eventName.equalsIgnoreCase("ft_remittancetxn"))
		{
			msg.put("sys_time", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
			msg.put("eventts", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));

		}
		else {
			msg.put("sys-time", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
			msg.put("eventts", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS").format(new Timestamp(System.currentTimeMillis())));
		}
		if(eventName.equalsIgnoreCase("nft_loanintratetxn"))
		{
			msg.put("Flag",Calculate.getFLAG(msg.getString("DepositAcct_Int_Rate"),msg.getString("LoanAcct_Int_Rate")));
		}
		msg.put("host-id","F");
		// Adding lots of replace to avoid any replace within data itself.
		j.put("msgBody", msg.toString().replace("{\"", "{'").replace("\",", "',").replace(",\"", ",'").
				replace("\"}", "'}").replace(":\"", ":'").replace("\":", "':"));
                		
		return j;
	}
	private String genEventId() throws Exception {
		++eventIdDuplicator;
		if(eventIdDuplicator == BepCon.getConfig().getSimultaneousRowCount()) {
			eventIdDuplicator = 0;
		}
		String eventId = String.valueOf(Instant.now().toEpochMilli()) + "-" + eventIdDuplicator;
		return eventId;
	}
}
