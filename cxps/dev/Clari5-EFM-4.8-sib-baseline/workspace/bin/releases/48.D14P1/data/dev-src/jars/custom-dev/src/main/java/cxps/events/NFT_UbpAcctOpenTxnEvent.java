// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
//import clari5.platform.logger.CXLog;
//import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_UBP_ACCT_OPEN_TXN", Schema="rice")
public class NFT_UbpAcctOpenTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String acctBranchId;
       @Field(size=50) public String eventName;
       @Field(size=50) public String custSolId;
       @Field(size=50) public String eventsubtype;
       @Field(size=50) public String eventtype;
       @Field(size=50) public String hostId;
       @Field public java.sql.Timestamp sysTime;
       @Field(size=50) public String custId;
       @Field public java.sql.Timestamp eventts;
       @Field(size=50) public String acctNo;


    @JsonIgnore
    public ITable<NFT_UbpAcctOpenTxnEvent> t = AEF.getITable(this);

	public NFT_UbpAcctOpenTxnEvent(){}

    public NFT_UbpAcctOpenTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("UbpAcctOpenTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setAcctBranchId(json.getString("Acct_Branch_id"));
            setEventName(json.getString("event-name"));
            setCustSolId(json.getString("Cust_Sol_Id"));
            setEventsubtype(json.getString("eventsubtype"));
            setEventtype(json.getString("eventtype"));
            setHostId(json.getString("host-id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setCustId(json.getString("Cust_Id"));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setAcctNo(json.getString("AcctNo"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "UB"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getAcctBranchId(){ return acctBranchId; }

    public String getEventName(){ return eventName; }

    public String getCustSolId(){ return custSolId; }

    public String getEventsubtype(){ return eventsubtype; }

    public String getEventtype(){ return eventtype; }

    public String getHostId(){ return hostId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public String getCustId(){ return custId; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getAcctNo(){ return acctNo; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setAcctBranchId(String val){ this.acctBranchId = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setCustSolId(String val){ this.custSolId = val; }
    public void setEventsubtype(String val){ this.eventsubtype = val; }
    public void setEventtype(String val){ this.eventtype = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setAcctNo(String val){ this.acctNo = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
       // ICXLog cxLog = CXLog.fenter("derive.NFT_UbpAcctOpenTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.acctNo);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

        //cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_UbpAcctOpenTxn");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "UbpAcctOpenTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}