cxps.noesis.glossary.entity.sol-avg-amt-tbl {
        db-name = SOL_AVG_AMT_TBL
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
		        { name = SOL_ID, column = SOL_ID, type = "string:200" }
		        { name = SUM_TRAN_AMT, column = SUM_TRAN_AMT, type = "number:32,2"}


                ]
        }

