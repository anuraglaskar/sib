package clari5.custom.dev.service.util;

import clari5.platform.fileq.Clari5Payload;
import clari5.platform.logger.CxpsLogger;
import cxps.apex.utils.StringUtils;
import cxps.eoi.Incident;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Author: Shashank Devisetty
 * Organization: CustomerXPs
 * Created On: 10/05/2019
 * Reviewed By:
 */

public class EventServiceUtility {
    private static final CxpsLogger logger = CxpsLogger.getLogger(EventServiceUtility.class);

    public static Clari5Payload convert(String message) throws IOException {
        if (message == null || message.trim().isEmpty()) return null;
        String[] tokens = message.split("[|]");
        if (tokens.length != 5) return null;
        try {
            return (new Clari5Payload(null, tokens[0], tokens[1], tokens[2], Long.parseLong(tokens[3]), tokens[4]));
        } catch (Exception e) {
            logger.error("Exception occurred while converting string message event to Clari5 Payload: " + e.getMessage(), e);
            throw new IOException(e);
        }
    }

    public static String extractEventId(Clari5Payload payload) {
        String pm = payload.message;
        String message = pm.replaceAll("\\\\<", "<").replaceAll("\\\\>", ">").replaceAll("\\\\&", "&")
                .replaceAll("[\t]", " ");
        message = StringUtils.removeNonPrintable(message, null);
        JSONObject json = new JSONObject(message);
        JSONObject eventObj = new JSONObject(json.getString("event"));
        String eventId;
        try {
            eventId = eventObj.getString("eventId");
        } catch (Exception e) {
            try {
                eventId = eventObj.getString("event-id");
            } catch (Exception ex) {
                try {
                    eventId = eventObj.getString("event_id");
                } catch (Exception exp) {
                    eventId = null;
                }
            }
        }
        return eventId;
    }

    public static List<Incident> loadIncidents(String eventId) {
        if (eventId != null && !eventId.isEmpty()) {
            try {
                return Incident.loadIncidentsByEvent(eventId);
            } catch (Exception e) {
                logger.error("Exception occurred while fetching processed details of [" + eventId + "]: " + e.getMessage(), e);
            }
        }
        return new ArrayList<>();
    }

    public static String extractPayload(HttpServletRequest request) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;

        try {
            InputStream stream = request.getInputStream();
            if (stream != null) {
                reader = new BufferedReader(new InputStreamReader(stream));
                char[] charBuffer = new char[256];
                int bytesRead = -1;
                while ((bytesRead = reader.read(charBuffer)) > 0) {
                    sb.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception occurred while reading payload from request: " + ex.getMessage(), ex);
            throw new IOException(ex);
        } finally {
            if (reader != null) reader.close();
        }
        return sb.toString();
    }

    public static void writeResponse(HttpServletResponse response, String resp) throws IOException {
        response.setContentType("application/json");
        PrintWriter pw = response.getWriter();
        pw.write((resp != null) ? resp : "");
        pw.flush();
        pw.close();
    }
}
