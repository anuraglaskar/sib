// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_AcctInqEventMapper extends EventMapper<NFT_AcctInqEvent> {

public NFT_AcctInqEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_AcctInqEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_AcctInqEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_AcctInqEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_AcctInqEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_AcctInqEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_AcctInqEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getAcctId());
            preparedStatement.setString(i++, obj.getBranchIdDesc());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getRmCodeFlag());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setTimestamp(i++, obj.getSysTime());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getMenuId());
            preparedStatement.setTimestamp(i++, obj.getEventts());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getNoncustKey());
            preparedStatement.setString(i++, obj.getHostId());
            preparedStatement.setString(i++, obj.getCustType());
            preparedStatement.setString(i++, obj.getAcctStatus());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_ACCOUNTINQUIRY]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_AcctInqEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_AcctInqEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCOUNTINQUIRY"));
        putList = new ArrayList<>();

        for (NFT_AcctInqEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "ACCT_ID",  obj.getAcctId());
            p = this.insert(p, "BRANCH_ID_DESC",  obj.getBranchIdDesc());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "RM_CODE_FLAG",  obj.getRmCodeFlag());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "SYS_TIME", String.valueOf(obj.getSysTime()));
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "MENU_ID",  obj.getMenuId());
            p = this.insert(p, "EVENTTS", String.valueOf(obj.getEventts()));
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "NONCUST_KEY",  obj.getNoncustKey());
            p = this.insert(p, "HOST_ID",  obj.getHostId());
            p = this.insert(p, "CUST_TYPE",  obj.getCustType());
            p = this.insert(p, "ACCT_STATUS",  obj.getAcctStatus());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_ACCOUNTINQUIRY"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_ACCOUNTINQUIRY]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_ACCOUNTINQUIRY]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_AcctInqEvent obj = new NFT_AcctInqEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setAcctId(rs.getString("ACCT_ID"));
    obj.setBranchIdDesc(rs.getString("BRANCH_ID_DESC"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setRmCodeFlag(rs.getString("RM_CODE_FLAG"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setSysTime(rs.getTimestamp("SYS_TIME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setMenuId(rs.getString("MENU_ID"));
    obj.setEventts(rs.getTimestamp("EVENTTS"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setNoncustKey(rs.getString("NONCUST_KEY"));
    obj.setHostId(rs.getString("HOST_ID"));
    obj.setCustType(rs.getString("CUST_TYPE"));
    obj.setAcctStatus(rs.getString("ACCT_STATUS"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_ACCOUNTINQUIRY]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_AcctInqEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_AcctInqEvent> events;
 NFT_AcctInqEvent obj = new NFT_AcctInqEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_ACCOUNTINQUIRY"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_AcctInqEvent obj = new NFT_AcctInqEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setAcctId(getColumnValue(rs, "ACCT_ID"));
            obj.setBranchIdDesc(getColumnValue(rs, "BRANCH_ID_DESC"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setRmCodeFlag(getColumnValue(rs, "RM_CODE_FLAG"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setSysTime(EventHelper.toTimestamp(getColumnValue(rs, "SYS_TIME")));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setMenuId(getColumnValue(rs, "MENU_ID"));
            obj.setEventts(EventHelper.toTimestamp(getColumnValue(rs, "EVENTTS")));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setNoncustKey(getColumnValue(rs, "NONCUST_KEY"));
            obj.setHostId(getColumnValue(rs, "HOST_ID"));
            obj.setCustType(getColumnValue(rs, "CUST_TYPE"));
            obj.setAcctStatus(getColumnValue(rs, "ACCT_STATUS"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCOUNTINQUIRY]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_ACCOUNTINQUIRY]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"ACCT_ID\",\"BRANCH_ID_DESC\",\"USER_ID\",\"RM_CODE_FLAG\",\"BRANCH_ID\",\"SYS_TIME\",\"CUST_ID\",\"MENU_ID\",\"EVENTTS\",\"AVL_BAL\",\"NONCUST_KEY\",\"HOST_ID\",\"CUST_TYPE\",\"ACCT_STATUS\"" +
              " FROM EVENT_NFT_ACCOUNTINQUIRY";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [ACCT_ID],[BRANCH_ID_DESC],[USER_ID],[RM_CODE_FLAG],[BRANCH_ID],[SYS_TIME],[CUST_ID],[MENU_ID],[EVENTTS],[AVL_BAL],[NONCUST_KEY],[HOST_ID],[CUST_TYPE],[ACCT_STATUS]" +
              " FROM EVENT_NFT_ACCOUNTINQUIRY";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`ACCT_ID`,`BRANCH_ID_DESC`,`USER_ID`,`RM_CODE_FLAG`,`BRANCH_ID`,`SYS_TIME`,`CUST_ID`,`MENU_ID`,`EVENTTS`,`AVL_BAL`,`NONCUST_KEY`,`HOST_ID`,`CUST_TYPE`,`ACCT_STATUS`" +
              " FROM EVENT_NFT_ACCOUNTINQUIRY";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_ACCOUNTINQUIRY (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"ACCT_ID\",\"BRANCH_ID_DESC\",\"USER_ID\",\"RM_CODE_FLAG\",\"BRANCH_ID\",\"SYS_TIME\",\"CUST_ID\",\"MENU_ID\",\"EVENTTS\",\"AVL_BAL\",\"NONCUST_KEY\",\"HOST_ID\",\"CUST_TYPE\",\"ACCT_STATUS\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[ACCT_ID],[BRANCH_ID_DESC],[USER_ID],[RM_CODE_FLAG],[BRANCH_ID],[SYS_TIME],[CUST_ID],[MENU_ID],[EVENTTS],[AVL_BAL],[NONCUST_KEY],[HOST_ID],[CUST_TYPE],[ACCT_STATUS]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`ACCT_ID`,`BRANCH_ID_DESC`,`USER_ID`,`RM_CODE_FLAG`,`BRANCH_ID`,`SYS_TIME`,`CUST_ID`,`MENU_ID`,`EVENTTS`,`AVL_BAL`,`NONCUST_KEY`,`HOST_ID`,`CUST_TYPE`,`ACCT_STATUS`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

