cxps.noesis.glossary.entity.fact-user {
        db-name = FACT_USER
        generate = false
        db_column_quoted = true

        tablespace = CXPS_USERS
        attributes = [
                { name = FACT_NAME, column = FACT_NAME, type = "string:50", key=true }
                { name = USER_LIST, column = USER_LIST, type = "string:50"}
		        { name = CUR_USER, column = CUR_USER, type = "string:50"}
		        {name = FLAG, column = FLAG, type = "string:50"}
                     ]
        }

