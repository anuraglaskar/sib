package cxps.events;

import clari5.rdbms.Rdbms;
import java.sql.*;
import cxps.apex.utils.StringUtils;


public class CustomDerivation {

    public static String getAppBenName(FT_RemittanceTxnEvent event) {
        return "";
    }


    public static String getAmtRemBenAcct(FT_RemittanceTxnEvent event) {
        return "";
    }

    public static String getBenNameCountry(FT_RemittanceTxnEvent event) {
        return "";
    }

    /*public static String acctName(String obj){
        String abc = obj;
        System.out.println("val" +abc);
        String accname = abc.replaceAll("\"", "");
            System.out.println("accname" +accname);
        return accname;
    }

    public static String tranPart(String obj){
        String abc = obj;
        System.out.println("val" +abc);
        String tranparticular = abc.replaceAll("\"", "");
        System.out.println("tranparticular" +tranparticular);
        return tranparticular;
    }

    public static String getisStaff(FT_AccountTxnEvent event){
        ResultSet result = null;
        String isStaff="";

        Connection con = Rdbms.getAppConnection();
        Statement st = null;

        try
        {

            String query2 = "select \"staff\" from customer where \"staff\" = 'Y' and \"hostCustId\" = '" + event.getCustId() + "'" ;
            st = con.createStatement();
            result = st.executeQuery(query2);
            while(result.next()) {
                System.out.println("staff is :" + result.getString("staff"));
                if(!StringUtils.isNullOrEmpty(result.getString( "staff")))
                    event.setIsStaff(result.getString("staff"));
                    isStaff = result.getString("staff");
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (st != null) {
                    st.close();
                }

                if (con != null) {
                    con.close();
                }
                if(result !=null){
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return isStaff;
    }*/

    public static String getEntryVerifFlg(FT_AccountTxnEvent event) {
        String entryDeg = "";
        String verifyDeg = "";
        String entverFlg = "N";
        try {
            String query = "select \"DESIGNATION\" from USER_DESIGNATION where \"USERID\" = '" + event.getEntryUser() + "'";
            try (Connection con = Rdbms.getAppConnection(); Statement st = con.createStatement(); ResultSet result = st.executeQuery(query)) {
                while (result.next()) {
                    if (!StringUtils.isNullOrEmpty(result.getString("DESIGNATION")) && (result.getString("DESIGNATION").equalsIgnoreCase("OFFICER"))){
                        entryDeg = result.getString("DESIGNATION");
                        event.setEntryusrDesignation(entryDeg);
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            String query1 = "select \"DESIGNATION\" from USER_DESIGNATION where \"USERID\" = '" + event.getVerifyUser() + "'";
            try (Connection con1 = Rdbms.getAppConnection(); Statement st1 = con1.createStatement(); ResultSet result1 = st1.executeQuery(query1)) {
                while (result1.next()) {
                    if (!StringUtils.isNullOrEmpty(result1.getString("DESIGNATION")) && (result1.getString("DESIGNATION").equalsIgnoreCase("OFFICER"))){
                        verifyDeg = result1.getString("DESIGNATION");
                        event.setVerifyusrDesignation(verifyDeg);
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if ((entryDeg.equalsIgnoreCase("OFFICER")) && (verifyDeg.equalsIgnoreCase("OFFICER"))) {
                entverFlg = "Y";
            }
        else {
            entverFlg = "N";
        }
        return entverFlg;
    }


    // Added and Modified by bhagya for Scenario 37 on 18-04-2020
    // Strated from,
    public static String benName(String name) {
        String string = name;
        if(!StringUtils.isNullOrEmpty(string)) {
            String str = string.trim().replaceAll(" +", " ");
            String st = str.replace("BEN NAME: ", "");

            if(st.contains(" ")){
                String n[] = st.split(" ");
                String a = n[0] + " " + n[1];
                name = a.trim();
                return name;
            }
            else {
                return null;
            }
        }
        return name;
    }
    //Ended here

    public static String getJointCustId(FT_AccountTxnEvent event) {
        String jointcustid = "";
        try {
            String query = "select \"join_custId\" from dda where \"hostAcctId\" = '" + event.getAcctId() + "'";
            try (Connection con = Rdbms.getAppConnection(); Statement st = con.createStatement(); ResultSet result = st.executeQuery(query)) {
                while (result.next()) {
                        jointcustid = result.getString("join_custId");
                        event.setJointCustId(jointcustid);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return jointcustid;
    }

}

