// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
//import clari5.platform.logger.CXLog;
//import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_LOAN_INT_RATE_TXN", Schema="rice")
public class NFT_LoanIntRateTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String flag;
       @Field(size=20) public Double loanAcctIntRate;
       @Field(size=200) public String custID;
       @Field(size=50) public String eventName;
       @Field(size=50) public String eventsubtype;
       @Field(size=20) public Double depositAcctIntRate;
       @Field(size=50) public String eventtype;
       @Field(size=200) public String depositAcctNo;
       @Field(size=50) public String hostId;
       @Field public java.sql.Timestamp sysTime;
       @Field public java.sql.Timestamp eventts;
       @Field(size=200) public String loanAcctNo;


    @JsonIgnore
    public ITable<NFT_LoanIntRateTxnEvent> t = AEF.getITable(this);

	public NFT_LoanIntRateTxnEvent(){}

    public NFT_LoanIntRateTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("LoanIntRateTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setFlag(json.getString("Flag"));
            setLoanAcctIntRate(EventHelper.toDouble(json.getString("LoanAcct_Int_Rate")));
            setCustID(json.getString("Cust_ID"));
            setEventName(json.getString("event-name"));
            setEventsubtype(json.getString("eventsubtype"));
            setDepositAcctIntRate(EventHelper.toDouble(json.getString("DepositAcct_Int_Rate")));
            setEventtype(json.getString("eventtype"));
            setDepositAcctNo(json.getString("DepositAcctNo"));
            setHostId(json.getString("host-id"));
            setSysTime(EventHelper.toTimestamp(json.getString("sys-time")));
            setEventts(EventHelper.toTimestamp(json.getString("eventts")));
            setLoanAcctNo(json.getString("LoanAcctNo"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "LI"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getFlag(){ return flag; }

    public Double getLoanAcctIntRate(){ return loanAcctIntRate; }

    public String getCustID(){ return custID; }

    public String getEventName(){ return eventName; }

    public String getEventsubtype(){ return eventsubtype; }

    public Double getDepositAcctIntRate(){ return depositAcctIntRate; }

    public String getEventtype(){ return eventtype; }

    public String getDepositAcctNo(){ return depositAcctNo; }

    public String getHostId(){ return hostId; }

    public java.sql.Timestamp getSysTime(){ return sysTime; }

    public java.sql.Timestamp getEventts(){ return eventts; }

    public String getLoanAcctNo(){ return loanAcctNo; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setFlag(String val){ this.flag = val; }
    public void setLoanAcctIntRate(Double val){ this.loanAcctIntRate = val; }
    public void setCustID(String val){ this.custID = val; }
    public void setEventName(String val){ this.eventName = val; }
    public void setEventsubtype(String val){ this.eventsubtype = val; }
    public void setDepositAcctIntRate(Double val){ this.depositAcctIntRate = val; }
    public void setEventtype(String val){ this.eventtype = val; }
    public void setDepositAcctNo(String val){ this.depositAcctNo = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setSysTime(java.sql.Timestamp val){ this.sysTime = val; }
    public void setEventts(java.sql.Timestamp val){ this.eventts = val; }
    public void setLoanAcctNo(String val){ this.loanAcctNo = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
       // ICXLog cxLog = CXLog.fenter("derive.NFT_LoanIntRateTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.loanAcctNo);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));

       // cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_LoanIntRateTxn");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "LoanIntRateTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
