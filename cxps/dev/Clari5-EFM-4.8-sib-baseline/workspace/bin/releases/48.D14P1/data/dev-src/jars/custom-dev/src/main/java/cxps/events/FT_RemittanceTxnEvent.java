// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
//import clari5.platform.logger.CXLog;
//import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_FT_REMITTANCE_TXN", Schema="rice")
public class FT_RemittanceTxnEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=50) public String remAdd1;
       @Field(size=50) public String remType;
       @Field(size=50) public String remAdd3;
       @Field(size=32) public Double inrAmount;
       @Field(size=50) public String remAdd2;
       @Field public java.sql.Timestamp trnDate;
       @Field(size=50) public String purposeCode;
       @Field(size=50) public String appBenName;
       @Field(size=50) public String benCity;
       @Field(size=50) public String amtRemBenAcct;
       @Field(size=50) public String type;
       @Field(size=50) public String branch;
       @Field(size=50) public String benCntryCode;
       @Field(size=50) public String benAdd3;
       @Field(size=50) public String purposeDesc;
       @Field(size=50) public String benAdd2;
       @Field(size=50) public String remAcctNo;
       @Field(size=50) public String remName;
       @Field(size=50) public String benAdd1;
       @Field(size=50) public String benBic;
       @Field(size=50) public String hostId;
       @Field(size=50) public String msgtype;
       @Field(size=50) public String tranRefNo;
       @Field(size=32) public Double tranAmt;
       @Field(size=50) public String remCustId;
       @Field(size=50) public String accountCategory;
       @Field(size=50) public String benName;
       @Field(size=50) public String benAcctNo;
       @Field(size=32) public Double usdEqvAmt;
       @Field(size=50) public String benCustId;
       @Field(size=50) public String remCntryCode;
       @Field(size=50) public String cptyAcNo;
       @Field(size=50) public String system;
       @Field(size=50) public String sno;
       @Field(size=50) public String tranCurr;
       @Field(size=50) public String remCity;
       @Field(size=50) public String benNameCountry;
       @Field(size=50) public String clientAccNo;
       @Field(size=50) public String remBic;


    @JsonIgnore
    public ITable<FT_RemittanceTxnEvent> t = AEF.getITable(this);

	public FT_RemittanceTxnEvent(){}

    public FT_RemittanceTxnEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("RemittanceTxn");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setRemAdd1(json.getString("REM_ADD1"));
            setRemType(json.getString("REM_TYPE"));
            setRemAdd3(json.getString("REM_ADD3"));
            setInrAmount(EventHelper.toDouble(json.getString("INR_AMOUNT")));
            setRemAdd2(json.getString("REM_ADD2"));
            setTrnDate(EventHelper.toTimestamp(json.getString("TRN_DATE")));
            setPurposeCode(json.getString("PURPOSE_CODE"));
            setBenCity(json.getString("BEN_CITY"));
            setType(json.getString("TYPE"));
            setBranch(json.getString("BRANCH"));
            setBenCntryCode(json.getString("BEN_CNTRY_CODE"));
            setBenAdd3(json.getString("BEN_ADD3"));
            setPurposeDesc(json.getString("PURPOSE_DESC"));
            setBenAdd2(json.getString("BEN_ADD2"));
            setRemAcctNo(json.getString("REM_ACCT_NO"));
            setRemName(json.getString("REM_NAME"));
            setBenAdd1(json.getString("BEN_ADD1"));
            setBenBic(json.getString("BEN_BIC"));
            setHostId(json.getString("host-id"));
            setMsgtype(json.getString("MSGTYPE"));
            setTranRefNo(json.getString("TRAN_REF_NO"));
            setTranAmt(EventHelper.toDouble(json.getString("TRAN_AMT")));
            setRemCustId(json.getString("REM_CUST_ID"));
            setAccountCategory(json.getString("ACCOUNT_CATEGORY"));
            setBenName(json.getString("BEN_NAME"));
            setBenAcctNo(json.getString("BEN_ACCT_NO"));
            setUsdEqvAmt(EventHelper.toDouble(json.getString("USD_EQV_AMT")));
            setBenCustId(json.getString("BEN_CUST_ID"));
            setRemCntryCode(json.getString("REM_CNTRY_CODE"));
            setCptyAcNo(json.getString("CPTY_AC_NO"));
            setSystem(json.getString("SYSTEM"));
            setSno(json.getString("SNO"));
            setTranCurr(json.getString("TRAN_CURR"));
            setRemCity(json.getString("REM_CITY"));
            setClientAccNo(json.getString("CLIENT_ACC_NO"));
            setRemBic(json.getString("REM_BIC"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setAppBenName(CustomDerivation.getAppBenName(this));
        setAmtRemBenAcct(CustomDerivation.getAmtRemBenAcct(this));
        setBenNameCountry(CustomDerivation.getBenNameCountry(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "RM"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getRemAdd1(){ return remAdd1; }

    public String getRemType(){ return remType; }

    public String getRemAdd3(){ return remAdd3; }

    public Double getInrAmount(){ return inrAmount; }

    public String getRemAdd2(){ return remAdd2; }

    public java.sql.Timestamp getTrnDate(){ return trnDate; }

    public String getPurposeCode(){ return purposeCode; }

    public String getBenCity(){ return benCity; }

    public String getType(){ return type; }

    public String getBranch(){ return branch; }

    public String getBenCntryCode(){ return benCntryCode; }

    public String getBenAdd3(){ return benAdd3; }

    public String getPurposeDesc(){ return purposeDesc; }

    public String getBenAdd2(){ return benAdd2; }

    public String getRemAcctNo(){ return remAcctNo; }

    public String getRemName(){ return remName; }

    public String getBenAdd1(){ return benAdd1; }

    public String getBenBic(){ return benBic; }

    public String getHostId(){ return hostId; }

    public String getMsgtype(){ return msgtype; }

    public String getTranRefNo(){ return tranRefNo; }

    public Double getTranAmt(){ return tranAmt; }

    public String getRemCustId(){ return remCustId; }

    public String getAccountCategory(){ return accountCategory; }

    public String getBenName(){ return benName; }

    public String getBenAcctNo(){ return benAcctNo; }

    public Double getUsdEqvAmt(){ return usdEqvAmt; }

    public String getBenCustId(){ return benCustId; }

    public String getRemCntryCode(){ return remCntryCode; }

    public String getCptyAcNo(){ return cptyAcNo; }

    public String getSystem(){ return system; }

    public String getSno(){ return sno; }

    public String getTranCurr(){ return tranCurr; }

    public String getRemCity(){ return remCity; }

    public String getClientAccNo(){ return clientAccNo; }

    public String getRemBic(){ return remBic; }
    public String getAppBenName(){ return appBenName; }

    public String getAmtRemBenAcct(){ return amtRemBenAcct; }

    public String getBenNameCountry(){ return benNameCountry; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setRemAdd1(String val){ this.remAdd1 = val; }
    public void setRemType(String val){ this.remType = val; }
    public void setRemAdd3(String val){ this.remAdd3 = val; }
    public void setInrAmount(Double val){ this.inrAmount = val; }
    public void setRemAdd2(String val){ this.remAdd2 = val; }
    public void setTrnDate(java.sql.Timestamp val){ this.trnDate = val; }
    public void setPurposeCode(String val){ this.purposeCode = val; }
    public void setBenCity(String val){ this.benCity = val; }
    public void setType(String val){ this.type = val; }
    public void setBranch(String val){ this.branch = val; }
    public void setBenCntryCode(String val){ this.benCntryCode = val; }
    public void setBenAdd3(String val){ this.benAdd3 = val; }
    public void setPurposeDesc(String val){ this.purposeDesc = val; }
    public void setBenAdd2(String val){ this.benAdd2 = val; }
    public void setRemAcctNo(String val){ this.remAcctNo = val; }
    public void setRemName(String val){ this.remName = val; }
    public void setBenAdd1(String val){ this.benAdd1 = val; }
    public void setBenBic(String val){ this.benBic = val; }
    public void setHostId(String val){ this.hostId = val; }
    public void setMsgtype(String val){ this.msgtype = val; }
    public void setTranRefNo(String val){ this.tranRefNo = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setRemCustId(String val){ this.remCustId = val; }
    public void setAccountCategory(String val){ this.accountCategory = val; }
    public void setBenName(String val){ this.benName = val; }
    public void setBenAcctNo(String val){ this.benAcctNo = val; }
    public void setUsdEqvAmt(Double val){ this.usdEqvAmt = val; }
    public void setBenCustId(String val){ this.benCustId = val; }
    public void setRemCntryCode(String val){ this.remCntryCode = val; }
    public void setCptyAcNo(String val){ this.cptyAcNo = val; }
    public void setSystem(String val){ this.system = val; }
    public void setSno(String val){ this.sno = val; }
    public void setTranCurr(String val){ this.tranCurr = val; }
    public void setRemCity(String val){ this.remCity = val; }
    public void setClientAccNo(String val){ this.clientAccNo = val; }
    public void setRemBic(String val){ this.remBic = val; }
    public void setAppBenName(String val){ this.appBenName = val; }
    public void setAmtRemBenAcct(String val){ this.amtRemBenAcct = val; }
    public void setBenNameCountry(String val){ this.benNameCountry = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
       // ICXLog cxLog = CXLog.fenter("derive.FT_RemittanceTxnEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

	if(getRemType().equalsIgnoreCase("I") ) {
        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.benAcctNo);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));    
        }
       
        if(getRemType().equalsIgnoreCase("O") ) {
        String accountKey= h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.remAcctNo);
        wsInfoSet.add(new WorkspaceInfo("Account", accountKey));    
        }

        String noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.tranRefNo);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));


        noncustomerKey= h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.benName+this.benCntryCode);
        wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
 
        
        if(getRemType().equalsIgnoreCase("O") ) { 
    
         String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.remCustId);
         wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        }
       
        if(getRemType().equalsIgnoreCase("I") ){    
    
         String customerKey= h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.benCustId);
         wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
        }

       // cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_RemittanceTxn");
        json.put("event_type", "FT");
        json.put("event_sub_type", "RemittanceTxn");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
