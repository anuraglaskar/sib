package cxps.events;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import clari5.rdbms.Rdbms;


public class SOLamt {

    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection connection = null;
    Double amount = null;

    public Double getSOLamt(String acctSOL)
    {

        String sql = "select sum_tran_amt from sol_avg_amt_tbl where sol_id = '" +acctSOL+ "'" ;
        connection = Rdbms.getConnection();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                amount = rs.getDouble("sum_tran_amt");
            }
            connection.close();
            if(amount != null)
            {
                return amount;
            }
            else {
                return Double.valueOf(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Double.valueOf(0);
    }
}
