#!/usr/bin/env bash
# This script expected to be set as a cron
# with user called clari5
# There must be an ssh key exchange between
# clari5@localhost and mvnrep@192.168.5.56
# and
# clari5@localhost and cxsvn@192.168.5.56
# for the build to work unattended.
# Output of the build (timestamps) can be checked at
# http://192.168.5.56/resources/cxpsdev-resources/4.8/published/installers/dev/48.D14P1/
# and
# http://192.168.5.56/resources/cxpsdev-resources/4.8/published/installers/dev/ (look for cc-platform-48.D14P1-installer.bash)
# The build logs can be checked via the mail command.


. /home/clari5/.bashrc 

smd platform 20.04.02
svn update
fbuild.sh publish --all

smd cc 48.D14P1
svn update
fbuild.sh publish --all

smd efm 48.D14P1
svn update
fbuild.sh publish --all

sw Clari5 EFM-4.8-dev-baseline

ws
srel 48.D14P1
svn update
rel
creinstaller.sh
scp efm-product-dev-48.D14P1-installer.tgz \
cxsvn@192.168.5.56:www/resources/cxpsdev-resources/4.8/published/installers/dev/48.D14P1/efm-product-dev-48.D14P1-installer.tgz

sdb mysql
fbuild.sh
creplugin.sh --all

scp dev-plugins-48.D14P1.tgz \
cxsvn@192.168.5.56:www/resources/cxpsdev-resources/4.8/published/installers/dev/48.D14P1/dev-plugins-48.D14P1.tgz
