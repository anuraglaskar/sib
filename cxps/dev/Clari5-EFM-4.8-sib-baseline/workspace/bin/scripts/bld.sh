#!/usr/bin/env bash
# Author: Lovish

# Executes main code
main() {
    
    [ -z "${CL5_REL_NAME}" ] && echo "Error: Release not selected" && return 1

    CL5_REL_NAME=`echo $CL5_REL_NAME | tr -d '/'`

    # Get BASEDIR
    local BASEDIR=$( cd $(pwd) && cd $(dirname $0) && pwd )

    local _workDir=${w}/work
    local _status

    # Creates the dev-src source
    cresrc ${_workDir} || return 1

    [[ "$1" == "--src" ]] && return 0

    # Build over the work directory
    _header "Compile and installer creation"
    ( cd ${_workDir} && ${BASEDIR}/nbuild.sh $* )
    status=$?

    [ ${status} -ne 0 ] && rm -fr ${_workDir} && return ${status}

    # Merge custom bank
    _header "Merge custom over core deployment"
    ${CXPS_WORKSPACE}/bin/scripts/mergeCustom.sh ${_workDir}
    status=$?

    rm -fr ${_workDir}
    return ${status}
}

# Creates dev-src
cresrc() {

    local _workDir=$1

    rm -fr ${_workDir}
    mkdir -p ${_workDir}

    # Create workDir
    ( cd ${_workDir} && mkdir -p $( cd ${w}/dev-src && find . -type d ) )
    for file in $( cd ${w}/dev-src && find . -type f && find . -type l )
    do
        ln -fs ${w}/dev-src/${file} ${_workDir}/${file}
    done

    # Merge over the directory if needed
    cmdparser ${_workDir} || return 1

    # Remove .gradle and .idea directory from ${_workDir}
    rm -fr ${_workDir}/.gradle ${_workDir}/.idea
}

# ------------------------------------------
# Parse the commands given in (driver) file
# ------------------------------------------
# Operations supported :
#
# extend|cp     Copies the changes over the work directory
# replace|mv    Replaces the exsiting content in work directory with selected dir
# delete|rm     Delete the content from the work directory
#
# Also creates the version file for the build to work
cmdparser() {
    
    local _workDir=$1

    local _srcDir=${w}/bin/releases/${CL5_REL_NAME}/data/dev-src
    local _cmdfile=${_srcDir}/driver.txt
    local _operationFile=/tmp/bld.${CL5_REL_NAME}.$$
    local _versionFile=${_workDir}/.version

    local _cmd _args _mainRev _verRev
    
    _mainRev=$( svnversion -c ${w}/dev-src | cut -f2 -d: )

    [ ! -f ${_cmdfile} ] && echo ${_mainRev} > ${_versionFile} && return 0

    _verRev=$( svnversion -c ${_srcDir} | cut -f2 -d: )
    ( printf "${_mainRev}\n${_verRev}\n" | sort -un | tail -1 ) > ${_versionFile}

    # Operate on the operation file
    while read line
    do
        _cmd=$( echo ${line} | cut -d' ' -f1 )
        _args=$( echo ${line} | cut -d' ' -f2- )

        rm -f ${_operationFile}
        echo ${_args} | tr ' ' '\n' | sed "s${_srcDir}/" > ${_operationFile}

        case ${_cmd} in
            "checkout")
                echo "[${_cmd}] In progress"
                ;;
            "extend"|"cp")
                # Copy from source to work directory
                ( moveData "${_srcDir}" "${_workDir}" "${_operationFile}" ) || return 1
                ;;
            "replace"|"mv")
                # Replace from source to work directory
                ( moveData -c "${_srcDir}" "${_workDir}" "${_operationFile}" ) || return 1
                ;;
            "link"|"ln")
                # Link from source to work directory
                ( linkData "${_srcDir}" "${_workDir}" "${_operationFile}" ) || return 1
                ;;
            "delete"|"rm")
                # Remove the specified fields
                rm -fr $( cat ${_operationFile} | sed "s^.*$${_workDir}/&" )
                ;;
        esac
    done < <( cat ${_cmdfile} | grep -v -e '^[ 	]*#' -e '^[ 	]*$' | tr '	' ' ' | tr -s ' ' )
}

moveData() {
    
    local _remove=1
    [[ "$1" = "-c" ]] && _remove=0 && shift 1

    local _srcDir=$1
    local _workDir=$2
    local _oFile=$3

    while read file
    do
        [ ! -e "${_srcDir}/${file}" ] && echo "Error: Unable to find [${file}]" && return 1

        [ ${_remove} -eq 0 ] && rm -fr ${_workDir}/${file}

        mkdir -p ${_workDir}/$(dirname ${file})
        for _f in $( cd ${_srcDir} && find ${file} )
        do
            [ -d ${_srcDir}/${_f} ] && mkdir -p ${_workDir}/${_f} && continue

            rm -f ${_workDir}/${_f}
            ln -fs ${_srcDir}/${_f} ${_workDir}/${_f}
        done

    done < ${_oFile}
}

linkData() {
    
    local _srcDir=$1
    local _workDir=$2
    local _oFile=$3

    while read file
    do
        [ ! -e "${_srcDir}/${file}" ] && echo "Error: Unable to find [${file}]" && return 1

        rm -fr ${_workDir}/${file}
        ln -fs ${_srcDir}/${file} ${_workDir}/${file}

    done < ${_oFile}
}

_header() {

    local _message="`echo $*`"
    local _length=${#_message}
    local i=0
    local _tag=`i=0; while [ $i -lt $((_length+4 )) ]; do printf "-"; i=$((i+1)); done`

echo -e '\033[01;36m' 2>/dev/null
cat << EOF
${_tag}
  ${_message}
${_tag}
EOF
echo -en '\033[00m' 2>/dev/null

}

# -----------
# Main block
# -----------
main $* || exit 1
