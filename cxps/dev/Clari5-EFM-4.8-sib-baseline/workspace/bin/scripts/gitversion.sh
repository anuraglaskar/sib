#!/usr/bin/env bash

BITBUCKET_URL="https://api.bitbucket.org/2.0/repositories/anuraglaskar/sib/commits"
GIT_STATUS=$(git status --porcelain | wc -l)
LOCAL_GIT_COMMIT_HASH_VALUE=$((16#$(git rev-parse HEAD | cut -c 1-7)))
REMOTE_GIT_COMMIT_HASH_VALUE=$((16#$(curl $BITBUCKET_URL | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["values"][0]["hash"]' | cut -c 1-7)))

#echo $REMOTE_GIT_COMMIT_HASH_VALUE
#echo $LOCAL_GIT_COMMIT_HASH_VALUE

if [ $GIT_STATUS == 0 ]
then
	if [ $REMOTE_GIT_COMMIT_HASH_VALUE == $LOCAL_GIT_COMMIT_HASH_VALUE ]
	then
		echo $REMOTE_GIT_COMMIT_HASH_VALUE > $CXPS_WORKSPACE/bitbucketversion.txt
	else
		echo "unversioned" > $CXPS_WORKSPACE/bitbucketversion.txt
	fi
else
	echo "unversioned" > $CXPS_WORKSPACE/bitbucketversion.txt
fi


