#!/usr/bin/env bash

# Get basedir
CWD=`pwd`

_revision=`svnversion -c ${CXPS_WORKSPACE} | cut -d ':' -f2 | tr -d 'S'`
INSTALLERDIR=${CL5_WS_REP}

ARCHIVE=${CXPS_WORKSPACE}/efm-product-${CL5_CUSTOM_ID}-${CL5_REL_NAME}-installer
( echo ${_revision} | grep -qE '[A-Z]' ) && ARCHIVE="${ARCHIVE}-dirty"
ARCHIVE="${ARCHIVE}.tgz"

[ "$1" = "--name" ] && echo "$(basename ${ARCHIVE} )" && exit

( cd ${INSTALLERDIR} && tar chzf - --exclude='tools' --checkpoint=.1000 * ) > ${ARCHIVE}
