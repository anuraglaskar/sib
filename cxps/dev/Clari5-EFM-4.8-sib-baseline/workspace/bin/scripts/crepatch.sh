#!/usr/bin/env bash
# Author: Lovish

_REPD=${HOME}/.cxps-rep/.downloads
_RELNAME=efm-product-${CL5_CUSTOM_ID}

main() {

    [ $# -lt 1 ] && echo "Usage: $(basename $0) <from_version>" && return 1

    local _toV=${CL5_REL_NAME}
    local _fromV=$1
    local _workD=${d}/work
    local _temp _from _to _name

    # Get basedir
    basedir=$( cd $(pwd) && cd $(dirname $0) && cd .. && pwd )

    # Validate starting version
    ( echo ${_fromV}; echo ${_toV} ) | sort -V | tail -1 | grep -xq ${_toV}
    [ $? -ne 0 ] \
        && echo "Version [${_toV}] has to be a higher version than [${_fromV}]" && return 1

    # Create a clean directory
    rm -fr ${_workD}
    mkdir -p ${_workD}

    # Create installer for current version and extract in work directory
    creinstaller.sh || return 1
    _temp=${w}/${_RELNAME}-${_toV}-installer.tgz
    _name=$(basename ${_temp} .tgz)
    ( mkdir -p ${_workD}/${_name} && cd ${_workD}/${_name} && tar xf ${_temp} )

    # Download the previous version installer, and extract in work directory
    _download ${_fromV} || return 1
    _temp=${_REPD}/${_RELNAME}/${_fromV}-installer/${_RELNAME}-${_fromV}-installer.tgz
    _name=$(basename ${_temp} .tgz)
    ( mkdir -p ${_workD}/${_name} && cd ${_workD}/${_name} && tar xf ${_temp} )

    # Define the variables for patch creation
    _from=${_workD}/${_RELNAME}-${_fromV}-installer
    _to=${_workD}/${_RELNAME}-${_toV}-installer
    _temp=${_RELNAME}-${_fromV}-to-${_toV}-patch
    ( cd ${_workD} && unset JAVA_OPTS && ${basedir}/groovy/src/crepatch.groovy ${_from} ${_to} --out=${_temp} ) \
        || return 1

    ( cd ${_workD}/${_temp} && tar chzf $w/${_temp}.tgz * )
    rm -fr ${_workD}
    echo "Patch available at [$w/${_temp}]"
}

# Download method to download the installer for comparison
_download() {
    
    local _version=${1}-installer

    # Make sure the package is downloaded, else return false
    python $( which cxget.py) --url=${GLB_DEVRES_URL}/4.8/published/installers/${CL5_CUSTOM_ID} ${_RELNAME} ${_version}
    [ ! -e ${_REPD}/${_RELNAME}/${_version}/${_RELNAME}-${_version}.tgz ] \
        && echo "Error: Unable to download [${_RELNAME}-${_version}]" \
        && return 1 \
        || return 0
}

# -----------------
# Main method call
# -----------------
main $*
