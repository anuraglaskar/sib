#!/usr/bin/env groovy
// Author           : Utsav
// Reviewer         : Lovish
// Created on       : Tue Dec 27 14:22:57 IST 2016

// Define all the imports
import groovy.json.JsonSlurper

import java.nio.file.*

/* ---------------------
    Method declarations
   --------------------- */

// Define main method
int main(String oldInstaller, String newInstaller) {


    String
    // Check if old installer path exists
    File temp = new File(oldInstaller)
    if (!temp.exists()) {
        println "Error: Path [${oldInstaller}] not found!"
        return 1
    }

    // Check if new installer path exists
    temp = new File(newInstaller)
    if (!temp.exists()) {
        println "Error: Path [${newInstaller}] not found!"
        return 1
    }

    // Create patching area
    println "Patch creation area is [${patchArea}]"
    temp = new File(patchArea)
    if (temp.exists()) deleteRecursiveIfExists(temp)
    temp.mkdirs()

    /* --  Adding default content -- */

    // Copy package-meta.conf
    if (!copyFile("package-meta.conf", patchArea)) return 1

    // Copy ReleaseNotes.html
    if (!copyFile("ReleaseNotes.html", patchArea)) return 1

    // Copy scripts
    if (!copyFile("scripts", patchArea, false)) return 1

    // Copy meta
    if (!copyFile("meta", patchArea)) return 1

    // Copy rep/conf
    if (!copyFile("rep/conf", patchArea)) return 1

    // Copy rep/db
    if (!copyFile("rep/db", patchArea)) return 1

    // Copy rep/ui
    if (!copyFile("rep/ui", patchArea, false)) return 1

    /* -- Adding applications--  */
    if(!copyApps(patchArea)) return 1;

    /* -- Adding internal jars -- */
    if(!copyInternalLibs(patchArea)) return 1

    /* -- Adding external jars-- */
    if(!copyExternalLibs(patchArea)) return 1

    /* -- Adding dependencies -- */
    if(!copyDeps(patchArea)) return 1

    return 0
}

boolean copyFile(String filename, String patchArea, boolean mandatory = true) {

    String src, dest

    print "Copying [${filename}] ... "
    src = "${this.newInstallerPath}/${filename}"
    dest = "${patchArea}/${filename}"

    try { createLink(src, dest, mandatory) }
    catch (FileNotFoundException e) {
        println "failed"
        println e.message
        return false
    }
    println "done"

    return true
}

//copies applications
boolean copyApps(String patchArea) {
    //message for console
    print "Copying [rep/apps] ... "
    if(!creDirs("${patchArea}/rep/apps")) return false

    def apps = getPackageMetaContent(newInstallerPath)["apps"]
    apps.each {appname, value ->
        try {  createLink("${newInstallerPath}/rep/apps/${appname}.war","${patchArea}/rep/apps/${appname}.war") }
        catch (FileNotFoundException e) {
            println "failed"
            println e.message
            return false
        }
    }
    println "done"
    return true
}

//copies internal-libs
boolean copyInternalLibs(String patchArea) {

    //message for console
    print "Copying [rep/ilib] ... "

    if(!creDirs("${patchArea}/rep/ilib")) return false

    //map of jar:revision
    def jarVersionMap = [:]

    //list of internal jars
    def iJarList = []

    //reads <appname>-ijar.list from meta in old installer and creates a map of jarname:revision
    new File("${oldInstallerPath}/meta").eachFileRecurse { file ->
        if (file.name.endsWith("ijar.list")) {
            iJarList = file.readLines()
            iJarList.forEach() { jar ->
                //jar.split("\\|")[x]. x=0 gives the jarname, x=1 gives the revision num
                jarVersionMap.put(jar.split("[|]")[0], jar.split("[|]")[1])
            }
        }
    }

    /*
    reads <appname>-ijar.list from meta in new installer and
    compares the revision of internal jars for an app with the
    revision in old installer
    */
    new File("${newInstallerPath}/meta").eachFileRecurse { file ->
        if (file.name.endsWith("ijar.list")) {
            iJarList = file.readLines()
            iJarList.forEach() { jar ->
                def jarname = jar.split("\\|")[0]
                def version = jar.split("\\|")[1]

                if (jarVersionMap.get(jarname) != version) {
                    try {  createLink("${newInstallerPath}/rep/ilib/${jarname}", "${patchArea}/rep/ilib/${jarname}") }
                    catch (FileNotFoundException e) {
                        println "failed"
                        println e.message
                        return false
                    }
                }
            }
        }
    }
    println "done"
    return true
}

//copies external libs
boolean copyExternalLibs(String patchArea) {

    //message for console
    print "Copying [rep/elib] ... "

    if(!creDirs("${patchArea}/rep/elib")) return false

    //map of externalJar:checksum
    def eJarDigestMap = [:]

    //creates a map of externalJar:checksum in old installer
    new File("${oldInstallerPath}/rep/elib").eachFileRecurse {file ->
        eJarDigestMap.put(file.name,getDigest(file.path))
    }

    /*
    compares the checksum of external jars in new installer with it's
    corresponding jar in old installer. creates a symlink if any diff is found OR
    a new jar has been added.
    */
    new File("${newInstallerPath}/rep/elib").eachFileRecurse {file ->
        if(eJarDigestMap.containsKey(file.name)){
            if(eJarDigestMap.get(file.name) != getDigest(file.path)){
                try { createLink("${newInstallerPath}/rep/elib/${file.name}", "${patchArea}/rep/elib/${file.name}") }
                catch (FileNotFoundException e) {
                    println "failed"
                    println e.message
                    return false
                }
            }
        } else {
            try { createLink("${newInstallerPath}/rep/elib/${file.name}", "${patchArea}/rep/elib/${file.name}") }
            catch (FileNotFoundException e) {
                println "failed"
                println e.message
                return false
            }
        }
    }

    println "done"
    return true
}

boolean copyDeps(String patchArea) {
    print "Copying [deps] ... "

    if (!addPackages("web-server", patchArea, "web-server", false)) return false
    if (!addPackages("container", patchArea, "container")) return false
    if (!addPackages("dependency", patchArea, "others")) return false

    println "done"
    return true
}

// Creates symbolic link for the sourcePath
void createLink(String src, String dest, boolean mandatory = true) {

    new File(dest).parentFile.mkdirs()
    Path srcPath = Paths.get(src)

    if (!srcPath.toFile().exists()) {
        if (!mandatory) return
        throw new FileNotFoundException("Error: Path [${src}] not found")
    }

    "ln -fs ${src} ${dest}".execute().waitFor()
}

boolean deleteRecursiveIfExists(File item) {
    if (!item.exists()) return true
    if (!Files.isSymbolicLink(item.toPath()) && item.isDirectory()) {
        File[] subitems = item.listFiles()
        for (File subitem : subitems)
            if (!deleteRecursiveIfExists(subitem)) return false
    }
    return item.delete()
}

File getFile (String whichInstaller, String join, String name, Map details){
    String compression = details["compression"];
    String filename = "${name}-${details["version"]}." + compression.toLowerCase()
    return new File("${whichInstaller}/${join}/${name}/${details["version"]}/${filename}")
}

//creates dir for reps beforehand
boolean creDirs(String path) {
    try {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs()
            if (file.exists())
                return true;
            else return false;
        }
    }catch (IOException e){
        println e.getMessage();
        return false
    }
}
boolean addPackages(String pkgType, String outDir, String dirMapping, boolean isMultiple = true) {

    String oldPath = oldInstallerPath as String
    String newPath = newInstallerPath as String

    Map depsDMap = [:]
    File file

    Map deps = getPackageMetaContent(oldPath)[pkgType] as Map
    if(deps) {
        if (isMultiple) {
            deps.each { name, Map details ->
                file = getFile(oldPath, "deps/${dirMapping}", name as String, details)
                depsDMap.put(file.name, getDigest(file.path))
            }
        } else {
            file = getFile(oldPath, "deps/${dirMapping}", deps["name"] as String, deps)
            depsDMap.put(file.name, getDigest(file.path))
        }
    }

    Closure<Boolean> addDeps = { String name, Map details ->
        file = getFile(newPath, "deps/${dirMapping}", name, details)
        try {
            String dest = "${outDir}" + ("${file.path}" - "${newPath}")
            if (depsDMap.containsKey(file.name)) {
                if (depsDMap.get(file.name) != getDigest(file.path)) {
                    createLink(file.path, dest)
                }
            } else {
                createLink(file.path, dest)
            }

            return true
        } catch (FileNotFoundException e) {
            println "failed"
            println e.message
            return false
        }
    }

    deps = getPackageMetaContent(newPath)[pkgType] as Map
    boolean result = true
    if(deps) {
        if (isMultiple) {
            deps.each { name, details ->
                if (!addDeps.call(name as String, details as Map)) {
                    result = false
                    return false
                }
            }
        } else {
            result = addDeps.call(deps["name"] as String, deps)
        }
    }

    return result
}

// Take the input from user, and return the same
String getUserInput(String message) {

    Scanner scan = new Scanner(System.in)

    String input = ""
    while (!input) {
        print "${message} : "
        input = scan.nextLine()
        if (input) break
        println "Error: Provide a proper non-empty input"
    }

    return input
}

// Reads the package-meta.conf and returns the content as string
def getPackageMetaContent(String whichInstaller) {
    return new JsonSlurper().parseText(new File("${whichInstaller}/package-meta.conf").text)
}

String getDigest(String path) {
    def ant = new AntBuilder();
    ant.checksum( file: path, property: "${path}")
    return ant.project.properties."${path}"
}

/* ---------------------
 *  Main script section
 * --------------------- */

boolean readFromConsole = false
patchArea = "${System.getProperty("user.dir")}/patch_${new Date().format('yyyyMMdd')}"

List leftArgs = []
for (String argument: args) {
    if (argument == "--console") {
        readFromConsole = true
    } else if (argument.startsWith("--out=")) {
        patchArea = argument.split("=", 2)[1]
    } else {
        leftArgs.add(argument)
    }
}

if (leftArgs.size() < 2) readFromConsole = true
else {
    oldInstallerPath = leftArgs.get(0)
    newInstallerPath = leftArgs.get(1)
}

// Accepts inputs from the user
if (readFromConsole) {
    println " Patch creator"
    oldInstallerPath = getUserInput("Provide old installer path") as String
    newInstallerPath = getUserInput("Provide latest installer path") as String
}

// Call for main
int status = main(oldInstallerPath, newInstallerPath)
System.exit(status)
