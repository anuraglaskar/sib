:
# ===========================================================================
# GEN
# This is a utility to generate code for events.
# - Hocon spec -> Java assist class
#
# Note: This utility only generates assisted code with the extension
#   .java.generated, which needs to be committed after review into main
# ===========================================================================

__usage() {
	echo "Usage: $0 (settings.gradle|<project-folder> ...)"
    exit 1
}

eventgen() {
    
    local _cwd=`pwd`

    projects=""
    for dir in $*
    do
        [ -d ${dir}/src ] && projects="${projects} ${dir}/src"
    done
    
    C_PATH=""
	bdir=$( dirname ${basepath} )
    for lib in `ls ${bdir}/lib/*.jar`
    do
        C_PATH="${lib}:${C_PATH}"
    done

    # TODO verify
	java -cp "${C_PATH}" clari5.tools.gen.CodeGen --events -b "${_cwd}" ${projects}
}

# -----------------
# MAIN ACTION BLOCK
# -----------------
[ $# -eq 0 ] && __usage

if [ $# -eq 1 ] && echo $1 | grep -q settings.gradle
then
	file=$1
	[ ! -f $file ] && echo "Unable to read [$file]" && exit 1
	bdir=$( dirname $file )
	[ "$bdir" != "." ] && echo "You need to be in folder containing settings.gradle" && exit 2
	dirs=`grep '^[ 	]*include' $file | tr "'" '\012' | grep -v include | grep -v -e ',' -e '^[ 	]*$' | sort -u | sed 's,:,/,g'`
else
	dirs=$*
fi

# Get basepath
if \ls -l ${0} | grep -q -- '->' 
then
	basepath=$( ls -l $0 )
    basepath=$( expr "${basepath}" : '^.* -> \(.*\)$' )
	basepath=$( dirname $basepath )
else
	basepath=$( dirname $0 )
fi

# DEBUG
cwd=$( pwd )
cd ${cwd}
cd ${basepath}
basepath=`pwd`

specfolder=""

cd ${cwd}
for folder in $dirs
do
    [ ! -d ${folder} ] && echo "Dir ${folder} not found. Skipping ..." && continue
	[ -d $folder/src/main/resources/spec ] && specfolder="${specfolder} ${folder}"
done

[ ! -z "$specfolder" ] && eventgen $( echo $specfolder )

exit 0
