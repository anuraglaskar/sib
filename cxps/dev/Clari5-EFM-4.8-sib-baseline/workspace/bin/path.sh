#!/bin/sh

[ -d ${CXPS_WORKSPACE}/bin/ ] && export FLEX_HOME=${CXPS_WORKSPACE}/bin/
[ -d ${CXPS_WORKSPACE}/bin/maven ] && export M2_HOME=${CXPS_WORKSPACE}/bin/maven
[ -d ${CXPS_WORKSPACE}/bin/oracle ] && export ORACLE_HOME=${CXPS_WORKSPACE}/bin/oracle
[ -d ${CXPS_WORKSPACE}/bin/antlr ] && export ANTLR_HOME=${CXPS_WORKSPACE}/bin/antlr

[ ! -z "${my_FLEX_HOME}" ] && export FLEX_HOME=${my_FLEX_HOME}
if [ -d ${CXPS_WORKSPACE}/bin/oracle -a ! -z "${my_ORACLE_HOME}" ] ; then
	\rm -f ${CXPS_WORKSPACE}/bin/oracle
	ln -fs ${my_ORACLE_HOME} ${CXPS_WORKSPACE}/bin/oracle
fi

export MAVEN_PROGRAM=
export FLEX_PROGRAM=
export TOMCAT_PROGRAM=
export OPENFIRE_PROGRAM=
export ACTIVEMQ_PROGRAM=
export ORACLE_PROGRAM=

export TOMCAT_HOME=/home/anurag/repo/sib/cxps/dev/Clari5-EFM-4.8-sib-baseline/deployment/
export ACTIVEMQ_HOME=/home/anurag/repo/sib/cxps/dev/Clari5-EFM-4.8-sib-baseline/deployment/
export OPENFIRE_HOME=/home/anurag/repo/sib/cxps/dev/Clari5-EFM-4.8-sib-baseline/deployment/

[ -z "$MAVEN_OPTS" ] && export MAVEN_OPTS="-server -Xms100m -Xmx512m -XX:MaxPermSize=256m"

# Set up path for flex, java, python, mysql and maven
[ -z "$M2_HOME" ] || PATH=${M2_HOME}/bin:$PATH
[ -z "$FLEX_HOME" ] || PATH=${FLEX_HOME}/bin:$PATH
if [ ! -z "$ORACLE_HOME" ] ; then
	PATH=${ORACLE_HOME}:${ORACLE_HOME}/bin:$PATH
	export LD_LIBRARY_PATH=${ORACLE_HOME}:${ORACLE_HOME}/lib
	export DYLD_LIBRARY_PATH=${LD_LIBRARY_PATH}
	export LIBPATH=${LD_LIBRARY_PATH}
fi

[ ! -z "${ANTLR_HOME}" ] && export PATH=${PATH}:${ANTLR_HOME}/bin

# Put workspace/bin in PATH
export PATH=${CXPS_WORKSPACE}/bin:${PATH}
