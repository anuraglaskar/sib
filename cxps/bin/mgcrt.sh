#!/bin/bash
# This script overwrites the previous certificate with the new one

dep=$1
[ -z "$dep" ] && dep=$CXPS_DEPLOYMENT
[ -z "$dep" ] && echo "Usage: mgcrt.sh <deployment-folder>" && exit 1

# Check JAVA_HOME and SECURITY FOLDER
[ -z ${JAVA_HOME} ] && echo "JAVA_HOME not set !" && exit 2
[ -z "${JAVA_SECURITY_FOLDER}" ] && echo "JAVA_SECURITY_FOLDER not set. Check Env" && exit 3

echo "This script removes the old certificate and creates a fresh one. It requires SUDO permission."
echo "Press <ENTER> to continue or <Ctrl-C> to quit"
read x

#Remove older certificates before installing new one
echo "Removing old certificate..."
rm -f keystore acegisecurity.txt
rm -f  ${dep}/keystore
sudo rm -f ${JAVA_SECURITY_FOLDER}/acegisecurity.txt
sudo $JAVA_HOME/bin/keytool -keystore ${JAVA_SECURITY_FOLDER}/cacerts -delete -alias acegisecurity -storepass changeit
[ $? -eq 0 ] && echo "Completed!"

#Install new certificate
echo "Installing new certifiate..."
$JAVA_HOME/bin/keytool -keystore keystore -alias acegisecurity -genkey -keyalg RSA -validity 710 -storepass changeit -keypass changeit -dname "CN=`hostname`,OU=CXPS,O=CXPS,L=Bangalore,S=Kar,C=IN" 
$JAVA_HOME/bin/keytool -export -v -rfc -alias acegisecurity -file acegisecurity.txt -keystore keystore -storepass changeit
sudo cp acegisecurity.txt ${JAVA_SECURITY_FOLDER}
cp -r keystore ${dep}/
# Remove the generated files
[ "$dep" != "." -a "$dep" != `pwd` ] && \rm -f keystore acegisecurity.txt

cd ${JAVA_SECURITY_FOLDER}
sudo $JAVA_HOME/bin/keytool -import -v -file acegisecurity.txt -keypass changeit -keystore cacerts -storepass changeit -alias acegisecurity
[ $? -eq 0 ] && echo "Completed!"
