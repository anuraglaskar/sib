#!/bin/sh

usage(){
	echo "build.sh [o|s|c] <app>"
}

build(){
	[ $# -eq 0 ] && return
	cwd=`pwd`
	cd $1
	if [ -f pom.xml ] ; then
		mvn $option install
	elif [ -f build.sh ] ; then
		chmod +x build.sh
		./build.sh
	fi
	cd $cwd
}

# ==================
# MAIN ACTION BLOCK
# ==================

[ $# -eq 0 ] && usage && exit 1
if [ $# -ge 1 ] ; then
	option=""
	for opt in `echo "$1" | ${SED} 's/\(.\)/\1 /g'`
	do
		case "$opt" in
			"s") option="$option -DskipTests" ;;
			"c") option="$option clean" ;;
			"o") option="$option -o" ;;
			*)	usage && exit 2 ;;
		esac
	done
	shift 1
fi

# In case no application is provided then build '.'
if [ $# -eq 0 ] ; then
	echo "Building ."
	build .
else
	for app in $*
	do
		found=0
		# Try to find the given application on both current folder and in CXPS_WORKSPACE
		for dir in . $CXPS_WORKSPACE
		do
			if [ -d $dir/$app ] ; then
				echo "Building $dir/$app"
				build $dir/$app
				if [ -d $dir/$app/deployment ] ; then
					build $dir/$app/deployment
				fi
				found=1
				break
			fi
		done
		if [ $found -eq 0 ] ; then
			echo "skipping $app ... not found!"
		fi
	done
fi
