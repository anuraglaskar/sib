#!/usr/bin/env bash

# ===========================================================================
# This program installs the certificate for SSL connection to public IP
# ===========================================================================

CACERT="$JAVA_HOME/jre/lib/security/cacerts"
if [ ! -f $CACERT ] ; then
	CACERT="$JAVA_HOME/lib/security/cacerts"
fi
if [ ! -f $CACERT ] ; then
	echo "Error: Unable to locate cacerts file in JAVA_HOME" >&2
	exit 1
fi

TMPFILE=/tmp/cxsvn.customerxps.com.cert

echo -n | openssl s_client -connect cxsvn.customerxps.com:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > $TMPFILE

ALIAS=cxpsvn

echo "Removing previous certificate ..."
sudo keytool -delete -alias "$ALIAS" -keystore $CACERT -storepass changeit

echo "Installing new certificate ..."
sudo keytool -importcert -alias "$ALIAS" -ext san=dns:cxsvn.customerxps.com -file $TMPFILE -keystore $CACERT -storepass changeit

\rm -f $TMPFILE
