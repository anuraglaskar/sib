#!/bin/bash

# Assuming path is set
. cxpsvars.sh

. ${CXPS_LIB}/cxpsfuns.sh

. ${CXPS_BIN}/cxpsaliases.sh

# Unset all env variables.  # Developer must run sw() explicitly
CXPS_PRODUCT_NAME=""
CXPS_PRODUCT_VERSION=""
CXPS_PRODUCT=""
CXPS_WORKSPACE=""
CXPS_DEPLOYMENT=""

echo "WARNING: You must run 'sw' to setup product environment"
