#!/bin/sh


# ===========================================================================
# This file holds the global variable names for Unix utilities
# ===========================================================================
 
if [ -z "$__PORT_SH_EXECUTED" ] ; then

	SED=sed
	GREP=grep
	AWK=awk
	TAR=tar
	os=`uname -s`

	case "$os" in 
		"Linux")	p="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
					;;
		"SunOS")	p="/opt/csw/bin:/usr/xpg4/bin:/usr/bin:/bin"
					SED=gsed
					AWK=gawk
					TAR=gtar
					GREP=ggrep
					DIFF=gdiff
					;;
		"Darwin")	p="/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin"
					;;
		"*")		p="/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:"
					;;
	esac

	export __PORT_SH_EXECUTED=executed

	PATH=$p:$PATH

	export SED GREP AWK TAR PATH
fi

# Set the globals / constants
setGlobals(){

	export GLB_LOCAL_IP=$my_GLB_LOCAL_IP
	export GLB_REMOTE_IP=$my_GLB_REMOTE_IP
	export GLB_REMOTE_PORT=$my_GLB_REMOTE_PORT
	[ -z "$GLB_LOCAL_IP" ] && GLB_LOCAL_IP=192.168.5.56
	[ -z "$GLB_REMOTE_IP" ] && GLB_REMOTE_IP=cxsvn.customerxps.com
	[ -z "$GLB_REMOTE_PORT" ] && GLB_REMOTE_PORT=:8443

	export GLB_LOCAL_URL="http://${GLB_LOCAL_IP}/resources"
	export GLB_REMOTE_URL="https://${GLB_REMOTE_IP}${GLB_REMOTE_PORT}/resources"
	export GLB_LOCAL_SVN_URL="http://${GLB_LOCAL_IP}/svn-product/products"
	export GLB_REMOTE_SVN_URL="https://${GLB_REMOTE_IP}${GLB_REMOTE_PORT}/svn-product/products"

	export GLB_SVN_URL="$GLB_LOCAL_SVN_URL"
	export GLB_RES_URL="$GLB_LOCAL_URL"

	export GLB_DEVRES_URL="${GLB_RES_URL}/cxpsdev-resources/."
	export GLB_MAVEN_REP="${GLB_RES_URL}/extrep/."
	export GLB_MAVEN_LOCAL_REP="${GLB_RES_URL}/localrep/."
	export GLB_SCP_URL="mvnrep@${GLB_LOCAL_IP}:/home/mvnrep/resources"
}

isLocalSVN(){
	[ -z "$CX_WORKING_REMOTE" ]
	return $?
}

swRemoteSVN(){
	setGlobals
	export GLB_SVN_URL="$GLB_REMOTE_SVN_URL"
	export GLB_RES_URL="$GLB_REMOTE_URL"

	export GLB_DEVRES_URL="${GLB_RES_URL}/cxpsdev-resources"
	export GLB_MAVEN_REP="${GLB_RES_URL}/extrep/."
	export GLB_MAVEN_LOCAL_REP="${GLB_RES_URL}/localrep/."

	export CX_WORKING_REMOTE="Y"
}

swLocalSVN(){
	setGlobals
	unset CX_WORKING_REMOTE
}

# Set environment for local SVN
if isLocalSVN
then
	swLocalSVN
else
	swRemoteSVN
fi

unalias download 2>/dev/null
unset download 2>/dev/null
download()
{
	url=$1
	[ -z "$url" ] && echo "Usage: $0 <url>" && return 1
	file=`basename $url`
	curl --insecure --retry 2 --connect-timeout 2 -kfs $url > $file
	[ $? -ne 0 ] && \rm -f $file && return 1
	return 0
}

# Defining installation functions
unalias cxinstall 2>/dev/null
cxinstall()
{
	TARGET=$1
	DIR=$2
	WORKDIR=$3

	# Assume current directory in case WORKDIR is not passed
	[ -z "$WORKDIR" ] && WORKDIR=`pwd`

	# Save the current working directory
	cwd=`pwd`
	cd ${WORKDIR}

	# Download the file in case it does not exist
	if echo ${TARGET} | grep -q SNAPSHOT || [ ! -f ${TARGET} ] ; then
		download ${GLB_DEVRES_URL}/${TARGET}
		if [ $? -ne 0 ] ; then
			echo "ERROR: Unable to download ${TARGET}"
			cd ${cwd}
			return 1
		fi
	fi

	extension=`echo $TARGET | ${SED} 's/^.*\.\([^\.][^\.]*\)$/\1/'`
	case $extension in
		"bz2") UNZIP="${TAR} -xjf" ;;
		"tar") UNZIP="${TAR} -xf" ;;
		"gz"|"tgz")  UNZIP="${TAR} -zxf" ;;
		*)     UNZIP="unzip" ;;
	esac
	`echo $UNZIP` ${TARGET} ${DIR}
	cd ${cwd}
	return $?
}

boldEcho(){
	if [ "$OS" = "Darwin" ] ; then
		echo $* | ${SED} 's/\(.\)/\1\1/g' | more
	else
		tput bold
		echo $*
		tput sgr0
	fi
}

# Setting most frequently used variables
export CXPS_HOME=/home/anurag/repo/sib/cxps
export PATH=${CXPS_HOME}/bin:$PATH
export CXPS_PROGRAMS="${CXPS_HOME}/.cxps-src"
export CXPS_DEVENV_VERSION="0.8.37"
export CXPS_BIN="${CXPS_HOME}/bin"
export CXPS_LIB="${CXPS_HOME}/lib"
export CXPS_DEV="${CXPS_HOME}/dev"
export CL5_LOCAL_REP="${HOME}/.cxps-rep"

SYSTEM=`uname -m`
OS=`uname -s`
LOGNAME=anurag
U=anurag
G=anurag

export SYSTEM OS U G

# Define the security folder where cacerts will get installed [ JAVA_SECURITY_FOLDER ]
JAVA_SECURITY_FOLDER="${JAVA_HOME}/jre/lib/security"
if [ ! -d "${JAVA_SECURITY_FOLDER}" ] ; then
    JAVA_SECURITY_FOLDER="${JAVA_HOME}/lib/security"
    if [ ! -d "${JAVA_SECURITY_FOLDER}" ] ; then
        echo "ERROR: Your JAVA_HOME does not contain the expected security folder. ${JAVA_HOME}/[jre/]lib/security"
    fi
fi
export JAVA_SECURITY_FOLDER

# Populate JAVA_OPTS and CATALINA_OPTS
export CATALINA_OPTS="${CATALINA_OPTS} -Djava.library.path=${CXPS_LIB}"
export CATALINA_OPTS=`echo $CATALINA_OUTS | tr ' 	' '\012\012' | grep -v '^[ 	]*$' | sort -u`
[ -z "$JAVA_OPTS" ] && export JAVA_OPTS="-server -Xms512m -Xmx1024m -XX:MaxPermSize=256m"

# Set up path for java, python and mysql
[ -z "$JAVA_HOME" ] || PATH=${JAVA_HOME}/bin:$PATH
[ -z "$PYTHON_HOME" ] || PATH=${PYTHON_HOME}/bin:$PATH
[ -z "$MYSQL_HOME" ] || PATH=${MYSQL_HOME}/bin:$PATH

export PATH=${CXPS_BIN}:${PATH}

# Making sure entire folder structure is available
[ -d ${CXPS_HOME} ] || mkdir -p ${CXPS_HOME}
[ -d ${CXPS_DEV} ] || mkdir -p ${CXPS_DEV}
[ -d ${CXPS_BIN} ] || mkdir -p ${CXPS_BIN}
[ -d ${CXPS_LIB} ] || mkdir -p ${CXPS_LIB}
[ -d ${CXPS_PROGRAMS} ] || mkdir -p ${CXPS_PROGRAMS}

export w=${CXPS_WORKSPACE}
export d=${CXPS_DEPLOYMENT}
export tl=${TOMCAT_HOME}/logs
export tw=${TOMCAT_HOME}/webapps

