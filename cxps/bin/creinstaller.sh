:

# ------------------
# MAIN ACTION BLOCK
# ------------------
. cxpsvars.sh

DEP=$1
[ -z "$DEP" ] && echo "Usage: creinstaller.sh <path-to-deployment-folder>" && exit 1

# Check if the folder exists
[ ! -d ${DEP} ] && echo "No deployment data found!" && exit 2

DEPLOYER=`pwd`/${CXPS_PRODUCT_NAME}-${CXPS_PRODUCT_VERSION}-`date +%y-%m-%d`-installer.sh
TMPDIR=/tmp/creinstaller
mkdir $TMPDIR

# Make cxpsvar ready 
printf "Creating zipped tar ... "
( cd ${DEP} && ${TAR} -czf $TMPDIR/PRODUCT.tgz * )
echo "done!"

# Create an extractor qouting the 'EOT' escapes the special meanings 
printf "Creating installer ..."
cat ${CXPS_BIN}/port.sh > ${DEPLOYER}
cat >> ${DEPLOYER} <<'EOT'

#check if the user is running this program as ROOT
if [ "`id -un`" = root ] ; then
	echo "ERROR: You must *NOT* execute this program as ROOT. Exiting ..."
	exit 1
fi

# ===================
# MAIN ACTION BLOCK
# ===================

# Remember the current executable
if echo "$0" | ${GREP} -q '^/'
then
	THIS=$0
else
	THIS=`pwd`/$0
fi

# Read the directory path for setting up the deployment env
while :
do
	echo "Enter the new deployment folder : "
	read deployment
	if [ ! -z "$deployment" ] ; then
		[ ! -d $deployment ] && mkdir -p $deployment && break 
		echo "Directory already exists!"
	else
		echo "Invalid Input"
	fi
done
cwd=`pwd`
cd $deployment
echo "Working ..."

SKIP=`${AWK} '/^__TARFILE_FOLLOWS__/ { print NR + 1; exit 0; }' $THIS`

# take the tarfile and pipe it into tar
${SED} -n $SKIP',$p' $THIS | ${TAR} zxf -

chmod +x bin/*.sh

[ -f bin/README ] && cat bin/README
exit 0

# NOTE: Don't place any newline characters after the last line below.
__TARFILE_FOLLOWS__
EOT

cat $TMPDIR/PRODUCT.tgz >> ${DEPLOYER}
echo "done!"

\rm -rf $TMPDIR
echo "Installer [${DEPLOYER}] is ready!"
