:

[ "$OS" = "Darwin" ] && alias ls='/bin/ls -p'
[ "$OS" = "Linux" ] && alias ls='/bin/ls --color=auto'

alias l='ls -l'
alias ll="/bin/ls -ltphr"
alias rm="rm -i"
alias rmf="rm -f"
alias rmo='rm -f *.o *.pyc *.pyo'
alias lo=exit
[ -z "`which vim`" ] || alias vi=vim

build(){
	build.sh $*
}

alias mci='build c'
alias mi='build ""'
alias moci='build oc'
alias mosci='build osc'
alias mosi='build os'
alias msci='build sc'
alias msi='build s'

alias ws='cd ${CXPS_DEV}/${CXPS_PRODUCT}/workspace'
alias dev='cd ${CXPS_DEV}'
alias dep='cd ${CXPS_DEPLOYMENT}'
alias apex='ws && cd Apex'
alias rice='ws && cd Rice'

alias tomcat='cd ${CXPS_DEPLOYMENT}/tomcat/logs'
alias st='tomcat ; ls -l; ${CXPS_WORKSPACE}/bin/cxpsctl.sh start && tail -f catalina.out'
alias St='tomcat ; ${CXPS_WORKSPACE}/bin/cxpsctl.sh stop'
alias ct='${CXPS_WORKSPACE}/bin/cxpsctl.sh status'

alias ,="cd ..;"
alias ,,="cd ../.."
alias ,,,="cd ../../.."
alias ,,,,="cd ../../../.."
alias ,,,,,="cd ../../../../.."
alias ,,,,,,="cd ../../../../../.."

alias b="cd ~-"

alias cxhelp="[ -f ${CXPS_LIB}/cxpshelp.txt ] && more ${CXPS_LIB}/cxpshelp.txt || echo 'Error:help file not found'"
