#!/usr/bin/env python

from __future__ import print_function

import builtins
from builtins import input

# ===========================================================================
# cxlister.py
# Aditya, June 2013
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This program is used for selecting a module / version
# from the list available in Global SVN URL
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This program starts by displaying local folder structure for any selection.
# In case of URL fetch it displays heirarchic selection
# ===========================================================================

import os,sys,time
import ColorText,SVNFetch

# ===========================================================================
# This function helps user choose from given list of items
# Items are given as (name,flag)
# 	flag True => bold else regular
# ===========================================================================
def choose(items, additional=None, extra=None) :

	ct = ColorText.ColorText(ColorText.BOLD, ColorText.FG_BLUE, ColorText.BG_WHITE)

	while True :
		for i in range(0,len(items)) :
			s = str(i+1)+") "+items[i][0]
			if items[i][1] == True :
				ct.write(s)
			else :
				print (s)
		if additional != None :
			for x in additional :
				i += 1
				print (str(i+1)+") "+x)
		print ("-------------------------------")
		if extra != None :
			for x in extra :
				print (x[0]+") "+x[1])
		print ("q) Quit")
		ans = str(input("Enter Choice: ")).strip()
		try :
			v = int(ans)
			if v > len(items) or v < 1 :
				raise ValueError
			return (v-1)
		except ValueError :
			if ans == "q" :
				return None
			elif ans == "" :
				continue
			if additional != None :
				if len(items) < v <= len(items)+len(additional) :
					print (additional[v-len(items)-1])
					return additional[v-len(items)-1]
					
			elif extra != None :
				for x in extra :
					if ans[0] == x[0] :
						return x[0]
			print ('Error: Invalid Value. Try again!')
			time.sleep(1)

def getLocalProjects() :
	dvdir = os.environ['CXPS_DEV']
	lst = [ dvdir + "/" + dir for dir in os.listdir(dvdir) if dir[0] != '.' ]
	lst.sort(key=os.path.getmtime, reverse=True)	
	return [ d[d.rfind('/') + 1:] for d in lst ]

def chooseLocal() :
	dirs = [ (d,False) for d in getLocalProjects() ]
	print ('---------------')
	print ('LOCAL Projects')
	print ('---------------')
	extra = [('s',"Set Remote SVN [currently LOCAL]")]
	if 'CX_WORKING_REMOTE' in os.environ :
		working_remote = os.environ['CX_WORKING_REMOTE']
		if working_remote != '' :
			extra = [('s','Set Local SVN [currently REMOTE]')]
	extra.append(('f',"Fetch all SVN Projects"))
	choice = choose(dirs, None, extra)
	if choice in ['f','s',None] :
		return choice
	else :
		return dirs[choice][0]
	
def output(outfile, project) :
	MODDIR=''
	if project.find('/') >= 0 :
		prodver = project.split('/')
		PRODUCT = prodver[0]
		VERSION = prodver[2]
		MODDIR = 'mod_' + project.replace('//','/')
	else :
		prodver = project.split('-',1)
		PRODUCT = prodver[0]
		if len(prodver) > 1 :
			VERSION=prodver[1]
		else :
			VERSION=''

	with open(outfile,'w') as f :
		f.write("PRODUCT="+PRODUCT+"\n")
		f.write("VERSION="+VERSION+"\n")
		if MODDIR != '' : f.write("MODDIR="+MODDIR+"\n")
	return

def isValid(envars) :
	for ev in envars :
		if ev not in os.environ :
			print ('Error: '+ev+' ENV variable not found!. Are you in DEVENV ?')
			sys.exit(1)

# ===================
# MAIN ACTION BLOCK
# ===================
if __name__ == '__main__' :

	isValid(['CXPS_PROGRAMS','CXPS_DEV','GLB_SVN_URL'])

	force = False
	ndx = 1
	if len(sys.argv) > 1 :
		if sys.argv[1].strip() == '-f' :
			force = True
			ndx = 2
	else :
		print('Usage:python %s [-f] outfile' % sys.argv[0])
		sys.exit(4)

	try :
		outfile = sys.argv[ndx].strip()
	except IndexError :
		print('Usage:python %s [-f] outfile' % sys.argv[0])
		sys.exit(4)

	# First choose Local
	choice = chooseLocal()
	if choice == None :
		sys.exit(1)
	elif choice == 's' :
		sys.exit(3)
	elif choice != 'f' :
		output(outfile,choice)
		sys.exit(0)

	lprojs = getLocalProjects()
	svn = SVNFetch.SVNFetch(force)
	svn.getUserPw()
	while True :
		(status,folders) = svn.fetch('.')
		if status == 'OK' :
			dirs = []
			modules = []
			for d in folders :
				if d.startswith('mod_') :
					# Module found
					modules.append('['+d[4:]+']...')
				else :
					dirs.append((d, True if d in lprojs else False))
			dirs.sort()
			choice = choose(dirs,modules)
			if choice == None :
				sys.exit(1)

			# Check if selection is that of module
			if str(choice).find('...') < 0 :
				output(outfile,dirs[choice][0])
				sys.exit(0)

			# Handle the module
			module = choice[1:-4]
			moddir = 'mod_' + module
			(status, folders) = svn.fetchModule(moddir)
			if status == 'OK_BUT' :	# basically its not a module
				output(outfile,moddir)
				sys.exit(0)
			elif status == 'OK' :
				# The folders are module format !/branch
				dirs = []
				for d in folders :
					m = d.split('/')
					mod = module +'-'+m[1]
					dirs.append((mod, True if mod in lprojs else False))

				print ('---------------------')
				print ('Module: ' + module)
				print ('---------------------')
				choice = choose(dirs)
				if choice == None :
					continue
				# DEBUG:
				# print choice, moddir + '/' + folders[choice]
				output(outfile, module + '/' + folders[choice])
				sys.exit(0)

		if status == 'INVALID_USPW' :
			print ('Error: Invalid SVN User/Passwd')
			svn.getUserPw()
		elif status == 'TIMEOUT' :
			print ('Error: Unable to connect to SVN URL')
			sys.exit(2)
		elif status == 'URLError' :
			print ('Error: Unable to connect ('+folders[0]+')')
			sys.exit(2)
		elif status == 'CONNECTError' :
			print ('Error: Unable to connect')
			sys.exit(2)
		elif status != 'OK' :
			print ('Error: Got HTTP error (' + str(folders[0]) + ')')
			sys.exit(2)
