#!/usr/bin/env python

import os, dbm

class cxCache :

	def __init__(self, name) :
		self.cacheFile = os.environ['CXPS_PROGRAMS'] + '/' + name
		self.readDB = None

	def __del__(self) :
		if self.readDB != None :
			self.readDB.close()

	def put(self, dic) :
		try :
			self.db = dbm.open(self.cacheFile, 'c')
		except dbm.error :
			self.readDB = None
		for (key,value) in dic.items() :
			self.db[key] = value
		self.db.close()

	def get(self, names) :
		if self.readDB == None :
			try :
				self.readDB = dbm.open(self.cacheFile, 'r')
			except dbm.error :
				self.readDB = None
				return {}
		output = {}
		for key in names :
			try :
				output[key] = self.readDB[key]
			except KeyError:
				output[key] = None
		return output
