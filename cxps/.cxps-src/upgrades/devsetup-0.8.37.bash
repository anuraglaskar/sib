#!/usr/bin/env bash

# ===========================================================================
# This file holds the global variable names for Unix utilities
# ===========================================================================
 
if [ -z "$__PORT_SH_EXECUTED" ] ; then

	SED=sed
	GREP=grep
	AWK=awk
	TAR=tar
	os=`uname -s`

	case "$os" in 
		"Linux")	p="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
					;;
		"SunOS")	p="/opt/csw/bin:/usr/xpg4/bin:/usr/bin:/bin"
					SED=gsed
					AWK=gawk
					TAR=gtar
					GREP=ggrep
					DIFF=gdiff
					;;
		"Darwin")	p="/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin"
					;;
		"*")		p="/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:"
					;;
	esac

	export __PORT_SH_EXECUTED=executed

	PATH=$p:$PATH

	export SED GREP AWK TAR PATH
fi

# MAIN ACTION BLOCK
# ------------------
if echo $0 | ${GREP} -q ^/
then
	PROGRAM="$0"
else
	PROGRAM=`pwd`/"$0"
fi

# Dump dependency information
if [ "$1" = "--info" ] ; then
	cat <<EOT
apr      : 1.3.9
maven    : 2.2.1
python   : 2.7
pyRXP    : 1.13
mysql    : 5.1
java     : 1.7
flex_sdk : 3.5
tomcat   : 6.0.33
activemq : 5.4.0
openfire : 3.6.3
oracle   : 11.2
kestrel  : 4.8.1
EOT
	exit 0
fi

# Check if the user is running this program as ROOT
if [ "`id -un`" = root ] ; then
	echo "ERROR: You must *NOT* execute this program as ROOT. Exiting ..."
	exit 1
fi

# Check if its an upgrade
UPGRADE=""
[ "$1" = "upgrade" ] && UPGRADE=upgrade

# Specify the devenv version
export CXPS_DEVENV_VERSION=0.8.37

#Author : Hemanth hemanth@customerxps.com
TMPFOLDER=`pwd`/cxps-onetime
if [ -d $TMPFOLDER ] ; then
	echo "!! WARNING !! WARNING !!"
	echo "Looks like another installation going on or a previous failed attempt."
	echo "Press Y to continue overwriting previous installation and any other key to quit"
	read answer
	[ "$answer" != "Y" ] && exit 0
fi

printf "Self Extracting into $TMPFOLDER ... "
SKIP=`${AWK} '/^__TARFILE_FOLLOWS__/ { print NR + 1; exit 0; }' $PROGRAM`

# take the tarfile and pipe it into tar
${SED} -n $SKIP',$p' $PROGRAM | ${TAR} -x

echo "Finished"
cd ${TMPFOLDER}
./cxpsdevsetup.sh ${UPGRADE}
cd ${TMPFOLDER}/..
\rm -rf ${TMPFOLDER}
exit 0

# NOTE: Don't place any newline characters after the last line below.
__TARFILE_FOLLOWS__
cxps-onetime/                                                                                       0000775 0001751 0001751 00000000000 13634711530 013072  5                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 cxps-onetime/bin/                                                                                   0000775 0001751 0001751 00000000000 13634711530 013642  5                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 cxps-onetime/bin/cxtemplate.sh                                                                      0000775 0001751 0001751 00000011425 13634711530 016352  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/bin/bash
# This script generates the file from its template after doing following substitutions.

. port.sh

# This method configures the template for the substitution parameters
# These are specified in an environment variable CX_TEMPLATE_SUBSTITUTIONS in a 
# pipe separated name=value format as follows -
# 	VERSION=baseline|DBVERSION=baseline|CXPS_DEPLOYMENT|..
# In case the '=' is not specified, it substitutes from the current env

__cxtemplate_crecmd(){
	if [ ! -z "$CX_TEMPLATE_SUBSTITUTIONS" ] ; then 
		export CX_TEMPLATE_CMD="${SED} "
		for nvp in `echo $CX_TEMPLATE_SUBSTITUTIONS | tr '|' '\012'`
		do
			if echo $nvp | ${GREP} -q '='
			then
				name=`echo $nvp | cut -f1 -d'='`
				value=`echo $nvp | cut -f2- -d'='`
			else
				name=`echo $nvp`
				value=`eval echo '$'$name`
			fi
			CX_TEMPLATE_CMD="$CX_TEMPLATE_CMD -e s,\\\${$name},$value,"
		done 
	fi
	export CX_TEMPLATE_SKIPLIST="${CX_TEMPLATE_SKIPLIST} -e /target/ -e /.svn/"
	# echo CX_TEMPLATE_SKIPLIST=$CX_TEMPLATE_SKIPLIST
	# echo CX_TEMPLATE_CMD=$CX_TEMPLATE_CMD
}

__cxtemplate_processFile(){
	input=$1
	output=$2
	$CX_TEMPLATE_CMD $input > $output
	if echo $output | ${GREP} -q '\.sh$'
	then
		chmod +x $output
	fi
}

# This method processes all templates within a given folder
__cxtemplate_processFolder(){

	quiet=0
	[ "$1" = '-q' ] && quiet=1 && shift 1
	folder=$1
	[ $quiet -eq 0 ] && echo "Working on all templates in [$folder/] "
	__cwd=`pwd`
	cd $folder
	templates=`find . -name '*.template.*' | ${GREP} -v $CX_TEMPLATE_SKIPLIST`
	for templateFile in $templates
	do
		echo "Processing $templateFile ..."
		file=`echo $templateFile | ${SED} 's/\.template\././'`
		__cxtemplate_processFile $templateFile $file
	done
	cd $__cwd
}

# This method processes a given set of jar files 
__cxtemplate_processJar(){
	cwd=`pwd`
	TMPFOLDER=/tmp/cxtemplate.$$
	[ -d $TMPFOLDER ] || mkdir -p $TMPFOLDER
	[ ! -z "$TMPFOLDER" ] && \rm -rf "${TMPFOLDER}/*"

	for jar in $*
	do
		if echo $jar | ${GREP} -q '.jar$'
		then
			allTemplates=`jar tf $jar | ${GREP} -v $CX_TEMPLATE_SKIPLIST | ${GREP} '\.template\.' | tr '\012' ' '`
			if [ ! -z "$allTemplates" ] ; then
				echo "Processing $jar ..."
				base=""
				if echo $jar | ${GREP} -q '^[^/]'
				then
					base=${cwd}/
				fi
				cd $TMPFOLDER
				# echo "jar xf ${base}$jar `echo $allTemplates`"
				jar xf ${base}$jar `echo $allTemplates`
				__cxtemplate_processFolder $TMPFOLDER >/dev/null
				# echo "jar uf ${base}$jar *"
				jar uf ${base}$jar *
				rm -f $allTemplates `echo $allTemplates | ${SED} 's/\.template\././g'`
				cd $cwd
			else
				echo "Skipping $jar ..."
			fi
		else
			echo "Skipping $jar ..."
		fi
	done

	cd $cwd
	rmdir ${TMPFOLDER}
}

# This method generates a makefile for all templates within a given folder.
genMakefile(){

	folder=$1
	makefile=$2
	[ -z "$makefile" ] && makefile=makefile
	cwd=`pwd`

	cd $folder

	templates=`find . -name '*.template.*' | ${GREP} -v $CX_TEMPLATE_SKIPLIST`
	files=`echo $templates | ${SED} 's/\.template\././g'`

	echo "ALLFILES = \\" > $makefile 
	for file in $files
	do   
		echo "      $file \\"
	done >> $makefile

	cat >> $makefile <<EOT

all: \$(ALLFILES)

clean:
	-rm \$(ALLFILES)

EOT

	for templateFile in $templates
	do
		file=`echo $templateFile | ${SED} 's/\.template\././'`
		echo "$file: $templateFile"
		echo "	@cxtemplate.sh $templateFile $file"
		echo
	done >> $makefile
}

__cxtemplate_usage(){
	echo "Usage: cxtemplate.sh configure [-skip pattern,pattern,...] [<substitution-string>]"
	echo "       cxtemplate.sh make <folder-name> [makefile]"
	echo "       cxtemplate.sh <folder-name>"
	echo "       cxtemplate.sh <jar> ..."
	echo "       cxtemplate.sh <template-file> [output-file]"
}

# ------------------
# MAIN ACTION BLOCK
# ------------------
if [ $# -eq 0 ] ; then
	__cxtemplate_usage

elif [ "$1" = "configure" ] ; then
	if [ "$2" = "-skip" ] ; then
		if [ -z "$3" ] ; then
			export CX_TEMPLATE_SKIPLIST=''
		else
			export CX_TEMPLATE_SKIPLIST="`echo ,$3 | ${SED} -e 's/,,*/,/g' -e 's/,$//' -e 's/,/ -e /g'`"
		fi
		shift 2
	fi
	[ ! -z "$2" ] && CX_TEMPLATE_SUBSTITUTIONS=$2

	printf "Configuring template processor ..."
	__cxtemplate_crecmd
	echo " Done!"

elif [ "$1" = "make" ] ; then
	if [ -d "$2" ] ; then
		printf "Generating template processor makefile ..."
		genMakefile $2 $3
		echo " Done!"
	else
		__cxtemplate_usage
	fi

elif [ ! -z "$CX_TEMPLATE_CMD" ] ; then

	if [ -d "$1" ] ; then
		__cxtemplate_processFolder $1

	elif [ -f "$1" ] ; then
		if echo "$1" | ${GREP} -q '.jar$' ; then
			__cxtemplate_processJar $*
		else
			templateFile=$1
			file=$2
			[ -z "$file" ] && file=`echo $templateFile | ${SED} 's/\.template\././'`
			printf "Processing $templateFile ..."
			__cxtemplate_processFile $templateFile $file
			echo " Done!"
		fi
	else
		echo "Error:$1 is not a folder/file"
	fi
else
	echo "Error: cxtemplate.sh has not been configured yet"
fi
                                                                                                                                                                                                                                           cxps-onetime/bin/cxupg.sh                                                                           0000775 0001751 0001751 00000004761 13634711530 015337  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/bin/bash

. ${CXPS_BIN}/cxpsvars.sh

DEVRES=${GLB_RES_URL}/resources

upgradeDevenv()
{
	version=$1
	[ -z "$version" ] && return 0
	printf "Version $version available. Install [Y/N] ? "
	read ans
	if [ "$ans" = "Y" ] ; then
		[ ! -d ${CXPS_PROGRAMS}/upgrades ] && mkdir -p ${CXPS_PROGRAMS}/upgrades
		cd ${CXPS_PROGRAMS}/upgrades
		file="devsetup-${version}.bash"
		[ -f ${file} ] && \rm -f ${file}
		download ${DEVRES}/${file}
		[ -f ${file} ] && bash ./${file} upgrade && return 0
		echo "Unable to download ${file} from ${DEVRES}"
		return 1
	fi
}

isHigher()
{
	patch=""
	if [ "$1" = "patch" ] ; then
		patch=patch
		shift 1
	fi

	v1=$1
	v2=$2

	major1=`echo $v1 | cut -f1 -d'.'`
	minor1=`echo $v1 | cut -f2 -d'.'`
	patch1=`echo $v1 | cut -f3 -d'.'`
	[ -z "$patch1" ] && patch1="0"

	major2=`echo $v2 | cut -f1 -d'.'`
	minor2=`echo $v2 | cut -f2 -d'.'`
	patch2=`echo $v2 | cut -f3 -d'.'`
	[ -z "$patch2" ] && patch2="0"

	if [ ! -z "$patch" ] ; then
		if [ $major1 -ne $major2 -o $minor1 -ne $minor2 ] ; then
			return 1
		fi
	fi

	[ $major1 -gt $major2 ] && echo $v1 && return 0
	[ $major2 -gt $major1 ] && echo $v2 && return 0

	[ $minor1 -gt $minor2 ] && echo $v1 && return 0
	[ $minor2 -gt $minor1 ] && echo $v2 && return 0

	[ $patch1 -gt $patch2 ] && echo $v1 && return 0
	[ $patch2 -gt $patch1 ] && echo $v2 && return 0

	echo "EQUAL"
	return 1
}

deriveVersion()
{
	patch=$1
	nextVersion=$CXPS_DEVENV_VERSION
	LIST=/tmp/upg-filelist.$$
	curl --silent --insecure --connect-timeout 1 --retry 1 ${DEVRES}/ > $LIST
	[ $? -ne 0 ] && return 1
	cat $LIST | 
		${GREP} devsetup | 					# filter  only devsetup
		${SED} 's/^.*href=//' | 			# Clean up till href
		cut -f2 -d'"' | 					# extract the file
		${SED} 's/^[^0-9]*\([0-9]\)/\1/' |	# Remove "devsetup[-fat]-"
		${SED} 's/.bash//' |				# Remove ".bash"
		sort -u	| 							# Get the unique version list

	while read ver
	do
		n=`isHigher $patch "$nextVersion" "$ver"`
		[ $? -eq 0 ] && nextVersion=$n
		echo $nextVersion
	done | 
	tail -1
	\rm -f $LIST
	return 0
}

# -------------------
# Main Action Block
# -------------------

if [ -z "$CXPS_DEVENV_VERSION" ] ; then
	echo "Error: CXPS_DEVENV_VERSION variable is not set. Check your env"
	exit 1
fi

patch=""
if [ "$1" = "patch" ] ; then
	shift 1
	patch=patch
fi

if [ $# -eq 1 ] ; then
	version=$1
else
	version=`deriveVersion $patch`
	[ $? -ne 0 ] && exit 1
fi

if [ "$CXPS_DEVENV_VERSION" = "$version" ] ; then
	[ -z "$patch" ] && echo "Your current version $version is already the latest"
	exit 0
fi

upgradeDevenv ${version}
               cxps-onetime/bin/creinstaller.sh                                                                    0000664 0001751 0001751 00000003366 13634711530 016675  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 :

# ------------------
# MAIN ACTION BLOCK
# ------------------
. cxpsvars.sh

DEP=$1
[ -z "$DEP" ] && echo "Usage: creinstaller.sh <path-to-deployment-folder>" && exit 1

# Check if the folder exists
[ ! -d ${DEP} ] && echo "No deployment data found!" && exit 2

DEPLOYER=`pwd`/${CXPS_PRODUCT_NAME}-${CXPS_PRODUCT_VERSION}-`date +%y-%m-%d`-installer.sh
TMPDIR=/tmp/creinstaller
mkdir $TMPDIR

# Make cxpsvar ready 
printf "Creating zipped tar ... "
( cd ${DEP} && ${TAR} -czf $TMPDIR/PRODUCT.tgz * )
echo "done!"

# Create an extractor qouting the 'EOT' escapes the special meanings 
printf "Creating installer ..."
cat ${CXPS_BIN}/port.sh > ${DEPLOYER}
cat >> ${DEPLOYER} <<'EOT'

#check if the user is running this program as ROOT
if [ "`id -un`" = root ] ; then
	echo "ERROR: You must *NOT* execute this program as ROOT. Exiting ..."
	exit 1
fi

# ===================
# MAIN ACTION BLOCK
# ===================

# Remember the current executable
if echo "$0" | ${GREP} -q '^/'
then
	THIS=$0
else
	THIS=`pwd`/$0
fi

# Read the directory path for setting up the deployment env
while :
do
	echo "Enter the new deployment folder : "
	read deployment
	if [ ! -z "$deployment" ] ; then
		[ ! -d $deployment ] && mkdir -p $deployment && break 
		echo "Directory already exists!"
	else
		echo "Invalid Input"
	fi
done
cwd=`pwd`
cd $deployment
echo "Working ..."

SKIP=`${AWK} '/^__TARFILE_FOLLOWS__/ { print NR + 1; exit 0; }' $THIS`

# take the tarfile and pipe it into tar
${SED} -n $SKIP',$p' $THIS | ${TAR} zxf -

chmod +x bin/*.sh

[ -f bin/README ] && cat bin/README
exit 0

# NOTE: Don't place any newline characters after the last line below.
__TARFILE_FOLLOWS__
EOT

cat $TMPDIR/PRODUCT.tgz >> ${DEPLOYER}
echo "done!"

\rm -rf $TMPDIR
echo "Installer [${DEPLOYER}] is ready!"
                                                                                                                                                                                                                                                                          cxps-onetime/bin/mig-0.8.16.bash                                                                    0000664 0001751 0001751 00000000436 13634711530 016010  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 # The Oracle programs have been updated for tnsnames.ora
# Hence we need to clean up any leftover oracle tgz
cd $CXPS_HOME/.cxps-src
\rm -f oracle*.tgz

if [ ! -z "$ORACLE_HOME" ] ; then
	cd $ORACLE_HOME
	if [ ! -d network -a -d admin ] ; then
		mkdir network
		mv admin network
	fi
fi
                                                                                                                                                                                                                                  cxps-onetime/bin/bootdb.sh                                                                          0000775 0001751 0001751 00000002337 13634711530 015457  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/bin/bash

#Create DB based on the DB name 
createDb(){

	DB=$1
	PASS=""
	USER="-uroot"

	# Get Root password
	if [ $# -gt 0 ];
	then
		PASS=$1
		if [ $PASS = 'None' ];
		then
			PASS=""
		fi
	else
		printf "Enter password for root [default=NULL]:"
		read ans
		PASS=$ans
	fi
	[ -z "$PASS" ] || PASS="-p$PASS"

	U=cxps
	P=cxps

	echo "Dropping $DB ..."
	mysqladmin -sf $USER $PASS drop $DB

	echo "Creating $DB ..."
	mysqladmin -sf $USER $PASS create $DB

	echo "Creating User $U ..."
	echo "CREATE USER '"$U"' IDENTIFIED BY '"$P"';" | mysql $USER $PASS
	echo "Granting privileges to local host ..."
	echo "GRANT ALL ON $DB.* to '"$U"'@'localhost' IDENTIFIED BY '"$P"';" | mysql $USER $PASS
	echo "Granting privileges to other hosts ..."
	echo "GRANT ALL ON $DB.* to '"$U"'@'%' IDENTIFIED BY '"$P"';" | mysql $USER $PASS
}

# Create DB for cas-server
casDb(){
	printf "Setting up DB for cas-server..."
	( cd ${CXPS_WORKSPACE}/cas-server ; createDb "CASDB" )
	[ $? -eq 0 ] && echo "Done!" || echo "Failed to create DB!"
}

# Create DB for Rice
riceDb(){
	printf "Setting up DB for Rice deployment..."
	( cd ${CXPS_WORKSPACE}/Rice/Deployment ; createDb "SEER_DB_2_0" )
	[ $? -eq 0 ] && echo "Done!" || echo "Failed to create DB!"
}

casDb 
riceDb
                                                                                                                                                                                                                                                                                                 cxps-onetime/bin/depsetup.sh                                                                        0000775 0001751 0001751 00000040273 13634711530 016040  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env bash

# This program sets the deployment folder along with configuration setup for the same.

. cxpsvars.sh	# Assuming path is set

unalias ins_maven 2>/dev/null
ins_maven()
{
	MAVEN_VERSION=`${GREP} maven ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export MAVEN_PROGRAM="maven-${MAVEN_VERSION}"
	if [ ! -d ${CXPS_BIN}/${MAVEN_PROGRAM} ] ; then
		printf "Installing MAVEN ... "
		cxinstall ${MAVEN_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f maven && ln -fs ${CXPS_BIN}/${MAVEN_PROGRAM} maven )
	else
		( cd ${CXPS_BIN} && rm -f maven && ln -fs ${MAVEN_PROGRAM} maven )
	fi
}

unalias ins_oracleXE 2>/dev/null
ins_oracleXE()
{
	if [ "$OS" != "Linux" ] ; then
		echo "Oracle XE is not available for your platform"
		return
	fi
	if uname -a | ${GREP} -q x86_64 >/dev/null
	then
		echo "Oracle-XE is not available for 64-bit"
	else
		# Install Oracle XE
		if ${GREP} -q 192.168.5.56 /etc/apt/sources.list
		then
			echo "/etc/apt/sources.list is already up-to-date"
		else
			sudo sh -c 'echo "deb http://192.168.5.56/debian unstable main non-free" >> /etc/apt/sources.list'
			wget http://oss.oracle.com/el4/RPM-GPG-KEY-oracle -O- | sudo apt-key add -
			sudo apt-get update
		fi

		if [ ! -f /etc/init.d/oracle-xe ] ; then
			sudo apt-get install oracle-xe
			echo "You will be asked for Oracle sys/system password for which provide 'manager' as default. Provide 8080 as default HTTP port."
			echo "Press <ENTER> to start configuration"
			read ans
			sudo /etc/init.d/oracle-xe configure
		else
			echo "Oracle-XE is already installed!"
		fi

	# Do not set ORACLE_HOME because it has to be explicitly set in devvars.sh
	#
	#	if [ -f /etc/init.d/oracle-xe -a -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
	#		( cd ${CXPS_WORKSPACE}/bin && \rm -f oracle && ln -fs /usr/lib/oracle/xe/app/oracle/product/10.2.0/server oracle )
	#	else
	#		( cd ${CXPS_BIN} && \rm -f oracle && ln -fs /usr/lib/oracle/xe/app/oracle/product/10.2.0/server oracle )
	#	fi
	fi
}

unalias ins_oracle 2>/dev/null
ins_oracle()
{
	ORACLE_VERSION=`${GREP} -i 'oracle[ 	]' ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	ORACLE_PROGRAM=""
	if [ -z "`which sqlplus`" ] ; then
		case "$OS" in
			"Linux" )	if uname -a | ${GREP} -q x86_64 >/dev/null
						then
							export ORACLE_PROGRAM=oracle64-${ORACLE_VERSION}
						else
							export ORACLE_PROGRAM=oracle32-${ORACLE_VERSION}
						fi
						;;
			"Darwin")	ORACLE_VERSION=10.2
						export ORACLE_PROGRAM=oracle32-Darwin-${ORACLE_VERSION}
						;;
			"*")		echo "WARNING: No oracle found. Please install and add its path in .profile" >&2
						;;
		esac

		if [ ! -z "$ORACLE_PROGRAM" ] ; then
			if [ ! -d "${CXPS_BIN}/${ORACLE_PROGRAM}" ] ; then
				printf "Installing Oracle Client..."
				cxinstall ${ORACLE_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
				[ $? -eq 0 ] && echo "done!"
			fi

            if [ ! -d ${CXPS_BIN}/${ORACLE_PROGRAM}/network ]; then
                [ -d ${CXPS_BIN}/${ORACLE_PROGRAM}/admin ] \
                    && mkdir ${CXPS_BIN}/${ORACLE_PROGRAM}/network \
                    && mv ${CXPS_BIN}/${ORACLE_PROGRAM}/admin ${CXPS_BIN}/${ORACLE_PROGRAM}/network
            fi

			if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
				( cd ${CXPS_WORKSPACE}/bin && \rm -f oracle && ln -fs ${CXPS_BIN}/${ORACLE_PROGRAM} oracle )
			else
				( cd ${CXPS_BIN} && \rm -f oracle && ln -fs ${ORACLE_PROGRAM} oracle )
			fi
		fi
	fi
}

unalias ins_flex 2>/dev/null
ins_flex()
{
    FLEX_VERSION=`${GREP} flex ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	FLEX=`${GREP} flex ${DEPFILE} | cut -f1 -d':' | ${SED} 's/[ 	]*//g'`
	export FLEX_PROGRAM="${FLEX}-${FLEX_VERSION}"
	if [ ! -d ${CXPS_BIN}/${FLEX_PROGRAM} ] ; then
		printf "Installing flex ... "
		cxinstall ${FLEX_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f ${FLEX} && ln -fs ${CXPS_BIN}/${FLEX_PROGRAM} ${FLEX} )
	else
		( cd ${CXPS_BIN} && \rm -f ${FLEX} && ln -fs ${FLEX_PROGRAM} ${FLEX} )
	fi
}

unalias ins_apr 2>/dev/null
ins_apr()
{
	# Note that this is a dependency of TOMCAT and therefore the deps.cfg should be picked from elsewhere
	APR_VERSION=`${GREP} apr ${CXPS_BIN}/deps.cfg | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	if [ ! -d "${CXPS_LIB}/apr" -a ! -f "${CXPS_LIB}/apr.failed" ] ; then
		echo "Installing Apache Portable Runtime..."
		cxinstall apr-${APR_VERSION}.tgz "--dir=${CXPS_LIB}" ${CXPS_PROGRAMS}
		if [ $? -eq 0 ] ; then
			( 
				cd ${CXPS_LIB}/apr-${APR_VERSION}
				./configure --prefix=${CXPS_LIB}/apr
				make -j 2 && make -j 2 install
			)
			[ ! -d "${CXPS_LIB}/apr" ] && touch ${CXPS_LIB}/apr.failed
		fi
	fi
}

unalias ins_cxpsapache 2>/dev/null
ins_cxpsapache()
{
    CXPSAPACHE_VERSION=`${GREP} cxpsapache ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[   ]*//g'`
    export CXPSAPACHE_PROGRAM=cxpsapache-${CXPSAPACHE_VERSION}
    if [ ! -d ${CXPS_DEPLOYMENT}/${CXPSAPACHE_PROGRAM} ] ; then
        if [ ! -f ${CXPS_PROGRAMS}/${CXPSAPACHE_PROGRAM}.tgz ] ; then
            printf "Downloading cxpsapache......"
            cxinstall ${CXPSAPACHE_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
            [ $? -eq 0 ] && echo "Done!"
        else
            cd ${CXPS_DEPLOYMENT}
            if [ ! -d ${CXPSAPACHE_PROGRAM} ] ; then
                printf "Installing cxpsapache ... "
                ${TAR} xf ${CXPS_PROGRAMS}/${CXPSAPACHE_PROGRAM}.tgz
                echo "Done!"
            fi
        fi
    fi

    cd ${CXPS_DEPLOYMENT}
    rm -f cxpsapache ; ln -fs ${APPAGENT_PROGRAM} cxpsapache 
}
unalias ins_tomcat 2>/dev/null
ins_tomcat()
{
	# Install the dependency
	ins_apr

	TOMCAT_VERSION=`${GREP} tomcat ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export TOMCAT_PROGRAM="tomcat-${TOMCAT_VERSION}"
	# Check if it already exist in ${CXPS_DEPLOYMENT}
	if [ ! -d ${CXPS_DEPLOYMENT}/${TOMCAT_PROGRAM} ] ; then
		if [ ! -d ${CXPS_PROGRAMS}/${TOMCAT_PROGRAM} ] ; then
			echo "Configuring Tomcat with APR ..."
			cxinstall ${TOMCAT_PROGRAM}.tgz "--dir=${CXPS_PROGRAMS}" ${CXPS_PROGRAMS}
			if [ $? -eq 0 ] ; then
				(	cd ${CXPS_PROGRAMS}/${TOMCAT_PROGRAM}/bin ;
					${TAR} -xzf tomcat-native.tar.gz ;
					cd tomcat-native-*-src/jni/native ;
					./configure --with-apr=${CXPS_LIB}/apr --with-java-home="$JAVA_HOME" ;
					make -j 2 ;
					( cd .libs && ${TAR} cvf - * ) | ( cd ${CXPS_LIB} && ${TAR} xvf - )
				)
			fi
		fi
		cd ${CXPS_DEPLOYMENT}
		if [ ! -d ${TOMCAT_PROGRAM} ] ; then
			printf "Installing Tomcat ... "
			( cd ${CXPS_PROGRAMS} && ${TAR} cf - ${TOMCAT_PROGRAM} ) | ${TAR} xf - 
			echo "Done!"
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f tomcat ; ln -fs ${TOMCAT_PROGRAM} tomcat
	( cd tomcat ; mkdir -p apex/lib ; mkdir -p apex/classes )
}

unalias ins_activemq 2>/dev/null
ins_activemq()
{
	ACTIVEMQ_VERSION=`${GREP} activemq ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export ACTIVEMQ_PROGRAM=activemq-${ACTIVEMQ_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${ACTIVEMQ_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${ACTIVEMQ_PROGRAM}.tgz ] ; then
			printf "Downloading ActiveMQ..."
			cxinstall ${ACTIVEMQ_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${ACTIVEMQ_PROGRAM} ] ; then
				printf "Installing ActiveMQ ... "
				${TAR} xf ${CXPS_PROGRAMS}/${ACTIVEMQ_PROGRAM}.tgz 
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f activemq ; ln -fs ${ACTIVEMQ_PROGRAM} activemq
}

unalias ins_openfire 2>/dev/null
ins_openfire()
{
	OPENFIRE_VERSION=`${GREP} openfire ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export OPENFIRE_PROGRAM=openfire-${OPENFIRE_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${OPENFIRE_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${OPENFIRE_PROGRAM}.tgz ] ; then
			printf "Downloading OpenFire..."
			cxinstall ${OPENFIRE_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${OPENFIRE_PROGRAM} ] ; then
				printf "Installing Openfire ... "
				${TAR} xf ${CXPS_PROGRAMS}/${OPENFIRE_PROGRAM}.tgz
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f openfire ; ln -fs ${OPENFIRE_PROGRAM} openfire
}

unalias ins_kestrel 2>/dev/null
ins_kestrel()
{
	KESTREL_VERSION=`${GREP} kestrel ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export KESTREL_PROGRAM=kestrel-${KESTREL_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${KESTREL_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${KESTREL_PROGRAM}.tgz ] ; then
			printf "Downloading Kestrel..."
			cxinstall ${KESTREL_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${KESTREL_PROGRAM} ] ; then
				printf "Installing Kestrel ... "
				${TAR} xf ${CXPS_PROGRAMS}/${KESTREL_PROGRAM}.tgz
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f kestrel ; ln -fs ${KESTREL_PROGRAM} kestrel
}

unalias ins_appagent 2>/dev/null
ins_appagent()
{
    APPAGENT_VERSION=`${GREP} appagent ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[   ]*//g'`
    export APPAGENT_PROGRAM=appagent-${APPAGENT_VERSION}
    if [ ! -d ${CXPS_DEPLOYMENT}/${APPAGENT_PROGRAM} ] ; then
        if [ ! -f ${CXPS_PROGRAMS}/${APPAGENT_PROGRAM}.tgz ] ; then
            printf "Downloading appdynamics appagent"
            cxinstall ${APPAGENT_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
            [ $? -eq 0 ] && echo "Done!"
        else
            cd ${CXPS_DEPLOYMENT}
            if [ ! -d ${APPAGENT_PROGRAM} ] ; then
                printf "Installing appdynamics appagent ... "
                ${TAR} xf ${CXPS_PROGRAMS}/${APPAGENT_PROGRAM}.tgz
                echo "Done!"
            fi
        fi
    fi

    cd ${CXPS_DEPLOYMENT}
    rm -f appdynamics ; ln -fs ${APPAGENT_PROGRAM} appdynamics 
}

unalias ins_CXQ 2>/dev/null
ins_CXQ()
{
	CSB_VERSION=`${GREP} CXQ ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export CSB_PROGRAM=CXQ-${CSB_VERSION}
	if [ ! -d ${CXPS_DEPLOYMENT}/${CSB_PROGRAM} ] ; then
		if [ ! -f ${CXPS_PROGRAMS}/${CSB_PROGRAM}.tgz ] ; then
			printf "Downloading CXQ"
			cxinstall ${CSB_PROGRAM}.tgz "--dir=${CXPS_DEPLOYMENT}" ${CXPS_PROGRAMS}
			[ $? -eq 0 ] && echo "Done!"
		else
			cd ${CXPS_DEPLOYMENT}
			if [ ! -d ${CSB_PROGRAM} ] ; then
				printf "Installing CXQ ... "
				${TAR} xf ${CXPS_PROGRAMS}/${CSB_PROGRAM}.tgz
				echo "Done!"
			fi
		fi
	fi

	cd ${CXPS_DEPLOYMENT}
	rm -f CXQ ; ln -fs ${CSB_PROGRAM} CXQ
}

unalias ins_antlr 2>/dev/null
ins_antlr()
{
    ANTLR_VERSION=`${GREP} antlr ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export ANTLR_PROGRAM="antlr-${ANTLR_VERSION}"
	if [ ! -d ${CXPS_BIN}/${ANTLR_PROGRAM} ] ; then
		printf "Installing antlr ... "
		cxinstall ${ANTLR_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f antlr && ln -fs ${CXPS_BIN}/${ANTLR_PROGRAM} antlr )
	else
		( cd ${CXPS_BIN} && \rm -f antlr && ln -fs ${ANTLR_PROGRAM} antlr )
	fi
}

unalias ins_gradle 2>/dev/null
ins_gradle()
{
    GRADLE_VERSION=`${GREP} gradle ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export GRADLE_PROGRAM="gradle-${GRADLE_VERSION}"
	local gradle_install=0
	if [ -z "`which gradle`" ] ; then
		gradle_install=1
	else
		version=`gradle --version | ${GREP} ^Gradle | cut -d ' ' -f2`
        if [ $version != ${GRADLE_VERSION} ]; then
            newer=$( echo -e "${version}\n${GRADLE_VERSION}" | sort -V | tail -1 )
            [[ $newer = ${GRADLE_VERSION} ]] && gradle_install=1
        fi
	fi
	[ $gradle_install -eq 0 ] && return
	if [ "$OS" = "Linux" ] ; then
		wget http://192.168.5.56/resources/${GRADLE_PROGRAM}-bin.zip
	    #wget --no-check-certificate ${GLB_RES_URL}/${file}-bin.zip 
		mv ${GRADLE_PROGRAM}-bin.zip /tmp/${GRADLE_PROGRAM}.zip
		if [ $? -eq 0 ] ; then
			( cd /usr/local && sudo unzip /tmp/${GRADLE_PROGRAM}.zip && \rm -f /tmp/${GRADLE_PROGRAM}.zip )
			( cd /usr/local/bin && sudo ln -fs ../${GRADLE_PROGRAM}/bin/gradle . )
		fi
	else
		echo "Please install gradle [$GRADLE_VERSION] using OS specific installer" >&2
	fi
}

unalias ins_codegen 2>/dev/null
ins_codegen()
{
    CODEGEN_VERSION=`${GREP} codegen ${DEPFILE} | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	export CODEGEN_PROGRAM="codegen-${CODEGEN_VERSION}"
	if [ ! -d ${CXPS_BIN}/${CODEGEN_PROGRAM} ] ; then
		printf "Installing codegen ... "
		cxinstall ${CODEGEN_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}
		[ $? -eq 0 ] && echo "Done!"
	fi
	if [ -f ${CXPS_WORKSPACE}/bin/deps.cfg ] ; then
		( cd ${CXPS_WORKSPACE}/bin && \rm -f codegen && ln -fs ${CXPS_BIN}/${CODEGEN_PROGRAM}/bin/gen codegen )
	fi
}

ins_maxmind(){
	curdir=`pwd`
	MAXMIND_PROGRAM=maxmind
	datFile=$CXPS_DEPLOYMENT/GeoLiteCity.dat
	#echo Checking ${MAXMIND_PROGRAM}...
	if [ ! -f $datFile ]; then
		if [ ! -f $CXPS_PROGRAMS/$MAXMIND_PROGRAM.zip ] ; then
			echo "Downloading maxmind.."
			cd ${CXPS_PROGRAMS}
			download $GLB_DEVRES_URL/${MAXMIND_PROGRAM}.zip
			[ $? -eq 0 ] && echo "Done!"
			cd $curdir
		fi

		echo "Deploying maxmind datafile in $CXPS_DEPLOYMENT"
		unzip ${CXPS_PROGRAMS}/${MAXMIND_PROGRAM}.zip -d ${CXPS_DEPLOYMENT} > /dev/null
		if [ -f $datFile ]; then
			echo "Maxmind setup done!"
		else
			echo "Maxmind setup failed!"
		fi
	fi
}

createPath()
{
	# Update the PATH file
	cat > ${CXPS_WORKSPACE}/bin/path.sh <<EOT
#!/bin/sh

[ -d \${CXPS_WORKSPACE}/bin/${FLEX} ] && export FLEX_HOME=\${CXPS_WORKSPACE}/bin/${FLEX}
[ -d \${CXPS_WORKSPACE}/bin/maven ] && export M2_HOME=\${CXPS_WORKSPACE}/bin/maven
[ -d \${CXPS_WORKSPACE}/bin/oracle ] && export ORACLE_HOME=\${CXPS_WORKSPACE}/bin/oracle
[ -d \${CXPS_WORKSPACE}/bin/antlr ] && export ANTLR_HOME=\${CXPS_WORKSPACE}/bin/antlr

[ ! -z "\${my_FLEX_HOME}" ] && export FLEX_HOME=\${my_FLEX_HOME}
if [ -d \${CXPS_WORKSPACE}/bin/oracle -a ! -z "\${my_ORACLE_HOME}" ] ; then
	\rm -f \${CXPS_WORKSPACE}/bin/oracle
	ln -fs \${my_ORACLE_HOME} \${CXPS_WORKSPACE}/bin/oracle
fi

export MAVEN_PROGRAM=${MAVEN_PROGRAM}
export FLEX_PROGRAM=${FLEX_PROGRAM}
export TOMCAT_PROGRAM=${TOMCAT_PROGRAM}
export OPENFIRE_PROGRAM=${OPENFIRE_PROGRAM}
export ACTIVEMQ_PROGRAM=${ACTIVEMQ_PROGRAM}
export ORACLE_PROGRAM=${ORACLE_PROGRAM}

export TOMCAT_HOME=${CXPS_DEPLOYMENT}/${TOMCAT_PROGRAM}
export ACTIVEMQ_HOME=${CXPS_DEPLOYMENT}/${ACTIVEMQ_PROGRAM}
export OPENFIRE_HOME=${CXPS_DEPLOYMENT}/${OPENFIRE_PROGRAM}

[ -z "\$MAVEN_OPTS" ] && export MAVEN_OPTS="-server -Xms100m -Xmx512m -XX:MaxPermSize=256m"

# Set up path for flex, java, python, mysql and maven
[ -z "\$M2_HOME" ] || PATH=\${M2_HOME}/bin:\$PATH
[ -z "\$FLEX_HOME" ] || PATH=\${FLEX_HOME}/bin:\$PATH
if [ ! -z "\$ORACLE_HOME" ] ; then
	PATH=\${ORACLE_HOME}:\${ORACLE_HOME}/bin:\$PATH
	export LD_LIBRARY_PATH=\${ORACLE_HOME}:\${ORACLE_HOME}/lib
	export DYLD_LIBRARY_PATH=\${LD_LIBRARY_PATH}
	export LIBPATH=\${LD_LIBRARY_PATH}
fi

[ ! -z "\${ANTLR_HOME}" ] && export PATH=\${PATH}:\${ANTLR_HOME}/bin

# Put workspace/bin in PATH
export PATH=\${CXPS_WORKSPACE}/bin:\${PATH}
EOT
}

# ------------------
# MAIN ACTION BLOCK
# ------------------
me=`basename $0`

if [ $# -ne 2 ] ; then
	echo "Usage:$me <DEPFILE> <DEPLOYMENT-FOLDER>"
	exit 1
fi

# Load localized installation methods
EXTN=$CXPS_WORKSPACE/bin/depsetupextn.sh
[ -f $EXTN ] && chmod +x $EXTN && . $EXTN

# Change to deployment folder to get debs

DEPFILE=$1
if [ ! -f "${DEPFILE}" ] ; then
	echo "Error:$me: ${DEPFILE} does not exist"
	exit 2
fi
if ! echo ${DEPFILE} | ${GREP} -q '^/' ; then
	DEPFILE=`pwd`/${DEPFILE}
fi

CXPS_DEPLOYMENT=$2
if [ ! -d ${CXPS_DEPLOYMENT} ] ; then
	mkdir -p ${CXPS_DEPLOYMENT} 2>/dev/null
	[ $? -ne 0 ] && echo "Error:$me: Unable to create ${CXPS_DEPLOYMENT}" && exit 2
fi
cd ${CXPS_DEPLOYMENT}

for dep in `cat ${DEPFILE} | ${SED} 's/[ 	]*//g' | tr ':' '-' | tr '\012' ' '`
do
	case "$dep" in
		maven*)		ins_maven ;;
		tomcat*)	ins_tomcat ;;
		cxpsapache*)ins_cxpsapache ;;
		openfire*)	ins_openfire ;;
		activemq*)	ins_activemq ;;
		oracleXE*)	ins_oracleXE ;;
		oracle*)	ins_oracle ;;
		kestrel*)	ins_kestrel ;;
		flex*)		ins_flex ;;
		CXQ*)		ins_CXQ ;;
		appagent*)  ins_appagent ;;
		antlr*)		ins_antlr ;;
		codegen*)	ins_codegen ;;
		maxmind*)	ins_maxmind ;;
		gradle*)	ins_gradle ;;
	esac

	type dep_inst 2>&1 | grep -q function && dep_inst $dep
done

# Create the PATH file
[ -d ${CXPS_WORKSPACE}/bin ] && createPath
                                                                                                                                                                                                                                                                                                                                     cxps-onetime/bin/dos2unix.sh                                                                        0000775 0001751 0001751 00000000513 13634711530 015753  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/bin/sh

[ $# -eq 0 ] && echo "dos2unix.sh <dos-file> ..." && exit 1

for file in $*
do
	TMPFILE=/tmp/tmp.`basename $file`
	cat $file | tr -d '' > $TMPFILE
	diff -q $file $TMPFILE > /dev/null
	if [ $? -eq 1 ] ; then
		echo "Processing $file ..." 
		mv $TMPFILE $file
	else
		echo "Skipping $file ..." 
		rm -f $TMPFILE
	fi
done
                                                                                                                                                                                     cxps-onetime/bin/instcert.sh                                                                        0000775 0001751 0001751 00000001672 13634711530 016042  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env bash

# ===========================================================================
# This program installs the certificate for SSL connection to public IP
# ===========================================================================

CACERT="$JAVA_HOME/jre/lib/security/cacerts"
if [ ! -f $CACERT ] ; then
	CACERT="$JAVA_HOME/lib/security/cacerts"
fi
if [ ! -f $CACERT ] ; then
	echo "Error: Unable to locate cacerts file in JAVA_HOME" >&2
	exit 1
fi

TMPFILE=/tmp/cxsvn.customerxps.com.cert

echo -n | openssl s_client -connect cxsvn.customerxps.com:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > $TMPFILE

ALIAS=cxpsvn

echo "Removing previous certificate ..."
sudo keytool -delete -alias "$ALIAS" -keystore $CACERT -storepass changeit

echo "Installing new certificate ..."
sudo keytool -importcert -alias "$ALIAS" -ext san=dns:cxsvn.customerxps.com -file $TMPFILE -keystore $CACERT -storepass changeit

\rm -f $TMPFILE
                                                                      cxps-onetime/bin/mgcrt.sh                                                                           0000664 0001751 0001751 00000003174 13634711530 015317  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/bin/bash
# This script overwrites the previous certificate with the new one

dep=$1
[ -z "$dep" ] && dep=$CXPS_DEPLOYMENT
[ -z "$dep" ] && echo "Usage: mgcrt.sh <deployment-folder>" && exit 1

# Check JAVA_HOME and SECURITY FOLDER
[ -z ${JAVA_HOME} ] && echo "JAVA_HOME not set !" && exit 2
[ -z "${JAVA_SECURITY_FOLDER}" ] && echo "JAVA_SECURITY_FOLDER not set. Check Env" && exit 3

echo "This script removes the old certificate and creates a fresh one. It requires SUDO permission."
echo "Press <ENTER> to continue or <Ctrl-C> to quit"
read x

#Remove older certificates before installing new one
echo "Removing old certificate..."
rm -f keystore acegisecurity.txt
rm -f  ${dep}/keystore
sudo rm -f ${JAVA_SECURITY_FOLDER}/acegisecurity.txt
sudo $JAVA_HOME/bin/keytool -keystore ${JAVA_SECURITY_FOLDER}/cacerts -delete -alias acegisecurity -storepass changeit
[ $? -eq 0 ] && echo "Completed!"

#Install new certificate
echo "Installing new certifiate..."
$JAVA_HOME/bin/keytool -keystore keystore -alias acegisecurity -genkey -keyalg RSA -validity 710 -storepass changeit -keypass changeit -dname "CN=`hostname`,OU=CXPS,O=CXPS,L=Bangalore,S=Kar,C=IN" 
$JAVA_HOME/bin/keytool -export -v -rfc -alias acegisecurity -file acegisecurity.txt -keystore keystore -storepass changeit
sudo cp acegisecurity.txt ${JAVA_SECURITY_FOLDER}
cp -r keystore ${dep}/
# Remove the generated files
[ "$dep" != "." -a "$dep" != `pwd` ] && \rm -f keystore acegisecurity.txt

cd ${JAVA_SECURITY_FOLDER}
sudo $JAVA_HOME/bin/keytool -import -v -file acegisecurity.txt -keypass changeit -keystore cacerts -storepass changeit -alias acegisecurity
[ $? -eq 0 ] && echo "Completed!"
                                                                                                                                                                                                                                                                                                                                                                                                    cxps-onetime/bin/cxpsprofile.sh                                                                     0000775 0001751 0001751 00000000517 13634711530 016542  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/bin/bash

# Assuming path is set
. cxpsvars.sh

. ${CXPS_LIB}/cxpsfuns.sh

. ${CXPS_BIN}/cxpsaliases.sh

# Unset all env variables.  # Developer must run sw() explicitly
CXPS_PRODUCT_NAME=""
CXPS_PRODUCT_VERSION=""
CXPS_PRODUCT=""
CXPS_WORKSPACE=""
CXPS_DEPLOYMENT=""

echo "WARNING: You must run 'sw' to setup product environment"
                                                                                                                                                                                 cxps-onetime/bin/build.sh                                                                           0000775 0001751 0001751 00000002140 13634711530 015275  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/bin/sh

usage(){
	echo "build.sh [o|s|c] <app>"
}

build(){
	[ $# -eq 0 ] && return
	cwd=`pwd`
	cd $1
	if [ -f pom.xml ] ; then
		mvn $option install
	elif [ -f build.sh ] ; then
		chmod +x build.sh
		./build.sh
	fi
	cd $cwd
}

# ==================
# MAIN ACTION BLOCK
# ==================

[ $# -eq 0 ] && usage && exit 1
if [ $# -ge 1 ] ; then
	option=""
	for opt in `echo "$1" | ${SED} 's/\(.\)/\1 /g'`
	do
		case "$opt" in
			"s") option="$option -DskipTests" ;;
			"c") option="$option clean" ;;
			"o") option="$option -o" ;;
			*)	usage && exit 2 ;;
		esac
	done
	shift 1
fi

# In case no application is provided then build '.'
if [ $# -eq 0 ] ; then
	echo "Building ."
	build .
else
	for app in $*
	do
		found=0
		# Try to find the given application on both current folder and in CXPS_WORKSPACE
		for dir in . $CXPS_WORKSPACE
		do
			if [ -d $dir/$app ] ; then
				echo "Building $dir/$app"
				build $dir/$app
				if [ -d $dir/$app/deployment ] ; then
					build $dir/$app/deployment
				fi
				found=1
				break
			fi
		done
		if [ $found -eq 0 ] ; then
			echo "skipping $app ... not found!"
		fi
	done
fi
                                                                                                                                                                                                                                                                                                                                                                                                                                cxps-onetime/bin/cxpsaliases.sh                                                                     0000775 0001751 0001751 00000002200 13634711530 016512  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 :

[ "$OS" = "Darwin" ] && alias ls='/bin/ls -p'
[ "$OS" = "Linux" ] && alias ls='/bin/ls --color=auto'

alias l='ls -l'
alias ll="/bin/ls -ltphr"
alias rm="rm -i"
alias rmf="rm -f"
alias rmo='rm -f *.o *.pyc *.pyo'
alias lo=exit
[ -z "`which vim`" ] || alias vi=vim

build(){
	build.sh $*
}

alias mci='build c'
alias mi='build ""'
alias moci='build oc'
alias mosci='build osc'
alias mosi='build os'
alias msci='build sc'
alias msi='build s'

alias ws='cd ${CXPS_DEV}/${CXPS_PRODUCT}/workspace'
alias dev='cd ${CXPS_DEV}'
alias dep='cd ${CXPS_DEPLOYMENT}'
alias apex='ws && cd Apex'
alias rice='ws && cd Rice'

alias tomcat='cd ${CXPS_DEPLOYMENT}/tomcat/logs'
alias st='tomcat ; ls -l; ${CXPS_WORKSPACE}/bin/cxpsctl.sh start && tail -f catalina.out'
alias St='tomcat ; ${CXPS_WORKSPACE}/bin/cxpsctl.sh stop'
alias ct='${CXPS_WORKSPACE}/bin/cxpsctl.sh status'

alias ,="cd ..;"
alias ,,="cd ../.."
alias ,,,="cd ../../.."
alias ,,,,="cd ../../../.."
alias ,,,,,="cd ../../../../.."
alias ,,,,,,="cd ../../../../../.."

alias b="cd ~-"

alias cxhelp="[ -f ${CXPS_LIB}/cxpshelp.txt ] && more ${CXPS_LIB}/cxpshelp.txt || echo 'Error:help file not found'"
                                                                                                                                                                                                                                                                                                                                                                                                cxps-onetime/bin/port.sh                                                                            0000664 0001751 0001751 00000001414 13634711530 015162  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 
# ===========================================================================
# This file holds the global variable names for Unix utilities
# ===========================================================================
 
if [ -z "$__PORT_SH_EXECUTED" ] ; then

	SED=sed
	GREP=grep
	AWK=awk
	TAR=tar
	os=`uname -s`

	case "$os" in 
		"Linux")	p="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
					;;
		"SunOS")	p="/opt/csw/bin:/usr/xpg4/bin:/usr/bin:/bin"
					SED=gsed
					AWK=gawk
					TAR=gtar
					GREP=ggrep
					DIFF=gdiff
					;;
		"Darwin")	p="/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin"
					;;
		"*")		p="/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:"
					;;
	esac

	export __PORT_SH_EXECUTED=executed

	PATH=$p:$PATH

	export SED GREP AWK TAR PATH
fi
                                                                                                                                                                                                                                                    cxps-onetime/bin/prodsetup.sh                                                                       0000775 0001751 0001751 00000003743 13634711530 016235  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env bash

# This program creates the directory structure for a given product that includes -
# - creation of workspace folder
# - source checkout from SVN_URL and given PRODUCT/VERSION
# - rt-config setup
# - deployment folder setup (copy TOMCAT, openfire, etc.)

. cxpsvars.sh	 # Assuming path is set

# -------------------
# MAIN ACTION BLOCK
# -------------------
if [ $# -ne 1 -a $# -ne 2 -a $# -ne 3 -a $# -ne 4 ] ; then
	echo "Usage: `basename $0` <PRODUCT> [VERSION] [SVN REVISION] [MODULE DIR]"
	exit 1
fi

PRODUCT=$1
VERSION=$2
REVISION=$3
MODDIR=$4

if [ -z "$VERSION" -a -z "${REVISION}" ] ; then
	rev=""
	SVN_PRODVER=$PRODUCT
	PRODVER=$PRODUCT
elif [ -z "$REVISION" ] ; then
	rev=""
	SVN_PRODVER=${PRODUCT}-${VERSION}
	PRODVER=${PRODUCT}-${VERSION}
elif [ -z "$VERSION" ] ; then
	rev="-r$REVISION"
	SVN_PRODVER=${PRODUCT}
	PRODVER=${PRODUCT}-r${REVISION}
else
	rev="-r$REVISION"
	SVN_PRODVER=${PRODUCT}-${VERSION}
	PRODVER=${PRODUCT}-${VERSION}-r${REVISION}
fi

# Setup the SVN Module Directory
[ ! -z "$MODDIR" ] && SVN_PRODVER=${MODDIR}

# Confirm from the user in case it already exist
if [ -d ${CXPS_DEV}/${PRODVER} ] ; then
	echo "WARNING: ${CXPS_DEV}/${PRODVER} already exists"
	echo "Press <Y> to continue overwriting it"
	read ans
	[ "$ans" != "Y" ] && echo "Exiting..." && exit 0
fi

# Create the Product Version folder
printf "Creating folders ..."
cd ${CXPS_DEV}
mkdir -p ${PRODVER} 2>/dev/null
mkdir -p ${PRODVER}/workspace 2>/dev/null
echo "done!"

# Checkout or update the source code
if [ -d ${PRODVER}/workspace ] ; then
	cd ${PRODVER}/workspace
	svn info >/dev/null 2>/dev/null
	if [ $? -ne 0 ] ; then
		echo "Source checkout ... "
		cd ..
		echo "svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} workspace"
		svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} workspace
	else
		echo "Source update ... "
		svn up
	fi
else
	echo "Full Source checkout ... "
	# echo svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} ${PRODVER}/workspace
	svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} ${PRODVER}/workspace
fi
                             cxps-onetime/lib/                                                                                   0000775 0001751 0001751 00000000000 13634711530 013640  5                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 cxps-onetime/lib/cxget.py                                                                           0000775 0001751 0001751 00000007707 13634711530 015342  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env python
# ===========================================================================
# cxget.py
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# This program checks if there is a upgraded version of given file
# in resources. In case yes, it downloads the same in $CL5_MAIN_REP_DIR
# 
# Usage: 
# python cxget.py <module> <version> <compression>
# ===========================================================================

from __future__ import print_function

import getopt
import os
import re
import sys
from datetime import datetime

import requests
from dateutil import parser

try:
    requests.packages.urllib3.disable_warnings()
except:
    pass

def getDevRes(pkgname, pkgver, compression='tgz', baseurl=None):
    status = 1
    if not baseurl:
        baseurl = os.environ['GLB_DEVRES_URL']

    rep = os.environ['CL5_LOCAL_REP']
    try:
        r = requests.get(baseurl + '/', timeout=1, verify=False)
    except requests.exceptions.ConnectionError:
        return None
    except:
        # Bail out on any exception!
        print ('Unable to get file list', file=sys.stderr)
        return None

    if r.status_code != 200:
        print ('requests returned [%d]' % r.status_code, file=sys.stderr)
        return None

    # Create RE pattern to extract all file,date that matches the package name
    pkg = pkgname + '-' + pkgver + '.' + compression

    checklist = {}
    pat = re.compile(pkg)
    for line in r.text.split("\n"):
        m = pat.findall(line)
        if len(m) > 0:
            try:
                # strip initial till 'href='
                s = line[line.find('href='):]
                # Split on '"'
                words = s.split('"')
                # Get file name
                (name, date) = (words[1], words[4][1:])
                # Clean up date
                date = date[0:date.find('  ')]
                checklist[name] = date
            except:
                continue

    rep += '/.downloads/' + pkgname + '/' + pkgver

    # Create directory, including parent
    if not os.access(rep, os.F_OK):
        os.makedirs(rep)

    # Verify and download all packages
    if pkg not in checklist:
        print ("Error: Package [%s] not found in remote repository!" % pkg, file=sys.stderr)
        return 1

    repfile = rep + '/' + pkg
    fetch = True
    if os.path.exists(repfile):
        # Check if the file is latest
        try:
            dt1 = parser.parse(checklist[pkg])
            dt2 = datetime.fromtimestamp(os.path.getmtime(repfile))
        except:
            pass

        if dt1 <= dt2:
            fetch = False

    if not fetch:
        return 0

    # Download the file
    sys.stdout.write('Downloading latest [' + pkg + '] ... ')
    sys.stdout.flush()
    r = requests.get(baseurl + '/' + pkg, timeout=2, verify=False)
    if r.status_code == 200:
        with open(repfile, 'wb') as fw:
            fw.write(r.content)
        print ("done")
        status = 0
        # print >> sys.stdout, 'Downloaded latest [%s]' % pkg
    else:
        print ("error")
        print ('Error! [%d], while fetching [%s]' % (r.status_code, pkg), file=sys.stderr)

    return status

# ------------------------------------------------------
# MAIN ACTION BLOCK
# ------------------------------------------------------
# python cxget.py <name> <ver> [compression]
# default compression is tgz
# ------------------------------------------------------
if __name__ == '__main__':

    (opts, args) = getopt.getopt(sys.argv[1:], "", ["url="])

    if len(args) < 2:
        print ('Usage: python cxget.py [--url=<resource_url>] <pkg-name> <pkg-ver> [compression]', file=sys.stderr)
        sys.exit(1)

    url = None
    for o, a in opts:
        if o == '--url':
            url = a

    c = ''
    try:
        c = args[2]
    except:
        pass

    if c:
        status = getDevRes(args[0], args[1], compression=c, baseurl=url)
    else:
        status = getDevRes(args[0], args[1], baseurl=url)

    sys.exit(status)
                                                         cxps-onetime/lib/cxlister.py                                                                        0000664 0001751 0001751 00000013422 13634711530 016051  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env python

from __future__ import print_function

import builtins
from builtins import input

# ===========================================================================
# cxlister.py
# Aditya, June 2013
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This program is used for selecting a module / version
# from the list available in Global SVN URL
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# This program starts by displaying local folder structure for any selection.
# In case of URL fetch it displays heirarchic selection
# ===========================================================================

import os,sys,time
import ColorText,SVNFetch

# ===========================================================================
# This function helps user choose from given list of items
# Items are given as (name,flag)
# 	flag True => bold else regular
# ===========================================================================
def choose(items, additional=None, extra=None) :

	ct = ColorText.ColorText(ColorText.BOLD, ColorText.FG_BLUE, ColorText.BG_WHITE)

	while True :
		for i in range(0,len(items)) :
			s = str(i+1)+") "+items[i][0]
			if items[i][1] == True :
				ct.write(s)
			else :
				print (s)
		if additional != None :
			for x in additional :
				i += 1
				print (str(i+1)+") "+x)
		print ("-------------------------------")
		if extra != None :
			for x in extra :
				print (x[0]+") "+x[1])
		print ("q) Quit")
		ans = str(input("Enter Choice: ")).strip()
		try :
			v = int(ans)
			if v > len(items) or v < 1 :
				raise ValueError
			return (v-1)
		except ValueError :
			if ans == "q" :
				return None
			elif ans == "" :
				continue
			if additional != None :
				if len(items) < v <= len(items)+len(additional) :
					print (additional[v-len(items)-1])
					return additional[v-len(items)-1]
					
			elif extra != None :
				for x in extra :
					if ans[0] == x[0] :
						return x[0]
			print ('Error: Invalid Value. Try again!')
			time.sleep(1)

def getLocalProjects() :
	dvdir = os.environ['CXPS_DEV']
	lst = [ dvdir + "/" + dir for dir in os.listdir(dvdir) if dir[0] != '.' ]
	lst.sort(key=os.path.getmtime, reverse=True)	
	return [ d[d.rfind('/') + 1:] for d in lst ]

def chooseLocal() :
	dirs = [ (d,False) for d in getLocalProjects() ]
	print ('---------------')
	print ('LOCAL Projects')
	print ('---------------')
	extra = [('s',"Set Remote SVN [currently LOCAL]")]
	if 'CX_WORKING_REMOTE' in os.environ :
		working_remote = os.environ['CX_WORKING_REMOTE']
		if working_remote != '' :
			extra = [('s','Set Local SVN [currently REMOTE]')]
	extra.append(('f',"Fetch all SVN Projects"))
	choice = choose(dirs, None, extra)
	if choice in ['f','s',None] :
		return choice
	else :
		return dirs[choice][0]
	
def output(outfile, project) :
	MODDIR=''
	if project.find('/') >= 0 :
		prodver = project.split('/')
		PRODUCT = prodver[0]
		VERSION = prodver[2]
		MODDIR = 'mod_' + project.replace('//','/')
	else :
		prodver = project.split('-',1)
		PRODUCT = prodver[0]
		if len(prodver) > 1 :
			VERSION=prodver[1]
		else :
			VERSION=''

	with open(outfile,'w') as f :
		f.write("PRODUCT="+PRODUCT+"\n")
		f.write("VERSION="+VERSION+"\n")
		if MODDIR != '' : f.write("MODDIR="+MODDIR+"\n")
	return

def isValid(envars) :
	for ev in envars :
		if ev not in os.environ :
			print ('Error: '+ev+' ENV variable not found!. Are you in DEVENV ?')
			sys.exit(1)

# ===================
# MAIN ACTION BLOCK
# ===================
if __name__ == '__main__' :

	isValid(['CXPS_PROGRAMS','CXPS_DEV','GLB_SVN_URL'])

	force = False
	ndx = 1
	if len(sys.argv) > 1 :
		if sys.argv[1].strip() == '-f' :
			force = True
			ndx = 2
	else :
		print('Usage:python %s [-f] outfile' % sys.argv[0])
		sys.exit(4)

	try :
		outfile = sys.argv[ndx].strip()
	except IndexError :
		print('Usage:python %s [-f] outfile' % sys.argv[0])
		sys.exit(4)

	# First choose Local
	choice = chooseLocal()
	if choice == None :
		sys.exit(1)
	elif choice == 's' :
		sys.exit(3)
	elif choice != 'f' :
		output(outfile,choice)
		sys.exit(0)

	lprojs = getLocalProjects()
	svn = SVNFetch.SVNFetch(force)
	svn.getUserPw()
	while True :
		(status,folders) = svn.fetch('.')
		if status == 'OK' :
			dirs = []
			modules = []
			for d in folders :
				if d.startswith('mod_') :
					# Module found
					modules.append('['+d[4:]+']...')
				else :
					dirs.append((d, True if d in lprojs else False))
			dirs.sort()
			choice = choose(dirs,modules)
			if choice == None :
				sys.exit(1)

			# Check if selection is that of module
			if str(choice).find('...') < 0 :
				output(outfile,dirs[choice][0])
				sys.exit(0)

			# Handle the module
			module = choice[1:-4]
			moddir = 'mod_' + module
			(status, folders) = svn.fetchModule(moddir)
			if status == 'OK_BUT' :	# basically its not a module
				output(outfile,moddir)
				sys.exit(0)
			elif status == 'OK' :
				# The folders are module format !/branch
				dirs = []
				for d in folders :
					m = d.split('/')
					mod = module +'-'+m[1]
					dirs.append((mod, True if mod in lprojs else False))

				print ('---------------------')
				print ('Module: ' + module)
				print ('---------------------')
				choice = choose(dirs)
				if choice == None :
					continue
				# DEBUG:
				# print choice, moddir + '/' + folders[choice]
				output(outfile, module + '/' + folders[choice])
				sys.exit(0)

		if status == 'INVALID_USPW' :
			print ('Error: Invalid SVN User/Passwd')
			svn.getUserPw()
		elif status == 'TIMEOUT' :
			print ('Error: Unable to connect to SVN URL')
			sys.exit(2)
		elif status == 'URLError' :
			print ('Error: Unable to connect ('+folders[0]+')')
			sys.exit(2)
		elif status == 'CONNECTError' :
			print ('Error: Unable to connect')
			sys.exit(2)
		elif status != 'OK' :
			print ('Error: Got HTTP error (' + str(folders[0]) + ')')
			sys.exit(2)
                                                                                                                                                                                                                                              cxps-onetime/lib/cxpshelp.txt                                                                       0000775 0001751 0001751 00000001747 13634711530 016243  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 Following utilities and functions are available:

sw:
	This is a shell function which allows developer to switch between
	various products. It takes product/version on command line. In case
	the product/version is not present in CXPS_DEV it executes prodsetup.sh
	for the specified product.
	In case the product/version is omitted it asks for user input. For this
	it looks for all versions that are available in CXPS_DEV folder.

	Usage: sw [Product] [Version]

upd:
	This is a shell function that allows developers to change the dependencies
	and update the deployment accordingly.

	Usage: upd

upg:
	This is a shell function which checks whether any new version for
	devenv is available in GLB_RES_URL. If yes, it upgrade the current
	devenv after user's confirmation.

	Usage: upg

cxhelp:
	Displays this help

	Usage: cxpshelp

A note:
	In case you wish to set it in .profile please add as follows -
		PATH=<path to CXPS_HOME>/bin:\$PATH
		. cxpsprofile.sh
	Note that PATH setup is mandatory.
                         cxps-onetime/lib/cxCache.py                                                                         0000664 0001751 0001751 00000001341 13634711530 015547  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env python

import os, dbm

class cxCache :

	def __init__(self, name) :
		self.cacheFile = os.environ['CXPS_PROGRAMS'] + '/' + name
		self.readDB = None

	def __del__(self) :
		if self.readDB != None :
			self.readDB.close()

	def put(self, dic) :
		try :
			self.db = dbm.open(self.cacheFile, 'c')
		except dbm.error :
			self.readDB = None
		for (key,value) in dic.items() :
			self.db[key] = value
		self.db.close()

	def get(self, names) :
		if self.readDB == None :
			try :
				self.readDB = dbm.open(self.cacheFile, 'r')
			except dbm.error :
				self.readDB = None
				return {}
		output = {}
		for key in names :
			try :
				output[key] = self.readDB[key]
			except KeyError:
				output[key] = None
		return output
                                                                                                                                                                                                                                                                                               cxps-onetime/lib/SVNFetch.py                                                                        0000664 0001751 0001751 00000006154 13634711530 015640  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env python

import os, time, sys
import getpass, requests, builtins

from builtins import input

if sys.version_info[0] == 2:
    import cPickle as cPK
else :
    import _pickle as cPK

import cxCache

class SVNFetch :

	def __init__(self, force=False) :
		self.FORCE = force
		self.cache = cxCache.cxCache('svnreg')	# Name of the cache file
		if self.FORCE :
			self.user = None
			self.pswd = None
		else :
			m = self.cache.get(['user','pswd'])
			self.user = m.get('user',None)
			if self.user :
			    self.user = self.user.decode("utf-8")
			self.pswd = m.get('pswd',None)
			if self.pswd :
			    self.pswd = self.pswd.decode("utf-8")
		self.userPwChanged = True

	# This method inputs the User / Password from the User.
	def getUserPw(self) :
		if self.user == None :
			self.userPwChanged = True
			userid = getpass.getuser()
			self.user = input("SVN user-name ["+userid+"]: ").decode("utf-8")
			if self.user == "" :
				self.user = userid
				self.pswd = None
		if self.user != None and self.pswd == None :
			self.userPwChanged = True
			self.user = self.user.decode("utf-8")
			self.pswd = getpass.getpass('SVN Password ['+self.user+']: ')

	# This method fetches the given folder from SVN
	def fetch(self, folder = '.') :

		if not self.FORCE :
			m = self.cache.get([folder])
			if folder in m and m[folder] != None :
				(ts,folders) = cPK.loads(m[folder])
				# Check if 5 mins have not elapsed
				if time.time() - ts <= 300 :
					return ('OK',folders)

		url = os.environ['GLB_SVN_URL']
		if folder != '.' :
			url += '/' + folder
		try :
			# print "url=[%s]" % (url)
			# HTTP(S) request with authorization/timeout and no SSL verification
			r = requests.get(url, auth=(self.user, self.pswd), timeout=2, verify=False)
		except requests.exceptions.Timeout :
			return ('TIMEOUT',None)
		except requests.packages.urllib3.exceptions.LocationParseError :
			return ('URLError',[url])
		except requests.exceptions.ConnectionError :
			return ('CONNECTError',None)

		if r.status_code == 200 :	# Success!
			folders = []
			for line in r.text.split('\n') :
				if line.find('<li><a href="') >= 0 :
					folders.append(line.split('"')[1].replace('/',''))
			# In case userPw was changed, it should be saved
			if self.userPwChanged :
				self.cache.put({ 'user' : self.user, 'pswd' : self.pswd })
				self.userPwChanged = False
			self.cache.put({ folder : cPK.dumps((time.time(), folders), protocol=2) })
			return ('OK',folders)
		elif r.status_code == 401 :	# Wrong Password!
			self.user = None
			self.pswd = None
			return ('INVALID_USPW',None)
		else :
			return ('ERROR',[r.status_code])

	# This method fetches module details and returns
	# - releases
	# - branches
	# - module-baseline
	def fetchModule(self, module) :
		(status,folders) = self.fetch(module)
		if status != 'OK' : return (status,None)
		if 'baseline' not in folders :
			return ('OK_BUT',None)
		moddirs = ['/baseline']
		for folder in ['releases','branches']:
			if folder in folders :
				(status,dirs) = self.fetch(module + '/' + folder)
				if status == 'OK' :
					dirs.remove('..')
					for d in dirs :
						moddirs.append( folder + '/' + d)
		return (status, moddirs)
                                                                                                                                                                                                                                                                                                                                                                                                                    cxps-onetime/lib/ColorText.py                                                                       0000664 0001751 0001751 00000003302 13634711530 016133  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 from __future__ import print_function
 
# Special END separator
END = '0e8ed89a-47ba-4cdb-938e-b8af8e084d5c'
 
# Text attributes
ALL_OFF = '\033[0m'
BOLD = '\033[1m'
UNDERSCORE = '\033[4m'
BLINK = '\033[5m'
REVERSE = '\033[7m'
CONCEALED = '\033[7m'
 
# Foreground colors
FG_BLACK = '\033[30m'
FG_RED = '\033[31m'
FG_GREEN = '\033[32m'
FG_YELLOW = '\033[33m'
FG_BLUE = '\033[34m'
FG_MAGENTA = '\033[35m'
FG_CYAN = '\033[36m'
FG_WHITE = '\033[37m'
 
# Background colors
BG_BLACK = '\033[40m'
BG_RED = '\033[41m'
BG_GREEN = '\033[42m'
BG_YELLOW = '\033[43m'
BG_BLUE = '\033[44m'
BG_MAGENTA = '\033[45m'
BG_CYAN = '\033[46m'
BG_WHITE = '\033[47m'

class ColorText():
	'''
	Context manager for pretty terminal prints
	'''

	def __init__(self, *attr):
		self.attributes = attr
 
	def __enter__(self):
		return self
 
	def __exit__(self, type, value, traceback):
		pass
 
	def write(self, msg):
		style = ''.join(self.attributes)
		print('{0}{1}{2}'.format(style, msg.replace(END, ALL_OFF + style), ALL_OFF))
 
 
if __name__ == '__main__':
 
	with ColorText(FG_RED) as out:
		out.write('This is a test in RED')
 
	with ColorText(FG_BLUE) as out:
		out.write('This is a test in BLUE')
 
	with ColorText(BOLD, FG_GREEN) as out:
		out.write('This is a bold text in green')
 
	with ColorText(BOLD, BG_GREEN) as out:
		out.write('This is a text with green background')
 
	with ColorText(BOLD, FG_BLUE, BG_WHITE) as out:
		out.write('This is blue text with white background')

	with ColorText(FG_GREEN) as out:
		out.write('This is a green text with ' + BOLD + 'bold' + END + ' text included')
 
	with ColorText() as out:
		out.write(BOLD + 'Use this' + END + ' even with ' + BOLD + FG_RED + 'no parameters' + END + ' in the with statement') 
                                                                                                                                                                                                                                                                                                                              cxps-onetime/lib/cxpsfuns.sh                                                                        0000775 0001751 0001751 00000025037 13634711530 016057  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env bash

unset shrink 2>/dev/null
shrink(){
	if [ $# -eq 0 ] ; then
		___VARNAME=PATH
	else
		___VARNAME=$1
	fi
	___VAR="\${`echo $___VARNAME`}"
	___VARVALUE=`eval echo $___VAR`
	___NEWVALUE=""
	for dir in `echo $___VARVALUE | tr ' ' '@' | tr ':' '\012'`
	do
		if ! echo $___NEWVALUE | ${GREP} -q ":${dir}:"
		then
			___NEWVALUE=${___NEWVALUE}:${dir}:
		fi
	done
	# Remove consecutive '::' and substitute spaces back
	___NEWVALUE=`echo $___NEWVALUE | ${SED} -e 's/::/:/g' -e 's/@/ /g'`

	___VAR="`echo $___VARNAME`='$___NEWVALUE'"
	# echo $___VAR
	eval $___VAR
}

unset f 2>/dev/null
unalias f 2>/dev/null
f(){
	if [ $# -eq 0 ] ; then
		echo "Error in usage - f() is equivalent to find \$*| ${GREP} -v .svn"
	else
		find $* | ${GREP} -v -e '.svn'
	fi
}

unset upg 2>/dev/null
unalias upg 2>/dev/null
upg(){
	if [ "$1" = "reset" ] ; then
		_lastUpged=0
		shift 1
	fi
	if [ -z "$_lastUpged" ] ; then
		bash cxupg.sh $*
		_lastUpged=`date +%s`
	else
		_x=`date +%s`
		let _xdiff=_x-_lastUpged
		if [ $_xdiff -gt 300 ] ; then
			bash cxupg.sh $*
			_lastUpged=`date +%s`
		else
			echo "Skipping ... b'cos I checked $_xdiff/300 seconds ago. To force upgrade do 'upg reset'" >&2
		fi
	fi
}

unset sw 2>/dev/null
sw(){
	cwd=`pwd`

	# Upgrade the devenv in case new is available
	upg patch 2>/dev/null

	# Check if particular revision is requested
	SVN_REVISION=""
	if echo "$1" | grep -q 'rev='
	then
		SVN_REVISION="`echo $1 | cut -f2 -d'='`"
		shift 1
	fi

	# Main code
	if [ $# -eq 2 ] ; then
		PRODUCT=$1
		VERSION=$2
	else
		TMPFILE=/tmp/cxlister.tmpfile
		\rm -f $TMPFILE
		while :
		do
			python ${CXPS_LIB}/cxlister.py $1 $TMPFILE
			___sts=$?
			if [ $___sts -eq 3 ] ; then
				if isLocalSVN
				then
					echo "Switching to Remote SVN." && swRemoteSVN
				else
					echo "Switching to Local SVN." && swLocalSVN
				fi
			elif [ $___sts -eq 2 ] ; then
				if isLocalSVN
				then
					printf "Are you working remotely [y/n] ? "
					read ans
					if [ "$ans" = "y" -o "$ans" = "Y" ] ; then
						echo "Switching to Remote SVN" && swRemoteSVN
					fi
				else
					printf "You are working remotely. Switch to local [y/n] ? "
					if [ "$ans" = "y" -o "$ans" = "Y" ] ; then
						echo "Switching to Local SVN" && swLocalSVN
					fi
				fi
			else
				break
			fi
		done

		[ $___sts -ne 0 ] && return 1
		[ -f $TMPFILE ] && eval `cat $TMPFILE` && \rm -f $TMPFILE
		# This will output PRODUCT, VERSION and MODDIR
	fi

	# Check if revision is embedded in VERSION
	if echo $VERSION | ${GREP} -q -- '-r[0-9][0-9]*$'
	then
		SVN_REVISION=`echo $VERSION | ${SED} 's/^.*-r\([0-9][0-9]*\)$/\1/'`
		VERSION=`echo $VERSION | ${SED} 's/^\(.*\)-r[0-9][0-9]*$/\1/'`
	fi
	# echo $SVN_REVISION

	if [ -z "$VERSION" -a -z "${SVN_REVISION}" ] ; then
		PRODVER=$PRODUCT
	elif [ -z "$SVN_REVISION" ] ; then
		PRODVER=${PRODUCT}-${VERSION}
	elif [ -z "$VERSION" ] ; then
		PRODVER=${PRODUCT}-r${SVN_REVISION}
	else
		PRODVER=${PRODUCT}-${VERSION}-r${SVN_REVISION}
	fi

	# Remove all folders having the previous product from PATH
	if [ ! -z "$CXPS_PRODUCT" ] ; then
		shrink
		PATH=`echo $PATH | tr ':' '\012' | ${GREP} -v "/${CXPS_PRODUCT}/" | ${GREP} -v '^[ 	]*$' | tr '\012' ':'`
	fi

	# Set the main variables
	export CXPS_PRODUCT_NAME=${PRODUCT}
	export CXPS_PRODUCT_VERSION=${VERSION}
	export CXPS_PRODUCT_SVN_REVISION=${SVN_REVISION}
	export CXPS_MODDIR=${MODDIR}
	export CXPS_PRODUCT=${PRODVER}
	export CXPS_WORKSPACE=${CXPS_DEV}/${CXPS_PRODUCT}/workspace
	export CXPS_DEPLOYMENT=${CXPS_DEV}/${CXPS_PRODUCT}/deployment

	# In case the product is not available, set up the product as well.
	if [ ! -d "${CXPS_DEV}/${PRODVER}" ] ; then
		echo "Executing prodsetup.sh ..."
		# echo ${CXPS_BIN}/prodsetup.sh ${PRODUCT} ${VERSION} ${SVN_REVISION}
		${CXPS_BIN}/prodsetup.sh "${PRODUCT}" "${VERSION}" "${SVN_REVISION}" "${MODDIR}"
	fi

	# Run the profile for the product
	if [ -f ${CXPS_WORKSPACE}/bin/profile.sh ] ; then
		echo "Executing ${CXPS_WORKSPACE}/bin/profile.sh ..."
		chmod +x ${CXPS_WORKSPACE}/bin/profile.sh
		. ${CXPS_WORKSPACE}/bin/profile.sh
	fi

	shrink

	svnsw

	cd $cwd
}

setdbtypes(){

	DEPFILE=$1
	[ -z "$DEPFILE" ] && DEPFILE=$CXPS_WORKSPACE/bin/deps.cfg
	[ ! -f $DEPFILE ] && echo "setdbtypes:Invalid dep file $DEPFILE" && return 1

	count=`${GREP} -e mysql -e oracle -e postgresql -e sqlserver $DEPFILE | wc -l`
	if [ $count -eq 0 ] ; then
		echo "This product does not have any Database dependencies" >&2
		export DB_TYPES=""
		return 0
	elif [ $count -eq 1 ] ; then
		if ${GREP} -q -e mysql $DEPFILE
		then
			export DB_TYPES=mysql
		elif ${GREP} -q -e oracleXE $DEPFILE ; then
			export DB_TYPES=oracleXE
		elif ${GREP} -q -e sqlserver $DEPFILE ; then
			export DB_TYPES=sqlserver
		elif ${GREP} -q -e postgresql $DEPFILE ; then
			export DB_TYPES=postgresql
		else
			export DB_TYPES=oracle
		fi
		return 1
	else
		export DB_TYPES="mysql oracle oracleXE postgresql sqlserver"
		return 2
	fi
	DB_TYPES=`echo $DB_TYPES | ${SED} 's/[ 	][ 	]*/ /g'`
}

sdb(){

    export DB_TYPE

	dbpref=$1
	setdbtypes
	__status=$?
	__done=1

	if [ $__status -eq 1 ] ; then
		export DB_TYPE=$DB_TYPES
		__done=0
	fi

	if [ ! -z "$dbpref" ] ; then
		if echo $DB_TYPES | ${GREP} -q -w $dbpref
		then
			export DB_TYPE=$dbpref
			__done=0
		else
			echo "Invalid db preference $dbpref"
			return 1
		fi
	fi

	if [ $__done -eq 1 -a $__status -eq 2 ] ; then

		echo "Current DB_TYPE=$DB_TYPE"

		while :
		do
			let i=1
			for db_type in `echo $DB_TYPES`
			do
				out="$i) $db_type"
				echo $out
				let i=i+1
			done
			echo "------------"
			echo "q) quit"
			printf "Enter Choice: "
			read ans
			[ "$ans" = "q" ] && echo "Database is $DB_TYPE" && return 2
			[ -z "$ans" ] && continue

			if echo "$ans" | ${GREP} -q '^[0-9][0-9]*$'
			then
				if [ $ans -le `echo $DB_TYPES | wc -w` ] ; then
					DB_TYPE=`echo $DB_TYPES | cut -f$ans -d' '`
					break
				fi
			fi
			echo "Wrong Choice!"
		done
	fi

	echo "Database is $DB_TYPE"
	if [ -f ${CXPS_WORKSPACE}/bin/devvars.sh ] ; then
		. ${CXPS_WORKSPACE}/bin/devvars.sh
	elif [ -f ${CXPS_WORKSPACE}/bin/depvars.sh ] ; then
		. ${CXPS_WORKSPACE}/bin/depvars.sh
	fi
	return 0
}

# This function checks if the current WORKSPACE folder is in sync with the SVN.
# If not, it will relocate the SVN folder else it just comes out.

svnsw()
{
	local cwd curIP svnIP suffix prefix fromURL toURL
	cwd=`pwd`
	cd $CXPS_WORKSPACE
	curIP=`svn info | grep ^URL | cut -f3 -d'/' | cut -f1 -d:`
	svnIP=`echo $GLB_SVN_URL | cut -f3 -d'/' | cut -f1 -d:`
	if [ "$curIP" != "$svnIP" ] ; then
		suffix=`svn info | grep ^URL | cut -f2- -d':' | cut -f4- -d'/'`
		if isLocalSVN
		then
			# Move from remote to local
			prefix=`echo $GLB_LOCAL_SVN_URL | cut -f1-3 -d'/'`
            echo "Relocating [remote -> local]. This may take few moments ..."
		else
			# Move from local to remote
			prefix=`echo $GLB_REMOTE_SVN_URL | cut -f1-3 -d'/'`
            echo "Relocating [local -> remote]. This may take few moments ..."
		fi
		fromURL=$( svn info | grep ^URL | cut -c6- | cut -f-3 -d'/' )
		toURL=${prefix}

		printf "Continue [y/n] : "
		read ans
		if [ "$ans" = "y" ] ; then
			printf "Executing [svn relocate $fromURL $toURL] ... "
			svn relocate ${fromURL} ${toURL}
			curIP=`svn info | grep ^URL | cut -f3 -d'/' | cut -f1 -d:`
			if [ "$curIP" != "$svnIP" ] ; then
				echo " failed!"
				echo "Please run this command directly from \$CXPS_WORKSPACE."
			else
				echo " done!"
			fi
		fi
	fi
	cd $cwd
}

# Private function
# This function reads a variable with default value

unset __readVar 2>/dev/null
unalias __readVar 2>/dev/null
__readVar(){
    while :
    do
        printf "$1 [$2] : " >&2
        read __fl
        if [ -z "$__fl" -a ! -z "$2" ] ; then
            ret=$2
            break
        elif [ ! -z "$__fl" ] ; then
            ret=$__fl
            break
        fi
    done
    echo $ret | tr -d '"' | tr '|' '\;'
}

# Private function
# This function reads a description

unset __readDesc 2>/dev/null
unalias __readDesc 2>/dev/null
__readDesc(){
    echo "$1 (put 'EOT' to end) : " >&2
    ret=""
    while :
    do
        read __fl
        [ "$__fl" = "EOT" ] && break
        ret="$ret $__fl"
    done
    echo $ret | tr -d '"' | tr '|' '\;'
}

# This function is used to commit in the right way
unset commit 2>/dev/null
unalias commit 2>/dev/null
commit () {

    local _id _rev _dev _msg _tag _args
    while :
    do
        case $1 in
            "-c"|"-m"|"--message"|"--comment")
                shift 1
                _msg="$1"
                shift 1
                ;;
            "-i"|"--issue")
                shift 1
                _id="$1"
                shift 1
                ;;
            "-u"|"-d"|"--user"|"--developer")
                shift 1
                _dev="$1"
                shift 1
                ;;
            "-r"|"--reviewer")
                shift 1
                _rev="$1"
                shift 1
                ;;
            *)
                _args=( ${_args[@]} $1 )
                shift 1
                ;;
        esac

        [ $# -eq 0 ] && break
    done

    ( [ -z "${_id}" ] || [ -z "${_msg}" ] ) \
        && echo "Usage: commit -i <issue> [-d|-u <developer>] -c|-m <comment> [-r <reviewer>]" \
        && return 1

    [ -z "${_dev}" ] && _dev=${USER}
    _tag="ISSUE-ID:${_id} | Comment: ${_msg} | Developer: ${_dev}"
    [ ! -z "${_rev}" ] && _tag="${_tag} | Reviewer: ${_rev}"

    svn commit -m "${_tag}" ${_args[@]}
}

# Public function
# This function allows one to find the difference between svn version
# for a given file.
unset dff 2>/dev/null
unalias dff 2>/dev/null
dff(){
	case $# in
		1) __dff_r1='-0'; __dff_r2='-1';;
		2) __dff_r1='-0'; __dff_r2="$1"; shift 1;;
		3) __dff_r1="$1"; __dff_r2="$2"; shift 2;;
		*) echo "Usage: dff [-<r1>][-<r2>] file" >&2 ; return ;;
	esac
	__dff_valid=0
	if echo $__dff_r1 | grep '^-[0-9][0-9]*$' >/dev/null
	then
		if echo $__dff_r2 | grep '^-[0-9][0-9]*$' >/dev/null
		then
			__dff_r1=`echo $__dff_r1 | cut -f2 -d-`
			__dff_r2=`echo $__dff_r2 | cut -f2 -d-`
			__dff_valid=1
		fi
	fi
	[ $__dff_valid -eq 0 ] && echo "Invalid rev. Use -0 for current, -1 for previous, etc." >&2 && return

	__dff_r1v=`svn log -l$__dff_r1 $1 2>/dev/null | grep '^r[0-9][0-9]' | tail -1 | ${SED} 's/^.*r\([0-9][0-9]*\).*$/\1/'`
	__dff_r2v=`svn log -l$__dff_r2 $1 2>/dev/null | grep '^r[0-9][0-9]' | tail -1 | ${SED} 's/^.*r\([0-9][0-9]*\).*$/\1/'`

	if [ -z "$__dff_r1v" -a -z "$__dff_r2v" ] ; then
		echo "Invalid file $1" >&2
	elif [ -z "$__dff_r1v" ] ; then
		#echo svn diff -r${__dff_r2v} $1
		svn diff -r${__dff_r2v} $1
	elif [ -z "$__dff_r2v" ] ; then
		#echo svn diff -r${__dff_r1v} $1
		svn diff -r${__dff_r1v} $1
	else
		#echo svn diff -r${__dff_r1v}:${__dff_r2v} $1
		svn diff -r${__dff_r1v}:${__dff_r2v} $1
	fi
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 cxps-onetime/deps.cfg                                                                               0000664 0001751 0001751 00000000303 13634711530 014502  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 apr      : 1.3.9
maven    : 2.2.1
python   : 2.7
pyRXP    : 1.13
mysql    : 5.1
java     : 1.7
flex_sdk : 3.5
tomcat   : 6.0.33
activemq : 5.4.0
openfire : 3.6.3
oracle   : 11.2
kestrel  : 4.8.1
                                                                                                                                                                                                                                                                                                                             cxps-onetime/cxpsdevsetup.sh                                                                        0000775 0001751 0001751 00000027140 13634711530 016172  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 #!/usr/bin/env bash
PATH=$PATH:.

. bin/port.sh
. defCmd.sh

# --------------------------------------------------------------------------------
# Get the right installer (yum for REDHAT and apt-get for Debian)
# --------------------------------------------------------------------------------
which brew >/dev/null 2>&1
[ $? -eq 0 ] && installer="brew"
which yum >/dev/null 2>&1
[ $? -eq 0 ] && installer="yum -y"
which apt-get >/dev/null 2>&1
[ $? -eq 0 ] && installer="apt-get"

if [ -z "$installer" ]; then
	echo "SEVERE ERROR!!!"
	echo "Unable to find any installer (apt-get or yum or brew) on this machine".
	echo "Please continue with caution."
	installer="echo"
fi

# -------------------
# Java installation
# -------------------
unalias ins_java 2>/dev/null
ins_java()
{
	JAVA_VERSION=`${GREP} java deps.cfg | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`

	if [ -z "$JAVA_HOME" -o ! -x "${JAVA_HOME}/bin/java" ]
	then
		# Deduce JAVA_HOME from java executable
		j=`which java`
		if [ ! -z "$j" ] ; then
			j=`readlink $j 2>/dev/null`
			if [ $? -eq 0 ] ; then
				j=`dirname $j`
				export JAVA_HOME=`dirname $j`
				export PATH=${JAVA_HOME}/bin:${PATH}
			fi
		fi
	fi

	# Check for right version
	if [ -z "${JAVA_HOME}" -o -z "`java -version 2>&1 | head -1 | ${GREP} ${JAVA_VERSION}`" ] ; then
		if [ "$OS" = "Linux" ] ; then
			echo "Installing JAVA ..."
			sudo $installer install openjdk-6-jdk
			export JAVA_HOME="/usr/lib/jvm/java-6-openjdk"
			export PATH=${JAVA_HOME}/bin:${PATH}
		else
			echo "You need to install Java ${JAVA_VERSION} for your platform"
		fi
	fi
}

# -------------------
# SVN Installation
# -------------------
unalias ins_svn 2>/dev/null
ins_svn()
{
	if [ -z "`which svn`" ] ; then
		echo "Installing Subversion..."
		sudo $installer install subversion
	fi
}

#--------------------------
# SQLCMD Installation
#--------------------------
ins_sqlcmd()
{
	if [ "$OS" = "Darwin" ] ; then
		echo "Installing freetds ..."
		brew install freetds
		ln -fs /usr/local/bin/bsqldb /usr/local/bin/sqlcmd
	else
		if [ -z "`which sqlcmd 2>/dev/null`" ] ; then
			curdir=`pwd`
			SQLCMD_VERSION='11.0.2270.0'
			SQLCMD_PROGRAM="sqlcmd-${SQLCMD_VERSION}"

			# Download the installer
			cxinstall ${SQLCMD_PROGRAM}.tgz "--dir=${CXPS_BIN}" ${CXPS_PROGRAMS}

			# Install Pre-requisite (UNIX ODBC)
			cd ${CXPS_BIN}/${SQLCMD_PROGRAM}/unixODBC*
			export CPPFLAGS="-DSIZEOF_LONG_INT=8"
			./configure --prefix=/usr --libdir=/usr/lib64 --sysconfdir=/etc --enable-gui=no --enable-drivers=no --enable-iconv --with-iconv-char-enc=UTF8 --with-iconv-ucode-enc=UTF16LE
			echo "----------------------------------"
			echo "      Installing UnixODBC"
			echo "----------------------------------"
			sudo make install

			# Install SQLCMD
			cd ${CXPS_BIN}/${SQLCMD_PROGRAM}/msodbc*
			echo "-----------------------------------------"
			echo "      Installing MSODBC (SQLCMD)"
			echo "-----------------------------------------"
            if [[ "$installer" = "apt-get" ]] ; then
                sudo $installer install libssl-dev libssl1.0.0 libssl1.0.0-dbg
                sudo ln -s /lib/x86_64-linux-gnu/libssl.so.1.0.0 /usr/lib/libssl.so.10
                sudo ln -s /lib/x86_64-linux-gnu/libcrypto.so.1.0.0 /usr/lib/libcrypto.so.10
            fi

			sudo bash ./install.sh install --accept-license --force
			sudo ln -fs /opt/microsoft/msodbcsql/bin/$SQLCMD_PROGRAM /usr/local/bin/sqlcmd
			cd $curdir
		fi
	fi
}

# ---------------------
# mySQL Installation
# ---------------------
unalias ins_mysql 2>/dev/null
ins_mysql()
{
	if [ -z "`which mysql`" -o -z "`mysql --version 2>&1 | cut -f1 -d',' | ${GREP} '5\.'`" ] ; then
		if [ "$OS" = "Linux" ] ; then
			echo "Installing Mysql..."
			if [ "$installer" = "apt-get" ] ; then
				sudo $installer install mysql-query-browser mysql-server
			else
				sudo $installer install mysql mysql-server
			fi
		else
			echo "WARNING: No mysql found. Please install and add its path in .profile" >&2
		fi
	fi
}

# ---------------------
# Python Installation
# ---------------------
unalias ins_python 2>/dev/null
ins_python()
{
	PYTHON_VERSION=`${GREP} python deps.cfg | cut -f2 -d':' | ${SED} 's/[ 	]*//g'`
	if [ -z "`which python`" -o -z "`python --version 2>&1 | ${GREP} ${PYTHON_VERSION}`" ] ; then
		if [ "$OS" = "Linux" ] ; then
			echo "Installing Python..."
			sudo $installer install python2.7
			if [ "$installer" = "apt-get" ] ; then
				sudo apt-get install python-dev
			else
				sudo $installer install python-devel
			fi
		elif [ "$OS" = "SunOS" ] ; then
			cxinstall python-${PYTHON_VERSION}*.tgz "--dir=${CXPS_PROGRAMS}"
			if [ $? -eq 0 ] ; then
				(
					cd ${CXPS_PROGRAMS}/python-${PYTHON_VERSION}*
					./configure
					make -j 6
				)
			fi
		else
			echo "WARNING: No Python 2.6+ found. Please install and add its path in .profile." >&2
			return
		fi
	fi
}

unalias ins_pythonUtils 2>/dev/null
ins_pythonUtils()
{
	SUDO=sudo
	[ -w /usr/local/bin -a -w /usr/local/lib ] && SUDO=''

	# Install PIP
	which pip >/dev/null 2>&1
	if [ $? -ne 0 ] ; then
		which easy_install >/dev/null 2>&1
		if [ $? -ne 0 ] ; then
			# Install easy_install
			which wget >/dev/null 2>&1
			if [ $? -eq 0 ] ; then
				wget http://python-distribute.org/distribute_setup.py
			else
				which curl >/dev/null 2>&1
				if [ $? -eq 0 ] ; then
					curl http://python-distribute.org/distribute_setup.py > distribute_setup.py
				else
					echo "WARNING: No downloader (wget/curl) found!" >&2
					return
				fi
			fi

			$SUDO python distribute_setup.py
			PATH=/usr/local/bin:$PATH
		fi
		which easy_install >/dev/null 2>&1
		if [ $? -ne 0 ] ; then
			echo "WARNING: easy_install was not installed properly!" >&2
			return
		fi
		$SUDO easy_install pip
	fi

	# Install various utils
	$SUDO pip install pyRXP requests
}

# ----------------------------------------
# Install Linux Development Environment
# ----------------------------------------
unalias ins_linux_dev_env 2>/dev/null
ins_linux_dev_env()
{
	# Install make
	if [ -z "`which make`" ] ; then
		echo "Installing make..."
		sudo $installer install make
	else
		echo "Make is installed"
	fi

	# Installing C environment
	if [ -z "`which cc`" ] ; then
		echo "Installing Libc6-dev..."
		sudo $installer install libc6-dev
		sudo $installer install gcc
	else
		echo "C compiler is installed"
	fi

	# Install Curl
	if [ -z "`which curl`" ] ; then
		echo "Installing curl..."
		sudo $installer install curl
	else
		echo "Curl is installed"
	fi

	# Install required libraries 
	echo "Installing libraries ..."
	sudo $installer install libaio
	sudo $installer install libaio1
}

finish()
{
	( cd bin ; cp * "${CXPS_BIN}")
	( cd lib ; cp * "${CXPS_LIB}")
	cp deps.cfg ${CXPS_BIN}
	cp -R rel-notes ${CXPS_BIN}
}

catLatestRelNotes()
{
	relDir=$1
	cwd=`pwd`
	cd $relDir
	# Get major #
	major=`ls -1 release-notes-*.txt | cut -f3- -d'-' | cut -f1 -d'.' | sort -nr | head -1`
	minor=`ls -1 release-notes-${major}.*.txt | cut -f3- -d'-' | cut -f2 -d'.' | sort -nr | head -1`
	patch=`ls -1 release-notes-${major}.${minor}.*.txt | cut -f3- -d'-' | cut -f3 -d'.' | sort -nr | head -1`
	cat release-notes-${major}.${minor}.${patch}.txt
	cd $cwd
}

setVars()
{
	TMPFILE=/tmp/cxpsvars.sh
	SYSTEM='SYSTEM=`uname -m`'
	OS='OS=`uname -s`'
	U=`id | tr ' ' '\012' | ${GREP} uid | cut -f2 -d'(' | cut -f1 -d')'`
	G=`id | tr ' ' '\012' | ${GREP} gid | cut -f2 -d'(' | cut -f1 -d')'`

	cat > ${TMPFILE} <<EOT
#!/bin/sh

`cat bin/port.sh`
`cat defCmd.sh`

# Setting most frequently used variables
export CXPS_HOME=${CXPS_HOME}
export PATH=\${CXPS_HOME}/bin:\$PATH
export CXPS_PROGRAMS="\${CXPS_HOME}/.cxps-src"
export CXPS_DEVENV_VERSION="${CXPS_DEVENV_VERSION}"
export CXPS_BIN="\${CXPS_HOME}/bin"
export CXPS_LIB="\${CXPS_HOME}/lib"
export CXPS_DEV="\${CXPS_HOME}/dev"
export CL5_LOCAL_REP="\${HOME}/.cxps-rep"

$SYSTEM
$OS
LOGNAME=${LOGNAME}
U=${U}
G=${G}

export SYSTEM OS U G

# Define the security folder where cacerts will get installed [ JAVA_SECURITY_FOLDER ]
JAVA_SECURITY_FOLDER="\${JAVA_HOME}/jre/lib/security"
if [ ! -d "\${JAVA_SECURITY_FOLDER}" ] ; then
    JAVA_SECURITY_FOLDER="\${JAVA_HOME}/lib/security"
    if [ ! -d "\${JAVA_SECURITY_FOLDER}" ] ; then
        echo "ERROR: Your JAVA_HOME does not contain the expected security folder. \${JAVA_HOME}/[jre/]lib/security"
    fi
fi
export JAVA_SECURITY_FOLDER

# Populate JAVA_OPTS and CATALINA_OPTS
export CATALINA_OPTS="\${CATALINA_OPTS} -Djava.library.path=\${CXPS_LIB}"
export CATALINA_OPTS=\`echo \$CATALINA_OUTS | tr ' 	' '\\012\\012' | grep -v '^[ 	]*$' | sort -u\`
[ -z "\$JAVA_OPTS" ] && export JAVA_OPTS="-server -Xms512m -Xmx1024m -XX:MaxPermSize=256m"

# Set up path for java, python and mysql
[ -z "\$JAVA_HOME" ] || PATH=\${JAVA_HOME}/bin:\$PATH
[ -z "\$PYTHON_HOME" ] || PATH=\${PYTHON_HOME}/bin:\$PATH
[ -z "\$MYSQL_HOME" ] || PATH=\${MYSQL_HOME}/bin:\$PATH

export PATH=\${CXPS_BIN}:\${PATH}

# Making sure entire folder structure is available
[ -d \${CXPS_HOME} ] || mkdir -p \${CXPS_HOME}
[ -d \${CXPS_DEV} ] || mkdir -p \${CXPS_DEV}
[ -d \${CXPS_BIN} ] || mkdir -p \${CXPS_BIN}
[ -d \${CXPS_LIB} ] || mkdir -p \${CXPS_LIB}
[ -d \${CXPS_PROGRAMS} ] || mkdir -p \${CXPS_PROGRAMS}

export w=\${CXPS_WORKSPACE}
export d=\${CXPS_DEPLOYMENT}
export tl=\${TOMCAT_HOME}/logs
export tw=\${TOMCAT_HOME}/webapps

EOT

	# Loading the cxps shell variables
	. ${TMPFILE}

	# Moving the temporary file to bin folder
	mv ${TMPFILE} ${CXPS_BIN}/cxpsvars.sh

	# Make sure the files/folders are executable the user
	chmod -R 755 ${CXPS_BIN} ${CXPS_PROGRAMS}
}

# ==================
# MAIN ACTION BLOCK
# ==================

currentVersion="$CXPS_DEVENV_VERSION"

# Check if its an upgrade in which case CXPS_HOME has to be picked
# up from ENV and continue without prompting

if [ "$1" != "upgrade" ] ; then
	[ -z "$CXPS_HOME" ] && CXPS_HOME=$HOME
	export CXPS_HOME
fi

if [ "$1" != "upgrade" -o -z "$CXPS_HOME" ] ; then
	# Input the base installation folder
	printf "Enter CXPS_HOME [${CXPS_HOME}] : "
	read home
	if [ ! -z $home ] ; then
		if echo $home | ${GREP} -v '^/' ; then
			home=`pwd`/$home
		fi
		export CXPS_HOME=$home
	fi
fi

# Create the folder if it does not exist
if [ ! -d ${CXPS_HOME} ] ; then
	mkdir -p ${CXPS_HOME} 2>/dev/null
fi

# Make sure this folder is writable
touch ${CXPS_HOME}/X.$$ 2>/dev/null
if [ $? -ne 0 ] ; then
	echo "CXPS_HOME is not writable!!. Exiting ..."
	exit 1
fi
\rm ${CXPS_HOME}/X.$$

# Clean up all the old shell scripts (if they exist)
if [ ! -z "${CXPS_HOME}" -a -d ${CXPS_HOME} ] ; then
	[ -d "${CXPS_HOME}/bin" ] && \rm -rf ${CXPS_HOME}/bin/*.sh ${CXPS_HOME}/bin/*.txt ${CXPS_HOME}/bin/rel-notes
	[ -d "${CXPS_HOME}/lib" ] && \rm -rf ${CXPS_HOME}/lib/*.sh
fi

# Set the environment variables
setVars

# Start installing
if [ "$OS" = "Linux" ] ; then
	ins_linux_dev_env
fi

items="java svn mysql python pythonUtils sqlcmd"
for item in `echo $items`
do
	eval ins_${item}
done
finish

# Set the VARS again after installation
setVars

# Copy the TGZ for rest of the items to CXPS_PROGRAMS
TMPFILE=/tmp/x.$$
echo $items | ${SED} 's/[ 	][ 	]*/ /g' | tr ' ' '\012' > $TMPFILE
Rest=`${GREP} -vf ${TMPFILE} deps.cfg | ${GREP} -v '^[ 	]*$' | ${SED} 's/[ 	]*//g' | tr ':' '-' | ${SED} 's/$/.tgz/' | tr '\012' ' '`
if [ ! -z "$Rest" ] ; then
	for file in `echo $Rest`
	do
		[ -f $file ] && mv $file ${CXPS_PROGRAMS}
	done
fi

# Run migration script (if available)
scr=`ls ${CXPS_BIN}/mig-*.sh 2>/dev/null`
if [ ! -z "$scr" ] ; then
	echo "Executing the migration script ..."
	bash $scr
fi

cat <<EOT
Finished!

Please ensure that you have the following in .profile

export PATH=\$CXPS_HOME/bin:\$PATH
. cxpsprofile.sh

Make changes in .bashrc to call .profile.
Also remember to remove call to .bashrc from .profile (if there is any).

Rerun .profile before proceeding.

Please read the release notes available in \$CXPS_BIN/rel-notes
`catLatestRelNotes $CXPS_HOME/bin/rel-notes`
EOT
                                                                                                                                                                                                                                                                                                                                                                                                                                cxps-onetime/defCmd.sh                                                                              0000664 0001751 0001751 00000005147 13634711530 014617  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 
# Set the globals / constants
setGlobals(){

	export GLB_LOCAL_IP=$my_GLB_LOCAL_IP
	export GLB_REMOTE_IP=$my_GLB_REMOTE_IP
	export GLB_REMOTE_PORT=$my_GLB_REMOTE_PORT
	[ -z "$GLB_LOCAL_IP" ] && GLB_LOCAL_IP=192.168.5.56
	[ -z "$GLB_REMOTE_IP" ] && GLB_REMOTE_IP=cxsvn.customerxps.com
	[ -z "$GLB_REMOTE_PORT" ] && GLB_REMOTE_PORT=:8443

	export GLB_LOCAL_URL="http://${GLB_LOCAL_IP}/resources"
	export GLB_REMOTE_URL="https://${GLB_REMOTE_IP}${GLB_REMOTE_PORT}/resources"
	export GLB_LOCAL_SVN_URL="http://${GLB_LOCAL_IP}/svn-product/products"
	export GLB_REMOTE_SVN_URL="https://${GLB_REMOTE_IP}${GLB_REMOTE_PORT}/svn-product/products"

	export GLB_SVN_URL="$GLB_LOCAL_SVN_URL"
	export GLB_RES_URL="$GLB_LOCAL_URL"

	export GLB_DEVRES_URL="${GLB_RES_URL}/cxpsdev-resources/."
	export GLB_MAVEN_REP="${GLB_RES_URL}/extrep/."
	export GLB_MAVEN_LOCAL_REP="${GLB_RES_URL}/localrep/."
	export GLB_SCP_URL="mvnrep@${GLB_LOCAL_IP}:/home/mvnrep/resources"
}

isLocalSVN(){
	[ -z "$CX_WORKING_REMOTE" ]
	return $?
}

swRemoteSVN(){
	setGlobals
	export GLB_SVN_URL="$GLB_REMOTE_SVN_URL"
	export GLB_RES_URL="$GLB_REMOTE_URL"

	export GLB_DEVRES_URL="${GLB_RES_URL}/cxpsdev-resources"
	export GLB_MAVEN_REP="${GLB_RES_URL}/extrep/."
	export GLB_MAVEN_LOCAL_REP="${GLB_RES_URL}/localrep/."

	export CX_WORKING_REMOTE="Y"
}

swLocalSVN(){
	setGlobals
	unset CX_WORKING_REMOTE
}

# Set environment for local SVN
if isLocalSVN
then
	swLocalSVN
else
	swRemoteSVN
fi

unalias download 2>/dev/null
unset download 2>/dev/null
download()
{
	url=$1
	[ -z "$url" ] && echo "Usage: $0 <url>" && return 1
	file=`basename $url`
	curl --insecure --retry 2 --connect-timeout 2 -kfs $url > $file
	[ $? -ne 0 ] && \rm -f $file && return 1
	return 0
}

# Defining installation functions
unalias cxinstall 2>/dev/null
cxinstall()
{
	TARGET=$1
	DIR=$2
	WORKDIR=$3

	# Assume current directory in case WORKDIR is not passed
	[ -z "$WORKDIR" ] && WORKDIR=`pwd`

	# Save the current working directory
	cwd=`pwd`
	cd ${WORKDIR}

	# Download the file in case it does not exist
	if echo ${TARGET} | grep -q SNAPSHOT || [ ! -f ${TARGET} ] ; then
		download ${GLB_DEVRES_URL}/${TARGET}
		if [ $? -ne 0 ] ; then
			echo "ERROR: Unable to download ${TARGET}"
			cd ${cwd}
			return 1
		fi
	fi

	extension=`echo $TARGET | ${SED} 's/^.*\.\([^\.][^\.]*\)$/\1/'`
	case $extension in
		"bz2") UNZIP="${TAR} -xjf" ;;
		"tar") UNZIP="${TAR} -xf" ;;
		"gz"|"tgz")  UNZIP="${TAR} -zxf" ;;
		*)     UNZIP="unzip" ;;
	esac
	`echo $UNZIP` ${TARGET} ${DIR}
	cd ${cwd}
	return $?
}

boldEcho(){
	if [ "$OS" = "Darwin" ] ; then
		echo $* | ${SED} 's/\(.\)/\1\1/g' | more
	else
		tput bold
		echo $*
		tput sgr0
	fi
}
                                                                                                                                                                                                                                                                                                                                                                                                                         cxps-onetime/rel-notes/                                                                             0000775 0001751 0001751 00000000000 13634711530 015002  5                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 cxps-onetime/rel-notes/release-notes-0.8.28.txt                                                     0000664 0001751 0001751 00000000270 13634711530 021043  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.28 ******
Added cxget.py to download latest packages for packaging of installers and local deployment
Added a param CL5_LOCAL_REP for local cxps repository
                                                                                                                                                                                                                                                                                                                                        cxps-onetime/rel-notes/release-notes-0.8.29.txt                                                     0000664 0001751 0001751 00000000133 13634711530 021042  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.29 ******
added --url in cxget, to download installers as well
                                                                                                                                                                                                                                                                                                                                                                                                                                     cxps-onetime/rel-notes/release-notes-0.8.0.txt                                                      0000664 0001751 0001751 00000001362 13634711530 020754  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.0 ******

-------------
New Feature:
-------------

Module Support

1. The Subversion repository will contain folders with module structures
	- baseline => points to the trunk
	- releases => points to READ-ONLY tags
		- these are always version #s viz 4.2, 4.3, 5.0, etc.
		- these are always read-only.
	- branches => points to various branches taken from trunk
		- these can be any name

2. sw() will mark modules with suffix "[...]"
	- The existing branches remain unchanged
	- On choosing the module, it opens up a new option set
		- list of baseline and all tags / branches
	- rest of the behavior is retained

3. In SVN the module is contains prefix as 'mod_' prefix for identification

4. Moved few utilities to python
                                                                                                                                                                                                                                                                              cxps-onetime/rel-notes/release-notes-0.8.1.txt                                                      0000664 0001751 0001751 00000000076 13634711530 020756  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.1 ******
Fixed remote / local svn
                                                                                                                                                                                                                                                                                                                                                                                                                                                                  cxps-onetime/rel-notes/release-notes-0.8.3.txt                                                      0000664 0001751 0001751 00000000101 13634711530 020745  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.3 ******

Fixed a bug in SVNFetch.py
                                                                                                                                                                                                                                                                                                                                                                                                                                                               cxps-onetime/rel-notes/release-notes-0.8.4.txt                                                      0000664 0001751 0001751 00000000311 13634711530 020751  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.4 ******
Set following variables -
- GLB_MAVEN_LOCAL_REP as localrep/ in global settings.
- GLB_MAVEN_REP points to extrep/
- GLB_SCP_URL points to 5.56/resources/resources
                                                                                                                                                                                                                                                                                                                       cxps-onetime/rel-notes/release-notes-0.8.5.txt                                                      0000664 0001751 0001751 00000000073 13634711530 020757  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.5 ******
Added support for CSB
                                                                                                                                                                                                                                                                                                                                                                                                                                                                     cxps-onetime/rel-notes/release-notes-0.8.6.txt                                                      0000664 0001751 0001751 00000000362 13634711530 020761  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.6 ******
1. Added new public IP and port related items.
2. added new utility instcert.sh for installing SSL certificate to accept public IP certificate.
3. Fixed svnsw function for proper handling of SVN URL ports
                                                                                                                                                                                                                                                                              cxps-onetime/rel-notes/release-notes-0.8.7.txt                                                      0000664 0001751 0001751 00000000072 13634711530 020760  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.7 ******
1. Fixed instcert.sh
                                                                                                                                                                                                                                                                                                                                                                                                                                                                      cxps-onetime/rel-notes/release-notes-0.8.8.txt                                                      0000664 0001751 0001751 00000000134 13634711530 020760  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.8 ******
1. Fixed cxupg.sh to pick up from dev resources folder
                                                                                                                                                                                                                                                                                                                                                                                                                                    cxps-onetime/rel-notes/release-notes-0.8.9.txt                                                      0000664 0001751 0001751 00000000151 13634711530 020760  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.9 ******
1. Changed the local environment to work with http instead of https
                                                                                                                                                                                                                                                                                                                                                                                                                       cxps-onetime/rel-notes/release-notes-0.8.10.txt                                                     0000664 0001751 0001751 00000000254 13634711530 021034  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.10 ******
1. Changed the public host name to cxsvn.customerxps.com for 5.56
2. Changed CSB to CXQ in depsetup.sh
3. Changed Java version to 1.7
                                                                                                                                                                                                                                                                                                                                                    cxps-onetime/rel-notes/release-notes-0.8.11.txt                                                     0000664 0001751 0001751 00000000234 13634711530 021033  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.11 ******
Changed the url for localrep and extrep to add '.' at the end.
This is to ensure that it is able to follow soft links
                                                                                                                                                                                                                                                                                                                                                                    cxps-onetime/rel-notes/release-notes-0.8.30.txt                                                     0000664 0001751 0001751 00000000142 13634711530 021032  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.30 ******
fixes in cxget.py to make sure compression is not mandatory
                                                                                                                                                                                                                                                                                                                                                                                                                              cxps-onetime/rel-notes/release-notes-0.8.12.txt                                                     0000664 0001751 0001751 00000000121 13634711530 021027  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.12 ******
Added check for version being unavailable.
                                                                                                                                                                                                                                                                                                                                                                                                                                               cxps-onetime/rel-notes/release-notes-0.8.13.txt                                                     0000664 0001751 0001751 00000000117 13634711530 021035  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.13 ******
Added antlr package as part of depsetup.
                                                                                                                                                                                                                                                                                                                                                                                                                                                 cxps-onetime/rel-notes/release-notes-0.8.31.txt                                                     0000664 0001751 0001751 00000000212 13634711530 021031  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.31 ******
Updated relocation to work seamlessly when switching remote/local for codelines with external links
                                                                                                                                                                                                                                                                                                                                                                                      cxps-onetime/rel-notes/release-notes-0.8.14.txt                                                     0000664 0001751 0001751 00000000126 13634711530 021036  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.14 ******
Updated depsetup.sh to include codegen utility.
                                                                                                                                                                                                                                                                                                                                                                                                                                          cxps-onetime/rel-notes/release-notes-0.8.32.txt                                                     0000664 0001751 0001751 00000000123 13634711530 021033  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.32 ******
added support for both python 2 and python 3
                                                                                                                                                                                                                                                                                                                                                                                                                                             cxps-onetime/rel-notes/release-notes-0.8.15.txt                                                     0000664 0001751 0001751 00000000114 13634711530 021034  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.15 ******
Fixed the ColorText.py for Python 2.6
                                                                                                                                                                                                                                                                                                                                                                                                                                                    cxps-onetime/rel-notes/release-notes-0.8.33.txt                                                     0000664 0001751 0001751 00000000046 13634711530 021040  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.33 ******
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          cxps-onetime/rel-notes/release-notes-0.8.16.txt                                                     0000664 0001751 0001751 00000000105 13634711530 021035  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.16 ******
Removed the call to TNS_ADMIN.
                                                                                                                                                                                                                                                                                                                                                                                                                                                           cxps-onetime/rel-notes/release-notes-0.8.34.txt                                                     0000664 0001751 0001751 00000000062 13634711530 021037  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 new version
***** Release Notes for 0.8.34 ******
                                                                                                                                                                                                                                                                                                                                                                                                                                                                              cxps-onetime/rel-notes/release-notes-0.8.17.txt                                                     0000664 0001751 0001751 00000000175 13634711530 021045  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.17 ******

Changes
1. Now 'SNAPSHOT' version dependencies are now always pulled from repository.
                                                                                                                                                                                                                                                                                                                                                                                                   cxps-onetime/rel-notes/release-notes-0.8.35.txt                                                     0000664 0001751 0001751 00000000100 13634711530 021031  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.35 ******
Added new commit function
                                                                                                                                                                                                                                                                                                                                                                                                                                                                cxps-onetime/rel-notes/release-notes-0.8.18.txt                                                     0000664 0001751 0001751 00000000414 13634711530 021042  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.18 ******
Added installer for gradle and maxmind

depsetup.sh extension enabled:
- You can now extend depsetup.sh by adding depsetup.sh in WS/bin folder.
- You need to implement dep_inst() function that chooses additional deps.cfg packages
                                                                                                                                                                                                                                                    cxps-onetime/rel-notes/release-notes-0.8.36.txt                                                     0000664 0001751 0001751 00000000104 13634711530 021036  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.36 ******
improvment to commit function
                                                                                                                                                                                                                                                                                                                                                                                                                                                            cxps-onetime/rel-notes/release-notes-0.8.19.txt                                                     0000664 0001751 0001751 00000000121 13634711530 021036  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.19 ******
Added SQLServer and PostGreSql as DB Types
                                                                                                                                                                                                                                                                                                                                                                                                                                               cxps-onetime/rel-notes/release-notes-0.8.20.txt                                                     0000664 0001751 0001751 00000000112 13634711530 021026  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.20 ******

Added AppDynamics Agent handling.

                                                                                                                                                                                                                                                                                                                                                                                                                                                      cxps-onetime/rel-notes/release-notes-0.8.21.txt                                                     0000664 0001751 0001751 00000000071 13634711530 021033  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.21 ******
Enabled SQL Server
                                                                                                                                                                                                                                                                                                                                                                                                                                                                       cxps-onetime/rel-notes/release-notes-0.8.22.txt                                                     0000664 0001751 0001751 00000000071 13634711530 021034  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.22 ******
Enabled postGreSql
                                                                                                                                                                                                                                                                                                                                                                                                                                                                       cxps-onetime/rel-notes/release-notes-0.8.23.txt                                                     0000664 0001751 0001751 00000000114 13634711530 021033  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.23 ******
Issue with previous patch. Fixed it.

                                                                                                                                                                                                                                                                                                                                                                                                                                                    cxps-onetime/rel-notes/release-notes-0.8.24.txt                                                     0000664 0001751 0001751 00000000076 13634711530 021043  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.24 ******
Added SQLCMD installer.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                  cxps-onetime/rel-notes/release-notes-0.8.25.txt                                                     0000664 0001751 0001751 00000000102 13634711530 021032  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.25 ******
Fixed an issue with sqlcmd.
                                                                                                                                                                                                                                                                                                                                                                                                                                                              cxps-onetime/rel-notes/release-notes-0.8.26.txt                                                     0000664 0001751 0001751 00000000120 13634711530 021033  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 fixed depsetup.sh for gradle installation
***** Release Notes for 0.8.26 ******
                                                                                                                                                                                                                                                                                                                                                                                                                                                cxps-onetime/rel-notes/release-notes-0.8.27.txt                                                     0000664 0001751 0001751 00000000075 13634711530 021045  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.27 ******
Added Apache installer
                                                                                                                                                                                                                                                                                                                                                                                                                                                                   cxps-onetime/rel-notes/release-notes-0.8.37.txt                                                     0000600 0001751 0001751 00000000107 13634711530 021030  0                                                                                                    ustar   lovish                          lovish                                                                                                                                                                                                                 ***** Release Notes for 0.8.37 ******
fix in selective files in commit
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         